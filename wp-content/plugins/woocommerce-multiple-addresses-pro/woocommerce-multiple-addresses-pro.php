<?php
/**
 * Plugin Name:       Multiple Addresses for Woocommerce
 * Plugin URI:        https://themehigh.com/product/plugin-name
 * Description:       This plugin helps to add additional addresses.
 * Version:           1.0.6
 * Author:            ThemeHigh
 * Author URI:        https://themehigh.com/
 *
 * Text Domain:       woocommerce-multiple-addresses-pro
 * Domain Path:       /languages
 *
 * WC requires at least: 3.0.0
 * WC tested up to: 3.5.4
 */

if(!defined('WPINC')){	die; }

if (!function_exists('is_woocommerce_active')){
	function is_woocommerce_active(){
	    $active_plugins = (array) get_option('active_plugins', array());
	    if(is_multisite()){
		   $active_plugins = array_merge($active_plugins, get_site_option('active_sitewide_plugins', array()));
	    }
	    return in_array('woocommerce/woocommerce.php', $active_plugins) || array_key_exists('woocommerce/woocommerce.php', $active_plugins);
	}
}

if(is_woocommerce_active()) {
	define('THWMA_VERSION', '1.0.6');
	!defined('THWMA_SOFTWARE_TITLE') && define('THWMA_SOFTWARE_TITLE', 'WooCommerce Multiple Addresses');
	!defined('THWMA_FILE') && define('THWMA_FILE', __FILE__);
	!defined('THWMA_PATH') && define('THWMA_PATH', plugin_dir_path( __FILE__ ));
	!defined('THWMA_URL') && define('THWMA_URL', plugins_url( '/', __FILE__ ));
	!defined('THWMA_BASE_NAME') && define('THWMA_BASE_NAME', plugin_basename( __FILE__ ));
	
	/**
	 * The code that runs during plugin activation.
	 */
	function activate_thwma() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-thwma-activator.php';
		THWMA_Activator::activate();
	}
	
	/**
	 * The code that runs during plugin deactivation.
	 */
	function deactivate_thwma() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-thwma-deactivator.php';
		THWMA_Deactivator::deactivate();
	}
	
	register_activation_hook( __FILE__, 'activate_thwma' );
	register_deactivation_hook( __FILE__, 'deactivate_thwma' );

	function thwma_license_page_url($url, $prefix){
		$url = 'admin.php?page=th_multiple_addresses_pro&tab=license_settings';
		return admin_url($url);
	}
	
	function init_auto_updater_thwma(){
		if(!class_exists('THWMA_License_Manager') ) {
			add_filter('thlm_license_page_url_woocommerce_multiple_addresses', 'thwma_license_page_url', 10, 2);
			add_filter('thlm_enable_default_license_page', '__return_false');

			require_once( plugin_dir_path( __FILE__ ) . 'class-thwma-license-manager.php' );
			$api_url = 'https://themehigh.com/';
			THWMA_License_Manager::instance(__FILE__, $api_url, 'plugin', THWMA_SOFTWARE_TITLE);
		}
	}
	init_auto_updater_thwma();
	
	/**
	 * The core plugin class that is used to define internationalization,
	 * admin-specific hooks, and public-facing site hooks.
	 */
	require plugin_dir_path( __FILE__ ) . 'includes/class-thwma.php';
	
	/**
	 * Begins execution of the plugin.
	 */
	function run_thwma() {
		$plugin = new THWMA();
		$plugin->run();
	}
	run_thwma();
}