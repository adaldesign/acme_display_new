var thwma_auto_suggest = (function($, window, document) {

	var store_country = thwma_public_var.store_country;

	function init_autocomplete(){

		

		var  form_fields_value = {
			'billing_address_1': '',
		    'billing_address_2': '',
		    'billing_city': '',
		    'billing_state': '',
		    'billing_postcode': '',
		    'billing_country' : ''
		};
		var component_form ={
        	'street_number': ['billing_address_1', 'short_name'],
            'route': ['billing_address_1', 'long_name'],
            'locality': ['billing_city', 'long_name'],
            'postal_town': ['billing_city', 'long_name'],
            'sublocality_level_1': ['billing_city', 'long_name'],
            'administrative_area_level_1': ['billing_state', 'short_name'],
            'administrative_area_level_2': ['billing_state', 'short_name'],
            'administrative_area_level_3': ['billing_state', 'short_name'],
            'country': ['billing_country', 'short_name'],
            'postal_code': ['billing_postcode', 'short_name']
		}
		var  country_field = '';

		
		
			
			autocomplete = new google.maps.places.Autocomplete(
		    (document.getElementById('billing_address_1')),{
		    	
		       	types: ['geocode'],
		 		
		    });
		   
	 
		    autocomplete.addListener('place_changed', fillInAddress);

		    var billing_country = document.getElementById("billing_country");
	        if(billing_country != null){
	            billing_country.addEventListener("change", function( event ) {
	                set_autocomplete_country()
	            }, true);
	        }

		    var billing_address = document.getElementById("billing_address_1");
		    if(billing_address != null){
		        billing_address.addEventListener("focus", function( event ) {
		            set_autocomplete_country()
		        }, true);
		    } 		
		

		function fillInAddress(){

			clear_form_values();
			var place = autocomplete.getPlace();
			//console.log(place);
			//console.log(component_form);
			for (var field in place.address_components) {
				for(var type in place.address_components[field].types){
					for(var adrs_fld in component_form){

						if(adrs_fld == place.address_components[field].types[type]){
							var prop = component_form[adrs_fld][1];
							if(place.address_components[field].hasOwnProperty(prop)){
								//form_fields_value[component_form[adrs_fld][0]] = place.address_components[field][prop];
								$('#'+component_form[adrs_fld][0]).val([place.address_components[field][prop]]).change();

								//console.log(component_form[adrs_fld][0] + " = " + place.address_components[field][prop]);
							}
						}
					}
				}
			}

			fill_street_number(place);
			fill_state_field(place);
			
		}

		function clear_form_values(){
			 for (var field in form_fields_value) {
	            $('#'+field).val('').change();
	        }
		}

		function   set_autocomplete_country(){

			var country = '';
	        country = document.getElementById('billing_country').value;
	    	if(country == ''){
	    		country = store_country;
	    	}
	        autocomplete.setComponentRestrictions({
	            'country': country
	        });
		}

		function fill_street_number($place){

			var street_number = $place.name;
			if(street_number != ''){
		        {
		           
		           $('#billing_address_1').val([street_number]).change();
		        }
		    }
		}

		function fill_state_field(place){
			
			var billing_state = document.getElementById("billing_state").value;
		    if(billing_state == ''){
		    	for (var field in place.address_components) {
		    		for(var type in place.address_components[field].types){
		    			if( place.address_components[field].types[type] == 'administrative_area_level_2'){
		    				$('#billing_state').val([place.address_components[field]['short_name']]).change();
		    			}
		    		}
		    	}
				
			}
		}
	}

	function init_shipping_autocomplete(){

		var shipping_form_fields = {
			'shipping_address_1': '',
		    'shipping_address_2': '',
		    'shipping_city': '',
		    'shipping_state': '',
		    'shipping_postcode': '',
		    'shipping_country' : ''
		};

		var shipping_component_form ={
        	'street_number': ['shipping_address_1', 'short_name'],
            'route': ['shipping_address_1', 'long_name'],
            'locality': ['shipping_city', 'long_name'],
            'postal_town': ['shipping_city', 'long_name'],
            'sublocality_level_1': ['shipping_city', 'long_name'],
            'administrative_area_level_1': ['shipping_state', 'short_name'],
            'administrative_area_level_2': ['shipping_state', 'short_name'],
            'administrative_area_level_3': ['shipping_state', 'short_name'],
            'country': ['shipping_country', 'short_name'],
            'postal_code': ['shipping_postcode', 'short_name']
		}

		var  country_field = '';

		shipping_autocomplete = new google.maps.places.Autocomplete(
		    (document.getElementById('shipping_address_1')),{
		    	
		    types: ['geocode'],
		 		
		});

		var place = shipping_autocomplete.getPlace();

		shipping_autocomplete.addListener('place_changed',fill_shipping_Address);

	 	var shipping_country = document.getElementById("shipping_country");
        if(shipping_country != null){
            shipping_country.addEventListener("change", function( event ) {
                set_autocomplete_shipping_country()
            }, true);
        }

        var shipping_address = document.getElementById("shipping_address_1");
	    if(shipping_address != null){
	        shipping_address.addEventListener("focus", function( event ) {
	            set_autocomplete_shipping_country()
	        }, true);
	    } 

	    function fill_shipping_Address(){

			clear_shipping_form_values();
			var place = shipping_autocomplete.getPlace();
		
			//console.log(place.address_components);
			
			for (var field in place.address_components) {
				for(var type in place.address_components[field].types){
					for(var adrs_fld in shipping_component_form){

						if(adrs_fld == place.address_components[field].types[type]){
							var prop = shipping_component_form[adrs_fld][1];
							if(place.address_components[field].hasOwnProperty(prop)){
								//form_fields_value[component_form[adrs_fld][0]] = place.address_components[field][prop];
								$('#'+shipping_component_form[adrs_fld][0]).val([place.address_components[field][prop]]).change();

								//console.log(component_form[adrs_fld][0] + " = " + place.address_components[field][prop]);
							}
						}
					}
				}
			}

			fill_shipping_street_number(place);
			fill_shipping_state_field(place);
			
		}

		function clear_shipping_form_values(){
			 for (var field in shipping_form_fields) {
	            $('#'+field).val('').change();
	        }
		}

		function    set_autocomplete_shipping_country(){

			var country = '';
	        country = document.getElementById('shipping_country').value;
	    	if(country == ''){
	    		country = store_country;
	    	}
	        shipping_autocomplete.setComponentRestrictions({
	            'country': country
	        });
		}

		function fill_shipping_street_number($place){

			var street_number = $place.name;
			if(street_number != ''){
		        {
		           
		           $('#shipping_address_1').val([street_number]).change();
		        }
		    }
		}

		function fill_shipping_state_field(place){
			
			var shipping_state = document.getElementById("shipping_state").value;
		    if(shipping_state == ''){
		    	for (var field in place.address_components) {
		    		for(var type in place.address_components[field].types){
		    			if( place.address_components[field].types[type] == 'administrative_area_level_2'){
		    				$('#shipping_state').val([place.address_components[field]['short_name']]).change();
		    			}
		    		}
		    	}
				
			}
		}
	}






	if(thwma_public_var.enable_autofill == 'yes'){


		if(!(document.getElementById('billing_address_1') === null))
        	init_autocomplete();
    
    	if(!(document.getElementById('shipping_address_1') === null))
        	init_shipping_autocomplete();


		if(document.getElementById("billing_address_1") != null){
			var adrs_billing_field = document.getElementById("billing_address_1");
			
		 	google.maps.event.addDomListener(adrs_billing_field, 'keydown', function(e) { 
        		if (e.keyCode == 13) { 
           			e.preventDefault(); 
        		}
   			});	
		}

		if(document.getElementById("shipping_address_1") != null){
			var adrs_shipping_field = document.getElementById("shipping_address_1");
			google.maps.event.addDomListener(adrs_shipping_field, 'keydown', function(e){
				if (e.keyCode == 13) { 
           			e.preventDefault(); 
        		}
			});			
		}	
	}

		
	

}(window.jQuery, window, document));
var thwma_address = (function($, window, document) {
	'use strict';

	var default_shipping = $('#thwma_checkbox_shipping').val();
	var active = false;

	//var billing_address_fields = thwma_public_var.address_fields_billing;
	//var shipping_address_fields = thwma_public_var.address_fields_shipping;

	var bpopup = $("#thwma-billing-tile-field");
	var spopup = $("#thwma-shipping-tile-field");
	
	////////address style/////////
	if($(".address-text").length  > 0){
		var maxHeight = Math.max.apply(null, $(".address-text").map(function (){
   	 		return $(this).outerHeight();
		}).get());

		$(".address-text").outerHeight(maxHeight);
		$(".address-text.wrapper-only").outerHeight(maxHeight+41);
	}
	
	if($(window).width()<600){
		var popupwidth = $(window).width() - 20;
		bpopup.dialog({                   
	       'dialogClass'   	: 'wp-dialog thwma-popup',  
	       'title'         	: thwma_public_var.billing_address,         
	       'modal'         	: true,
	       'autoOpen'      	: false, 
	       'width'       	: popupwidth,   
 		});

 		spopup.dialog({                   
           'dialogClass'   : 'wp-dialog thwma-popup', 
           'title'         : thwma_public_var.shipping_address,
           'modal'         : true,
           'autoOpen'      : false, 
           // 'closeOnEscape' : true,  
           'width'         : popupwidth,   
       	});

	}else{
		bpopup.dialog({                   
	       'dialogClass'   	: 'wp-dialog thwma-popup',  
	       'title'         	: thwma_public_var.billing_address,       
	       'modal'         	: true,
	       'autoOpen'      	: false, 
	       'minHeight'		: 400,
	       'maxHeight'     	: 600, 
	       'width'       	: 780,   
 		});

		spopup.dialog({                   
           'dialogClass'   : 'wp-dialog thwma-popup',
           'title'         : thwma_public_var.shipping_address,
           'modal'         : true,
           'autoOpen'      : false, 
           // 'closeOnEscape' : true,  
           'minHeight'		: 400,
           'maxHeight'     : 600, 
           'width'         : 780,   
       });
	}
	
  	function show_billing_popup(e){
  		e.preventDefault();

  		$('.thwma-btn').removeClass('slctd-adrs');
  		var selected_address = $('#thwma_hidden_field_billing').val();

  		if(selected_address){
  		 	$('.'+selected_address).addClass('slctd-adrs');
  		}

		bpopup.dialog('open');
		active = false;
	}

	function show_shipping_popup(e){
  		e.preventDefault();
		spopup.dialog('open');
		active = false;
	}

	function show_custom_popup(e,section,map_type){
  		e.preventDefault();
  		var cus_popup = $("#thwma-custom-tile-field_"+section);

  		if($(window).width()<600){
			var popupwidth = $(window).width() - 20;

	  		cus_popup.dialog({                   
	           'dialogClass'   : 'wp-dialog thwma-popup', 
	           'title'         : thwma_public_var.addresses,          
	           'modal'         : true,
	           'autoOpen'      : false, 
	          
	           'width'         : popupwidth ,   
	       	});

	    }else{
	    	cus_popup.dialog({                   
	           'dialogClass'   : 'wp-dialog thwma-popup', 
	           'title'         : 'Addresses',          
	           'modal'         : true,
	           'autoOpen'      : false, 
	           // 'closeOnEscape' : true,  
	           'minHeight'		: 400,
	           'maxHeight'     : 600, 
	           'width'         : 780,   
	       	});
	    }
  		
  		active = false;
		setup_section_address_slider(section,map_type);
		cus_popup.dialog('open');
	}

	function populate_selected_address(elm, type, key){
		var selected_address_id = key;
		
		var data = {
			action: 'get_address_with_id',
        	selected_address_id: selected_address_id, 
        	selected_type:type,
		};

		$.ajax({
       		url: thwma_public_var.ajax_url,
       		data: data,
       		type: 'POST',
       		success: function (response) {
       			var sell_countries = thwma_public_var.sell_countries;
				var sell_countries_size = Object.keys(sell_countries).length;
       			var address_fields = [];

				if(type == 'billing'){
					address_fields = thwma_public_var.address_fields_billing;
				}else{
					address_fields = thwma_public_var.address_fields_shipping;
				}
				

				$.each( address_fields, function(address_key, address_type) {
					
					var input_elm = '';
					if(address_type == 'radio' || address_type == 'checkboxgroup'){

						input_elm = $("input[name="+address_key+"]");
					}else{
						input_elm = $('#'+address_key);
					}
					
					var skip = (sell_countries_size == 1 && address_key == type+'_country') ? true : false;

					if (!skip && input_elm.length) {
						var _type = input_elm.getType();
						var _value = response[address_key];

						thwma_public_base.set_field_value_by_elm(input_elm, _type, _value);
					}
				});
       		}
       	});

       	if(type == 'billing'){
       		$('#thwma_hidden_field_billing').val(selected_address_id);
       		bpopup.dialog('close');	
       	}else{
       		$('#thwma_hidden_field_shipping').val(selected_address_id);
       		spopup.dialog('close');
       	}
	}

	function add_new_address(elm,type){
		var sell_countries = thwma_public_var.sell_countries;
		var sell_countries_size = Object.keys(sell_countries).length;
		var address_fields = [];
		
		if(type=='billing'){
			address_fields = thwma_public_var.address_fields_billing;
			$('#thwma_hidden_field_billing').val('add_address');
		}else{
			address_fields = thwma_public_var.address_fields_shipping;
			$('#thwma_hidden_field_shipping').val('add_address');
		}
		
		$.each( address_fields, function(address_key, field_type) {

			var input_elm = '';
			if(field_type == 'radio' || field_type == 'checkboxgroup'){

				input_elm = $("input[name="+address_key+"]");
			}else{
				input_elm = $('#'+address_key);
			}

			var skip = (sell_countries_size == 1 && address_key == type+'_country') ? true : false;

			if (!skip && input_elm.length) {
				var _type = input_elm.getType();
				thwma_public_base.set_field_value_by_elm(input_elm, _type, '');
			}

			/*
				if(len_sell_countries == 1  ){
					if(key != type+'_country'  ){
						
						var meta_key = $('#'+key);
						var meta_val = '';
						thwma_public_base.set_field_value_by_elm(meta_key,field_type,meta_val);
					}
				}else{

					var meta_key = $('#'+key);
					var meta_val = '';
					thwma_public_base.set_field_value_by_elm(meta_key,field_type,meta_val);
				}	
			*/
		});
		
		bpopup.dialog('close');
		spopup.dialog('close');	
	}

	function delete_address(elm,type,key){
		if(type == 'billing'){
			$("#thwma-billing-tile-field").append('<div class="ajaxBusy"> <i class="fa fa-spinner" aria-hidden="true"></i></div>');
		}else{
			$("#thwma-shipping-tile-field").append('<div class="ajaxBusy"> <i class="fa fa-spinner" aria-hidden="true"></i></div>');
		}
		var selected_address_id = key;
		var selected_type = type;
		var data = {
			action: 'delete_address_with_id',
        	selected_address_id: selected_address_id,
        	selected_type : selected_type,
		};
		$('.ajaxBusy').show();
		$.ajax({
       		url: thwma_public_var.ajax_url,
       		data: data,
       		type: 'POST',
       		success: function (response) {

				$('#thwma-billing-tile-field').html(response.result_billing);
				$('#thwma-shipping-tile-field').html(response.result_shipping);	
				$('.ajaxBusy').hide();

				var selected_address = $('#thwma_hidden_field_'+type).val();

				if(type == 'billing'){
					setup_billing_address_slider(type);
				}else if(type == 'shipping'){
					setup_shipping_address_slider(type);
				}
  				// if(selected_address){
  				// 	$('.'+selected_address).html('<div class="radio-select"</div>');
  				// }
			}
       	});
	}

	function set_default_address(elm,type,key){
		if(type == 'billing'){
			$("#thwma-billing-tile-field").append('<div class="ajaxBusy"> <i class="fa fa-spinner" aria-hidden="true"></i></div>');
		}else{
			$("#thwma-shipping-tile-field").append('<div class="ajaxBusy"> <i class="fa fa-spinner" aria-hidden="true"></i></div>');
		}
		var selected_address_id = key;
		var data = {
			action: 'set_default_address',
        	selected_address_id : selected_address_id,
        	selected_type : type,
		};
		$('.ajaxBusy').show();
		$.ajax({
       		url: thwma_public_var.ajax_url,
       		data: data,
       		type: 'POST',
       		success: function (response) {
       			
				$('#thwma-billing-tile-field').html(response.result_billing);
				$('#thwma-shipping-tile-field').html(response.result_shipping);
				$('.ajaxBusy').hide();
				var selected_address = $('#thwma_hidden_field_'+type).val();

				if(type == 'billing'){
					setup_billing_address_slider(type);
				}else if(type == 'shipping'){
					setup_shipping_address_slider(type);
				}

  				// if(selected_address){
  				// 	$('.'+selected_address).html('<div class="radio-select"</div>');
  				// }
			}
       	});
	}


	$("#thwma-billing-alt").change(function(event){
		event.preventDefault();
		var select_type = this.value;
		var type = 'billing';
		var elm = '';
		
		if(select_type == 'add_address'){
			add_new_address(elm,type);
		}else{
			populate_selected_address(elm,type,select_type);
		}	
	});

	$("#thwma-shipping-alt").change(function(event){
		event.preventDefault();
		var select_type = this.value;
		var type = 'shipping';
		var elm = '';
		if(select_type == 'add_address'){
			add_new_address(elm,type);
		}else{
			populate_selected_address(elm,type,select_type);
		}	
	});

	////////////////////////////////////////////////////////
	///////////////////////custom section start////////////
	///////////////////////////////////////////////////////*

	var sections = thwma_public_var.custom_sections;

	if(sections){
		$.each(sections,function(key, val){

			var select_id = "#thwma_"+key;
			//var enabled = val['section_enable_'+key];
			var map_fields = val['map_fields'];
			$(select_id).change(function(event){

				var select_type = this.value;
				var map_type = val['maped_section'];

				populate_section_address(select_type,key,map_fields,map_type);	
				
			});
		});
	}	

	function populate_selected_section_address(e,elm,adrs_key,section,map_type){

		var sections = thwma_public_var.custom_sections;
		var map_fields = '';
			
		$.each(sections,function(key, val){
			
			if(key == section){
				map_fields = val['map_fields'];
			}
		});

		
		var select_type = adrs_key;
		populate_section_address(select_type,section,map_fields,map_type);

	}

	function populate_section_address(select_type,key,map_fields,map_type){
		var data = {
			action: 'get_address_with_id',
        	selected_address_id: select_type, 
        	selected_type: map_type,
        	section_name: key,
		};
		$.ajax({
       		url: thwma_public_var.ajax_url,
       		data: data,
       		type: 'POST',
       		success: function (response) {
       			
       			$.each( map_fields , function(address_key,section_field){
   					var meta_val = response[address_key];
					var meta_key=$('#'+section_field);
					thwma_public_base.set_field_value_by_elm(meta_key,'select', meta_val);
   				});
       		}


       	});
       	$(".ui-dialog-content").dialog("close");
	}
	


	/////////////////////////////////////////////////
	/////////////////////////END///////////////////////////////////////
	///////////////////////////////////////////////////////////////

	

	///////////Accordion//////////

	/*$( "#thwma_billing_accordion" ).accordion({
		active:false,
		collapsible:true,
		autoHeight:false,
	});

	$("#thwma_billing_toggle_show").hide();

	$(".thwma_billing_toggle_accordion").click(function(event){
		event.preventDefault();
        $("#thwma_billing_toggle_show").toggle();
    });

    $( "#thwma_shipping_accordion" ).accordion({
		active:false,
		collapsible:true,
		autoHeight:false,
	});

	$("#thwma_shipping_toggle_show").hide();

	$(".thwma_shipping_toggle_accordion").click(function(event){
		event.preventDefault();
        $("#thwma_shipping_toggle_show").toggle();
    });*/


    ///////////////////////////
    $('#ship-to-different-address-checkbox').change(function(){

    	
		if ($('#ship-to-different-address-checkbox').is(':checked')) {
				
			$('#thwma_checkbox_shipping').val('ship_select');
		}else{
		
			$('#thwma_checkbox_shipping').val(default_shipping);
		}
		
	});

    /*******************************
    **** Address Slider - START ****
    ********************************/


	function move_slider(slider, prevBtn, nextBtn, leftPos, startPos, endPos, totalItems, itemsPerView){
		
		active = true; 
		slider.animate(
        	{left: leftPos}, 
        	{duration:500, 
        		complete: function(){ 
        			enable_disable_prev_next_action(prevBtn, nextBtn, startPos, endPos, totalItems, itemsPerView);
        			active = false; 
        		} 
        	}
        );
	}

	function enable_disable_prev_next_action(prevBtn, nextBtn, startPos, endPos, totalItems, itemsPerView){
		var disablePrev = false;
		var disableNext = false;

		if(startPos === 0){
			disablePrev = true;
		}

		if(endPos === totalItems){
			disableNext = true;
		}

		prevBtn.removeClass('disabled');
		nextBtn.removeClass('disabled');

		if(disablePrev){
	    	prevBtn.addClass('disabled');
		}
		if(disableNext){
	    	nextBtn.addClass('disabled');
		}
	}

	/////////////********  ******************////////////////////
	var exist_slider = $('.thslider-box').length;
	
	if(exist_slider>0){

		if($(window).width() > 600){

			var box = $('.thslider-box');
			var maxWidth = Math.max.apply(null, (box).map(function (){
			
   	 			return $(this).width();
			}).get());	
			
   	 		
			if(maxWidth < 460 &&  (!(maxWidth <=0))){
				var items_per_view = 1;
    			(box).css("width", "260px");

			}else if(maxWidth < 690 &&  (!(maxWidth <=0))){
    			var items_per_view = 2;
    			(box).css("width", "480px");
    		
    		}else{
    			var items_per_view = 3;
    		}

			setup_shipping_address_slider('shipping');
			setup_billing_address_slider('billing');

		}	
	}

	function setup_billing_address_slider(type){
		
    	active = false;
    	
    	var viewport = $('.thslider-viewport');
    	var list = $('.thslider-list.bill');
    	var prevBtn = $('.control-buttons .prev.billing');
    	var nextBtn = $('.control-buttons .next.billing');
    	var total_items = $('.thslider-item.'+type).length;
    	var item_width = 210+20;
    	var total_width = (total_items*item_width);
    	//var items_per_view = 3;
    	//ar left_margin = 0;
    	var initialPos = 1;
    	var finalPos = total_items;
    	var leftPos = '';
    	var startPos = initialPos;
    	var endPos = total_items > items_per_view ? items_per_view : finalPos;
		list.css('width', total_width);
		//move_slider(list, prevBtn, nextBtn, leftPos, startPos, endPos, total_items, items_per_view);
		prevBtn.click(function () { 
		    if (active === false){
		    	var rem_to_view = startPos-1;
		        var items_next_view = items_per_view;
		        if(rem_to_view < items_per_view){
		        	startPos = initialPos;
		        	endPos = items_per_view;
		        	items_next_view = rem_to_view;

		        }else{
		        	startPos -= items_per_view;
		        	endPos -= items_per_view;
		        }

		        if(rem_to_view > 0){
		        	if(startPos === initialPos){
			            leftPos = initialPos;
			        }else{
			            leftPos = '+='+(item_width * items_next_view);
			        }
			        move_slider(list, prevBtn, nextBtn, leftPos, startPos, endPos, total_items, items_per_view);
			    }
		 
		    }
		});

		nextBtn.click(function() {
			
		 	if (active === false){
		 		
		     var    rem_to_view = total_items - endPos;
		     var   items_next_view = items_per_view;


		        if(rem_to_view < items_per_view){
		        	startPos += rem_to_view;
		        	endPos += rem_to_view;
		        	items_next_view = rem_to_view;

		        }else{
		        	startPos += items_per_view;
		        	endPos += items_per_view;
		        }


		        if(rem_to_view > 0){
			        leftPos = '-='+(item_width * items_next_view);
			        move_slider(list, prevBtn, nextBtn, leftPos, startPos, endPos, total_items, items_per_view);
			    }
		    }
		});
	}

	function setup_shipping_address_slider(type){
		
    	active = false;

    	var ship_viewport = $('.thslider-viewport');
    	var ship_list = $('.thslider-list.ship');
    	var ship_prevBtn = $('.control-buttons .prev.shipping');
    	var ship_nextBtn = $('.control-buttons .next.shipping');
    	var ship_total_items = $('.thslider-item.'+type).length;
    	var item_width = 210+20;
    	var ship_total_width = ship_total_items*item_width;
    	var ship_initialPos = 1;
    	var ship_finalPos = ship_total_items;
    	var ship_leftPos = '';
    	var ship_startPos = ship_initialPos;
    	var ship_endPos = ship_total_items > items_per_view ? items_per_view : ship_finalPos;
		ship_list.css('width', ship_total_width);
		//move_slider(list, prevBtn, nextBtn, leftPos, startPos, endPos, total_items, items_per_view);
		ship_prevBtn.click(function () { 
			
		    if (active === false){
		    	var rem_to_view = ship_startPos-1;
		        var items_next_view = items_per_view;

		        if(rem_to_view < items_per_view){
		        	ship_startPos = ship_initialPos;
		        	ship_endPos = items_per_view;
		        	items_next_view = rem_to_view;

		        }else{
		        	ship_startPos -= items_per_view;
		        	ship_endPos -= items_per_view;
		        }

		        if(rem_to_view > 0){
		        	if(ship_startPos === ship_initialPos){
			            ship_leftPos = ship_initialPos;
			        }else{
			            ship_leftPos = '+='+(item_width * items_next_view);
			        }
			        move_slider(ship_list,ship_prevBtn,ship_nextBtn, ship_leftPos,ship_startPos, ship_endPos,ship_total_items,items_per_view);
			    }
		    }
		});

		ship_nextBtn.click(function() {
			
		 	if (active === false){
		 		
		     var    rem_to_view = ship_total_items - ship_endPos;
		     var   items_next_view = items_per_view;

		        if(rem_to_view < items_per_view){
		        	ship_startPos += rem_to_view;
		        	ship_endPos += rem_to_view;
		        	items_next_view = rem_to_view;

		        }else{
		        	ship_startPos += items_per_view;
		        	ship_endPos += items_per_view;
		        }

		        if(rem_to_view > 0){
			        ship_leftPos = '-='+(item_width * items_next_view);
			        move_slider(ship_list, ship_prevBtn,ship_nextBtn,ship_leftPos, ship_startPos,ship_endPos, ship_total_items, items_per_view);
			    }
		    }
		});
	}

	function setup_section_address_slider(section,map_type){
		
    	active = false;
    	var sectn_parent = $('#thslider-'+section);
    	
    	var sectn_viewport = $('.thslider-viewport');

    	var sectn_list = sectn_parent.find('.thslider-list.'+section);

    	var sectn_prevBtn = sectn_parent.find('.control-buttons .prev.'+section);

    	var sectn_nextBtn = sectn_parent.find('.control-buttons .next.'+section);

    	move_slider(sectn_list,sectn_prevBtn,sectn_nextBtn, '',1, 3,3,3);

    	var sectn_total_items = $('.thslider-item.'+map_type).length + 1;
    
    	var item_width = 210+20;
    	var sectn_total_width = sectn_total_items*item_width;
    	var items_per_view = 3;
    	//ar left_margin = 0;
    	var sectn_initialPos = 1;
    	var sectn_finalPos = sectn_total_items;
    	var sectn_leftPos = '';
    	var sectn_startPos = sectn_initialPos;
    	var sectn_endPos = sectn_total_items > items_per_view ? items_per_view : sectn_finalPos;
		sectn_list.css('width', sectn_total_width);
		//move_slider(list, prevBtn, nextBtn, leftPos, startPos, endPos, total_items, items_per_view);
		//move_slider(sectn_list,sectn_prevBtn,sectn_nextBtn, sectn_leftPos,sectn_startPos, sectn_endPos,sectn_total_items,items_per_view);

		sectn_prevBtn.unbind().click(function () { 
			
		    if (active === false){
		    	
		    	var rem_to_view = sectn_startPos-1;
		        var items_next_view = items_per_view;


		        if(rem_to_view < items_per_view){
		        	sectn_startPos = sectn_initialPos;
		        	sectn_endPos = items_per_view;
		        	items_next_view = rem_to_view;

		        }else{
		        	sectn_startPos -= items_per_view;
		        	sectn_endPos -= items_per_view;
		        }

		        if(rem_to_view > 0){
		        	if(sectn_startPos === sectn_initialPos){
			            sectn_leftPos = sectn_initialPos;
			        }else{
			            sectn_leftPos = '+='+(item_width * items_next_view);
			        }
			        move_slider(sectn_list,sectn_prevBtn,sectn_nextBtn, sectn_leftPos,sectn_startPos, sectn_endPos,sectn_total_items,items_per_view);
			    }
		    }
		});

		sectn_nextBtn.unbind().click(function() {
			
			
		 	if (active === false){
		 		
		    	var  rem_to_view = sectn_total_items - sectn_endPos;
		     	var  items_next_view = items_per_view;
		        // console.log('Total Remaining: '+rem_to_view);
		        // console.log('Current START Position: '+sectn_startPos);
		        // console.log('Current END Position: '+sectn_endPos);
		        if(rem_to_view < items_per_view){
		        	sectn_startPos += rem_to_view;
		        	sectn_endPos += rem_to_view;
		        	items_next_view = rem_to_view;

		        }else{
		        	sectn_startPos += items_per_view;
		        	sectn_endPos += items_per_view;
		        }

		       	// console.log('Next items to show: '+items_next_view);
		       	// console.log('Next START Position: '+sectn_startPos);
		        // console.log('Next END Position: '+sectn_endPos);

		        if(rem_to_view > 0){
			       sectn_leftPos = '-='+(item_width * items_next_view);
			        move_slider(sectn_list,sectn_prevBtn,sectn_nextBtn,sectn_leftPos, sectn_startPos,sectn_endPos, sectn_total_items, items_per_view);
			    }
		    }
		   
		});
	}


	
	//setup_address_slider('billing');

	/*******************************
    **** Address Slider - END ******
    ********************************/



	////////////
	return{
    	show_billing_popup:show_billing_popup,
    	show_shipping_popup:show_shipping_popup,
    	populate_selected_address:populate_selected_address,
    	add_new_address : add_new_address,
    	delete_address : delete_address,
    	set_default_address : set_default_address,
    	show_custom_popup : show_custom_popup,
    	populate_selected_section_address : populate_selected_section_address,

    };
}(window.jQuery, window, document));


function  thwma_show_billing_popup(e){
	thwma_address.show_billing_popup(e);
}

function  thwma_show_shipping_popup(e){
	thwma_address.show_shipping_popup(e);
}

function thwma_populate_selected_address(e,elm,type,key){
	e.preventDefault();
 	thwma_address.populate_selected_address(elm,type,key);

}

function thwma_add_new_address(e,elm,type){
	e.preventDefault();
	thwma_address.add_new_address(elm,type);
}

function thwma_delete_selected_address(elm,type,key){

	thwma_address.delete_address(elm,type,key);
}

function thwma_set_default_address(elm,type,key){

	thwma_address.set_default_address(elm,type,key);
}

function thwma_show_custom_popup(e,section,map_type){
	
	thwma_address.show_custom_popup(e,section,map_type);
}

function thwma_populate_selected_section_address(e,elm,key,section,map_type){
	e.preventDefault();
	thwma_address.populate_selected_section_address(e,elm,key,section,map_type);
}
var thwma_public_base = (function($, window, document) {
	'use strict';
	
	function isEmpty(val){
		return (val === undefined || val == null || val.length <= 0) ? true : false;
	}
			
	$.fn.getType = function(){
        try{
            return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); 
        }catch(err) {
            return 'E001';
    	}
    }
    
	/********************************************
	***** CHARACTER COUNT FUNCTIONS - START *****
	********************************************/
	function display_char_count(elm, isCount){
		var fid = elm.prop('id');
        var len = elm.val().length;
		var displayElm = $('#'+fid+"-char-count");
		
		if(isCount){
			displayElm.text('('+len+' characters)');
		}else{
			var maxLen = elm.prop('maxlength');
			var left = maxLen-len;
			displayElm.text('('+left+' characters left)');
			if(rem < 0){
				displayElm.css('color', 'red');
			}
		}
	}
    /******************************************
	***** CHARACTER COUNT FUNCTIONS - END *****
	******************************************/
	
	function set_field_value_by_elm(elm, type, value){
		switch(type){
			case 'radio':
				elm.val([value]);
				break;
			case 'checkbox':
				if(elm.data('multiple') == 1){
					value = value ? value : [];
					elm.val([value]).change();
				}else{
					elm.val([value]).change();
				}
				break;
			case 'select':

				var options_append = thwma_public_var.select_options;
				if(options_append == true){
					var option_values = [];
					elm.find('option').each(function(option_key,option_val) {
						if($(this).val() != ""){
							option_values[option_key] = $(this).val();
						}
					});
					
					if( $.inArray(value,option_values) != -1){
						if(elm.prop('multiple')){
							elm.val(value);
						}else{
							elm.val([value]).change();
						}
					}else{
						elm.append($("<option></option>").attr("value",value).text(value)); 
						elm.val([value]).change();
					}
				}else{

					if(elm.prop('multiple')){
						elm.val(value);
					}else{
						elm.val([value]).change();
					}
				}
				break;
			case 'multiselect':
			
				if(elm.prop('multiple')){
					if(typeof(value) != "undefined"){
						elm.val(value.split(',')).change();
					}
				}else{
					elm.val([value]).change();
				}
				break;
			default:
				elm.val(value).change();
				//elm.trigger("change")
				break;
		}
	}
	
	function get_field_value(type, elm, name){
		var value = '';
		switch(type){
			case 'radio':
				value = $("input[type=radio][name="+name+"]:checked").val();
				break;
			case 'checkbox':
				if(elm.data('multiple') == 1){
					var valueArr = [];
					$("input[type=checkbox][name='"+name+"[]']:checked").each(function(){
					   valueArr.push($(this).val());
					});
					value = valueArr;//.toString();
				}else{
					value = $("input[type=checkbox][name="+name+"]:checked").val();
				}
				break;
			case 'select':
				value = elm.val();
				break;
			case 'multiselect':
				value = elm.val();
				break;
			default:
				value = elm.val();
				break;
		}
		return value;
	}
	
	return {
		display_char_count : display_char_count,
		set_field_value_by_elm : set_field_value_by_elm,
		get_field_value : get_field_value,
	};
}(window.jQuery, window, document));

(function( $ ) {
	'use strict';

	function initialize_thwma(){
		var form_wrapper = $('.wrapper-class');
		if(form_wrapper){											 

		}		
	}
	
	$("#billing_address_1").attr("autocomplete", "off");
	$(".checkout woocommerce-checkout").attr("autocomplete", "off");

	var address= thwma_public_var.address_array;

	/***----- INIT -----***/
	initialize_thwma();


})( jQuery );
