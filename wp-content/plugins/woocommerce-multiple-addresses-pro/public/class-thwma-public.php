<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    woocommerce-multiple-addresses-pro
 * @subpackage woocommerce-multiple-addresses-pro/public
 */
if(!defined('WPINC')){	die; }

if(!class_exists('THWMA_Public')):
 
class THWMA_Public {
	
	const DEFAULT_SHIPPING_ADDRESS_KEY = 'thwma-shipping-alt';
	const DEFAULT_BILLING_ADDRESS_KEY  = 'thwma-billing-alt';
	const THWMA_TEXT_DOMAIN = 'thwma';
	
	private $plugin_name;
	private $version;

	public function __construct( $plugin_name, $version ) {		
		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_action('after_setup_theme', array($this, 'define_public_hooks'));
	
	}

	public function enqueue_styles_and_scripts() {

		global $wp_scripts;
		if(is_wc_endpoint_url( 'edit-address' )|| (is_checkout())){
			$debug_mode = apply_filters('thwma_debug_mode', false);
			$suffix = $debug_mode ? '' : '.min';
			
			$jquery_version = isset($wp_scripts->registered['jquery-ui-core']->ver) ? $wp_scripts->registered['jquery-ui-core']->ver : '1.9.2';
			
			$this->enqueue_styles($suffix, $jquery_version);
			$this->enqueue_scripts($suffix, $jquery_version);
		}
	}
	
	private function enqueue_styles($suffix, $jquery_version) {

		wp_register_style('select2', THWMA_WOO_ASSETS_URL.'/css/select2.css');
		wp_enqueue_style('woocommerce_frontend_styles');
		wp_enqueue_style('select2');
		wp_enqueue_style('dashicons');
		wp_enqueue_style('jquery-ui-style', '//ajax.googleapis.com/ajax/libs/jqueryui/'. $jquery_version .'/themes/smoothness/jquery-ui.css');		
		wp_enqueue_style('FontAwesome','https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');
		wp_enqueue_style('thwma-public-style', THWMA_ASSETS_URL_PUBLIC . 'css/thwma-public'. $suffix .'.css', $this->version);
		
	}

	private function enqueue_scripts($suffix, $jquery_version) {

		wp_register_script('thwma-public-script', THWMA_ASSETS_URL_PUBLIC . 'js/thwma-public'. $suffix .'.js', array('jquery','jquery-ui-dialog','jquery-ui-accordion','select2',), $this->version, true );
		wp_enqueue_script('thwma-public-script');

		if( THWMA_Utils:: get_advanced_settings_value('enable_autofill') == 'yes'){

			$api_key = THWMA_Utils:: get_advanced_settings_value('autofill_apikey');
			if($api_key){

				wp_enqueue_script('google-autocomplete', 'https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key='.$api_key);
			}
		}

		$slelect_options = apply_filters('thwma_checkout_select_options',false);
		
		$address_fields_billing = THWMA_Utils::get_address_fields_by_address_key('billing_');
		$address_fields_shipping = THWMA_Utils::get_address_fields_by_address_key('shipping_');

		$section_settings = THWMA_Utils::get_custom_section_settings();
		$section_settings = $section_settings ? $section_settings : '';

		$store_country = WC()->countries->get_base_country();	
		$sell_countries =  WC()->countries->get_allowed_countries();
		
		
		$script_var = array(
			'ajax_url'    => admin_url( 'admin-ajax.php' ),
			'address_fields_billing'=>$address_fields_billing,
			'address_fields_shipping'=>$address_fields_shipping,
			
			'enable_autofill' => THWMA_Utils:: get_advanced_settings_value('enable_autofill'),
			'store_country' => $store_country,
			'sell_countries' => $sell_countries,
			
			'custom_sections' => $section_settings,
			'select_options' => $slelect_options,
			
			'billing_address' 	=> __('Billing Addresses','woocommerce-multiple-addresses-pro'),
			'shipping_address'  => __('Shipping Addresses','woocommerce-multiple-addresses-pro'),
			'addresses'			=> __('Addresses','woocommerce-multiple-addresses-pro'),
			
		);
		wp_localize_script('thwma-public-script', 'thwma_public_var', $script_var);
	}
	
	public function define_public_hooks(){	

		add_filter('woocommerce_address_to_edit',array($this,'change_edit_form'),10,2);	
		add_action('woocommerce_after_save_address_validation',array($this,'save_address'),10,3);
			

		add_action('thwma_after_address_display',array($this,'display_custom_addresses'));		

		add_action('woocommerce_before_edit_account_address_form',array($this,'delete_address'));
		add_action('woocommerce_before_edit_account_address_form',array($this,'set_default_billing_address'));
		add_action('woocommerce_before_edit_account_address_form',array($this,'set_default_shipping_address'));
		add_action('woocommerce_before_checkout_billing_form',array($this,'session_update_billing'));
		add_action('woocommerce_before_checkout_shipping_form',array($this,'session_update_shipping'));
		add_action('woocommerce_before_checkout_billing_form',array($this,'address_above_billing_form'));
		add_action('woocommerce_after_checkout_billing_form',array($this,'address_below_billing_form'));
		add_filter('woocommerce_checkout_fields',array($this,'add_hidden_field_to_checkout_fields'));

		add_filter('woocommerce_form_field_hidden',array($this,'add_hidden_field'),5,4);
		add_action('woocommerce_checkout_order_processed',array($this,'update_custom_billing_address_from_checkout'),10,3);
		add_action('woocommerce_before_checkout_shipping_form',array($this,'address_above_shipping_form'));
		add_action('woocommerce_after_checkout_shipping_form',array($this,'address_below_shipping_form'));
		add_action('woocommerce_checkout_order_processed',array($this,'update_custom_shipping_address_from_checkout'),10,3);

		//add_action('woocommerce_checkout_order_processed',array($this,'update_custom_default_address_from_checkout'),10,3);

		add_action('wp_ajax_get_address_with_id',array($this,'get_addresses_by_id'));
		add_action('wp_ajax_delete_address_with_id',array($this,'delete_address_from_checkout'));
		add_action('wp_ajax_set_default_address',array($this,'default_address_from_checkout'));

		if(isset($_GET['atype'])){
			add_filter('woocommerce_billing_fields', array($this,'add_additional_billing_field'),1000,2);
			add_filter('woocommerce_shipping_fields', array($this,'add_additional_shipping_field'),1000,2);
		}

		add_action( 'woocommerce_after_checkout_validation',array($this,'add_address_from_checkout'),30,2);		
		add_filter('woocommerce_billing_fields', array($this, 'prepare_address_fields_before_billing'),1500, 2);
		add_filter('woocommerce_shipping_fields', array($this, 'prepare_address_fields_before_shipping'),1500, 2);		

			
		add_filter('woocommerce_localisation_address_formats',array($this,'localisation_address_formats'));
		//////////////////////////////////////////////////////////

		$section_settings = THWMA_Utils::get_custom_section_settings();
		if(is_user_logged_in()){
			if($section_settings){
				foreach ($section_settings as $section => $props) {
					
					if($props['enable_section'] == 'yes'){
						
						add_action('thwcfe_before_section_fields_'.$section,array($this,'add_additional_fields'),10,1);
					}
						
				}
				
			}
		}

		////////////////////////////////////////////

		//add_action('thwma_after_address_display',array($this,'display_custom_sections'));
		//add_filter('woocommerce_address_to_edit',array($this,'add_custom_form'),1200,2);
		//add_action('woocommerce_after_save_address_validation',array($this,'custom_section_save_address'),1200,3);
		//apply_filters( 'woocommerce_get_endpoint_url', $url, $endpoint, $value, $permalink )
		//add_filter('woocommerce_get_endpoint_url',array($this,'add_url'),1000,3);
		//add_filter('woocommerce_my_account_edit_address_field_value',array($this,'set_field',10,3);
		
	}	

  

	public function address_template($template, $template_name, $template_path ){
            
        if('myaccount/my-address.php' == $template_name ){           	
        	$template = THWMA_TEMPLATE_URL_PUBLIC.'myaccount/my-address.php';   
        }
        return $template;
    }  

	//////////////////Change Address Edit form///////////////

	public function change_edit_form($address, $load_address){
			
		if($load_address ){
			if(isset($_GET['atype'])){
				$url = $_GET['atype'];
				$type = str_replace('/','',$url);
				if($type=='add-address'){
					$sell_countries =  WC()->countries->get_allowed_countries();
					$sell_countries_size = count($sell_countries);
					
					foreach ($address as $key => $value) {
						$skip = ($sell_countries_size === 1 && $key == $load_address.'_country') ? true : false;
						if (!$skip) {

							$address[$key]['value']='';		
						}
					}

				}else{

					$address = $this->get_address_to_edit($load_address,$address,$type);
				}
			}
		}
		return $address;	
	}

	////////////Add heading//////////////

	public function add_additional_billing_field($address_fields, $country){
		$address_fields['billing_heading'] = array(
				'label'        => __( 'Address Type','woocommerce-multiple-addresses-pro'),
				'required'     => false,
				'class'        => array( 'form-row-wide', 'address-field' ),
				'autocomplete' => '',
				'placeholder'  => __('Home/Office/Other','woocommerce-multiple-addresses-pro'),
				'priority'     => 0.5,
			);
		return $address_fields;
	}

	public function add_additional_shipping_field($address_fields, $country){
		$address_fields['shipping_heading'] = array(
				'label'        => __( 'Address Type','woocommerce-multiple-addresses-pro'),
				'required'     => false,
				'class'        => array( 'form-row-wide', 'address-field' ),
				'autocomplete' => '',
				'placeholder'  => __('Home/Office/Other','woocommerce-multiple-addresses-pro'),
				'priority'     => 0.5,
			);
		return $address_fields;
	}

	//////////////////////Save Address//////////////////////////////////////////////////////////

	public function save_address($user_id, $load_address, $address){
		if($load_address != ''){
			if(isset($_GET['atype'])){
				$url = $_GET['atype'];
				$type = str_replace('/','',$url);	
				if ( 0 === wc_notice_count( 'error') ){
					if($type =='add-address'){
						if($load_address == 'billing'){
							$new_address = $this->prepare_posted_address($user_id,$address,'billing');
							THWMA_Utils::save_address_to_user($user_id,$new_address,'billing');	
						}elseif($load_address == 'shipping'){
							$custom_address=$this->prepare_posted_address($user_id,$address,'shipping');
							THWMA_Utils::save_address_to_user($user_id,$custom_address,'shipping');
						}
					}else{

						$this->update_address($user_id,$load_address,$address,$type);
					}

					if($type == 'add-address'){
						wc_add_notice( __('Address Added successfully.','woocommerce-multiple-addresses-pro') );
					}else{
						wc_add_notice( __('Address Changed successfully.','woocommerce-multiple-addresses-pro') );
					}
					wp_safe_redirect( wc_get_endpoint_url( 'edit-address', '', wc_get_page_permalink( 'myaccount' ) ) );
 					exit;
				}		
			}else{

				$exist_address = get_user_meta($user_id,THWMA_Utils::ADDRESS_KEY,true);
				if(is_array($exist_address)){
					if ( 0 === wc_notice_count( 'error') ){
						$default_key = THWMA_Utils::get_custom_addresses($user_id,'default_'.$load_address);
						$same_address_key = THWMA_Utils::is_same_address_exists($user_id,$load_address);
						$address_key = ($default_key) ? $default_key : $same_address_key;
						
						if($address_key){
							$this->update_address($user_id,$load_address,$address,$address_key);
						}else{

							$new_address = $this->prepare_posted_address($user_id,$address,$load_address);
							THWMA_Utils::save_address_to_user($user_id,$new_address,$load_address);
						}
					}	
				}
			}
		}
	}

	private function prepare_posted_address($user_id,$address,$type){
		$address_new=array();
		foreach ($address as $key => $value) {
			$address_value = is_array($_POST[ $key ]) ? implode(',', wc_clean($_POST[ $key ])) : wc_clean($_POST[ $key ]);
			$address_new[$key]=$address_value;
		}

		$default_heading = apply_filters('thwma_default_heading',false);
		if($default_heading){
			if($type=='billing'){
				if(isset($address_new['billing_heading']) && ($address_new['billing_heading'] == '')){
					$address_new['billing_heading'] = __('Home','woocommerce-multiple-addresses-pro');
				}
			}elseif($type=='shipping'){
				if(isset($address_new['shipping_heading']) && ($address_new['shipping_heading'] == '')){
					$address_new['shipping_heading'] = __('Home','woocommerce-multiple-addresses-pro');
				}
			}
		}		
	
		return $address_new;
	}

	

	

	public function display_custom_addresses($customer_id){


		$enable_billing = THWMA_Utils::get_setting_value('settings_billing','enable_billing');
		$enable_shipping = THWMA_Utils::get_setting_value('settings_shipping','enable_shipping');

		$custom_addresses_billing	= THWMA_Utils::get_addresses($customer_id,'billing');
		
		if(is_array($custom_addresses_billing)){
			$billing_addresses = $this->get_account_addresses($customer_id,'billing',$custom_addresses_billing);
		}



		$custom_addresses_shipping	= THWMA_Utils::get_addresses($customer_id,'shipping');

		if(is_array($custom_addresses_shipping)){
			$shipping_addresses = $this->get_account_addresses($customer_id,'shipping',$custom_addresses_shipping);
		}
		
		?>
		<div class= 'th-custom'><?php 
			if($enable_billing == 'yes'){?>
				
				<div class='th-custom-address'>
					<div class = 'th-head'><h3><?php _e('Additional billing addresses','woocommerce-multiple-addresses-pro'); ?> </h3></div>
					<?php 
					if($custom_addresses_billing){
						echo $billing_addresses; 
					}else{?>
						<div class="add-acnt-adrs new-adrs">
	               		<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', 'billing?atype=add-address')); ?>" <i class="fa fa-plus"></i><?php  _e('Add new address','woocommerce-multiple-addresses-pro'); ?> </a>
						 
	            		</div><?php
					}?>
				</div><?php	
			}
			if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ){
				if($enable_shipping == 'yes'){?>
					
					<div class='th-custom-address'>

						<div class = 'th-head'><h3><?php _e("Additional shipping addresses",'woocommerce-multiple-addresses-pro'); ?> </h3></div><?php 
						if($custom_addresses_shipping){
							 	echo $shipping_addresses;  
						}else{?>
							<div class="add-acnt-adrs new-adrs">
		               			<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', 'shipping?atype=add-address')); ?>" <i class="fa fa-plus"></i> <?php _e('Add new address','woocommerce-multiple-addresses-pro'); ?></a> 
		            		</div><?php
						}?>
					</div><?php	
				}
			}?>
		</div>
		
		<?php
		
	}


	public function get_account_addresses($customer_id,$type,$custom_addresses){		
		$return_html = '';
		$add_class='';
		$address_type = "'$type'";

		$address_count = count(THWMA_Utils::get_custom_addresses($customer_id,$type));
		$address_limit = THWMA_Utils::get_setting_value('settings_'.$type , $type.'_address_limit');



		if(is_array($custom_addresses)){
			$add_list_class  = ($type == 'billing') ? " thslider-list bill " : " thslider-list ship";  
			$return_html .= '<div class="thslider">';
           	$return_html .= '<div class="thslider-box">';
           	$return_html .= '<div class="thslider-viewport">';
           	$return_html .= '<ul id="th-list" class="'.$add_list_class.'">';

			$i=0;


			foreach ($custom_addresses as $name => $title ) {

				$default_heading = apply_filters('thwma_default_heading',false);
				if($default_heading){
					$heading = !empty($title) ? $title : __('Home','woocommerce-multiple-addresses-pro') ;
				}else{
					$heading = !empty($title) ? $title : __('','woocommerce-multiple-addresses-pro') ;
				}
				

				$address = THWMA_Utils::get_all_addresses($customer_id,$name);
				$address_key = substr($name, strpos($name,"=") + 1);
				$action_row_html = '';
				$action_def_html = '';

				$confirm = "return confirm('Are you sure you want to delete this address?');";

				$action_row_html .= '<div class="acnt-address-footer">';					

						$action_row_html .= '<div class="btn-acnt-edit"> <a href=" '. esc_url( wc_get_endpoint_url( 'edit-address', $name ) ).'" class="" title="Edit"> <span> '.__( 'Edit','woocommerce-multiple-addresses-pro' ).' </span> </a></div>';

						$action_row_html .= '<form action="" method="post">';

						$action_row_html .=' <button type="submit" name="thwma_del_addr"  class="th-del-acnt " title="Delete"  onclick="'.$confirm .'">'.__('Delete','woocommerce-multiple-addresses-pro').'</button>';
						$action_row_html .=	'<input type="hidden" name="thwma_deleteby" value="'.$type.'_'. $address_key.'"/>';

					$action_row_html .= '</form>';
				$action_row_html .= '</div>';				 

					if($type == "billing"){
						$action_def_html.=  ' <form action="" method="post"><button type="submit" name="thwma_default_bil_addr" id="submit-billing" class="account-default "  >'.__('Set as default','woocommerce-multiple-addresses-pro').' </button> 
						<input type="hidden" name="thwma_bil_defaultby" value="'.$type.'_'. $address_key.'"/> </form>';
					}else{ 
						$action_def_html.= '<form action="" method="post"> <button type="submit" name="thwma_default_ship_addr" id="submit-shipping" class="account-default " >'.__('Set as default','woocommerce-multiple-addresses-pro').'</button>
						<input type="hidden" name="thwma_ship_defaultby" value="'.$type.'_'. $address_key.'"/> </form>';
					
					}				

				$add_address_btn = '<div class="add-acnt-adrs">
	               	<a href=" '.esc_url( wc_get_endpoint_url('edit-address',''.$type.'?atype=add-address')).'"
	                	<i class="fa fa-plus"></i> '.__('Add new address','woocommerce-multiple-addresses-pro').' </a>   
	            </div>';

				$add_class  = "thslider-item $type " ;
				$add_class .= $i == 0 ? ' first' : '';

				if(isset($heading) && $heading != '' ){
					$show_heading = '<div class="address-type-wrapper row"> 
						<div title="'.$heading.'" class="address-type">'.$heading.'</div>  
						</div>
						<div class="address-text address-wrapper">'.$address.'</div>' ;
				}else{
					$show_heading = '<div class="address-text address-wrapper wrapper-only">'.$address.'</div>';
				}

							

				$return_html .= '<li class="'.$add_class.'" value="'. $address_key.'" >
					<div class="address-box" data-index="'.$i.'" data-address-id=""> 
						<div class="main-content"> 
							<div class="complete-aaddress">  
								'.$show_heading.'

								
							</div>
						
							<div class="btn-continue address-wrapper"> 								
								'.$action_def_html.'	 								
							</div> 
						</div>
							'.$action_row_html.'
					</div>
				</li>';

				$i++;
			}

			$return_html .= '</ul>';
			$return_html .= '</div>';

			$return_html .= '</div>';
			$return_html .= '<div class="control-buttons">';
			if($address_count && $address_count > 3){
           		$return_html .= '<div class="prev '.$type.'"><i class="fa fa-angle-left fa-3x"></i></div>';
           		 $return_html .= '<div class="next '.$type.'"><i class="fa fa-angle-right fa-3x"></i></div>';
           	}
           	$return_html .= '</div>';
           	if($address_limit > $address_count){

           		$return_html .= $add_address_btn;
           	}
           	$return_html .= '</div>';
		}

		return $return_html;
	}

	public function get_address_to_edit($load_address,$address,$type){
		
		$user_id = get_current_user_id();	
		$custom_address = THWMA_Utils::get_custom_addresses($user_id,$load_address,$type);
		 foreach ($address as $key => $value) {
		 	if(isset($custom_address[$key])){
					
				$address[$key]['value'] = $custom_address[$key];
			}

		}
		return $address;
	}

	public function update_address($user_id,$address_type,$address,$type){
		
		$edited_address = $this->prepare_posted_address($user_id,$address,$address_type);
		THWMA_Utils::update_address_to_user($user_id,$edited_address,$address_type,$type);	
	}



	
	////////////////////////Delete Address////////////////
	public function delete_address(){

		if(isset($_POST['thwma_del_addr'])){
			$user_id = get_current_user_id();
			$buton_id = $_POST['thwma_deleteby'];
			$type = substr($buton_id.'_', 0, strpos($buton_id, '_'));
			$address_key = substr($buton_id, strpos($buton_id,"_") + 1);
			THWMA_Utils::delete($user_id,$type,$address_key);
		}

	}

	/////////Set As Default Address/////////////// 
	public function set_default_billing_address(){
		$customer_id = get_current_user_id();
		if(isset($_POST['thwma_default_bil_addr'])){
			$address_key = $_POST['thwma_bil_defaultby'];
			$address_key= str_replace(' ', '',$address_key);
			$type = substr($address_key.'_', 0, strpos($address_key, '_'));
			$custom_key = substr($address_key, strpos($address_key,"_") + 1);
			$this->change_default_address($customer_id,$type,$custom_key);
		}
	}

	public function set_default_shipping_address(){
		$customer_id=get_current_user_id();
		if(isset($_POST['thwma_default_ship_addr'])){
			$address_key=$_POST['thwma_ship_defaultby'];
			$type = substr($address_key.'_', 0, strpos($address_key, '_'));
			$custom_key = substr($address_key, strpos($address_key,"_") + 1);
			$this->change_default_address($customer_id,$type,$custom_key);
		}
	}

	public function change_default_address($customer_id,$type,$custom_key){
		$default_address = THWMA_Utils::get_custom_addresses($customer_id,$type,$custom_key);
		$current_address = THWMA_Utils::get_default_address($customer_id,$type);
		foreach ($current_address as $key => $value) {
			$new_address = (isset($default_address[$key])) ? $default_address[$key] : '';
			
			update_user_meta($customer_id,$key,$new_address,$current_address[$key]);	
		}
		$custom_addresses = get_user_meta($customer_id,THWMA_Utils::ADDRESS_KEY,true);
		$custom_addresses['default_'.$type] = $custom_key;
		update_user_meta($customer_id,THWMA_Utils::ADDRESS_KEY,$custom_addresses);
		$current_address = THWMA_Utils::get_default_address($customer_id,$type);
	}
	
	/////////////////////////////////////////////////////////////////////////
	/////////////////////Checkout///////////////////////////////

	public function add_hidden_field_to_checkout_fields($fields){
		$user_id = get_current_user_id();
		$default_bil_key = THWMA_Utils::get_custom_addresses($user_id,'default_billing');
		$same_bil_key = THWMA_Utils:: is_same_address_exists($user_id,'billing'); 
		$default_value = $default_bil_key ? $default_bil_key : $same_bil_key;
		$fields['billing']['thwma_hidden_field_billing'] = array(
			'label'    => __( 'hidden value','' ),
			'required' => false,
			'class'    => array( 'form-row' ),
			'clear'    => true,
			'default'	=> $default_value,
			'type'     => 'hidden',
			'priority' => '',
		);

		$default_ship_key = THWMA_Utils::get_custom_addresses($user_id,'default_shipping');
		$same_ship_key = THWMA_Utils:: is_same_address_exists($user_id,'shipping'); 
		$default_value_ship = $default_ship_key ? $default_ship_key : $same_ship_key;

		$fields['billing']['thwma_checkbox_shipping'] = array(
			'label'    => __( 'hidden value','' ),
			'required' => false,
			'class'    => array( 'form-row' ),
			'clear'    => true,
			'default'	=> $default_value_ship,
			'type'     => 'hidden',
			'priority' => '', 
		);		

		$fields['shipping']['thwma_hidden_field_shipping'] = array(
			'label'    => __( 'hidden value','' ),
			'required' => false,
			'class'    => array( 'form-row' ),
			'clear'    => true,
			'default'	=> $default_value_ship,
			'type'     => 'hidden',
			'priority' => '', 
		);
		return $fields;
	}

	public function add_hidden_field($field, $key, $args, $value){
		 return '<input type="hidden" name="'.esc_attr($key).'" id="'.esc_attr($args['id']).'" value="'.esc_attr($args['default']).'" />';
	}

	public function session_update_billing($checkout){
		$customer_id = get_current_user_id();
		
		if(is_user_logged_in()){	
			$default_address = THWMA_Utils::get_default_address($customer_id,'billing');
			$addresfields = array('first_name', 'last_name', 'company','address_1','address_2','city','state','postcode','country','phone','email');
			if($default_address && array_filter($default_address) && (count(array_filter($default_address)) > 2 )){
				foreach ($addresfields as $key ) {
					if(isset($default_address['billing_'.$key])){
						$temp_value = $default_address['billing_'.$key];
						WC()->customer->{"set_billing_"."{$key}"}($temp_value);
			 			WC()->customer->save();
			 		}
				}
			}
			

			//////// Fix for deactivate & activate/////////////////////////////
			$default_set_address = THWMA_Utils::get_custom_addresses($customer_id,'default_billing');

			if($default_set_address){
				$address_key = THWMA_Utils::is_same_address_exists($customer_id,'billing');
				if(!$address_key){
					
					THWMA_Utils::update_address_to_user($customer_id,$default_address,'billing',$default_set_address);
				}
			}
		}
	}

	public function session_update_shipping($checkout){
		$customer_id = get_current_user_id();
		if (is_user_logged_in()){
			
			$default_address = THWMA_Utils::get_default_address($customer_id,'shipping');
				$addresfields = array('first_name', 'last_name', 'company','address_1','address_2','city','state','postcode','country');
			if($default_address && array_filter($default_address) && (count(array_filter($default_address)) > 2 )){
				foreach ($addresfields as $key ) {
					if(isset($default_address['shipping_'.$key])){
						$temp_value=$default_address['shipping_'.$key];
						WC()->customer->{"set_shipping_"."{$key}"}($temp_value);
			 			WC()->customer->save();
			 		}
				}
			}
			/// Fix for deactivate & activate///////////
			$default_set_address = THWMA_Utils::get_custom_addresses($customer_id,'default_shipping');

			if($default_set_address){
				$address_key = THWMA_Utils::is_same_address_exists($customer_id,'shipping');
				if(!$address_key){
					
					THWMA_Utils::update_address_to_user($customer_id,$default_address,'shipping',$default_set_address);
				}
			}
		}

	}

	////////////////Choose Display Style/////////

	public function address_above_billing_form(){

		
		$settings = THWMA_Utils::get_setting_value('settings_billing');
		if (is_user_logged_in()){
			if($settings && !empty($settings)){
				if($settings['enable_billing'] == 'yes'){
					if($settings['billing_display_position'] == 'above'){
						if($settings['billing_display'] == 'popup_display'){
							$this->add_tile_to_checkout_billing_fields();
						}
						elseif($settings['billing_display'] == 'dropdown_display'){
							$this->add_dd_to_checkout_billing();
						}
						else{
							$this->add_accordion_to_checkout_billing();
						}
						
					}
				}
			}
		}
	}

	public function address_below_billing_form($checkout){
	
		$settings = THWMA_Utils::get_setting_value('settings_billing');
		if (is_user_logged_in()){
			
			if($settings && !empty($settings)){
				if($settings['enable_billing'] == 'yes'){
					if($settings['billing_display_position']=='below'){
						if($settings['billing_display']=='popup_display'){
							$this->add_tile_to_checkout_billing_fields();
						}
						elseif($settings['billing_display']=='dropdown_display'){
							$this->add_dd_to_checkout_billing();
						}
						else{
							$this->add_accordion_to_checkout_billing();
						}
						
					}
				}
			}
		
		}
	}


	public function address_above_shipping_form(){
		$settings=THWMA_Utils::get_setting_value('settings_shipping');
		if (is_user_logged_in()){
			if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ){
				if($settings && !empty($settings)){
					if($settings['enable_shipping'] == 'yes'){
						if($settings['shipping_display_position'] == 'above'){
							if($settings['shipping_display'] =='popup_display'){
								$this->add_tile_to_checkout_shipping_fields();
							}
							elseif($settings['shipping_display']=='dropdown_display'){
								$this->add_dd_to_checkout_shipping();
							}
							else{
								$this->add_accordion_to_checkout_shipping();
							}
							
						}
					}
				}
			}
		}
	}

	public function address_below_shipping_form(){
		$settings=THWMA_Utils::get_setting_value('settings_shipping');
		if (is_user_logged_in()){
			if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ){
				if($settings && !empty($settings)){
					if($settings['enable_shipping']=='yes'){
						if($settings['shipping_display_position']=='below'){
							if($settings['shipping_display']=='popup_display'){
								$this->add_tile_to_checkout_shipping_fields();
							}
							elseif($settings['shipping_display']=='dropdown_display'){
								$this->add_dd_to_checkout_shipping();
							}
							else{
								$this->add_accordion_to_checkout_shipping();
							}
							
						}
					}
				}
			}
		}
	}

	///////////////popup display/////////////////////

	public function add_tile_to_checkout_billing_fields(){
		$customer_id = get_current_user_id();
		if (is_user_logged_in()){
			$settings = THWMA_Utils::get_setting_value('settings_billing'); 
			//$button_color = THWMA_Utils::get_setting_value('button_color'); 
			?>

			<div id="billing_tiles"> <?php
				if($settings['billing_display_title'] == 'button'){?>
				<div class = "add-address btn-checkout">	
					<button type="button" id = "thwma-popup-show-billing" class="btn-add-adrs-checkout"  onclick="thwma_show_billing_popup(event)">
					<?php _e( $settings['billing_display_text'], 'woocommerce-multiple-addresses-pro' ); ?>
					</button></div><?php
				}else{?>
					<a href='#' id="thma-popup-show-billing_link" class='th-popup-billing th-pop-link' onclick="thwma_show_billing_popup(event)">
					<?php _e( $settings['billing_display_text'], 'woocommerce-multiple-addresses-pro' ); ?>
					</a>
					<?php
				}
				$all_address=''; 

				$html_address = $this->get_tile_field($customer_id,'billing');
					$all_address.= '<div id="thwma-billing-tile-field" >
						<div>'. $html_address.'</div>
					</div>' ;?>
				<div class="u-columns woocommerce-Addresses col2-set addresses  ">
					<?php echo $all_address; ?>
				</div>	
			</div><?php			
		}
	}

	


	public function add_tile_to_checkout_shipping_fields(){
		
		if (is_user_logged_in()){
			if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ){
				$customer_id=get_current_user_id();
				$settings=THWMA_Utils::get_setting_value('settings_shipping'); ?>	
				<div id="shipping_tiles"> <?php
					if($settings['shipping_display_title']=='button'){?>
						<div class = "add-address btn-checkout">
						<button type="button" id="thwma-popup-show-shipping" class="btn-add-adrs-checkout"  onclick="thwma_show_shipping_popup(event)">
						<?php _e( $settings['shipping_display_text'], 'woocommerce-multiple-addresses-pro' ); ?>
						</button> </div><?php
					} 
					else{?>
						<a href='#' id="thwma-popup-show-shipping_link" class='th-popup-shipping th-pop-link' onclick="thwma_show_shipping_popup(event)">
						<?php _e( $settings['shipping_display_text'], 'woocommerce-multiple-addresses-pro' ); ?>
						</a><?php

					}
					$all_address=''; 
					$html_address = $this->get_tile_field($customer_id,'shipping');
					$all_address.= '<div id="thwma-shipping-tile-field" >'. $html_address.'</div>' ?>
					<div class="u-columns woocommerce-Addresses col2-set addresses billing-addresses ">
						<?php echo $all_address; ?>
					</div>
				</div><?php	
				
			}
		}
	}

	public function get_tile_field($customer_id,$type){
		$oldcols = 1;
		$cols    = 1;
		// $custom_address = THWMA_Utils::get_custom_addresses($customer_id,$type) ? THWMA_Utils::get_custom_addresses($customer_id,$type) : array();
		$custom_address = THWMA_Utils::get_custom_addresses($customer_id,$type);

		$default_set_address = THWMA_Utils::get_custom_addresses($customer_id,'default_'.$type);
		$same_address = THWMA_Utils::is_same_address_exists($customer_id,$type);

		$default_address = $default_set_address ? $default_set_address : $same_address;
		
		$address_count = is_array($custom_address) ? count($custom_address) : 0 ;
		$address_limit=THWMA_Utils::get_setting_value('settings_'.$type, $type.'_address_limit');
		$return_html = '';
		$add_class='';
		$address_type = "'$type'";		

			$add_list_class  = ($type == 'billing') ? " thslider-list bill " : " thslider-list ship"; 
			
			$add_address_btn = '<div class="add-address">
                <button class="btn-add-address" onclick="thwma_add_new_address(event,this,'.$address_type.')">
                	<i class="fa fa-plus"></i> '.__('Add new address','woocommerce-multiple-addresses-pro').'
                </button>
            </div>';	
        if(is_array($custom_address)){
        	$all_addresses = $custom_address;
        }else{
			$all_addresses = array();
			$def_address = THWMA_Utils::get_default_address($customer_id,$type);
			
			if(array_filter($def_address) && (count(array_filter($def_address)) > 2 )){
				$all_addresses ['selected_address'] = $def_address;
			}
		}
		
			$return_html .= '<div class="thslider">';
		if($all_addresses && is_array($all_addresses)){
           	$return_html .= '<div class="thslider-box">';
           	$return_html .= '<div class="thslider-viewport">';
           	$return_html .= '<ul class=" '.$add_list_class.'">';

			$i=0;
			foreach ($all_addresses as $address_key => $value ) {
				$new_address = $all_addresses[$address_key];
				$new_address_format = THWMA_Utils::get_formated_address($type,$new_address);
				$options_arr = WC()->countries->get_formatted_address($new_address_format);
				$address_key_param = "'".$address_key."'";
				
				$heading = !empty($new_address[$type.'_heading']) ? $new_address[$type.'_heading'] : __('','woocommerce-multiple-addresses-pro') ;
				$action_row_html = '';

				if($default_address){
					$is_default = ($default_address == $address_key) ? true : false;
				}else{
					$is_default = false;
				}

				$address_type_css = '';

				if(!$is_default){							

					$action_row_html .= '<div class="address-footer"> 
						<div class="btn-delete" data-index="0" data-address-id="" onclick="thwma_delete_selected_address(this,'.$address_type.','.$address_key_param.')" title="'.__('Delete','woocommerce-multiple-addresses-pro').'"> 
							<span>'.__('Delete','woocommerce-multiple-addresses-pro').'</span> 
						</div> 
						<div class="btn-default" data-index="0" data-address-id="" onclick="thwma_set_default_address(this,'.$address_type.','.$address_key_param.')" title="'.__('Default','woocommerce-multiple-addresses-pro').'"> 
							<span>'.__('Default','woocommerce-multiple-addresses-pro').'</span>
						</div>   
					</div>';
				}else{
					$address_type_css = 'default';
					$heading = sprintf( __( 'Default', 'woocommerce-multiple-addresses-pro' ));

					$action_row_html .= '<div class="address-footer '.$address_type_css.'"> 
						<div class="th-btn btn-delete"><span>'.__('Delete','woocommerce-multiple-addresses-pro').'</span></div> 
						<div class="th-btn btn-default"><span>'.__('Default','woocommerce-multiple-addresses-pro').'</span></div>   
					</div>';
				}

				if(isset($heading) && $heading != ''){
					$heading_css = '<div class="address-type-wrapper row"> 
						<div title="'.$heading.'" class="address-type '.$address_type_css.'">'.$heading.'</div>  
						</div>       
						<div class="address-text address-wrapper">'.$options_arr.'</div>'; 

				}else{
					$heading_css = '<div class="address-text address-wrapper wrapper-only">'.$options_arr.'</div>'; 
				}			

				//$add_class  = "u-columns address-single  $type " ;
				$add_class  = "thslider-item $type " ;
				$add_class .= $i == 0 ? ' first' : '';				

				$return_html .= '<li class="'.$add_class.'" value="'. $address_key.'" >
					<div class="address-box" data-index="'.$i.'" data-address-id=""> 
						<div class="main-content"> 
							<div class="complete-aaddress">  
								'.$heading_css.'

							</div> 
							<div class="btn-continue address-wrapper"> 
								<div class="th-btn '.$address_key.'" onclick="thwma_populate_selected_address(event,this,'.$address_type.', '.$address_key_param.')"> 
									<span>'.__('Choose This Address','woocommerce-multiple-addresses-pro').'</span> 
								</div> 
							</div> 
						</div>'.$action_row_html.'</div></li>';

				$i++;
			}

		

			$return_html .= '</ul>';
			$return_html .= '</div>';

			$return_html .= '</div>';
			$return_html .= '<div class="control-buttons">';
			if($address_count && $address_count > 3){
            	$return_html .= '<div class="prev '.$type.'"><i class="fa fa-angle-left fa-3x"></i></div>';
            	$return_html .= '<div class="next '.$type.'"><i class="fa fa-angle-right fa-3x"></i></div>';
            }

           	$return_html .= '</div>';
           		if(((int)($address_limit)) > $address_count){
           		$return_html .= $add_address_btn;
           	}
        }else{
        	$return_html .= '<div class="th-no-address-msg"  >	<span>'.__('No saved addresses found','woocommerce-multiple-addresses-pro').'</span>  </div>';
        }
       
           
        $return_html .= '</div>';
       
        
		return $return_html;
	}	

	///////////////Section start/////////
	//////////////////////////////////////

	public function add_additional_fields($section){
		$section_name = $section->name;
		//$section_settings = get_option('thwma_sectionz',true);
		//$mapped_section = $section_settings[$section_name]['maped_section'];
		$mapped_section = THWMA_Utils::get_maped_sections_settings_value($section_name,'maped_section');
	
		if(!($mapped_section == 'shipping' &&  (wc_ship_to_billing_address_only()  || !wc_shipping_enabled()))){
		
			$customer_id = get_current_user_id();
			$settings = THWMA_Utils::get_setting_value('settings_'.$mapped_section);
			$display_type = $settings[$mapped_section.'_display'];
			$display_title = $settings[$mapped_section.'_display_title'];
			if($display_type == 'popup_display'){
				if($display_title == 'link'){?>
					<a href='#' class='thma-popup-show-custom_link th-pop-link' onclick="thwma_show_custom_popup(event,'<?php echo $section_name; ?>','<?php echo $mapped_section; ?>')" > <?php _e('Choose Different Address','woocommerce-multiple-addresses-pro') ?> </a><?php
				}else{?>
					<div class = "add-address btn-checkout">
					<button type="button"  class="btn-add-adrs-checkout" onclick = "thwma_show_custom_popup(event,'<?php echo $section_name; ?>','<?php echo $mapped_section; ?>')"><?php _e('Choose Different Address','woocommerce-multiple-addresses-pro') ?></button></div><?php
					
				}
				$all_address = '';
				$html_address = $this->get_custom_tile_field($customer_id,$section,$mapped_section);
				$all_address.= '<div id="thwma-custom-tile-field_'.$section_name.'" style="display:none;">'. $html_address.'</div>' ?>
				<div class="u-columns woocommerce-Addresses  addresses custom_section_address ">
					<?php echo $all_address; ?>
				</div><?php

			}else{

				$section_name = $section->name;

				$customer_id = get_current_user_id();
				$custom_address = THWMA_Utils::get_custom_addresses($customer_id,$mapped_section);
				$options = array();
				$def_section_address =  $this->get_default_section_address($customer_id,$section_name);
				
				if($custom_address){
					
					foreach ($custom_address as $key => $address_values) {
						$heading = ($address_values[$mapped_section.'_heading'] != '') ? $address_values[$mapped_section.'_heading'] : __('','woocommerce-multiple-addresses-pro');
						$adrsvalues_to_dd = array();
						foreach ($address_values as $adrs_key => $adrs_value) {
							if($adrs_key == $mapped_section.'_address_1' || $adrs_key == $mapped_section.'_address_2' || $adrs_key ==$mapped_section.'_city' || $adrs_key == $mapped_section.'_state' || $adrs_key == $mapped_section.'_postcode'){
								if($adrs_value){
									$adrsvalues_to_dd[] = $adrs_value;
								}
							}
						}
						$adrs_string = implode(',',$adrsvalues_to_dd);
						if(isset($heading) && $heading  != ''){
							$options[$key] = $heading .' - '.$adrs_string;
						}else{
							$options[$key] = $adrs_string;
						}
						
					}
				}
					if(array_filter($def_section_address)){
						$options['section_address'] = __('Default Address','woocommerce-multiple-addresses-pro');
					}

					$alt_field = array(
						'required' => false,
						'class'    => array( 'form-row form-row-wide enhanced_select select2-selection th-select' ),
						'clear'    => true,
						'type'     => 'select',
						//'required' => 'true',
						//'label'    => 'Choose Address',
						//'placeholder' =>__('Choose an Address..',''),
						
						'options'  => array( '' => __( 'Choose saved address&hellip;','woocommerce-multiple-addresses-pro' ) )+$options,
					);

					$this->thwma_woocommerce_form_field('thwma_'.$section_name, $alt_field);
			}
		}

	}

	public function get_custom_tile_field($customer_id,$section,$maped_section){
		

		$custom_address = THWMA_Utils::get_custom_addresses($customer_id,$maped_section);
		$type = $maped_section;
		$section_name = $section->name;
		$section_address = array('section_address' => $this->get_default_section_address($customer_id,$section_name));
		//
		
		//$new_address_format = self::get_formated_address('billing',$section_address);


		$section_key_param = "'".$section_name."'";
		
		$return_html = '';
		$add_class='';
		$address_type ="'". $maped_section."'";		

		$add_list_class  =  "thslider-list  "  .$section_name ; 
			
		$all_addresses = 	$custom_address;	

        if(is_array($custom_address)){
        	if($section_address['section_address'] != null && array_filter($section_address['section_address'])){

        		$all_addresses = array_merge($section_address,$custom_address);
        	}

        }else{
			$all_addresses = array();
			
			if(array_filter($section_address['section_address'])){

				$all_addresses  = $section_address;
			}
		}

	
		
		$return_html .= '<div class="thslider" id="thslider-'.$section_name.'">';
		if($all_addresses && is_array($all_addresses)){
           	$return_html .= '<div class="thslider-box">';
           	$return_html .= '<div  class = "thslider-viewport" >';
           	$return_html .= '<ul class=" '.$add_list_class.'">';

			$i=0;

			foreach ($all_addresses as $address_key => $value ) {
				$new_address = $all_addresses[$address_key];
				$new_address_format = THWMA_Utils::get_formated_address($maped_section,$new_address);
				$options_arr = WC()->countries->get_formatted_address($new_address_format);
				$address_key_param = "'".$address_key."'";
				
				$heading = !empty($new_address[$maped_section.'_heading']) ? $new_address[$maped_section.'_heading'] : __('',self::THWMA_TEXT_DOMAIN) ;
				$action_row_html = '';
				$address_type_css = '';
				
				if($address_key == 'section_address'){
					$address_type_css = 'default';
					$heading = sprintf( __( 'Default ', 'woocommerce-multiple-addresses-pro' ));
				}
				
				
				$add_class  = "thslider-item  section_name " ;
				$add_class .= $i == 0 ? ' first' : '';	

				if(isset($heading) && $heading){
					$heading_css =	'<div class="address-type-wrapper row"> 
								<div title="'.$heading.'" class="address-type  '.$address_type_css.'">'.$heading.'</div>  
								</div>       
								<div class="address-text address-wrapper">'.$options_arr.'</div>';
				}else{
					$heading_css =	'<div class="address-text address-wrapper wrapper-only">'.$options_arr.'</div>';
				}
						

				$return_html .= '<li class="'.$add_class.'" value="'. $address_key.'" >
					<div class="address-box" data-index="'.$i.'" data-address-id=""> 
						<div class="main-content"> 
							<div class="complete-aaddress">  
								'.$heading_css.'

							</div> 
							<div class="btn-continue address-wrapper"> 
								<div class="th-btn '.$address_key.'" onclick="thwma_populate_selected_section_address(event,this,'.$address_key_param.','.$section_key_param.','.$address_type.')"> 
									<span>'.__('Choose This Address','woocommerce-multiple-addresses-pro').'</span> 
								</div> 
							</div> 
						</div>'.$action_row_html.'
					</div>
				</li>';

				$i++;
			}

		

			$return_html .= '</ul>';
			$return_html .= '</div>';
			$return_html .= '</div>';
			$return_html .= '<div class="control-buttons">';
			
            	$return_html .= '<div class="prev  '.$section_name.'"><i class="fa fa-angle-left fa-3x"></i></div>';
            	$return_html .= '<div class="next  '.$section_name.'"><i class="fa fa-angle-right fa-3x"></i></div>';
            
            $return_html .= '</div>';
          
        }else{
        	$return_html .= '<div class="th-no-address-msg"  >	<span>'. __('No saved addresses found','woocommerce-multiple-addresses-pro').'</span>  </div>';
        }
       
           
        $return_html .= '</div>';
       
        
		return $return_html;
	}


	/////////////end/////////////////////

	public function add_dd_to_checkout_billing(){
		$customer_id = get_current_user_id();
		$custom_addresses = THWMA_Utils::get_custom_addresses($customer_id,'billing');
		$default_bil_address = THWMA_Utils::get_custom_addresses($customer_id,'default_billing');
		$same_address = THWMA_Utils::is_same_address_exists($customer_id,'billing');
		$default_address = $default_bil_address ? $default_bil_address : $same_address ;
		$options = array();

		if(is_array($custom_addresses)){
        	$custom_address = $custom_addresses;
        }else{
			$custom_address = array();
			$def_address = THWMA_Utils::get_default_address($customer_id,'billing');
			
			if(array_filter($def_address) && (count(array_filter($def_address)) > 2 )){
				$custom_address ['selected_address'] = $def_address;
			}
		}
		
		
		if($custom_address){
		
			$billing_heading = (isset( $custom_address[$default_address]['billing_heading']) && $custom_address[$default_address]['billing_heading'] !='') ? $custom_address[$default_address]['billing_heading'] : __('','');
			if($default_address){
				if(isset($options[$default_address])){
					$options[$default_address] = $billing_heading .'&nbsp - &nbsp'.$custom_address[$default_address]['billing_address_1'];
				}
			}else{
				$default_address = 'selected_address';
				$options[$default_address]  = __('Billing Address','woocommerce-multiple-addresses-pro');
			}
			
		
		
			foreach ($custom_address as $key => $address_values) {
				$heading = (isset($address_values['billing_heading']) && $address_values['billing_heading'] != '') ? $address_values['billing_heading'] : __('','');
				$adrsvalues_to_dd = array();
				foreach ($address_values as $adrs_key => $adrs_value) {
					if($adrs_key == 'billing_address_1' || $adrs_key =='billing_address_2' || $adrs_key =='billing_city' || $adrs_key =='billing_state' || $adrs_key =='billing_postcode'){
						if($adrs_value){
							$adrsvalues_to_dd[] = $adrs_value;
						}
					}
				}
				$adrs_string = implode(',',$adrsvalues_to_dd);
				if(isset($heading) && $heading!= ''){
					$options[$key] = $heading .' - '.$adrs_string;
				}else{
					$options[$key] = $adrs_string;
				}
				
			}
			$address_count = count($custom_address);
			$address_limit = THWMA_Utils::get_setting_value('settings_billing','billing_address_limit');
			if(((int)($address_limit)) > $address_count){
				$options['add_address'] = __('Add New Address','woocommerce-multiple-addresses-pro');
			}

		}else{
			$default_address = 'selected_address';
			$options[$default_address] = __('Billing Address','woocommerce-multiple-addresses-pro');
		}
		
		
		$alt_field = array(
			'required' => false,
			'class'    => array( 'form-row form-row-wide enhanced_select select2-selection th-select' ),
			'clear'    => true,
			'type'     => 'select',
			//'required' => 'true',
			'label'    => THWMA_Utils::get_setting_value('settings_billing','billing_display_text'),
			//'placeholder' =>__('Choose an Address..',''),
			'options'  => $options
		);

		//woocommerce_form_field(self::DEFAULT_BILLING_ADDRESS_KEY, $alt_field,$options[$default_address]);

		$this->thwma_woocommerce_form_field(self::DEFAULT_BILLING_ADDRESS_KEY, $alt_field,$default_address);	
	}

	public function add_dd_to_checkout_shipping(){

		$customer_id = get_current_user_id();
		$custom_addresses = THWMA_Utils::get_custom_addresses($customer_id,'shipping');
		$default_ship_address = THWMA_Utils::get_custom_addresses($customer_id,'default_shipping');
		$same_address = THWMA_Utils::is_same_address_exists($customer_id,'shipping');
		$default_address = $default_ship_address ? $default_ship_address : $same_address;
		$options=array();

		if(is_array($custom_addresses)){
        	$custom_address = $custom_addresses;
        }else{
			$custom_address = array();
			$def_address = THWMA_Utils::get_default_address($customer_id,'shipping');
			
			if(array_filter($def_address) && (count(array_filter($def_address)) > 2 )){
				$custom_address ['selected_address'] = $def_address;
			}
		}

		if($custom_address){	
			$shipping_heading = (isset( $custom_address[$default_address]['shipping_heading']) && $custom_address[$default_address]['shipping_heading'] !='') ? $custom_address[$default_address]['shipping_heading'] : __('','');
			if($default_address){
				if(isset($options[$default_address])){
					$options[$default_address] = $shipping_heading .' - '. $custom_address[$default_address]['shipping_address_1'];
				}
			}else{
				$default_address = 'selected_address';
				$options[$default_address] = __('Shipping Address','woocommerce-multiple-addresses-pro');
			}

			foreach ($custom_address as $key => $address_values) {

				$heading = (isset($address_values['shipping_heading']) && $address_values['shipping_heading'] != '') ? $address_values['shipping_heading'] : __('','woocommerce-multiple-addresses-pro');

				$adrsvalues_to_dd = array();
				foreach ($address_values as $adrs_key => $adrs_value) {
					if($adrs_key == 'shipping_address_1' || $adrs_key =='shipping_address_2' || $adrs_key =='shipping_city' || $adrs_key =='shipping_state' || $adrs_key =='shipping_postcode'){
						if($adrs_value){
							$adrsvalues_to_dd[] = $adrs_value;
						}
					}
				}
				$adrs_string = implode(',',$adrsvalues_to_dd);
				if(isset($heading) && $heading != ''){
					$options[$key] = $heading .' - '.$adrs_string;
				}else{
					$options[$key] = $adrs_string;
				}
				
			}
			$address_count = count($custom_address);
			$address_limit = THWMA_Utils::get_setting_value('settings_shipping','shipping_address_limit');
			if(((int)($address_limit)) > $address_count){
				$options['add_address'] = __('Add New Address','woocommerce-multiple-addresses-pro');
			}
		}else{
			$default_address = 'selected_address';
			$options[$default_address] = __('Shipping Address','woocommerce-multiple-addresses-pro');
		}
		
		
		$alt_field = array(
			'required' => false,
			'class'    => array( 'form-row form-row-wide enhanced_select','select2-selection' ),
			'clear'    => true,
			'type'     => 'select',
			'label'    => THWMA_Utils::get_setting_value('settings_shipping','shipping_display_text'),
			//'placeholder' =>__('Choose an Address..',''),
			'options'  => $options
		);
		//woocommerce_form_field(self::DEFAULT_SHIPPING_ADDRESS_KEY, $alt_field,$options[$default_address]);
		$this->thwma_woocommerce_form_field(self::DEFAULT_SHIPPING_ADDRESS_KEY, $alt_field,$default_address);
	}


	public function thwma_woocommerce_form_field($key, $args, $value = null ){
			
		$defaults = array(
			'type'              => '',
			'label'             => '',
			'description'       => '',
			'placeholder'       => '',
			'maxlength'         => false,
			'required'          => false,
			'autocomplete'      => false,
			'id'                => $key,
			'class'             => array(),
			'label_class'       => array(),
			'input_class'       => array(),
			'return'            => false,
			'options'           => array(),
			'custom_attributes' => array(),
			'validate'          => array(),
			'default'           => '',
			'autofocus'         => '',
			'priority'          => '',
		);

		$args = wp_parse_args( $args, $defaults );
		$field           = '';
		$label_id        = $args['id'];
		$sort            = $args['priority'] ? $args['priority'] : '';
		$field_container = '<p class="form-row %1$s" id="%2$s" data-priority="' . esc_attr( $sort ) . '">%3$s</p>';

		
		$field   = '';
		$options = '';

		$custom_attributes= array();

		if ( ! empty( $args['options'] ) ) {
			foreach ( $args['options'] as $option_key => $option_text ) {
				// if ( '' === $option_key ) {
							// If we have a blank option, select2 needs a placeholder.
					if ( empty( $args['placeholder'] ) ) {
						$args['placeholder'] = $option_text ? $option_text : __( 'Choose an option','woocommerce-multiple-addresses-pro');
					}

					$custom_attributes[] = 'data-allow_clear="true"';
				//}
				$options .= '<option value="' . esc_attr( $option_key ) . '" ' . selected( $value, $option_key, false ) . '>' . esc_attr( $option_text ) . '</option>';
			}

			$field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">' . $options . '</select>';
		}

		if ( ! empty( $field ) ) {
			$field_html = '';

			if ( $args['label']) {
				$field_html .= '<label for="' . esc_attr( $label_id ) . '" class="' . esc_attr( implode( ' ', $args['label_class'] ) ) . '">' . $args['label']  .'</label>';
			}

			$field_html .= '<span class="woocommerce-input-wrapper">' . $field;

			if ( $args['description'] ) {
				$field_html .= '<span class="description" id="' . esc_attr( $args['id'] ) . '-description" aria-hidden="true">' . wp_kses_post( $args['description'] ) . '</span>';
			}

			$field_html .= '</span>';

			$container_class = esc_attr( implode( ' ', $args['class'] ) );
			$container_id    = esc_attr( $args['id'] ) . '_field';
			$field           = sprintf( $field_container, $container_class, $container_id, $field_html );
		}

		echo $field; // WPCS: XSS ok.
		
	}

	//////////Accordion Display////////////////
	
				
	public function add_accordion_to_checkout_billing(){

		if (is_user_logged_in()){
			$options_arr = array();
			$customer_id = get_current_user_id();
			$custom_address = THWMA_Utils::get_custom_addresses($customer_id,'billing');
			$address_count = count($custom_address);
			$address_limit = THWMA_Utils::get_setting_value('settings_billing','billing_address_limit');
			$settings = THWMA_Utils::get_setting_value('settings_billing');
			if($settings['billing_display_title']=='button' ){?>
			
				<button class = 'thwma_billing_toggle_accordion'><?php echo $settings['billing_display_text']; ?>
				</button> <?php
			}else{ ?>
				<a href="#" class = 'thwma_billing_toggle_accordion'><?php echo $settings['billing_display_text']; ?></a> <?php
			} ?>
			<div id='thwma_billing_toggle_show' >
			<div id="thwma_billing_accordion" class="th_accordion"><?php
				if($custom_address)	{	
					foreach ($custom_address as $address_key => $value ) {
						$new_address=$custom_address[$address_key];
						$new_address_format=THWMA_Utils::get_formated_address('billing',$new_address);
						$options_arr= WC()->countries->get_formatted_address($new_address_format);
						
						if(empty($new_address['billing_heading'])){
							$heading=$new_address['billing_address_1'];
						}
						else{
							$heading=$new_address['billing_heading'];
						}
						?>
						
							<h4><?php echo $heading ?></h4>
							
							<div class='acordion_para'>
								<?php echo $options_arr; ?>
							
								
									<button class="thwma-acord-billing"  value=<?php echo $address_key ?> onclick="thwma_populate_selected_address(event,this,'billing','<?php echo $address_key ?>' )">Billing with this Address</button>	
							</div>
						<?php	
					}
				}?>		 	
				</div> <?php
				if(((int)($address_limit)) > $address_count){ ?>
					<button class="thwma-add-new-address" id="thwma-billing_accordion_new_address" onclick="thwma_add_new_address(event,this,'billing')">Billing with A New Address</button> <?php
				} ?>
			</div><?php
		}			
	}
	
	public function add_accordion_to_checkout_shipping(){
		if (is_user_logged_in()){
			$options_arr = array();
			$customer_id=get_current_user_id();
			$custom_address=THWMA_Utils::get_custom_addresses($customer_id,'shipping');
			$address_count=count($custom_address);
			$address_limit=THWMA_Utils::get_setting_value('settings_shipping','shipping_address_limit');
			$settings = THWMA_Utils::get_setting_value('settings_shipping');
			if($settings['shipping_display_title'] == 'button'){?>
		
				<button class = 'thwma_shipping_toggle_accordion'><?php echo $settings['shipping_display_text']; ?></button> <?php
			}else{ ?>
				<a href="#" class = 'thwma_shipping_toggle_accordion'><?php echo $settings['shipping_display_text']; ?></a> <?php
			} ?>
			<div id='thwma_shipping_toggle_show' >
			<div id="thwma_shipping_accordion" class="th_accordion" ><?php
			if($custom_address){	
				foreach ($custom_address as $address_key => $value ) {
					$new_address = $custom_address[$address_key];
					$new_address_format = THWMA_Utils::get_formated_address('shipping',$new_address);
					$options_arr = WC()->countries->get_formatted_address($new_address_format);
					
					$heading = !empty($new_address['shipping_heading']) ? $new_address['shipping_heading'] : $new_address['shipping_address_1'];
					
					?>
					
						<h4><?php echo $heading ?></h4>
						
						<div class='acordion_para'>
							<?php echo $options_arr; ?>
						
							
								<button class="button-shipping-address"  value=<?php echo $address_key ?> onclick="thwma_populate_selected_address(event,this,'shipping','<?php echo $address_key ?>' )"><?php _e('Shipping with this Address','woocommerce-multiple-addresses-pro'); ?></button>	
						</div>
					<?php	
				}
			}?>		 	
			</div> <?php
			if(((int)($address_limit)) > $address_count){ ?>
				<button class="thwma-add-new-address" id="thwma-shipping_accordion_new_address" onclick="thwma_add_new_address(event,this,'shipping')"><?php _e('Add New Address','woocommerce-multiple-addresses-pro');?></button><?php
			} ?>
			</div><?php
		}			
	}

 /////////////ajax response////////////////////
	public function get_addresses_by_id(){
		$address_key = $_POST['selected_address_id'];
		$type = $_POST['selected_type'];

		
		$section_name = isset($_POST['section_name']) ? $_POST['section_name'] : '';
		
		
		$customer_id = get_current_user_id();

		if(!empty($section_name) && $address_key == 'section_address'){
			

			$custom_address = $this->get_default_section_address($customer_id,$section_name);

		}else{
			if($address_key == 'selected_address'){
				$custom_address = THWMA_Utils::get_default_address($customer_id,$type);
			}else{
				$custom_address = THWMA_Utils::get_custom_addresses($customer_id,$type,$address_key);
			}
			
		}

		wp_send_json($custom_address);
		
	}

	public function delete_address_from_checkout(){
		$address_key = $_POST['selected_address_id'];
		$type = $_POST['selected_type'];
		
		$customer_id = get_current_user_id();
		THWMA_Utils::delete($customer_id,$type,$address_key);
		$output_shipping = $this->get_tile_field($customer_id,'shipping');
		$output_billing = $this->get_tile_field($customer_id,'billing');
		$response=array(
			'result_billing' => $output_billing,
			'result_shipping' => $output_shipping,	
		);
		wp_send_json($response );
	}

	public function default_address_from_checkout(){
		$address_key = $_POST['selected_address_id'];
		$type = $_POST['selected_type'];
		$user_id = get_current_user_id();
		$this->change_default_address($user_id,$type,$address_key);
		$output_shipping = $this->get_tile_field($user_id,'shipping');
		$output_billing = $this->get_tile_field($user_id,'billing');
		$response = array(
			'result_billing' => $output_billing,
			'result_shipping' => $output_shipping,	
		);
		wp_send_json($response );
	}


	public function get_default_section_address($user_id,$section_name){

		//$section_settings = get_option('thwma_sectionz');
		$section_fields  = THWMA_Utils::get_maped_sections_settings_value($section_name,'map_fields');	
		//$section_fields = $section_settings[$section_name];
		$section_address = array();
		if(is_array($section_fields)){
			foreach ($section_fields as $default_field => $custom_field) {
				$section_address[$default_field] = get_user_meta($user_id,$custom_field,true);
			}
			return $section_address ;
		}
	}

	/////////Update from checkout//////////

	public function update_custom_billing_address_from_checkout($order_id, $posted_data, $order){
		if (is_user_logged_in()){

			$address_key = $posted_data['thwma_hidden_field_billing'];
			$user_id = get_current_user_id();
			$custom_key = THWMA_Utils::get_custom_addresses($user_id,'default_billing');
			$same_address_key = THWMA_Utils::is_same_address_exists($user_id,'billing');
			$default_key = ($custom_key) ? $custom_key : $same_address_key ;

			$this->update_address_from_checkout('billing',$address_key,$posted_data,$default_key);
			
			if($custom_key){
				$modify = apply_filters('thwma_modify_billing_update_address',true);
				if($modify){

					$this->change_default_address($user_id,'billing',$default_key);

				}else{
					if ($address_key == 'add_address'){
						$new_key_id = (THWMA_Utils::get_new_custom_id($user_id,'billing')) - 1;
						$new_key = 'address_'.$new_key_id;
						$this->change_default_address($user_id,'billing',$new_key);
					}elseif(!empty($address_key)){
						$this->change_default_address($user_id,'billing',$address_key);
					}			
				}
			}		
		}
	}

	public function update_custom_shipping_address_from_checkout($order_id, $posted_data, $order){

		if (is_user_logged_in()){
			$user_id = get_current_user_id();
			$custom_key = THWMA_Utils::get_custom_addresses($user_id,'default_shipping');
			$same_address_key = THWMA_Utils::is_same_address_exists($user_id,'shipping');
			$default_key = ($custom_key) ? $custom_key : $same_address_key ;
			
			if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ){
				$address_key = $posted_data['thwma_hidden_field_shipping'];
				$ship_select = $posted_data['thwma_checkbox_shipping'];
				
				if($ship_select == 'ship_select'){

					$this->update_address_from_checkout('shipping',$address_key,$posted_data,$default_key);
				}else{
					if(!$custom_key){

						$this->update_address_from_checkout('shipping',$ship_select,$posted_data,$default_key);

						//$custom_address = self::get_custom_addresses($user_id,'shipping',$ship_select);
						//$this->update_address_to_user($user_id,$custom_address,'shipping',$ship_select);
					}
				}
			}
			if($custom_key){
				$modify = apply_filters('thwma_modify_shipping_update_address', true);
				if($modify){
					$this->change_default_address($user_id,'shipping',$default_key);
				}else{
					if ($address_key == 'add_address'){
						$new_key_id = (THWMA_Utils::get_new_custom_id($user_id,'shipping')) - 1;
						$new_key = 'address_'.$new_key_id;
						$this->change_default_address($user_id,'shipping',$new_key);
					}elseif(!empty($address_key)){
						$this->change_default_address($user_id,'shipping',$address_key);
					}
				}
			}	


		}
	}

	public function update_address_from_checkout($type,$address_key,$posted_data,$default_key){

		$user_id = get_current_user_id();
		
		$added_address = array();
		$added_address = $this->prepare_order_placed_address($user_id,$posted_data,$type);
		$heading = THWMA_Utils::get_custom_addresses($user_id,$type,$address_key,$type.'_heading');
		$added_address[$type.'_heading'] = $heading ? $heading : __('','woocommerce-multiple-addresses-pro');
		if($address_key == 'add_address'){
			
			self::save_address_to_user_from_checkout($added_address,$type);


		}
		elseif(($default_key) && (empty($address_key)|| ($address_key == $default_key))){
			
			THWMA_Utils::update_address_to_user($user_id,$added_address,$type,$default_key);

		}elseif( $address_key && ($address_key != 'selected_address')){

			$this->update_address_to_user_from_checkout($user_id,$added_address,$type,$address_key);
		}
	}


	private function save_address_to_user_from_checkout($address,$type){
		$user_id = get_current_user_id();
		$custom_addresses = get_user_meta($user_id,THWMA_Utils::ADDRESS_KEY,true);
		$custom_addresses = is_array($custom_addresses) ? $custom_addresses : array();
		$saved_address = THWMA_Utils::get_custom_addresses($user_id,$type);

		if(!is_array($saved_address)){
			$custom_address = array();
			$default_address = THWMA_Utils::get_default_address($user_id,$type);
			if(!array_key_exists($type.'_heading',$default_address)){
				$default_address[$type.'_heading'] = __('','woocommerce-multiple-addresses-pro');
			}
			$custom_address['address_0'] = $default_address;
			//$custom_address['address_1'] = $address;
			$custom_key = THWMA_Utils::get_custom_addresses($user_id,'default_'.$type);

			// if(!$custom_key){
			// 	$custom_addresses['default_'.$type] = 'address_1';
			// }
			
			$custom_addresses[$type] = $custom_address;
			
		}else{

			if(is_array($saved_address)) {
				if(isset($custom_addresses[$type])){
					$exist_custom = $custom_addresses[$type];
					$new_key_id =THWMA_Utils::get_new_custom_id($user_id,$type);
					$new_key = 'address_'.$new_key_id;
					$custom_address[$new_key] = $address; 
					$custom_key = THWMA_Utils::get_custom_addresses($user_id,'default_'.$type);
					if(!$custom_key){
						$custom_addresses['default_'.$type] = $new_key;
					}

					$custom_addresses[$type] = array_merge($exist_custom,$custom_address);		
				}
			}		
		}	
		update_user_meta($user_id,THWMA_Utils::ADDRESS_KEY,$custom_addresses);
	}


	private function update_address_to_user_from_checkout($user_id,$address,$type,$address_key){

		$custom_addresses = get_user_meta($user_id,THWMA_Utils::ADDRESS_KEY,true);
		$exist_custom = $custom_addresses[$type];
		$custom_address[$address_key] = $address;
		$custom_key = THWMA_Utils::get_custom_addresses($user_id,'default_'.$type);
		if(!$custom_key){
			$custom_addresses['default_'.$type] = $address_key;
		}

		$custom_addresses[$type] = array_merge($exist_custom,$custom_address);
		
		update_user_meta($user_id,THWMA_Utils::ADDRESS_KEY,$custom_addresses);
	}

/////////////////for initial case////////////////////////////////////////

	public function add_address_from_checkout($data, $errors ){
		$user_id = get_current_user_id();	
		if(empty($errors->get_error_messages()  ) ){
			if(isset($_POST['thwma_hidden_field_billing'])){
				$checkout_bil_key = $_POST['thwma_hidden_field_billing'];
			
				if($checkout_bil_key == 'add_address'){
					$this->set_first_address_from_checkout($user_id,'billing');
				}
			}
			if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ){
				if(isset($_POST['thwma_hidden_field_shipping'])){
					$checkout_ship_key = $_POST['thwma_hidden_field_shipping'];
					if($checkout_ship_key == 'add_address'){
						$this->set_first_address_from_checkout($user_id,'shipping');
					}
				}
			}
		}
	}

	
	public function set_first_address_from_checkout($user_id,$type){

		$custom_addresses = get_user_meta($user_id,THWMA_Utils::ADDRESS_KEY,true);
		
		$custom_address = THWMA_Utils::get_custom_addresses($user_id,$type);
		
		
		$checkout_address_key = $_POST['thwma_hidden_field_'.$type];

		if(!$custom_address && $checkout_address_key == 'add_address' ){

			$custom_address = array();
			$custom_addresses = is_array($custom_addresses) ? $custom_addresses : array();

			$default_address = THWMA_Utils::get_default_address($user_id,$type);

			if(array_filter($default_address) && (count(array_filter($default_address)) > 2 )){
				if(!array_key_exists($type.'_heading',$default_address)){
					$default_address[$type.'_heading'] = __('','woocommerce-multiple-addresses-pro');
				}	
				$custom_address['address_0'] = $default_address;
				$custom_addresses[$type] = $custom_address;
				update_user_meta($user_id,THWMA_Utils::ADDRESS_KEY,$custom_addresses);
			}
		}

	}

	private function prepare_order_placed_address($user_id,$posted_data,$type){
		$fields = THWMA_Utils::get_address_fields($type);
		$new_address = array();

		foreach ($fields as $key ) {
			$new_address[$key] = is_array($posted_data[$key]) ? implode(',', $posted_data[$key]) : $posted_data[$key];
		}

		return $new_address;
	}

	public function prepare_address_fields_before_billing($fields, $country){

		foreach ( $fields as $key => $value ) {
			if ( 'billing_state' === $key ) {
				if(!isset($fields[$key]['country_field'])){
					$fields[$key]['country_field'] = 'billing_country';
				}
			}
		
		}
		return $fields;
	}

	public function prepare_address_fields_before_shipping($fields, $country){

		foreach ( $fields as $key => $value ) {
			if ( 'shipping_state' === $key ) {
				if(!isset($fields[$key]['country_field'])){
					$fields[$key]['country_field'] = 'shipping_country';
				}
			}		
		}
		return $fields;
	}	

	/////////////////////////address ovride//
	public function localisation_address_formats($formats){
		
		$address_formats_str = THWMA_Utils:: get_advanced_settings_value('address_formats');
		$custom_formats = array();
		if(!empty($address_formats_str)){
			$address_formats_arr = explode("|", $address_formats_str);
			if(is_array($address_formats_arr) && !empty($address_formats_arr)){

				foreach($address_formats_arr as $address_format) {
					if(!empty($address_format)){
						$format_arr = explode("=>", $address_format);
						if(is_array($format_arr) && count($format_arr) == 2){
							$frmt = str_replace('\n', "\n", $format_arr[1]);
							$custom_formats[trim($format_arr[0])] = $frmt;
						}
					}
				}
			}
		}
		
		if(is_array($formats) && $custom_formats && is_array($custom_formats)){
			$formats = array_merge($formats, $custom_formats);
		}
		return $formats;
	}


/*///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////custom sections- start//////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
	public function display_custom_sections(){
		
		$user_id = get_current_user_id();

		$custom_address = get_user_meta($user_id,'thwa_section_address',true);
		$custom_address = $custom_address['section_1'];
		

		
		$html_address =  $this->get_account_section_addresses($user_id,'section_1',$custom_address);
		//$html_address = '';
		?>
		<div class='th-custom-address'>
			<div class = 'th-head'><h3><?php _e("Additional  addresses"); ?> </h3></div><?php 
			if($html_address != ''){
				echo $html_address;
			}else{?>
				<div class="add-acnt-adrs new-adrs">
	             	<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', 'custom_address?ctype=add-address')); ?>" <i class="fa fa-plus"></i><?php  THWMA_i18n::et(' ADD NEW ADDRESS '); ?> </a>
						 
	           	</div><?php
			}?>
		</div><?php	
	}



	public function add_custom_form( $address, $load_address){
	
		if(isset($_GET['ctype'])){
			$address = '';
			$fields = $this->get_custom_fields();
			$address = $fields;
			return $address;
		}
	}

	public function custom_section_save_address($user_id, $load_address, $address){
		if(isset($_GET['ctype'])){
			$url = $_GET['ctype'];
			$fields = $this->get_custom_fields();
			$new_address = $this->prepare_posted_address($user_id,$fields,$load_address);
			$this->save_custom_section_address_to_user($new_address,'section_1',$user_id);
			wp_safe_redirect( wc_get_endpoint_url( 'edit-address', '', wc_get_page_permalink( 'myaccount' ) ) );
 			exit;
		}
	}

	public function get_custom_fields(){
		$sections = THWCFE_Utils::get_custom_sections();
		$section = THWCFE_Utils::get_checkout_section('aditional_address');
		
		if(THWCFE_Utils_Section::is_valid_section($section)){
			$fields = THWCFE_Utils_Section::get_fieldset($section);						
			return ($fields);
		}
		return false;
	}

	private function save_custom_section_address_to_user($address,$type,$user_id){
		
		$custom_addresses = get_user_meta($user_id,'thwa_section_address',true);
		$custom_addresses = is_array($custom_addresses) ? $custom_addresses : array();

		
			$custom_address = array();
			$custom_address['address_0'] = $address;
			$custom_addresses[$type] = $custom_address;

				
			
		update_user_meta($user_id,'thwa_section_address',$custom_addresses);
	}



	public function get_account_section_addresses($customer_id,$type,$custom_addresses){		
		$return_html = '';
		$add_class='';
		$address_type = "'$type'";

		if(is_array($custom_addresses)){
			//$add_list_class  = ($type == 'billing') ? " thslider-list bill " : " thslider-list ship";  
			$return_html .= '<div class="thslider">';
           	$return_html .= '<div class="thslider-box">';
           	$return_html .= '<div class="thslider-viewport">';
           	$return_html .= '<ul id="th-list" ';

			$i=0;

			foreach ($custom_addresses as $name => $address ) {				
				$address_string = $this->custom_address_formats($address);
				
				$heading = THWMA_i18n::t('Home') ;
				$action_row_html = '';
				$action_def_html = '';

				$confirm = "return confirm('Are you sure you want to delete this address?');";

				$action_row_html .= '<div class="acnt-address-footer">';					

				$action_row_html .= '</div>';				 				

				$add_address_btn = '<div class="add-acnt-adrs">
	               	<a href=" '.esc_url( wc_get_endpoint_url('edit-address','custom_address?ctype=add-address')).'"
	                	<i class="fa fa-plus"></i> '.THWMA_i18n::t('ADD NEW  ADDRESS').' </a>   
	            </div>';

				$add_class  = "thslider-item $type " ;
				$add_class .= $i == 0 ? ' first' : '';			

				$return_html .= '<li class="'.$add_class.'" value="'.$name.'" >
					<div class="address-box" data-index="'.$i.'" data-address-id=""> 
						<div class="main-content"> 
							<div class="complete-aaddress">  
								<div class="address-type-wrapper row"> 
									<div title="'.$heading.'" class="address-type">'.$heading.'</div>  
								</div>       
								<div class="address-text address-wrapper">'.$address_string.'</div> 
							</div>
						
							<div class="btn-continue address-wrapper"> 								
									 								
							</div> 
						</div>
							'.$action_row_html.'
					</div>
				</li>';

				$i++;
			}

			$return_html .= '</ul>';
			$return_html .= '</div>';

			$return_html .= '</div>';
			$return_html .= '<div class="control-buttons">';
			
           		$return_html .= '<div class="prev '.$type.'"><i class="fa fa-angle-left fa-3x"></i></div>';
           		 $return_html .= '<div class="next '.$type.'"><i class="fa fa-angle-right fa-3x"></i></div>';
           	
           	$return_html .= '</div>';

           		$return_html .= $add_address_btn;
           	$return_html .= '</div>';
		}

		return $return_html;
	}

public function custom_address_formats($args){
	$address_formats_str = "{comp_first_name}\n{comp_last_name}\n{comp_address_1}\n{comp_address_2}\n{comp_country}\n{comp_pincode}\n{comp_city}";
		
		
		$replace = array_map(
			'esc_html',  array(
					'{comp_first_name}'       => $args['comp_first_name'],
					'{comp_last_name}'        => $args['comp_last_name'],
					'{comp_address_1}'          => $args['comp_address_1'],
					'{comp_address_2}'        => $args['comp_address_2'],
					'{comp_pincode}'        => $args['comp_pincode'],
					'{comp_city}'             => $args['comp_city'],
					'{comp_country}'         =>  $args['comp_country'],
					
			)
		);
		$formatted_address = str_replace( array_keys( $replace ), $replace, $address_formats_str );
	
		// $formatted_address = preg_replace( '/  +/', ' ', trim( $formatted_address ) );
		// $formatted_address = preg_replace( '/\n\n+/', "\n", $formatted_address );
		// $formatted_address =  array_map( array( $this, 'trim_formatted_address_line' ), explode( "\n", $formatted_address ) ) ;
		return $formatted_address;
	}

	////////////////////////END////////////////////////////////////*/
	
}
endif;