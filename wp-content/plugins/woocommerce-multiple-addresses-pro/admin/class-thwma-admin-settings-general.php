<?php
/**
 * The admin general settings page functionality of the plugin.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    woocommerce-multiple-addresses-pro
 * @subpackage woocommerce-multiple-addresses-pro/admin
 */
if(!defined('WPINC')){	die; }

if(!class_exists('THWMA_Admin_Settings_General')):

class THWMA_Admin_Settings_General extends THWMA_Admin_Settings {
	protected static $_instance = null;
	
	private $cell_props_L = array();
	private $cell_props_R = array();
	private $cell_props_CB = array();
	private $cell_props_CBS = array();
	private $cell_props_CBL = array();
	private $cell_props_CP = array();
	private $checkbox_cell_props = array();
	
	private $settings_props = array();
	
	public function __construct() {
		parent::__construct('general_settings', '');
		$this->init_constants();
	}
	
	public static function instance() {
		if(is_null(self::$_instance)){
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public function init_constants(){
		$this->cell_props_L = array( 
			'label_cell_props' => 'width="23%"', 
			'input_cell_props' => 'width="75%"', 
			'input_width' => '250px',  
		);
		
		$this->cell_props_R = array( 
			'label_cell_props' => 'width="13%"', 
			'input_cell_props' => 'width="34%"', 
			'input_width' => '250px', 
		);
		
		$this->cell_props_CB = array( 
			'label_props' => 'style="margin-right: 40px;"', 
		);
		$this->cell_props_CBS = array( 
			'label_props' => 'style="margin-right: 15px;"', 
		);
		$this->cell_props_CBL = array( 
			'label_props' => 'style="margin-right: 52px;"', 
		);
		
		$this->cell_props_CP = array(
			'label_cell_props' => 'width="13%"', 
			'input_cell_props' => 'width="34%"', 
			'input_width' => '225px',
		);
		$this->checkbox_cell_props = array( 'cell_props' => '3' );
		
		$this->settings_props = $this->get_field_form_props();
	} 
		
	public function get_field_form_props(){			
		$hint_name = __('Used to save values in database. Name must begin with a lowercase letter.','woocommerce-multiple-addresses-pro');
		
		$display_styles = array(
			'dropdown_display' => __('Drop Down','woocommerce-multiple-addresses-pro'),
			'popup_display' => __('Pop Up','woocommerce-multiple-addresses-pro'),
			//'accordion' => 'Accordion',
		);

        $display_positions=array(
        	'above' => __('Above the form','woocommerce-multiple-addresses-pro'),
			'below' => __('Below the form','woocommerce-multiple-addresses-pro')
        ); 

        $link_types=array(
        	'button' => __('Button','woocommerce-multiple-addresses-pro'),
        	'link'=>__('Link','woocommerce-multiple-addresses-pro')
        );

        $settings_props_billing = array(
        	
			'enable_billing' => array('name'=>'enable_billing', 'label' => __('Enable multiple addresses','woocommerce-multiple-addresses-pro'),'type'=>'checkbox', 'value'=>'yes', 'checked'=>1), 		
			'billing_display' => array('type'=>'select', 'name'=>'billing_display', 'label'=>__('Display type','woocommerce-multiple-addresses-pro'), 'options'=>$display_styles , 'value'=> 'popup_display' ),
			'billing_display_position' => array('type'=>'select', 'name'=>'billing_display_position', 'label'=>__('Display position','woocommerce-multiple-addresses-pro'), 'options'=>$display_positions , 'value'=> 'above' ),
			'billing_display_title' => array('type'=>'select', 'name'=>'billing_display_title', 'label'=>__('Display style','woocommerce-multiple-addresses-pro'), 'options'=>$link_types, 'value'=>''),
			'billing_display_text' => array('type'=>'text', 'name'=>'billing_display_text', 'label'=>__('Display text','woocommerce-multiple-addresses-pro'),'value'=> __('Billing with a different address','woocommerce-multiple-addresses-pro')),
			'billing_address_limit' => array('type'=>'number', 'name'=>'billing_address_limit', 'label'=>__('Address limit','woocommerce-multiple-addresses-pro'),'id'=>'thb_limit_value','required' => '','value'=> 99,'min'=>1,'max'=>99),
        );

        $settings_props_shipping = array(
        	
			'enable_shipping'=> array('type'=>'checkbox', 'name'=>'enable_shipping', 'label' => __('Enable multiple addresses','woocommerce-multiple-addresses-pro'),'checked'=>1,'value'=>'yes'),
			'shipping_display' => array('type'=>'select', 'name'=>'shipping_display', 'label'=>__('Display type','woocommerce-multiple-addresses-pro'), 'value'=> 'popup_display' , 'options'=>$display_styles ), 
			'shipping_display_position' => array('type'=>'select', 'name'=>'shipping_display_position', 'label'=>__('Display position','woocommerce-multiple-addresses-pro'), 'options'=>$display_positions , 'value'=> 'above' ),
			'shipping_display_title' => array('type'=>'select', 'name'=>'shipping_display_title', 'label'=>__('Display style','woocommerce-multiple-addresses-pro'), 'options'=>$link_types, 'value'=>''),
			'shipping_display_text' => array('type'=>'text', 'name'=>'shipping_display_text', 'label'=>__('Display text','woocommerce-multiple-addresses-pro'),'value'=>__('Shipping to a different address','woocommerce-multiple-addresses-pro')),
			'shipping_address_limit' => array('type'=>'number', 'name'=>'shipping_address_limit', 'label'=>__('Address limit','woocommerce-multiple-addresses-pro'),'id'=>'ths_limit_value','required' => '','value'=> 99,'min'=>1,'max'=>99),
        ); //todo.adal changed max to 99

		return array(
			'section_address_fields' => array('title'=>__('Billing Address Properties','woocommerce-multiple-addresses-pro'), 'type'=>'separator', 'colspan'=>'3'),
			'settings_props_billing'  => $settings_props_billing,
			'section_shipping_address_fields' => array('title'=>__('Shipping Address Properties','woocommerce-multiple-addresses-pro'), 'type'=>'separator', 'colspan'=>'3'),
			'settings_props_shipping' => $settings_props_shipping,	
			//'custom_section' => array('title'=>'', 'type'=>'separator', 'colspan'=>'3'),
			//'enable_custom'=> array('type'=>'checkbox', 'name'=>'enable_custom', 'label' => 'Enable  custom Scetions as addresses','checked'=>0,'value'=>'no'),
			);		
	}
		
	public function render_page(){
		$this->render_tabs();
		$this->render_sections();
		$this->render_content();


	}

	public function save_settings(){
		$settings = array();

		$settings['settings_billing'] = $this->populate_posted_address_settings('billing');
		$settings['settings_shipping'] = $this->populate_posted_address_settings('shipping');
		$settings['enable_custom_section']=isset($_POST['i_enable_custom']) ? 'yes' :'no';
		update_option( THWMA_Utils::OPTION_KEY_THWMA_SETTINGS,$settings);
	}

	private function populate_posted_address_settings($type){
		$posted = array();
		$prefix='i_';
		$SETTINGS_PROPS = $this->settings_props['settings_props_'.$type];
		foreach($SETTINGS_PROPS as $props){
			$name  = $props['name'];
			if($props['type']== 'checkbox'){
				$value = isset($_POST[$prefix.$name]) ? 'yes' : 'no';
			}else{
				$value = isset($_POST[$prefix.$name]) ? $_POST[$prefix.$name] : $props['value'];
			}
			
			if($name === 'billing_display_text' || $name === 'shipping_display_text'){
				THWMA_i18n::wpml_register_string('Address Link - '.$name, $value);
			}
			
			$posted[$name] = $value;
		}
		
		return $posted;
	}
	
	public function reset_to_default() {
		delete_option(THWMA_Utils::OPTION_KEY_THWMA_SETTINGS);
		
		return '<div class="updated"><p>'. __('Settings successfully reset','woocommerce-multiple-addresses-pro') .'</p></div>';
	}
	
	private function render_content(){
		$prefix='i_';		
		if(isset($_POST['save_settings']))		
			echo $this->save_settings();		
		if(isset($_POST['reset_settings']))
			echo $this->reset_to_default();

		$settings_fields = $this->get_field_form_props();
		$settings_props_billing=$settings_fields['settings_props_billing'];
		$settings_props_shipping=$settings_fields['settings_props_shipping'];
		
		$settings_props_billing=$this->set_values_props($settings_props_billing,'billing');
		$settings_props_shipping=$this->set_values_props($settings_props_shipping,'shipping');
		?>            
		<div style="padding-left: 30px;">               
		    <form id="thwma_settings_fields_form" method="post" action="">
                <table class="form-table thpladmin-form-table">
                	<tbody>
                		<tr>
			            	<?php
			            	$this->render_form_section_separator($settings_fields['section_address_fields']);
							?>
			            </tr>
			            <tr>
			            	<?php 

			            	if($settings_props_billing['enable_billing']['value']=='no')
			            	{
			            		$settings_props_billing['enable_billing']['checked']=0;
			            	}
			            	$this->render_form_field_element($settings_props_billing['enable_billing']);
			            	?>
			            </tr>
			            <tr>
			            	<?php
			            	$this->render_form_field_element($settings_props_billing['billing_display'],$this->cell_props_L);
			            	?>
			            </tr>
			            <tr>
			            	<?php 
			            	$this->render_form_field_element($settings_props_billing['billing_display_position'],$this->cell_props_L);
			            	?>
			            </tr>
			             <tr>
			            	<?php 
			            	$this->render_form_field_element($settings_props_billing['billing_display_title'],$this->cell_props_L);
			            	?>
			            </tr>
			            <tr>
			            	<?php 
			            	$this->render_form_field_element($settings_props_billing['billing_display_text'],$this->cell_props_L);
			            	?>
			            </tr>
			            <tr>
			            	<?php
			           		$this->render_form_field_element($settings_props_billing['billing_address_limit'],$this->cell_props_L);
			           		?>
			        	</tr>
			            <tr>
			            	<?php
			            	$this->render_form_section_separator($settings_fields['section_shipping_address_fields']);
							?>
			            </tr>
			            <tr>
			            	<?php 
			            	if($settings_props_shipping['enable_shipping']['value']=='no')
			            	{
			            		$settings_props_shipping['enable_shipping']['checked']=0;
			            	}
			            	$this->render_form_field_element($settings_props_shipping['enable_shipping']);
			            	?>
			            </tr>
			            <tr>
			            	<?php 
			            	$this->render_form_field_element($settings_props_shipping['shipping_display'],$this->cell_props_L);
			            	?>
			            </tr>
			            <tr>
			            	<?php 
			            	$this->render_form_field_element($settings_props_shipping['shipping_display_position'],$this->cell_props_L);
			            	?>
			            </tr>
			             <tr>
			            	<?php 
			            	$this->render_form_field_element($settings_props_shipping['shipping_display_title'],$this->cell_props_L);
			            	?>
			            </tr>
			            <tr>
			            	<?php 
			            	$this->render_form_field_element($settings_props_shipping['shipping_display_text'],$this->cell_props_L);
			            	?>
			            </tr>

			            <tr>
			            	<?php 

			            	$this->render_form_field_element($settings_props_shipping['shipping_address_limit'],$this->cell_props_L);
			            	?>
			            </tr>
			       
                     
                    </tbody>
                </table> 
                <p class="submit">
					<input type="submit" name="save_settings" class="button-primary" value=" <?php _e('Save changes','woocommerce-multiple-addresses-pro'); ?> " id="thwma_save_settings">

                    <input type="submit" name="reset_settings" class="button" value="<?php _e('Reset to default','woocommerce-multiple-addresses-pro'); ?>" 
					onclick="return confirm('Are you sure you want to reset to default settings? all your changes will be deleted.');">
            	</p>
            </form>
    	</div> 
      
    	<?php
    }
	
	private function set_values_props($settings_props,$type){
		foreach ($settings_props as $name => $props) {
			$settings_props[$name]['value']=THWMA_Utils::get_setting_value('settings_'.$type,$name);
		}
		return $settings_props;
	}	
	
}

endif;