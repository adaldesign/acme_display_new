<?php
/**
 * The admin settings page common utility functionalities.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    woocommerce-multiple-addresses-pro
 * @subpackage woocommerce-multiple-addresses-pro/admin
 */
if(!defined('WPINC')){	die; }

if(!class_exists('THWMA_Admin_Utils')):

class THWMA_Admin_Utils {
	public static function get_sections(){				
		$sections = THWMA_Utils::get_custom_sections();
		
		if($sections && is_array($sections) && !empty($sections)){
			return $sections;
		}else{
			$section = THWMA_Utils_Section::prepare_default_section();
			
			$sections = array();
			$sections[$section->get_property('name')] = $section;
			return $sections;
		}		
	}
	
	public static function get_section($section_name){
	 	if($section_name){	
			$sections = self::get_sections();
			if(is_array($sections) && isset($sections[$section_name])){
				$section = $sections[$section_name];	
				if(THWMA_Utils_Section::is_valid_section($section)){
					return $section;
				} 
			}
		}
		return false;
	}
	
	

	
	public static function load_user_roles(){
		$user_roles = array();
		
		global $wp_roles;
    	$roles = $wp_roles->roles;
		//$roles = get_editable_roles();
		foreach($roles as $key => $role){
			$user_roles[] = array("id" => $key, "title" => $role['name']);
		}		
		
		return $user_roles;
	}
	
	
	
}

endif;