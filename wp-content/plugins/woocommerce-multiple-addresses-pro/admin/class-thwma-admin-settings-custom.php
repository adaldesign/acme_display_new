<?php
/**
 * The admin general settings page functionality of the plugin.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    woocommerce-multiple-addresses-pro
 * @subpackage woocommerce-multiple-addresses-pro/admin
 */
if(!defined('WPINC')){	die; }

if(!class_exists('THWMA_Admin_Settings_custom')):

class THWMA_Admin_Settings_custom extends THWMA_Admin_Settings {

	protected static $_instance = null;
	
	private $cell_props_L = array();
	private $cell_props_R = array();
	private $cell_props_CB = array();
	private $cell_props_CBS = array();
	private $cell_props_CBL = array();
	private $cell_props_CP = array();
	
	private $settings_props = array();
	
	public function __construct() {
		parent::__construct('custom_section_settings', '');
		$this->init_constants();
	}
	
	public static function instance() {
		if(is_null(self::$_instance)){
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public function init_constants(){
		$this->cell_props_L = array( 
			'label_cell_props' => 'width="23%"', 
			'input_cell_props' => 'width="75%"', 
			'input_width' => '250px',  
		);
		
		$this->cell_props_R = array( 
			'label_cell_props' => 'width="13%"', 
			'input_cell_props' => 'width="34%"', 
			'input_width' => '250px', 
		);
		
		$this->cell_props_CB = array( 
			'label_props' => 'style="margin-right: 40px;"', 
		);
		$this->cell_props_CBS = array( 
			'label_props' => 'style="margin-right: 15px;"', 
		);
		$this->cell_props_CBL = array( 
			'label_props' => 'style="margin-right: 52px;"', 
		);
		
		$this->cell_props_CP = array(
			'label_cell_props' => 'width="13%"', 
			'input_cell_props' => 'width="34%"', 
			'input_width' => '225px',
		);
		
		//$this->settings_props = $this->get_field_form_props();
	} 


	private function render_actions_row(){

		$sections = THWCFE_Utils::get_custom_sections();

		$custom_sections = array();
		foreach ($sections as $name => $values) {

			if($name != 'billing'  && $name != 'shipping' && $name != 'additional' ){

				$custom_sections[$name] = $values;
			} 
		}
		?>
		
        <th colspan="3">
        	<select name="cus_sections" class="th-map-select">
	        	<option value="">Select custom section </option> <?php 
				foreach($custom_sections as $name => $value){?>
					
					<option value="<?php echo $name; ?>"><?php echo $name; ?> </option>
				<?php } ?>

			</select>
			<select name="def_sections" class="th-map-select">
	        	<option value="">Select default section</option> 	
				<option value="billing"> Billing </option>
				<option value="shipping">Shipping</option>
			</select>
            <input type="submit" class="button-primary" name ="s_add_section_map" value="<?php _e('+ Add new Address Section','woocommerce-multiple-addresses-pro'); ?>"></input>
        </th>
        <th colspan="4">
           
            <input type="submit" name="reset_settings" class="button-secondary" value="<?php _e('Reset Settings','woocommerce-multiple-addresses-pro'); ?>" style="float:right; margin-right: 5px;" 
            onclick="return confirm('Are you sure you want to reset to default settings? all your changes will be deleted.');">
        </th> 


    	<?php 
	}
		
	/*public function get_field_form_props(){			
		
		$default_fields = THWMA_Public::get_address_fields('billing');
		$sections = THWCFE_Utils::get_custom_sections();
		$custom_sections = array();
		foreach ($sections as $name => $values) {

			if($name != 'billing'  && $name != 'shipping' && $name != 'additional' ){

				$custom_sections[$name] = $values;
			} 
		}


		$section_settings_fields = array();
		$saved_settings = get_option( THWMA_Utils::OPTION_KEY_SECTION_SETTINGS);
		foreach ($custom_sections as $name => $fields) {
			$position = $fields->position;
			$section_fields = $fields->get_property('fields');

			if($saved_settings){
				$section_values = $saved_settings[$name];
				$enable_value = $section_values ? $section_values['section_enable_'.$name] : 'no';
			}else{
				$enable_value = 'no';
			}

			$section_field = array();
			foreach ($section_fields as $key => $value) {
				if(isset($section_values)){
					$selected_value = $section_values['custom_section'][$key];
				}else{
					$selected_value = '';
				}

				$section_field[$key] =  array('type'=>'select', 'name'=>$key, 'label'=>$key, 'options'=>$default_fields , 'value'=> $selected_value); 
			}


			$section_settings_fields[$name] = array(
				'section_address_fields' => array('title'=>' Address Section ', 'type'=>'separator', 'colspan'=>'3'),
				'section_enable_'.$name  => array('name'=>'section_enable_'.$name  , 'label' => 'Enable Section  '.$name ,'type'=>'checkbox', 'value'=>$enable_value , 'checked'=>0),
				'section_position_'.$name => array('type'=>'text', 'name'=>'section_position_'.$name, 'label'=>'Checkout Position','value'=> $position),
				'section_fields' => $section_field,
			);
				
		}
       

		return $section_settings_fields;
	}*/
		
	public function render_page(){
		$this->render_tabs();
		$this->render_sections();
		$this->render_content();
	}

	/*public function save_settings(){
		$settings = array();
		$settings = $this->populate_posted_address_settings();
		update_option( THWMA_Utils::OPTION_KEY_SECTION_SETTINGS,$settings);
	}*/

	private function populate_posted_address_settings(){

		$posted_section = array();
		$prefix='i_';
		$SETTINGS_PROPS = $this->settings_props;
		
		foreach($SETTINGS_PROPS as $section => $fields  ){
			$posted = array();
			
			foreach ($fields as $field => $props) {
				$map_field = array();

				if($field == 'section_fields'){
					foreach ($props as $custom_fld) {
						$name = $custom_fld['name'];
						$value = isset($_POST[$prefix.$name]) ? $_POST[$prefix.$name] : $props['value'];
						$map_field[$name ] = $value;
					}
					$posted['custom_section'] = $map_field;
				}else{
					$name  = $props['name'];

					if($props['type']== 'checkbox'){
						$value = isset($_POST[$prefix.$name]) ? 'yes' : 'no';

			 		}else{
						$value = isset($_POST[$prefix.$name]) ? $_POST[$prefix.$name] : $props['value'];
					}
					
					//$name	= str_replace($section.'_','',$name);

					$posted[$name] = $value;
				}		
			}
			$posted_section[$section] = $posted;
		}

		
		
		return $posted_section;
	}
	
	public function reset_to_default() {
		
		delete_option(THWMA_Utils::OPTION_KEY_SECTION_SETTINGS);
		
		return '<div class="updated"><p>'. __('Settings successfully reset','woocommerce-multiple-addresses-pro') .'</p></div>';
	}

	private function save_mapping_fields($sect_name){
		$save_def_fields = $_POST[$sect_name.'_def_select'];
		$save_sec_fields = $_POST[$sect_name.'_sec_select'];
		$saved_fields = array();
		//$saved_fields = get_option(THWMA_Utils::OPTION_KEY_SECTION_SETTINGS,true);
		$saved_fields  = THWMA_Utils::get_custom_section_settings();
		$map_array = $saved_fields;
		
		foreach ($saved_fields as $section_name => $section_value) {
			//$new_sec_array[$section_name] = $section_name;
			if($section_name == $sect_name){
				$new_array = array();
				foreach ($save_def_fields as $def_key => $def_value) {
					foreach ($save_sec_fields as $sec_key => $sec_value) {

						if($sec_value != '' && $def_value != ''){
							if($def_key == $sec_key){
								$new_array[$def_value] = $sec_value;
							}
						}
					}
				}
				$map_array[$section_name]['map_fields'] =  $new_array;
				$saved_fields = array_merge($saved_fields,$map_array);
			} 

		}
		
		update_option(THWMA_Utils::OPTION_KEY_SECTION_SETTINGS,$saved_fields); 
		

	}

	private function enable_mapping_fields($sect_name){
		$saved_fields = array();
		//$saved_fields = get_option(THWMA_Utils::OPTION_KEY_SECTION_SETTINGS,true);
		$saved_fields  = THWMA_Utils::get_custom_section_settings();
		foreach ($saved_fields as $section_name => $section_value) {
			if($section_name == $sect_name){

				$enable_field = array('enable_section' => 'yes');

				if(is_array($section_value)){
					$new_field[$section_name] = array_merge($section_value,$enable_field);
				}else{

					$new_field[$section_name] = $enable_field;
				} 
			}

		}

		if($new_field){
			
			$saved_fields = array_merge($saved_fields,$new_field);
		}

		
		update_option(THWMA_Utils::OPTION_KEY_SECTION_SETTINGS,$saved_fields); 
	}

	public function disable_mapping_fields($sectn_name){
		$saved_fields = array();
		//$saved_fields = get_option(THWMA_Utils::OPTION_KEY_SECTION_SETTINGS,true);
		$saved_fields  = THWMA_Utils::get_custom_section_settings();

		foreach ($saved_fields as $section_name => $section_value) {
			if($section_name == $sectn_name){

				$enable_field = array('enable_section' => 'no');

				if(is_array($section_value)){
					$new_field[$section_name] = array_merge($section_value,$enable_field);
				}else{

					$new_field[$section_name] = $enable_field;
				} 
			}

		}

		if($new_field){
			
			$saved_fields = array_merge($saved_fields,$new_field);
		}
		
		update_option(THWMA_Utils::OPTION_KEY_SECTION_SETTINGS,$saved_fields); 
	}


	private function delete_mapping_fields($sect_name){

		
		$saved_fields = get_option(THWMA_Utils::OPTION_KEY_SECTION_SETTINGS,true);
		unset($saved_fields[$sect_name]);
		update_option(THWMA_Utils::OPTION_KEY_SECTION_SETTINGS,$saved_fields); 
			
	}
	
	private function render_content(){
		//$prefix='i_';
		
		// if(isset($_POST['save_settings']))		
		// 	echo $this->save_settings();		
		if(isset($_POST['reset_settings']))
			echo $this->reset_to_default();
		//$sectionx = get_option( 'thwma_sectionz',true);
		$custom_section  = THWMA_Utils::get_custom_section_settings();
		if(is_array($custom_section)){
			foreach ($custom_section as $sectn_name => $sectn_value) {
				
			
				if(isset($_POST[$sectn_name.'_map_save'])){
					$this->save_mapping_fields($sectn_name);
				}

				if(isset($_POST[$sectn_name.'_map_delete'])){
					$this->delete_mapping_fields($sectn_name);
				}

				if(isset($_POST[$sectn_name.'_map_enable'])){
					$this->enable_mapping_fields($sectn_name);
				}

				if(isset($_POST[$sectn_name.'_map_disable'])){
					$this->disable_mapping_fields($sectn_name);
				}
			}
		}

		// if(isset($_POST['s_add_section_map']))
		// 	$new_section	= $this->create_new_mapping();

		//$settings_fields = $this->get_field_form_props();
		$style = '';
		?>   

		<div style="margin-top: 20px; margin-right: 30px;">               
		    <form id="thwma_tab_custom_settings_form" method="post" action="">
                <table id="" class="wc_gateways widefat thpladmin_steps_table" cellspacing="0" >
                    <thead>
                        <?php $this->render_actions_row(); ?></thead>
                        <tr>
                        <?php
                    
                    	echo $this->create_new_mapping();
                    ?>
                    
                    </tr>
     
                </table>
            </form>
        </div>         

      
    	<?php
    }

    private function create_new_mapping(){

    	$step = $this->prepare_new_section($_POST);
		//$settings = get_option('thwma_sectionz',true);
		$settings = THWMA_Utils::get_custom_section_settings(); 

		if(is_array($settings)){

			foreach ($settings as $key => $S_value) { 
								

					$disabled = is_array($S_value) && isset($S_value['enable_section']) && $S_value['enable_section'] == 'yes' ?  "yes" : "no";
					

					$fields = self::get_sections_fields($key);

					$to_map = isset($S_value['maped_section'])  ?  $S_value['maped_section'] : "billing";

					$default_fields = self::get_sections_fields($to_map);
					
					$title =  $this->get_custom_section_title($key);
					$title = $title.'   ';
					//$step_fields = $this->prepare_section_fields($key);
					?>
					<table width="100%" class="widefat thpladmin_fields_table">
						<thead>
							<tr><th colspan="4" class="thpladmin-form-section-title th-section-title" style=" font-weight: 600;"><?php _e($title,'woocommerce-multiple-addresses-pro');   _e(  '('.$key.')',''); ?>  </th></tr>
							<tr>
								<th colspan="2">
									<input type="button" class="button" onclick="addRow('<?php echo $key ?>')" value="<?php _e('+ Add new mapping','woocommerce-multiple-addresses-pro'); ?>" >
									<input type="submit" name="<?php echo $key;?>_map_delete" class="button" value="<?php _e('Remove','woocommerce-multiple-addresses-pro'); ?>">
									<?php if($disabled == 'no'){?>

										<input type="submit" name="<?php echo $key;?>_map_enable"  class="button" value="<?php _e('Enable','woocommerce-multiple-addresses-pro'); ?>"> <?php
									}else{ ?>

										
										<input type="submit" name="<?php echo $key;?>_map_disable" class="button" value="<?php _e('Disable','woocommerce-multiple-addresses-pro'); ?>">	<?php
									} ?>
								</th>
								<th>
									<input type="submit" name="<?php echo $key;?>_map_save" class="button-primary" style="float:right" value="<?php _e(' Save changes','woocommerce-multiple-addresses-pro'); ?>">
								</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody id="<?php echo $key ?>">
							<tr style="display: none;">
								<td width="20%">
									<select name="<?php echo $key;?>_def_select[]" class="th-map-select">
					        			<option value=""><?php _e('Select...','woocommerce-multiple-addresses-pro'); ?></option> 
					        			<?php 
										foreach($default_fields as $name => $value){
										?>
											<option value="<?php echo $name; ?>"><?php echo $name; ?></option>
										<?php } ?>
									</select>
								</td>
								<td width="20%">
									<select name = "<?php echo $key;?>_sec_select[]" class="th-map-select" >
					        			<option value=""><?php _e('Select...','woocommerce-multiple-addresses-pro');?></option> 
					        			<?php 
										foreach($fields as $name => $value){
										?>
											<option value="<?php echo $name; ?>"><?php echo $name; ?> </option>
										<?php } ?>
									</select>
								</td>
								<td width="20%">
									<button type="button" class="f_delete_btn btn-red" onclick="thwmadelete(this,<?php echo $key; ?>)"> x </button>
								</td>
								<td width="40%">&nbsp;</td>
							</tr>



							<?php
							$map_fields = isset($S_value['map_fields']) ?  $S_value['map_fields'] : '';
							if(is_array($map_fields) ){
							
								foreach ($map_fields as $left => $right) {
									
									?>
									<tr>
										<td>
											<select name="<?php echo $key;?>_def_select[]"  class="th-map-select">
												<option value=""><?php _e('Select...','woocommerce-multiple-addresses-pro');?></option> 
			        							<?php 
												foreach($default_fields as $name => $value){
													
													if($name == $left ){?>
														<option value="<?php echo $name; ?>" selected><?php echo $left; ?> </option> <?php
													}else{?>
														<option value="<?php echo $name; ?>"><?php echo $name; ?> </option>
													<?php }
												} ?>

											</select>
										</td>
										<td>
											<select name = "<?php echo $key;?>_sec_select[]" class="th-map-select">
												<option value=""><?php _e('Select...','woocommerce-multiple-addresses-pro');?></option>
			        							<?php 
												foreach($fields as $name => $value){
													
													if($name == $right ){?>
														<option value="<?php echo $name; ?>" selected><?php echo $right; ?> </option> <?php
													}else{?>
														<option value="<?php echo $name; ?>"><?php echo $name; ?> </option>
													<?php }
												} ?>
											</select>
										</td>
										<td>
											<button type="button" class="f_delete_btn btn-red" onclick="thwmadelete(this,<?php echo $key; ?>)">x</button>
										</td>
									</tr><?php
								}
							}else{?>
								<tr>
									<td>
										<select name="<?php echo $key;?>_def_select[]" class="th-map-select">
					        			<option value=""><?php _e('Select...','woocommerce-multiple-addresses-pro');?></option> <?php 
										foreach($default_fields as $name => $value){?>
											<option value="<?php echo $name; ?>"><?php echo $name; ?> </option>
										<?php } ?>

										</select>
									</td>
									<td>
										<select name = "<?php echo $key;?>_sec_select[]" class="th-map-select" >
					        			<option value=""><?php _e('Select...','woocommerce-multiple-addresses-pro'); ?></option> <?php 
										foreach($fields as $name => $value){?>
									
											<option value="<?php echo $name; ?>"><?php echo $name; ?> </option>
											<?php } ?>

										</select>
									</td>
									<td>
										<button type="button" class="f_delete_btn btn-red" onclick="thwmadelete(this,<?php echo $key; ?>)">x</button>
									</td>
									
								</tr>
							<?php } ?>
						</tbody>
						
					</table> <?php
				
			}
  	
		}

    }

    private function prepare_new_section(){
    	$sectionee = array();
    	$settings = THWMA_Utils::get_custom_section_settings();
    	
    	if(isset($_POST['s_add_section_map'])){
    		$chosed_section = $_POST['cus_sections'];
    		$map_section = $_POST['def_sections'];
    		if($chosed_section != '' && $map_section != ''){
	    		if(!array_key_exists($chosed_section,$settings)){
	    			$settings_array = array('enable_section' => 'yes','maped_section' => $map_section,);
	    			$sectionee[$chosed_section] = $settings_array;
	    		}
	    	}
    			
    	}
    	
    	///array check
    	$settings = is_array($settings) ? $settings : array();
    	$sectionee = isset($sectionee) ? $sectionee : array();

    	$sectionzz = $settings ? array_merge($settings,$sectionee) : $sectionee;
    	//THWMA_Utils::get_custom_section_settings();
    	
    	update_option(THWMA_Utils::OPTION_KEY_SECTION_SETTINGS,$sectionzz);  	
    }
	
		

	private function get_custom_section_title($section_key){
		$custom_sections = THWCFE_Utils::get_custom_sections();
		foreach ($custom_sections as $name => $fields) {
			if($name == $section_key){
				return($fields->get_property('title'));
			}
		}
	}

	private function get_default_fields(){
		$sections = THWCFE_Utils::get_custom_sections();
	}

	public static function get_sections_fields($section_name){

		//$default_fields = THWMA_Public::get_address_fields('billing');
		$custom_sections = THWCFE_Utils::get_custom_sections();

		$section_settings_fields = array();
		$section_fields_value = array();
	
		foreach ($custom_sections as $name => $fields) {
			if($section_name == $name){
				//$section_fields_value = array();
				$section_fields = $fields->get_property('fields');
				foreach ($section_fields as $key => $value) {
					
					if(isset($value->user_meta) && ($value->user_meta =='yes')){
						$section_fields_value[$key] = $key;
					}
				}
				return $section_fields_value;
			}
			
		}
	}

	
}

endif;