<?php
/**
 * The common utility functionalities for the plugin.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    woocommerce-multiple-addresses-pro
 * @subpackage woocommerce-multiple-addresses-pro/includes/utils
 */
if(!defined('WPINC')){	die; }

if(!class_exists('THWMA_Utils')):

class THWMA_Utils {
	const THWMA_TEXT_DOMAIN ='thwma';

	const ADDRESS_KEY = 'thwma_custom_address';
	
	const OPTION_KEY_THWMA_SETTINGS = 'thwma_general_settings';
	const OPTION_KEY_ADVANCED_SETTINGS = 'thwma_advanced_settings';
	const OPTION_KEY_SECTION_SETTINGS = 'thwma_custom_section_map';
	//const OPTION_KEY_SECTION_HOOK_MAP='thwma_section_hook_map';
	//const OPTION_KEY_NAME_TITLE_MAP='thwma_name_title_map';
	
	public static function get_advanced_settings(){
		$settings = get_option(self::OPTION_KEY_ADVANCED_SETTINGS);
		return empty($settings) ? false : $settings;
	}
	
	public static function get_setting_value($key,$sections=false,$settings=false){
		if(!$settings){
			$settings = self::get_general_settings(); 
		}

		if(is_array($settings) && isset($settings[$key])){
			if($sections){

				return $settings[$key][$sections];
			}

			return $settings[$key];
		}
		return '';
	}
	
	public static function get_advanced_settings_value($key){
		$settings = self::get_advanced_settings();
		if(is_array($settings) && isset($settings[$key])){
			return $settings[$key];
		}
		return '';
	}

	public static function get_maped_sections_settings_value($section,$key=false){
		$settings = self::get_custom_section_settings();
		if($settings){
			$section_values = $settings[$section];
			if($key){
				$maped_fields = $section_values[$key];
				return $maped_fields;
			}

			return $section_values; 
		}
		return false;
	}

	public static function get_custom_section_settings(){
		$map_sections = get_option(self::OPTION_KEY_SECTION_SETTINGS,true);
		return is_array($map_sections) ? $map_sections : false;
	}

	public static function get_general_settings(){
		$default_settings=array(

			'settings_billing' =>array(
				'enable_billing'=>'yes',
				'billing_display'=>'popup_display',
				'billing_display_position'=>'above',
				'billing_display_title'=>'link',
				'billing_display_text'=> __('Billing with a different address','woocommerce-multiple-addresses-pro'),
				'billing_address_limit' =>99),
			'settings_shipping'=>array(
				'enable_shipping'=>'yes',
				'shipping_display'=>'popup_display',
				'shipping_display_position'=>'above',
			 	'shipping_display_title'=>'link',
				'shipping_display_text'=>__('Shipping to a different address','woocommerce-multiple-addresses-pro'),
				'shipping_address_limit' =>99 ),
			
		); // todo.adal changed max to 99
		$settings = get_option(self::OPTION_KEY_THWMA_SETTINGS);
		return empty($settings) ? $default_settings : $settings;
	}

	/////////////////////////////Address////////////////


	public static function get_address_fields_by_address_key($type){
    	$fields = WC()->countries->get_address_fields( WC()->countries->get_base_country(),$type);
    	$fields_keys = array();
    	foreach ($fields as $key => $value) {
    		if(isset($value['custom']) && $value['custom']){
    			if(isset($value['user_meta']) && ($value['user_meta']==='yes')){	
    				$fields_keys[$key] = $value['type'];   					
    			}	
    		}else{
    			if(isset($value['type'])){
    				$fields_keys[$key] = $value['type'];
    			}else{
    				$fields_keys[$key] = 'text';
    			}
    		}	
    	}
    	return $fields_keys;
    }

    public static function get_addresses($customer_id,$type){
		$address = self::get_custom_addresses($customer_id,$type);
		
		$default_key = self::get_custom_addresses($customer_id,'default_'.$type);
		$same_address = self::is_same_address_exists($customer_id,$type);
		$address_key = ($default_key) ? $default_key : $same_address;
		if(is_array($address )){
			$addresses = array();

			foreach ($address as $key => $value) {

				$get_heading = self::get_custom_addresses($customer_id,$type,$key,$type.'_heading');
				$default_heading = apply_filters('thwma_default_heading',false);
				if($default_heading && $default_heading != ''){
					$heading = $get_heading ? $get_heading : __('Home','woocommerce-multiple-addresses-pro') ;
				}else{
					$heading = $get_heading ? $get_heading : __('','woocommerce-multiple-addresses-pro') ;
				}
				if($key != $address_key){
					//if(isset($key['billing_first_name']) && !empty($key['billing_first_name'])){
						$addresses[$type.'?atype='.$key] = $heading;
					//}
				}	
			}
			
			$addresses = ($addresses) ?  $addresses :  false ;
			return $addresses;
		}else{
			return false;
		}
		
	}


	 public static function get_address_fields($type){
    	$fields = WC()->countries->get_address_fields( WC()->countries->get_base_country(),$type.'_');
    	$fields_keys = array();
    	foreach ($fields as $key => $value) {
    		if(isset($value['custom']) && $value['custom']){
    			if(isset($value['user_meta']) && ($value['user_meta']==='yes')){
    				$fields_keys[] = $key;
    			}
    		}else{
    			$fields_keys[$key] = $key;

    		}
    	}
    	return $fields_keys;
    }

	public static function get_custom_addresses($user_id,$type=false,$address_key=false,$key=false){

		$custom_address = get_user_meta($user_id,self::ADDRESS_KEY,true);
		
		if(is_array($custom_address)){
			if($type && isset($custom_address[$type])){
				if($address_key){
					if($key ){
						if(isset($custom_address[$type][$address_key][$key])){
							return $custom_address[$type][$address_key][$key];
						}else{
							return false;
						}	
					}
					return $custom_address[$type][$address_key];
				}
				
				return $custom_address[$type];	
			}
		}
		return false;	
	}

	public static function save_address_to_user($user_id,$address,$type){
		
		$custom_addresses = get_user_meta($user_id,self::ADDRESS_KEY,true);
		$custom_addresses = is_array($custom_addresses) ? $custom_addresses : array();
		$saved_address = THWMA_Utils::get_custom_addresses($user_id,$type);
		
		if(!is_array($saved_address)){
			$custom_address = array();
			$default_address = self::get_default_address($user_id,$type);
			
			$default_heading = apply_filters('thwma_default_heading',false);
			if($default_heading ){
				if(!array_key_exists($type.'_heading',$default_address)){
					$default_address[$type.'_heading'] = __('Home','woocommerce-multiple-addresses-pro');
				}
			}else{
				if(!array_key_exists($type.'_heading',$default_address)){
					$default_address[$type.'_heading'] = __('','woocommerce-multiple-addresses-pro');
				}
			}
				
			if($default_address && array_filter($default_address) && (count(array_filter($default_address)))>2){
				
				$custom_address['address_0'] = $default_address;
			}

			$custom_address['address_1'] = $address;
			$custom_addresses[$type] = $custom_address;	
					
		}else{

			if(is_array($saved_address)) {
				if(isset($custom_addresses[$type])){
					$exist_custom = $custom_addresses[$type];
					$new_key_id = self::get_new_custom_id($user_id,$type);
					$new_key = 'address_'.$new_key_id;
					$custom_address[$new_key] = $address; 
					$custom_addresses[$type] = array_merge($exist_custom,$custom_address);		
				}
			}		
		}	
		
		update_user_meta($user_id,self::ADDRESS_KEY,$custom_addresses);
		
	}


	public static function get_default_address($user_id,$type){
		$fields = self::get_address_fields($type);
		$default_address = array();
		foreach ($fields as $key) {
			$default_address[$key] = get_user_meta($user_id,$key,true);
		}
		return $default_address;
	}

	public static function get_new_custom_id($user_id,$type){

		$custom_address = THWMA_Utils::get_custom_addresses($user_id,$type);
		if($custom_address){
			$all_keys = array_keys($custom_address);
			foreach ($all_keys as $key) {
				$key_ids[]=str_replace('address_','', $key);
	 		}
			$new_id=max($key_ids)+1;
			return $new_id;
		}
		
	} 

	public static function update_address_to_user($user_id,$address,$type,$address_key){
		$custom_addresses = get_user_meta($user_id,self::ADDRESS_KEY,true);
		$exist_custom = $custom_addresses[$type];
		$custom_address[$address_key] = $address;
		$exist_custom = is_array($exist_custom) ? $exist_custom :  array();

		$custom_addresses[$type] = array_merge($exist_custom,$custom_address);
		
		update_user_meta($user_id,self::ADDRESS_KEY,$custom_addresses);
	}

	public static function delete($user_id,$type,$custom){
		
		$custom_addresses = get_user_meta($user_id,self::ADDRESS_KEY,true);	
		unset($custom_addresses[$type][$custom]);
		update_user_meta($user_id,self::ADDRESS_KEY,$custom_addresses);
	}


	public static function is_same_address_exists($user_id,$type){
		
		$default_address = THWMA_Utils::get_default_address($user_id,$type);	
		$addresses = THWMA_Utils::get_custom_addresses($user_id,$type);
		if($addresses){
			foreach ($addresses as $key => $value) {
				$is_exit = self::is_same_address($default_address,$value);
				if($is_exit == true){
					return $key;
					break;
				}
			}
		}

		return false;
	}	


	

 	public static function is_same_address($address_1,$address_2){
		$is_same = true;
		foreach($address_1 as $key => $value) {
			if(!(isset($address_2[$key]) && isset($address_1[$key]) && $address_2[$key] == $address_1[$key])){
				$is_same = false;
					break;
			}
		
			return $is_same;
		}
		return false;
	}

	public static function get_all_addresses($customer_id,$name){
		
		$new_address_format = self::get_address_format($customer_id,$name);
		$addresses = WC()->countries->get_formatted_address($new_address_format);
		return $addresses;
	}

	public static function get_address_format($customer,$name){
		$key_id = substr($name, strpos($name,"=") + 1);
		$type =substr($name.'?', 0, strpos($name, '?'));
		$changed_address = array();
		$address = THWMA_Utils::get_custom_addresses($customer,$type,$key_id);
		$changed_address = self::get_formated_address($type,$address);
		return $changed_address;		
	}

	public static function get_formated_address($type,$address){
		$format_address=array();
		foreach ($address as $key => $value) {
			$format_key=str_replace($type.'_','',$key);
			$format_address[$format_key]=$value;
		}
		return $format_address;
	}

	
}

endif;