jQuery(window).ready(function($){
    "use strict";

    var tabs = $('.wpmc-tab-item');
    var sections = $('.wpmc-step-item');
    var buttons = $('.wpmc-nav-buttons button');
    var checkout_form = $('form.woocommerce-checkout');
    var coupon_form = $('#checkout_coupon');
    var before_form = $('#woocommerce_before_checkout_form');
    
    /** 
     * // A try to remove the Billing and Shipping step if the user is logged in 
    var logged_in = $('body').hasClass('logged-in');
    var remove_billing = true;

    if ( logged_in && remove_billing && $('.woocommerce-error').length === 0) {
        $('.wpmc-billing').remove();
        sections = sections.filter(function() {
            return !$(this).hasClass('wpmc-step-billing');
        });
        $('.wpmc-step-billing').removeClass('current');
        sections.first().addClass('current');
        var number_steps = sections.length;
        $('.wpmc-tabs-list').removeClass( 'wpmc-'+(number_steps+1)+'-tabs' ).addClass('wpmc-'+ number_steps+'-tabs');
        $('.wpmc-tab-number').each(function() {
            $(this).text( $(this).text() - 1 );
        });
    }
    */

    $('.woocommerce-checkout').on('wpmc_switch_tab', function(event, theIndex) {
        switchTab(theIndex);
    });

    $('.wpmc-step-item:first').addClass('current');

    // Find the current index
    function currentIndex() {
        return sections.index(sections.filter('.current'));
    }

    // Click on "next" button
    $('#wpmc-next, #wpmc-skip-login').on('click', function() {
        if ( wpmcValidateThisStep() === 0 ) {
            switchTab(currentIndex() + 1);
        } else {
            wpmcToggleError(true);
        }
    });

    // Click on "previous" button
    $('#wpmc-prev').on('click', function() {
      switchTab(currentIndex() - 1);
    });

    // After submit, switch tabs where the invalid fields are
    $(document).on('checkout_error', function() {

        if ( ! $('#createaccount').is(':checked') ) {
            $('#account_password_field').removeClass('woocommerce-invalid-required-field');
        }
        
        var section_class = $('.woocommerce-invalid-required-field').closest('.wpmc-step-item').attr('class');

        $('.wpmc-step-item').each(function(i) {
            if ($(this).attr('class') === section_class ) {
                switchTab(i)
            }
        })
    });

    // Is step clickable?
    function set_clickability() {
        $('.wpmc-tab-item').each(function() {
            $(this).removeClass('wpmc-not-clickable');
            if ( currentIndex() < $(this).index() - 1 && !$(this).hasClass('visited')) { 
                $(this).addClass('wpmc-not-clickable');
            }
        });
    }

    // Click on a step
    if ( WPMC.clickable_steps === '1' ) {
        set_clickability();

        $('.wpmc-tab-item').on('click', function() {

            if ( $(this).hasClass('current') ) return; 

            if ( currentIndex() < $(this).index() - 1 && !$(this).hasClass('visited')) return; 

            if ( currentIndex() < $(this).index() ) {
                // Go to a next step 
                if ( wpmcValidateThisStep() === 0 ) {
                    switchTab( $(this).index() );
                } else {
                    wpmcToggleError(true);
                }
            } else {
                // Go to a previous step
                switchTab( $(this).index() ); 
            }

        });
    }

    // Validate the current step
    function wpmcValidateThisStep() {

        if ( WPMC.validation_per_step !== '1' ) return 0;

        var errors = 0;

        // Empty required fields
        $('.wpmc-step-item.current .validate-required input, .wpmc-step-item.current .validate-required select').each(function(i) {
            if ( $(this).parents('.shipping_address').length > 0 && $('.shipping_address').is(':hidden') ) {
                return;
            }
            if ( $(this).parents('.create-account').length > 0 && $('.create-account').is(':hidden')) {
                return;
            }
            var v = parseFloat($(this).val());

            if ( ( !$(this).is(':radio') && $(this).val().length === 0) 
                || ($(this).is(':radio') && !$('.wpmc-step-item.current .validate-required input[name='+$(this).attr('name')+']').is(':checked'))
                || ($(this).is(':checkbox') && $(this).parents('.wpmc-step-login').length == 0 && !$(this).is(':checked')) 
                || ($(this).is('[type=number]') && (v < $(this).attr('min') || v > $(this).attr('max')))) {
                $(this).parents('.validate-required').removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
                errors ++;
            } else {
                $(this).parents('.validate-required').addClass( 'woocommerce-validated' ).removeClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
            }
        });

        // Email
        $('.wpmc-step-item.current .validate-email input').each(function(i) {
            if ( $(this).parents('.shipping_address').length > 0 && $('.shipping_address').is(':hidden') ) {
                return;
            }
            // the pattern from the woocommerce/assets/js/frontend/checkout.js file
			var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

            var this_field = $(this).parent().parent(); 

            if (!pattern.test( $(this).val() ) ) {
                errors ++;
				this_field.removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-email' );
            } else {
				this_field.addClass( 'woocommerce-validated' ).removeClass( 'woocommerce-invalid woocommerce-invalid-email' );
            }
        });
							
        return errors; 
    }


    // Switch the tab
    function switchTab(theIndex) {

        $('.woocommerce-checkout').trigger('wpmc_before_switching_tab');

        if ( theIndex < 0 || theIndex > sections.length - 1 ) return false;

        // scroll to top
        var diff = $('.wpmc-tabs-wrapper').offset().top - $(window).scrollTop();
        if ( diff < -40 ) {
            $('html, body').animate({
                scrollTop: $('.wpmc-tabs-wrapper').offset().top - 70, 
            }, 800);
        }

        $('html, body').promise().done(function() {

            tabs.removeClass('previous').filter('.current').addClass('previous').addClass('visited');
            sections.removeClass('previous').filter('.current').addClass('previous').addClass('visited');

            // Change the tab
            tabs.removeClass('current', {duration: 500});
            tabs.eq(theIndex).addClass('current', {duration: 500});
     
            // Change the section
            sections.removeClass('current', {duration: 500});
            sections.eq(theIndex).addClass('current', {duration: 500});

            // Which buttons to show?
            buttons.removeClass('current');
            checkout_form.addClass( 'processing' );
            coupon_form.hide();
            before_form.hide();

            // Show "next" button 
            if ( theIndex < sections.length - 1 ) $('#wpmc-next').addClass('current');

            // Show "skip login" button
            if ( theIndex === 0 && $('.wpmc-step-login').length > 0 ) {
                $("#wpmc-skip-login").addClass('current');
                $("#wpmc-next").removeClass('current');
            }
            // Last section
            if ( theIndex === sections.length - 1) {
              $("#wpmc-prev").addClass('current');
              $('#wpmc-submit').addClass('current');
              checkout_form.removeClass( 'processing' ).unblock();
            }
            // Show "previous" button 
            if ( theIndex != 0 ) $('#wpmc-prev').addClass('current');


            // todo.adal if third step (before last), trigger boxzilla message
            if ( theIndex === sections.length - 2) {

                Boxzilla.show(33340);
            }
            

            if( $('.wpmc-step-review.current').length > 0 ) {
                coupon_form.show();
            }

            if( $('.wpmc-'+before_form.data('step')+'.current').length > 0 ) {
                before_form.show();
            }

            $('.woocommerce-checkout').trigger('wpmc_after_switching_tab');

        });

            set_clickability();

    }


    function wpmcToggleError(showError) {
        if(showError) {
            var errorMsg = (WPMC.error_msg.length > 0 ) ? WPMC.error_msg : 'Please fix the errors on this step before moving to the next step';
            var errorStyle = 'width: ' + $('.wpmc-tabs-wrapper').width() + 'px; left: ' + Math.round($('.wpmc-tabs-wrapper').offset().left) + 'px;';
            $('.wpmc-error').remove();
            var thisError = '<div style="clear: both;"></div>'
                + '<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout wpmc-error" style="'+errorStyle+'"><ul class="woocommerce-error" role="alert">'
                + '   <li>' + errorMsg +'</li>'
                + '</ul></div>';
            $(thisError).insertAfter('.wpmc-tabs-wrapper').delay(3000).fadeOut(); 
        } else {
            $('.wpmc-error').remove();
        }
    }


    // Compatibility with Super Socializer
    if ( $('.the_champ_sharing_container').length > 0 ) {
        $('.the_champ_sharing_container').insertAfter($(this).parent().find('#checkout_coupon'));
    }

    // Prevent form submission on Enter
    $('.woocommerce-checkout').keydown(function(e) {
        if (e.which === 13) {
            e.preventDefault();
            return false;
        }
    });

    // "Back to Cart" button
    $('#wpmc-back-to-cart').click(function() {
        window.location.href = $(this).data('href'); 
    });

    // Switch tabs with <- and -> keyboard arrows
    if ( WPMC.keyboard_nav === '1' ) {
        $(document).keydown(function (e) {
          var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
          if ( key === 39) {
              if ( wpmcValidateThisStep() === 0 ) {
                switchTab(currentIndex() + 1);
              } else {
                wpmcToggleError(true);
              }
          }
          if ( key === 37) {
              switchTab(currentIndex() - 1);
          }
        });
    }

    // Change tab if the hash #step-0 is present in the URL
    if (window.location.hash) changeTabOnHash(window.location.hash);
    $(window).on("hashchange", function() { changeTabOnHash(window.location.hash) }); 
    function changeTabOnHash(hash) {
        if ( /step-[0-9]/.test(hash) ) {
            var step = hash.match(/step-([0-9])/)[1];
            switchTab(step);
        }
    }


    // Ripple effect on an element 
    $('.wpmc-ripple').on('click', function (event) {
          var elem = $(this);
          
          var ripple = $('<div/>')
            .addClass('wpmc-ripple-effect')
            .css({
              height    : elem.height(),
              width     : elem.width(),
              top       : event.pageY - elem.offset().top - elem.height()/2, 
              left      : event.pageX - elem.offset().left - elem.width()/2,
            }) 
            .appendTo(elem);

          window.setTimeout(function(){
            ripple.remove();
          }, 2000);
    });
});
