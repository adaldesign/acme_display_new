<?php
/*
	Plugin Name: Acme: Woocommerce SKUs Everywhere
	Description: Adds SKUs everywhere on the Acme site.
	Version: 1.0
	Author: Danny Battison
	Author URI: https://danny.gg
*/


add_filter(
    'woocommerce_cart_item_name',
    function ($linkText, $productData) {
        $sku = $productData['data']->get_sku();

        if (!empty($sku)) {
            $linkText .= "<p><small><strong>SKU:</strong> $sku</small></p>";
        }

        return $linkText;
    },
    10,
    2
);

add_filter(
    'woocommerce_order_item_meta_start',
    function ($itemId, $item, $order) {
        $sku = $item->get_product()->get_sku();

        if (empty($sku)) {
            return;
        }

        echo "<p><small><strong>SKU:</strong> $sku</small></p>";
    },
    10,
    3
);
