<style type="text/css">

	#woocommerce_bulk_pricing_rules table.rules {
		width: 400px;
		padding: 5px 9px;
	}

    #woocommerce_bulk_pricing_rules table.rules th {
		text-align: left;
		width: 100px;
	}

    #woocommerce_bulk_pricing_rules table.rules td input.rule {
        width: 90%;
    }

    .woocommerce_bulk_pricing_variation_rules table.rules {
        padding: 0;
    }

    #woocommerce_bulk_pricing_rules {
        /*padding: 10px;*/
    }

</style>


    <table class="rules">
        <tr>
            <th>From Quantity</th>
            <th>To Quantity</th>
            <th>Discounted Price</th>
        </tr>
        <?php $rows_limit = isset( $wpl_item['rows_limit'] ) ? $wpl_item['rows_limit'] : 5; ?>
        <?php for ($i=0; $i < $rows_limit; $i++) : ?>
        <?php
            if ( ( $i == 0 ) && ( get_option('wc_bulk_pricing_expert_mode_enabled', 0 ) == 0 ) ) {
                // set default values for first row
                $wpl_item['rules'][$i]['min'] = 1;
                $wpl_item['rules'][$i]['val'] = '100%';
                if (! $wpl_item['id'] ) $wpl_item['rules'][$i]['max'] = '*';
            }

            $field_prefix = 'rules['.$i.']';
            if ( isset( $wpl_item['loop'] ) ) {
                $field_prefix = 'var_rules['.$wpl_item['loop'].']['.$i.']';
            }

            // echo "<pre>";print_r($field_prefix);echo"</pre>";die();
        ?>
        <tr>
            <td>
                <input type="text" name="<?php echo $field_prefix ?>[min]" class="rule" value="<?php echo isset( $wpl_item['rules'][$i]['min'] ) ? $wpl_item['rules'][$i]['min'] : '' ?>"/>
            </td>
            <td>
                <input type="text" name="<?php echo $field_prefix ?>[max]" class="rule" value="<?php echo isset( $wpl_item['rules'][$i]['max'] ) ? $wpl_item['rules'][$i]['max'] : '' ?>"/>
            </td>
            <td>
                <input type="text" name="<?php echo $field_prefix ?>[val]" class="rule" value="<?php echo isset( $wpl_item['rules'][$i]['val'] ) ? $wpl_item['rules'][$i]['val'] : '' ?>"/>
            </td>
        </tr>
        <?php endfor; ?>
    </table>
    <br class="clear" />

    <!--
    <p class="" style="display: block;">
        <input type="checkbox" id="is_cumulative" name="wpl_is_cumulative" />
        <label for="is_cumulative">is cumulative</label>
    </p>
    -->



