<?php include_once( dirname(__FILE__).'/common_header.php' ); ?>

<style type="text/css">

	th.column-post_id {
		width: 15%;
	}
    th {
        text-align:left;
    }

	td {
		line-height: 25px;
	}

    /* general styles for edit pages (edit template, edit profile and settings) */
    .wrap .postbox .inside {
        margin-bottom: 15px;
    }

    .wrap .postbox .inside p.desc {
        font-size: smaller;
        font-style: italic;
        margin-top: 0;
        margin-left: 35%;
    }
    .wrap .postbox .inside p.edit_links {
        margin-top: 0;
        margin-left: 35%;
    }

    .wrap .postbox label.text_label {
        display: block;
        float: left;
        width: 72%;
        margin: 1px;
        padding: 3px;
    }
    .wrap .postbox input.text_input,
    .wrap .postbox textarea,
    .wrap .postbox select.select {
        width: 25%;
        margin-bottom: 5px;
        padding: 3px 8px;
    }
    .wrap .postbox textarea {
        height: 120px;
    }

    .wrap #DiscountRolesBox label.text_label {
        width: 42%;
    }
    .wrap #DiscountRolesBox input.text_input {
        width: 20%;
    }
    .wrap #DiscountRolesBox select.select {
        width: 35%;
    }

</style>

<div class="wrap">
	<div class="icon32 woocommerce-bulk-pricing" id="wpl-icon"><br /></div>
    <?php include_once( dirname(__FILE__).'/common_tabs.php' ); ?>
	<?php #echo $wpl_message ?>
    <?php #echo "<pre>";print_r($wpl_role_discounts);echo"</pre>";die(); ?>
    <?php #echo "<pre>";print_r($wpl_role_discount_modes);echo"</pre>";#die(); ?>

    <form method="post" action="<?php echo $wpl_form_action; ?>">

    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">

            <div id="postbox-container-1" class="postbox-container">
                <div id="side-sortables" class="meta-box">

                    <div class="postbox" id="submitdiv">
                        <h3><span><?php echo __('Update','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <div id="submitpost" class="submitbox">

                                <div id="major-publishing-actions">
                                    <div id="publishing-action">
                                        <input type="hidden" name="action" value="update_role_settings" />
                                        <input type="submit" value="<?php echo __('Update','wc_bulk_pricing'); ?>" id="publish" class="button-primary" name="save">
                                    </div>
                                    <div class="clear"></div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div> <!-- #postbox-container-1 -->


            <!-- #postbox-container-2 -->
            <div id="postbox-container-2" class="postbox-container">
                <div class="meta-box-sortables ui-sortable">
                    
                    <div class="postbox" id="DiscountRolesBox">
                        <h3><span><?php echo __('Discounts per User Role','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <?php foreach ($wpl_available_roles as $role => $role_name) : ?>

                                <label for="role_discount_<?php echo $role ?>" class="text_label"><?php echo $role_name; ?></label>
                                <input type="text" 
                                       name="role_discount[<?php echo $role ?>]" 
                                       id="role_discount_<?php echo $role ?>" class="text_input" 
                                       placeholder="Enter percentage"
                                       value="<?php echo @$wpl_role_discounts[$role] ?>"/>

                                <select name="role_discount_mode[<?php echo $role ?>]" 
                                        id="role_discount_mode_<?php echo $role ?>" 
                                        class="select" />
                                
                                    <option value="0" <?php if ( @$wpl_role_discount_modes[$role] != '1' ): ?>selected="selected"<?php endif; ?> >Don't apply bulk pricing</option>
                                    <option value="1" <?php if ( @$wpl_role_discount_modes[$role] == '1' ): ?>selected="selected"<?php endif; ?> >Combine with bulk pricing</option>
                                </select>
                                <!-- <option value="<?php echo $role ?>" <?php if ( @$wpl_item['user_roles'] == $role ): ?>selected="selected"<?php endif; ?>><?php echo $role_name ?></option> -->

                            <?php endforeach; ?>
                            <p>
                                <?php echo __('Enter a percentage next to a user role to enable global cart discounts for all customers who are assigned to this role.','wc_bulk_pricing'); ?>
                            </p>

                        </div>
                    </div>

                        
                    <div class="postbox" id="ExcludeRolesBox">
                        <h3><span><?php echo __('Exclude Roles','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <?php foreach ($wpl_available_roles as $role => $role_name) : ?>

                                <label for="exclude_roles_<?php echo $role ?>" class="text_label"><?php echo $role_name; ?></label>
                                <select name="exclude_roles[<?php echo $role ?>]" 
                                        id="exclude_roles_<?php echo $role ?>" 
                                        class="select" />
                                
                                    <option value=""  <?php if ( ! in_array( $role, $wpl_exclude_roles ) ): ?>selected="selected"<?php endif; ?> >Enabled</option>
                                    <option value="1" <?php if ( in_array( $role, $wpl_exclude_roles ) ): ?>selected="selected"<?php endif; ?> >Disable bulk pricing</option>
                                </select>

                                <!-- <option value="<?php echo $role ?>" <?php if ( @$wpl_item['user_roles'] == $role ): ?>selected="selected"<?php endif; ?>><?php echo $role_name ?></option> -->

                            <?php endforeach; ?>
                            <br clear="both">
                            <p>
                                <?php echo __('Disable all bulk pricing discounts for certain user roles.','wc_bulk_pricing'); ?>
                            </p>

                        </div>
                    </div>
                        
                </div> <!-- .meta-box-sortables -->
            </div> <!-- #postbox-container-1 -->



        </div> <!-- #post-body -->
        <br class="clear">
    </div> <!-- #poststuff -->

    </form>


    <?php if ( get_option('wc_bulk_pricing_log_level') > 6 ): ?>
    <pre><?php print_r($wpl_item); ?></pre>
    <?php endif; ?>


    <script type="text/javascript">
        jQuery( document ).ready(
            function () {

            }
        );
    </script>

</div>
