<?php include_once( dirname(__FILE__).'/common_header.php' ); ?>

<style type="text/css">

	th.column-post_id {
		width: 15%;
	}
    th {
        text-align:left;
    }

	td {
		line-height: 25px;
	}

    /* general styles for edit pages (edit template, edit profile and settings) */
    .wrap .postbox .inside {
        margin-bottom: 15px;
    }

    .wrap .postbox .inside p.desc {
        font-size: smaller;
        font-style: italic;
        margin-top: 0;
        margin-left: 50%;
    }

    .wrap .postbox label.text_label {
        display: block;
        float: left;
        width: 48%;
        margin: 1px;
        padding: 3px;
    }
    .wrap .postbox input.text_input,
    .wrap .postbox textarea,
    .wrap .postbox select.select {
        width: 50%;
        margin-bottom: 5px;
        padding: 3px 8px;
    }
    .wrap .postbox textarea {
        height: 120px;
    }


</style>

<div class="wrap">
	<div class="icon32 woocommerce-bulk-pricing" id="wpl-icon"><br /></div>
    <?php include_once( dirname(__FILE__).'/common_tabs.php' ); ?>
	<?php #echo $wpl_message ?>


    <form method="post" action="<?php echo $wpl_form_action; ?>">

    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">

            <div id="postbox-container-1" class="postbox-container">
                <div id="side-sortables" class="meta-box">

                    <div class="postbox" id="submitdiv">
                        <h3><span><?php echo __('Update','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <div id="submitpost" class="submitbox">

                                <div id="major-publishing-actions">
                                    <div id="publishing-action">
                                        <input type="hidden" name="action" value="update_global_settings" />
                                        <input type="submit" value="<?php echo __('Update','wc_bulk_pricing'); ?>" id="publish" class="button-primary" name="save">
                                    </div>
                                    <div class="clear"></div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div> <!-- #postbox-container-1 -->


            <!-- #postbox-container-2 -->
            <div id="postbox-container-2" class="postbox-container">
                <div class="meta-box-sortables ui-sortable">
                    
                    <div class="postbox" id="GeneralSettingsBox">
                        <h3><span><?php echo __('Rounding Options','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <p>
                                <?php echo __('Select to how many decimals a single item discount price should be rounded.','wc_bulk_pricing'); ?><br>
                                <?php echo __('In rare cases you might want to use higher precision than the default of two decimals.','wc_bulk_pricing'); ?>
                            </p>

                            <!-- <label for="wpl-display_price_decimals" class="text_label"><?php echo __('Round decimals','wc_bulk_pricing'); ?>:</label> -->
                            <!-- <input type="text" name="wpl_display_price_decimals" id="wpl-display_price_decimals" value="<?php echo $wpl_display_price_decimals; ?>" class="text_input" /> -->

                            <label for="wpl-display_price_decimals" class="text_label"><?php echo __('Display single item price','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_display_price_decimals" id="wpl-display_price_decimals" class="select" >
                                <option value="0" <?php echo $wpl_display_price_decimals == '0' ? 'selected':'' ?> >Round to 0 decimals</option>
                                <option value="2" <?php echo $wpl_display_price_decimals == '2' ? 'selected':'' ?> >Round to 2 decimals</option>
                                <option value="3" <?php echo $wpl_display_price_decimals == '3' ? 'selected':'' ?> >Round to 3 decimals</option>
                                <option value="4" <?php echo $wpl_display_price_decimals == '4' ? 'selected':'' ?> >Round to 4 decimals</option>
                                <option value="6" <?php echo $wpl_display_price_decimals == '6' ? 'selected':'' ?> >Round to 6 decimals</option>
                            </select>

                            <label for="wpl-round_to_decimals" class="text_label"><?php echo __('Round internally','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_round_to_decimals" id="wpl-round_to_decimals" class="select" >
                                <option value="1" <?php echo $wpl_round_to_decimals == '1' ? 'selected':'' ?> >Yes, round to the number of decimals above</option>
                                <option value="0" <?php echo $wpl_round_to_decimals != '1' ? 'selected':'' ?> >No, calculate with inifinite precision</option>
                            </select>

                            <label for="wpl-display_extra_decimals" class="text_label"><?php echo __('Format extra decimals','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_display_extra_decimals" id="wpl-display_extra_decimals" class="select" >
                                <option value="1" <?php echo $wpl_display_extra_decimals == '1' ? 'selected':'' ?> >Show extra decimals as superscript</option>
                                <option value="2" <?php echo $wpl_display_extra_decimals == '2' ? 'selected':'' ?> >Wrap extra decimals in SPAN tags</option>
                                <option value="3" <?php echo $wpl_display_extra_decimals == '3' ? 'selected':'' ?> >Don't add extra markup</option>
                            </select>

                            <p>
                                <b>When do I need this?</b><br><br>
                                Let's say you have an item for $3.25 and you want to create a "buy one, get one free" discount rule, 
                                so you define a discount of 50% for 2 items or more.
                                <br><br>
                                Now what happens by default is that the plugin will take 50 percent from $3.25 which is $1.625,
                                and rounds this value to two decimals which becomes $1.63. But $1.63 times two items is $3.26 - 
                                which is one cent more than it should be in that particular case.
                                <br><br>
                                So if you actually need that additional precision math, you have two options:
                                <ol>
                                    <li>You could just set the "Internal rounding" option to No, which will still display the unit price rounded as $1.63,
                                        but use the exact value of $1.625 internally so the cart total will be $3.25.</li>
                                    <li>Or you could show the exact price to your customers as well by selecting "Round to 3 decimals" above, 
                                        which will show the price as $1.62<sup>5</sup>.</li>
                                </ol>
                            </p>

                        </div>
                    </div>

                        
                    <div class="postbox" id="OtherSettingsBox">
                        <h3><span><?php echo __('Other Options','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <label for="wpl-default_number_of_rows" class="text_label"><?php echo __('Default number of rows','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_default_number_of_rows" id="wpl-default_number_of_rows" class="select" >
                                <option value="5"  <?php echo $wpl_default_number_of_rows == '5'  ? 'selected':'' ?> >5 rows</option>
                                <option value="10" <?php echo $wpl_default_number_of_rows == '10' ? 'selected':'' ?> >10 rows</option>
                                <option value="20" <?php echo $wpl_default_number_of_rows == '20' ? 'selected':'' ?> >20 rows</option>
                            </select>

                            <label for="wpl-show_used_ruleset_in_cart" class="text_label"><?php echo __('Display profile title in cart','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_show_used_ruleset_in_cart" id="wpl-show_used_ruleset_in_cart" class="select" >
                                <option value="0" <?php echo $wpl_show_used_ruleset_in_cart != '1' ? 'selected':'' ?> >No</option>
                                <option value="1" <?php echo $wpl_show_used_ruleset_in_cart == '1' ? 'selected':'' ?> >Yes</option>
                            </select>

                            <label for="wpl-coupon_handling_mode" class="text_label"><?php echo __('Coupon handling','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_coupon_handling_mode" id="wpl-coupon_handling_mode" class="select" >
                                <option value="default" <?php echo $wpl_coupon_handling_mode == 'default' ? 'selected':'' ?> >Default</option>
                                <option value="coupon" <?php echo $wpl_coupon_handling_mode == 'coupon' ? 'selected':'' ?> >Ignore discount rules when coupon is applied</option>
                            </select>

                            <label for="wpl-disable_on_sale_products" class="text_label"><?php echo __('Disable on Sale Products','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_disable_on_sale_products" id="wpl-disable_on_sale_products" class="select" >
                                <option value="0" <?php echo $wpl_disable_on_sale_products == 0 ? 'selected':'' ?> >No (default)</option>
                                <option value="1" <?php echo $wpl_disable_on_sale_products == 1 ? 'selected':'' ?> >Yes, Ignore discount rules when product is on sale</option>
                            </select>

                            <label for="wpl-default_header" class="text_label"><?php echo __('Discount info header','wc_bulk_pricing'); ?>:</label>
                            <input name="wpl_default_header" id="wpl-default_header" type="text" class="text_input" value="<?php echo $wpl_default_header ?>" />

                            <label for="wpl-default_display_action_hook" class="text_label"><?php echo __('Discount info position','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_default_display_action_hook" id="wpl-default_display_action_hook" class="select" >
                                <option value="woocommerce_single_product_summary"          <?php echo $wpl_default_display_action_hook == 'woocommerce_single_product_summary' ? 'selected':'' ?>          >Default (product summary)</option>
                                <option value="woocommerce_before_single_product_summary"   <?php echo $wpl_default_display_action_hook == 'woocommerce_before_single_product_summary' ? 'selected':'' ?>   >before product summary</option>
                                <option value="woocommerce_template_single_title"           <?php echo $wpl_default_display_action_hook == 'woocommerce_template_single_title' ? 'selected':'' ?>           >before title</option>
                                <option value="woocommerce_template_single_price"           <?php echo $wpl_default_display_action_hook == 'woocommerce_template_single_price' ? 'selected':'' ?>           >before price</option>
                                <option value="woocommerce_template_single_excerpt"         <?php echo $wpl_default_display_action_hook == 'woocommerce_template_single_excerpt' ? 'selected':'' ?>         >before excerpt</option>
                                <option value="woocommerce_template_single_add_to_cart"     <?php echo $wpl_default_display_action_hook == 'woocommerce_template_single_add_to_cart' ? 'selected':'' ?>     >before add to cart button</option>
                                <option value="woocommerce_template_single_meta"            <?php echo $wpl_default_display_action_hook == 'woocommerce_template_single_meta' ? 'selected':'' ?>            >before meta</option>
                                <option value="woocommerce_after_single_product_summary"    <?php echo $wpl_default_display_action_hook == 'woocommerce_after_single_product_summary' ? 'selected':'' ?>    >after product summary</option>

                                <option value="woocommerce_product_meta_start"              <?php echo $wpl_default_display_action_hook == 'woocommerce_product_meta_start' ? 'selected':'' ?>              >product meta start</option>
                                <option value="woocommerce_product_meta_end"                <?php echo $wpl_default_display_action_hook == 'woocommerce_product_meta_end' ? 'selected':'' ?>                >product meta end</option>
                                <option value="woocommerce_before_single_product"           <?php echo $wpl_default_display_action_hook == 'woocommerce_before_single_product' ? 'selected':'' ?>           >before single product</option>
                                <option value="woocommerce_after_single_product"            <?php echo $wpl_default_display_action_hook == 'woocommerce_after_single_product' ? 'selected':'' ?>            >after single product</option>
                                <option value="woocommerce_before_main_content"             <?php echo $wpl_default_display_action_hook == 'woocommerce_before_main_content' ? 'selected':'' ?>             >before main content</option>
                                <option value="woocommerce_after_main_content"              <?php echo $wpl_default_display_action_hook == 'woocommerce_after_main_content' ? 'selected':'' ?>              >after main content</option>
                            </select>
                            <p class="desc">
                                <?php echo __('This option allows you to control the position of the discount info table on the product page.','wc_bulk_pricing'); ?><br>
                                <?php echo __('Some action hooks might not be supported by your WordPress theme.','wc_bulk_pricing'); ?><br>
                            </p>

                            <label for="wpl-default_display_priority" class="text_label"><?php echo __('Discount info priority','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_default_display_priority" id="wpl-default_display_priority" class="select" >
                                <option value="5"  <?php echo $wpl_default_display_priority == '5'  ? 'selected':'' ?> >5</option>
                                <option value="10" <?php echo $wpl_default_display_priority == '10' ? 'selected':'' ?> >10</option>
                                <option value="20" <?php echo $wpl_default_display_priority == '20' ? 'selected':'' ?> >20 (default)</option>
                                <option value="30" <?php echo $wpl_default_display_priority == '30' ? 'selected':'' ?> >30</option>
                                <option value="40" <?php echo $wpl_default_display_priority == '40' ? 'selected':'' ?> >40</option>
                                <option value="50" <?php echo $wpl_default_display_priority == '50' ? 'selected':'' ?> >50</option>
                                <option value="100" <?php echo $wpl_default_display_priority == '100' ? 'selected':'' ?> >100</option>
                            </select>
                            <p class="desc">
                                <?php echo __('This option allows you to control the position of the discount info table on the product page.','wc_bulk_pricing'); ?><br>
                                <?php echo __('Lower values move the block up, higher values move it down.','wc_bulk_pricing'); ?><br>
                            </p>

                            <label for="wpl-expert_mode_enabled" class="text_label"><?php echo __('Enable expert mode','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_expert_mode_enabled" id="wpl-expert_mode_enabled" class="select" >
                                <option value="0" <?php echo $wpl_expert_mode_enabled != '1' ? 'selected':'' ?> >No</option>
                                <option value="1" <?php echo $wpl_expert_mode_enabled == '1' ? 'selected':'' ?> >Yes</option>
                            </select>
                            <p class="desc">
                                <?php echo __('Expert mode will enable you to have rulesets that do not begin with 1 unit at 100%.','wc_bulk_pricing'); ?><br>
                            </p>

                            <label for="wpl-cumulate_custom_rules" class="text_label"><?php echo __('Rule scope for custom rules','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_cumulate_custom_rules" id="wpl-cumulate_custom_rules" class="select" >
                                <option value="0" <?php echo $wpl_cumulate_custom_rules != '1' ? 'selected':'' ?> >Exclusive</option>
                                <option value="1" <?php echo $wpl_cumulate_custom_rules == '1' ? 'selected':'' ?> >Cumulative</option>
                            </select>
                            <p class="desc">
                                <?php echo __('Set the rule scope for custom product rulesets.','wc_bulk_pricing'); ?><br>
                            </p>

                            <label for="wpl-show_lowest_price" class="text_label"><?php echo __('Show lowest price','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_show_lowest_price" id="wpl-show_lowest_price" class="select" >
                                <option value="0" <?php echo $wpl_show_lowest_price != '1' ? 'selected':'' ?> >No</option>
                                <option value="1" <?php echo $wpl_show_lowest_price == '1' ? 'selected':'' ?> >Yes</option>
                            </select>
                            <p class="desc">
                                <?php echo __('Select if you want to show the lowest price for products with custom product rulesets.','wc_bulk_pricing'); ?><br>
                            </p>

                            <label for="wpl-variation_discount_table_display" class="text_label"><?php echo __('Discount Table Display on Variation','wc_bulk_pricing'); ?>:</label>
                            <select name="wpl_variation_discount_table_display" id="wpl-variation_discount_table_display" class="select" >
                                <option value="show" <?php selected( 'show', $wpl_variation_display_mode ); ?> ><?php _e('Show the discount table', 'wc_bulk_pricing'); ?></option>
                                <option value="hide" <?php selected( 'hide', $wpl_variation_display_mode ); ?> ><?php _e('Hide until a variation has been selected', 'wc_bulk_pricing'); ?></option>
                            </select>
                            <p class="desc">
                                <?php echo __('Select whether you want the discount table to be displayed or hidden when no options are selected on variation products.','wc_bulk_pricing'); ?><br>
                            </p>

                        </div>
                    </div>

                        
                        
                </div> <!-- .meta-box-sortables -->
            </div> <!-- #postbox-container-1 -->



        </div> <!-- #post-body -->
        <br class="clear">
    </div> <!-- #poststuff -->

    </form>


    <?php if ( get_option('wc_bulk_pricing_log_level') > 6 ): ?>
    <pre><?php print_r($wpl_item); ?></pre>
    <?php endif; ?>


    <script type="text/javascript">
        jQuery( document ).ready(
            function () {

            }
        );
    </script>

</div>
