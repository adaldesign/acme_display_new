<?php include_once( dirname(__FILE__).'/common_header.php' ); ?>

<style type="text/css">

	th.column-term_id {
		width: 15%;
	}
    th {
        text-align:left;
    }

	td {
		line-height: 20px;
	}

    .wp-admin div.select_ruleset_wrapper {
        display: none;
    }


@media screen and (-webkit-min-device-pixel-ratio:0) {

    /* Chrome only */
    .wp-admin select.select_ruleset {
        /*height: 16px;*/
    }

}

</style>

<div class="wrap checkbox_autostyle">
	<div class="icon32 woocommerce-bulk-pricing" id="wpl-icon"><br /></div>
    <?php include_once( dirname(__FILE__).'/common_tabs.php' ); ?>

	<!-- <h2><?php echo __('Bulk Pricing Categories','wc_bulk_pricing') ?></h2> -->
	<?php #echo $wpl_message ?>


	<!-- show log table -->
    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <form id="bulk_pricing_categories" method="post" action="<?php echo $wpl_form_action; ?>" >
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <input type="hidden" name="action" value="update_bulk_pricing_categories" />

        <!-- Now we can render the completed list table -->
        <?php $wpl_categoriesTable->display() ?>
    
        <div class="submit" style="">
            <!-- <a href="#" id="btn_add_ruleset" class="button-primary">add new profile</a> -->
            <input type="submit" class="button-primary" value="Update categories" />
        </div>

    </form>

	<br style="clear:both;"/>


    <pre><?php #print_r( get_option('wc_bulk_pricing_categories') ); ?></pre>

</div>


<script type="text/javascript">
    jQuery(document).ready(function() {


        // add ruleset button
        jQuery('.btn_add_ruleset').click( function(){
            var self = jQuery(this);
            var parent = self.parent();
            var selectFields = parent.find('select');

            // clone select field and button
            selectFields.first().clone().appendTo( parent );
            parent.find('.btn_del_ruleset').first().clone(true).appendTo( parent ).show();

            return false;
        });

        // delete ruleset button
        jQuery('.btn_del_ruleset').click( function(){
            var self = jQuery(this);

            // remove select field and button
            self.prev().remove();
            self.remove();

            return false;
        });


        // handle enabled / disable button
        jQuery('input.ipstyle').change( function(){
            var self = jQuery(this);
            if ( 'checked' == self.attr('checked') ) {
                jQuery('#select_'+this.id).fadeIn(200);
            } else {
                jQuery('#select_'+this.id).fadeOut(200);
            }
        });

        // init visibility states
        jQuery('input.ipstyle').each( function(){
            var self = jQuery(this);
            if ( 'checked' == self.attr('checked') ) {
                jQuery('#select_'+this.id).show();
            } else {
                // jQuery('#select_'+this.id).hide();
            }
        });

    });

</script>