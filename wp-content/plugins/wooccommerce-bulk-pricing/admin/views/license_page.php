<?php include_once( dirname(__FILE__).'/common_header.php' ); ?>

<style type="text/css">


    /* general styles for edit pages (edit template, edit profile and settings) */
    .wrap .postbox .inside {
        margin-bottom: 15px;
    }

    .wrap .postbox .inside p.desc {
        font-size: smaller;
        font-style: italic;
        margin-top: 0;
        margin-left: 35%;
    }
    .wrap .postbox .inside p.edit_links {
        margin-top: 0;
        margin-left: 35%;
    }

    .wrap .postbox label.text_label {
        display: block;
        float: left;
        width: 33%;
        margin: 1px;
        padding: 3px;
    }
    .wrap .postbox input.text_input,
    .wrap .postbox textarea,
    .wrap .postbox select.select {
        width: 65%;
        margin-bottom: 5px;
        padding: 3px 8px;
    }
    .wrap .postbox textarea {
        height: 120px;
    }

</style>

<div class="wrap">
	<div class="icon32 woocommerce-bulk-pricing" id="wpl-icon"><br /></div>
    <?php include_once( dirname(__FILE__).'/common_tabs.php' ); ?>

	<!-- <h2><?php echo __('Bulk Pricing Categories','wc_bulk_pricing') ?></h2> -->
	<?php #echo $wpl_message ?>


    <div style="width:60%;min-width:640px;" class="postbox-container">
        <div class="metabox-holder">
            <div class="meta-box-sortables ui-sortable">


                <?php #if ( class_exists('WPL_CustomUpdater') ) : ?>
                <form method="post" action="<?php echo $wpl_form_action; ?>">
                    <input type="hidden" name="action" value="save_wc_bulk_pricing_license" >

                    <div class="postbox" id="LicenseBox" style="">
                        <h3 class="hndle"><span><?php echo __('License','wc_bulk_pricing') ?></span></h3>
                        <div class="inside">

                            <label for="wpl-text-license_email" class="text_label"><?php echo __('License email','wc_bulk_pricing'); ?>:</label>
                            <input type="text" name="wc_bulk_pricing_license_email" id="wpl-text-license_email" value="<?php echo $wpl_license_email; ?>" class="text_input" />

                            <label for="wpl-text-license_key" class="text_label"><?php echo __('License key','wc_bulk_pricing'); ?>:</label>
                            <input type="text" name="wc_bulk_pricing_license_key" id="wpl-text-license_key" value="<?php echo $wpl_license_key; ?>" class="text_input" />
<!--                            <p class="desc" style="display: block; font-style: normal">
                                <?php if ( $wpl_license_activated == '1' ) : ?>
                                    <?php echo __('Your license has been activated for','wc_bulk_pricing'); ?>
                                    <?php echo str_replace( 'http://','', get_bloginfo( 'url' ) ) ?>
                                <?php elseif ( $wpl_license_key != '' ): ?>
                                    <b><?php echo __('Your license has not been activated.','wc_bulk_pricing'); ?></b><br>
                                    <?php echo __('Please check if your license key matches your email address.','wc_bulk_pricing'); ?>
                                <?php endif; ?>
                            </p>
 -->
                            <?php if ( $wpl_license_activated == '1' ) : ?>

                                <label for="wpl-deactivate_license" class="text_label"><?php echo __('Deactivate license','wc_bulk_pricing'); ?>:</label>
                                <input type="checkbox" name="wc_bulk_pricing_deactivate_license" id="wpl-deactivate_license" value="1" class="checkbox_input" />
                                <span style="line-height: 24px">
                                    <?php echo __('Yes, I want to deactivate this license for','wc_bulk_pricing'); ?>
                                    <i><?php echo str_replace( 'http://','', get_bloginfo( 'url' ) ) ?></i>
                                </span>
                                
                            <?php endif; ?>
                        
                        </div>
                    </div>

                    <div class="postbox" id="UpdateSettingsBox">
                        <h3 class="hndle"><span><?php echo __('Beta testers','wc_bulk_pricing') ?></span></h3>
                        <div class="inside">

                            <p>
                                <?php echo __('If you want to test new features before they are released, select the "beta" channel.','wc_bulk_pricing'); ?>
                            </p>
                            <label for="wpl-option-update_channel" class="text_label"><?php echo __('Update channel','wc_bulk_pricing'); ?>:</label>
                            <select id="wpl-option-update_channel" name="wc_bulk_pricing_update_channel" title="Update channel" class=" required-entry select">
                                <option value="stable" <?php if ( $wpl_update_channel == 'stable' ): ?>selected="selected"<?php endif; ?>><?php echo __('stable','wc_bulk_pricing'); ?></option>
                                <option value="beta" <?php if ( $wpl_update_channel == 'beta' ): ?>selected="selected"<?php endif; ?>><?php echo __('beta','wc_bulk_pricing'); ?></option>
                            </select>

                        </div>
                    </div>

                    <div class="submit" style="padding-top: 0; float: left;">
                        <a href="<?php echo $wpl_form_action ?>&action=force_update_check" class="button-secondary"><?php echo __('Force update check','wc_bulk_pricing'); ?></a> 
                    </div>
                    <div class="submit" style="padding-top: 0; float: right;">
                        <a href="<?php echo $wpl_form_action ?>&action=wbp_check_license_status" class="button-secondary"><?php echo __('Check license activation','wc_bulk_pricing'); ?></a>                         
                        &nbsp;
                        <input type="submit" value="<?php echo __('Update settings','wc_bulk_pricing') ?>" name="submit" class="button-primary">
                    </div>
                </form>
                <?php #endif; ?>


            </div>
        </div>
    </div>




</div>

