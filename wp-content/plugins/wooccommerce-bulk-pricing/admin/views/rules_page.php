<?php include_once( dirname(__FILE__).'/common_header.php' ); ?>

<style type="text/css">

	th.column-id {
		width: 15%;
	}
	th {
		text-align:left;
	}

    td.column-rules {
        overflow-x: hidden;
    }

	.widefat table td {
		border-bottom: none;
	}

    #titlediv #title {
        width: 50%;
    }

    #import_export_container a.button-secondary {
        min-width: 170px;
        margin-right: 10px;
    }

</style>

<div class="wrap">
	<div class="icon32 woocommerce-bulk-pricing" id="wpl-icon"><br /></div>
    <?php include_once( dirname(__FILE__).'/common_tabs.php' ); ?>

	<!-- <h2><?php echo __('Bulk Pricing','wc_bulk_pricing') ?></h2> -->
	<?php #echo $wpl_message ?>


	<!-- show log table -->
    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <form id="rules-filter" method="post" action="<?php echo $wpl_form_action; ?>" >
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <!-- Now we can render the completed list table -->
        <?php $wpl_rulesTable->display() ?>
    </form>

	<!br style="clear:both;"/>

    <div class="submit" style="">
        <a href="<?php echo $wpl_form_action ?>&action=edit" class="button-secondary"><?php echo __('Add new discount profile','wc_bulk_pricing'); ?></a> 
        &nbsp;
        <a href="#" onclick="jQuery('#import_export_container').slideToggle();return false;" class="button-secondary"><?php echo __('Import / Export','wc_bulk_pricing'); ?></a> 
    </div>

    <div id="import_export_container" style="display:none">
        <h3><?php echo __('Export profiles','wc_bulk_pricing'); ?></h3>

            <p>
                <a href="<?php echo $wpl_form_action ?>&action=wc_bp_export_rulesets&mode=json" class="button-secondary"><?php echo __('Export profiles as JSON','wc_bulk_pricing'); ?></a> 
                Create a backup of your discount profiles including all options.
            </p><p>
                <a href="<?php echo $wpl_form_action ?>&action=wc_bp_export_rulesets&mode=csv" class="button-secondary"><?php echo __('Export profiles as CSV','wc_bulk_pricing'); ?></a> 
                Use this to batch edit your discount profiles in Excel or LibreOffice.
            </p>

        <h3><?php echo __('Import profiles','wc_bulk_pricing'); ?></h3>

            <p>
                <form id="upload_json" method="post" action="<?php echo $wpl_form_action; ?>" enctype="multipart/form-data" >

                    <a href="#" onclick="alert('Please select a file using the button right next to this button.');return false;" class="button-secondary"><?php echo __('Import profiles from JSON','wc_bulk_pricing'); ?></a> 

                    <input type="hidden" name="action" value="wc_bp_import_rulesets" />
                    <input type="hidden" name="mode" value="json" />
                    <input type="file" name="wpl_file_upload" onchange="this.form.submit();" />

                    Restore all profiles from backup.
                </form>
            </p><p>
                <form id="upload_json" method="post" action="<?php echo $wpl_form_action; ?>" enctype="multipart/form-data" >

                    <a href="#" onclick="alert('Please select a file using the button right next to this button.');return false;" class="button-secondary"><?php echo __('Import profiles from CSV','wc_bulk_pricing'); ?></a> 

                    <input type="hidden" name="action" value="wc_bp_import_rulesets" />
                    <input type="hidden" name="mode" value="csv" />
                    <input type="file" name="wpl_file_upload" onchange="this.form.submit();" />

                    Import profiles from CSV. 

                    <br><br>
                    <b>Hint:</b> You can enter multiple SKUs in a single row by using the separator <code>|</code>.

                    <br><br>
                    <b>Warning:</b> Importing will overwrite all existing profiles. Be sure to backup your profiles as JSON before importing from CSV!

                </form>
            </p>
    </div>


    <?php if (defined('WP_DEBUG') && WP_DEBUG) : ?>
    <pre><?php #print_r($wpl_rulesTable->items); ?></pre>
    <pre><?php #print_r( $wpl_rulesets ); ?></pre>
    <?php endif; ?>

</div>


<script type="text/javascript">
    jQuery(document).ready(function() {
    });
</script>