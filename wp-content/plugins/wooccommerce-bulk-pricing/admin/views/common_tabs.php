    <?php  
        $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'rules'; 
    ?>  

	<h2 class="nav-tab-wrapper">  
        <a href="<?php echo $wpl_form_action; ?>&tab=rules"      class="nav-tab <?php echo $active_tab == 'rules' ? 'nav-tab-active' : ''; ?>">Discount Profiles</a>  
        <a href="<?php echo $wpl_form_action; ?>&tab=categories" class="nav-tab <?php echo $active_tab == 'categories' ? 'nav-tab-active' : ''; ?>">Categories</a>  
        <a href="<?php echo $wpl_form_action; ?>&tab=products"   class="nav-tab <?php echo $active_tab == 'products' ? 'nav-tab-active' : ''; ?>">Products</a>  
        <a href="<?php echo $wpl_form_action; ?>&tab=roles"      class="nav-tab <?php echo $active_tab == 'roles' ? 'nav-tab-active' : ''; ?>">User Discounts</a>  
        <a href="<?php echo $wpl_form_action; ?>&tab=settings"   class="nav-tab <?php echo $active_tab == 'settings' ? 'nav-tab-active' : ''; ?>">Settings</a>  
        <a href="<?php echo $wpl_form_action; ?>&tab=license"    class="nav-tab <?php echo $active_tab == 'license' ? 'nav-tab-active' : ''; ?>">Updates</a>  
        <!-- <a href="<?php echo $wpl_form_action; ?>&tab=developer"  class="nav-tab <?php echo $active_tab == 'developer' ? 'nav-tab-active' : ''; ?>">Developer</a>   -->
    </h2>  
