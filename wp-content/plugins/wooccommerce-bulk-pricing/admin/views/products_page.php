<?php include_once( dirname(__FILE__).'/common_header.php' ); ?>

<style type="text/css">

	th.column-post_id {
		width: 15%;
	}
    th {
        text-align:left;
    }

	td {
		line-height: 25px;
	}

</style>

<div class="wrap">
	<div class="icon32 woocommerce-bulk-pricing" id="wpl-icon"><br /></div>
    <?php include_once( dirname(__FILE__).'/common_tabs.php' ); ?>
	<?php #echo $wpl_message ?>


	<!-- show log table -->
    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <form id="bulk_pricing_products" method="post" action="<?php echo $wpl_form_action; ?>" >
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <input type="hidden" name="action" value="update_bulk_pricing_products" />

        <!-- Now we can render the completed list table -->
        <?php $wpl_productsTable->display() ?>
    
    </form>

    <p>
        Hint: You can select a discount profile when editing a product. Look for "Discount profile" below the product price.
    </p>

    <div class="submit" style="">
        <a href="#" onclick="jQuery('#import_export_container').slideToggle();return false;" class="button-secondary"><?php echo __('Import / Export','wc_bulk_pricing'); ?></a> 
    </div>

    <div id="import_export_container" style="display:none">
        <h3><?php echo __('Export product rules','wc_bulk_pricing'); ?></h3>

            <p>
                <a href="<?php echo $wpl_form_action ?>&action=wc_bp_export_product_rules&mode=csv" class="button-secondary"><?php echo __('Export product rules as CSV','wc_bulk_pricing'); ?></a> 
                Use this to batch edit your discount rule in Excel or LibreOffice.
            </p>

        <h3><?php echo __('Import product rules','wc_bulk_pricing'); ?></h3>

            <p>
                <form id="upload_csv" method="post" action="<?php echo $wpl_form_action; ?>" enctype="multipart/form-data" >

                    <a href="#" onclick="alert('Please select a file using the button right next to this button.');return false;" class="button-secondary"><?php echo __('Import product rules from CSV','wc_bulk_pricing'); ?></a> 

                    <input type="hidden" name="action" value="wc_bp_import_product_rules" />
                    <input type="hidden" name="mode" value="csv" />
                    <input type="file" name="wpl_file_upload" onchange="this.form.submit();" />

                    Import product rules from CSV. 

                    <br><br>
                    <b>Hint:</b> You need to provide either product ID or SKU.

                    <br><br>
                    <b>Warning:</b> Importing will overwrite all existing product rules for matching products!

                </form>
            </p>
    </div>


</div>
