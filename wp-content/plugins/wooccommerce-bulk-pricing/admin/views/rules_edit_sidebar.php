<style type="text/css">

    ul.bullet li {
        list-style: disc;
        list-style-position: outside;
        margin-left: 20px;
    }


    #side-sortables .postbox input.text_input,
    #side-sortables .postbox select.select {
        width: 50%;
    }
    #side-sortables .postbox label.text_label {
        width: 45%;
    }

    /* backwards compatibility to WP 3.3 */
    #poststuff #post-body.columns-2 {
        margin-right: 300px;
    }
    #poststuff #post-body {
        padding: 0;
    }
    #post-body.columns-2 #postbox-container-1 {
        float: right;
        margin-right: -300px;
        width: 280px;
    }
    #poststuff .postbox-container {
        width: 100%;
    }
    #major-publishing-actions {
        border-top: 1px solid #F5F5F5;
        clear: both;
        margin-top: -2px;
        padding: 10px 10px 8px;
    }
    #post-body .misc-pub-section {
        max-width: 100%;
        border-right: none;
    }

</style>




                    <!-- first sidebox -->
                    <div class="postbox" id="submitdiv">
                        <!--<div title="Click to toggle" class="handlediv"><br></div>-->
                        <h3><span><?php echo __('General options','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <div id="submitpost" class="submitbox">

                                <div id="misc-publishing-actions">
                                    <div class="misc-pub-section">  
                                        <?php if ( ( $wpl_active_products > 0 ) && ( $wpl_active_categories > 0 ) ): ?>
                                            <p><?php printf( __('There are %s products and %s categories using this profile.','wc_bulk_pricing'), $wpl_active_products, $wpl_active_categories ) ?></p>
                                        <?php elseif ( $wpl_active_products > 0 ): ?>
                                            <p><?php printf( __('There are %s products using this profile.','wc_bulk_pricing'), $wpl_active_products ) ?></p>
                                        <?php elseif ( $wpl_active_categories > 0 ): ?>
                                            <p><?php printf( __('There are %s categories using this profile.','wc_bulk_pricing'), $wpl_active_categories ) ?></p>
                                        <?php elseif ( $wpl_add_new ): ?>
                                            <!-- show nothing when adding new profile -->
                                        <?php else: ?>
                                            <p><?php echo __('There are no products using this profile.','wc_bulk_pricing') ?></p>
                                        <?php endif; ?>

                                        <label for="wpl-user_roles" class="text_label"><?php echo __('Available for','wc_bulk_pricing'); ?></label>
                                        <select id="wpl-user_roles" name="user_roles" class=" required-entry select">
                                            <option value=""><?php echo 'all users' ?></option>
                                            <?php foreach ($wpl_available_roles as $role => $role_name) : ?>
                                                <option value="<?php echo $role ?>" <?php if ( @$wpl_item['user_roles'] == $role ): ?>selected="selected"<?php endif; ?>><?php echo $role_name ?></option>
                                            <?php endforeach; ?>
                                            <option value="_guest"><?php echo __('Guest (not logged in)','wc_bulk_pricing') ?></option>
                                        </select>

                                        <label for="wpl-rows_limit" class="text_label"><?php echo __('Number of rows','wc_bulk_pricing'); ?></label>
                                        <select id="wpl-rows_limit" name="rows_limit" class=" required-entry select">
                                            <option value="5"  <?php if ( @$wpl_item['rows_limit'] == '5' ): ?>selected="selected"<?php endif; ?> >5</option>
                                            <option value="10" <?php if ( @$wpl_item['rows_limit'] == '10' ): ?>selected="selected"<?php endif; ?> >10</option>
                                            <option value="20" <?php if ( @$wpl_item['rows_limit'] == '20' ): ?>selected="selected"<?php endif; ?> >20</option>
                                            <option value="50" <?php if ( @$wpl_item['rows_limit'] == '50' ): ?>selected="selected"<?php endif; ?> >50</option>
                                            <option value="100" <?php if ( @$wpl_item['rows_limit'] == '100' ): ?>selected="selected"<?php endif; ?> >100</option>
                                            <option value="200" <?php if ( @$wpl_item['rows_limit'] == '200' ): ?>selected="selected"<?php endif; ?> >200</option>
                                            <option value="500" <?php if ( @$wpl_item['rows_limit'] == '500' ): ?>selected="selected"<?php endif; ?> >500</option>
                                        </select>

                                        <label for="wpl-show_lowest_price" class="text_label"><?php echo __('Show lowest price','wc_bulk_pricing'); ?></label>
                                        <select id="wpl-show_lowest_price" name="show_lowest_price" class=" required-entry select">
                                            <option value="1" <?php if ( @$wpl_item['show_lowest_price'] == '1' ): ?>selected="selected"<?php endif; ?> ><?php echo __('Yes','wc_bulk_pricing'); ?></option>
                                            <option value="0" <?php if ( @$wpl_item['show_lowest_price'] != '1' ): ?>selected="selected"<?php endif; ?> ><?php echo __('No','wc_bulk_pricing'); ?></option>
                                        </select>

                                    </div>
                                </div>

                                <div id="major-publishing-actions">
                                    <div id="publishing-action">
                                        <a href="#" onclick="jQuery('#InstructionsBox').slideToggle(300);return false;" class="button-secondary"><?php echo __('Help','wc_bulk_pricing'); ?></a>
                                        <a href="#" onclick="jQuery('#ExamplesBox').slideToggle(300);return false;" class="button-secondary"><?php echo __('Examples','wc_bulk_pricing'); ?></a>
                                        <input type="hidden" name="action" value="update_bulk_pricing_ruleset" />
                                        <input type="hidden" name="ruleset_id" value="<?php echo $wpl_item['id']; ?>" />
                                        <input type="submit" value="<?php echo $wpl_add_new ? __('Add profile','wc_bulk_pricing') : __('Update','wc_bulk_pricing') ?>" id="publish" class="button-primary" name="save">
                                    </div>
                                    <div class="clear"></div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <?php $wpl_is_cumulative = isset($wpl_item['is_cumulative']) ? $wpl_item['is_cumulative'] : false; ?>

                    <div class="postbox" id="ScopeBox">
                        <h3><span><?php echo __('Rule scope','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <p>
                                <?php echo __('Select how the total quantity is calculated.','wc_bulk_pricing'); ?>
                            </p>
                            <label for="wpl-is_cumulative" class="text_label"><?php echo __('This profile is','wc_bulk_pricing'); ?>:</label>
                            <select id="wpl-is_cumulative" name="is_cumulative" class=" required-entry select">
                                <option value="1" <?php if ( $wpl_is_cumulative == '1' ): ?>selected="selected"<?php endif; ?>><?php echo __('cumulative','wc_bulk_pricing'); ?></option>
                                <option value="0" <?php if ( $wpl_is_cumulative != '1' ): ?>selected="selected"<?php endif; ?>><?php echo __('exclusive','wc_bulk_pricing'); ?></option>
                            </select>
                            <p>
                                <?php echo __('<b>Exclusive rules</b> only count product quantities of the same product.','wc_bulk_pricing'); ?>                                
                            </p>
                            <p>
                                <?php echo __('<b>Cumulative rules</b> count product quantities of all products with this profile assigned.','wc_bulk_pricing'); ?>                                
                            </p>
                            <p>
                                <a href="#" onclick="jQuery('#rulescope_example').slideToggle('fast');return false;"><?php echo __('Tell me more','wc_bulk_pricing'); ?></a>
                            </p>
                            <div id="rulescope_example" style="display:none">
                                <p>
                                    <b>Example:</b> 
                                </p>
                                <p>
                                    Let's say you have three products A, B and C and they all have the same profile applied which provides a discount for 3 items or more.
                                </p>
                                <p>
                                    If the rule scope is set to <em>exclusive</em> the customer has to purchase 3 x A or 3 x B or 3 x C to get the discount price.
                                </p>
                                <p>
                                    If the rule scope is <em>cumulative</em> the discount price will be applied even if the customer purchases one of each item: 1 x A, 1 x B and 1 x C.                            
                                </p>
                            </div>


                            <div id="rulescope_joined_rules" <?php if ( $wpl_is_cumulative != '1' ): ?>style="display:none"<?php endif; ?>>
                                <p>
                                    <b><?php echo __('Join other cumulative rulesets','wc_bulk_pricing'); ?></b><br>
                                </p>
                                <?php $rows_limit = isset( $wpl_item['rows_limit'] ) ? $wpl_item['rows_limit'] : 5; ?>
                                <?php for ($i=0; $i < $rows_limit; $i++) : ?>
                                    <label for="wpl-cumulates_with_<?php echo $i ?>" class="text_label"><?php echo __('Ruleset #','wc_bulk_pricing').($i+1); ?></label>
                                    <select id="wpl-cumulates_with_<?php echo $i ?>" name="cumulates_with[<?php echo $i ?>]" class="required-entry select">
                                        <option value="" <?php if ( @$wpl_cumulates_with[$i] == '' ): ?>selected="selected"<?php endif; ?>>-- <?php echo __('none','wc_bulk_pricing'); ?> --</option>
                                        <?php foreach ($wpl_available_rulesets as $ruleset_id => $ruleset) : ?>
                                            <option value="<?php echo $ruleset_id ?>" <?php if ( @$wpl_cumulates_with[$i] == $ruleset_id ): ?>selected="selected"<?php endif; ?>><?php echo $ruleset['name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                <?php endfor; ?>
                                <p>
                                    <?php echo __('<b>Note:</b> You need to join rulesets in both directions.','wc_bulk_pricing'); ?>                                    
                                </p>
                            </div>


                        </div>
                    </div>



                    <div class="postbox" id="SupportBox">
                        <h3><span><?php echo __('Support','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">
                            <p>
                                If you need any help, have a bug report or feature request - just contact us at support@wplab.com.
                            </p>
                        </div>
                    </div>


                    <script type="text/javascript">
                        jQuery( document ).ready(
                            function () {

                                jQuery('#wpl-is_cumulative').change( function(){
                                    if ( this.value == 1 ) {
                                        jQuery('#rulescope_joined_rules').slideDown(300);
                                    } else {
                                        jQuery('#rulescope_joined_rules').slideUp(300);
                                    }
                                })

                            }
                        );
                    </script>


