<?php include_once( dirname(__FILE__).'/common_header.php' ); ?>

<style type="text/css">

    th {
        text-align:left;
    }

    table.rules input {
        width: 99%;
    }

    /* general styles for edit pages (edit template, edit profile and settings) */
    .wrap .postbox .inside {
        margin-bottom: 15px;
    }

    .wrap .postbox .inside p.desc {
        font-size: smaller;
        font-style: italic;
        margin-top: 0;
        margin-left: 35%;
    }
    .wrap .postbox .inside p.edit_links {
        margin-top: 0;
        margin-left: 35%;
    }

    .wrap .postbox label.text_label {
        display: block;
        float: left;
        width: 33%;
        margin: 1px;
        padding: 3px;
    }
    .wrap .postbox input.text_input,
    .wrap .postbox textarea,
    .wrap .postbox select.select {
        width: 65%;
        margin-bottom: 5px;
        padding: 3px 8px;
    }
    .wrap .postbox textarea {
        height: 120px;
    }

    #ExamplesBox table {
        width: 50%;
    }
    #ExamplesBox table th,
    #ExamplesBox table td {
        text-align: right;
        border-bottom: 1px solid #ccc;
        padding: 3px 20px;
    }
    #ExamplesBox table th {
        border-bottom: 1px solid #999;
    }

</style>

<div class="wrap">
	<div class="icon32 woocommerce-bulk-pricing" id="wpl-icon"><br /></div>
    <?php #include_once( dirname(__FILE__).'/common_tabs.php' ); ?>
    <?php if ( ! $wpl_add_new ): ?>
    <h2><?php echo __('Edit Discount Profile','wc_bulk_pricing') ?></h2>
    <?php else: ?>
    <h2><?php echo __('New Discount Profile','wc_bulk_pricing') ?></h2>
    <?php endif; ?>
    
    <?php #echo $wpl_message ?>

    <form method="post" action="<?php echo $wpl_form_action; ?>">


    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">

            <div id="postbox-container-1" class="postbox-container">
                <div id="side-sortables" class="meta-box">
                    <?php include('rules_edit_sidebar.php') ?>
                </div>
            </div> <!-- #postbox-container-1 -->


            <!-- #postbox-container-2 -->
            <div id="postbox-container-2" class="postbox-container">
                <div class="meta-box-sortables ui-sortable">
                    
                    <div id="titlediv" style="">
                        <div id="titlewrap">
                            <label class="hide-if-no-js" style="visibility: hidden; " id="title-prompt-text" for="title">Enter title here</label>
                            <input type="text" name="ruleset_name" size="30" tabindex="1" value="<?php echo $wpl_item['name']; ?>" id="title" autocomplete="off">
                        </div>
                    </div>

                    <div class="postbox" id="GeneralSettingsBox">
                        <h3><span><?php echo __('Pricing rules','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <?php include_once( dirname(__FILE__).'/rules_edit_table.php' ); ?>

                        </div>
                    </div>

                        
                    <div class="postbox" id="InstructionsBox" style="display:none">
                        <h3><span><?php echo __('Instructions','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <p class="" style="display: block;">
                                <h4>You can set up the discount price in various ways:</h4>
                                <ul class="bullet">
                                    <li>enter a fixed discount price like <code>9.95</code></li>
                                    <li>enter an absolute discount like <code>-5</code> for $5 discount</li>
                                    <li>enter an relative discount like <code>-10%</code> for 10 percent discount</li>
                                </ul>
                            </p>

                            <p class="" style="display: block;">
                                <h4>Setting quantity ranges:</h4>
                                <ul class="bullet">
                                    <li>The first "From Quantity" field (top left) will always be 1.</li>
                                    <li>The last used "To Quantity" field should always contain <code>*</code> to represent "this quantity or more". </li>
                                </ul>
                                 
                            </p>

                        </div>
                    </div>


                    <div class="postbox" id="ExamplesBox" style="display:none">
                        <h3><span><?php echo __('Examples','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <p class="" style="display: block;">

                                <h4>Example using relative discount:</h4>
                                <p>To define 10% discount for 10 items or more and 20% discount for 100 items or more, enter the following values in the table above:</p>

                                <table>
                                    <tr>
                                        <th>To</th>
                                        <th>From</th>
                                        <th>Discount</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>9</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>99</td>
                                        <td>90%</td>
                                    </tr>
                                    <tr>
                                        <td>100</td>
                                        <td>*</td>
                                        <td>80%</td>
                                    </tr>
                                </table>


                                <p>If your product costs <b>$1</b> each, the products page will display the following discount information table:</p>

                                <table>
                                    <tr>
                                        <th>1-9</th>
                                        <th>10-99</th>
                                        <th>100+</th>
                                    </tr>
                                    <tr>
                                        <td>$1</td>
                                        <td>$0.90</td>
                                        <td>$0.80</td>
                                    </tr>
                                </table>

                                <p>If your product is <b>$10</b> each, this will be shown on the products page:</p>

                                <table>
                                    <tr>
                                        <th>1-9</th>
                                        <th>10-99</th>
                                        <th>100+</th>
                                    </tr>
                                    <tr>
                                        <td>$10</td>
                                        <td>$9</td>
                                        <td>$8</td>
                                    </tr>
                                </table>

                            </p>

                            <p class="" style="display: block;">
                                 
                            </p>

                        </div>
                    </div>


                        
                    <div class="postbox" id="DiscountInfoBox">
                        <h3><span><?php echo __('Custom Discount information','wc_bulk_pricing'); ?></span></h3>
                        <div class="inside">

                            <label for="wpl-custom_header" class="text_label"><b>Custom header</b><br><?php echo __('Enter any text or html that you want displayed above the discount table on the products page.','wc_bulk_pricing'); ?></label>
                            <textarea id="wpl-custom_header" name="custom_header"><?php echo stripslashes( @$wpl_item['custom_header'] ) ?></textarea>

                            <label for="wpl-custom_footer" class="text_label"><b>Custom footer</b><br><?php echo __('Enter any text or html that you want displayed below the discount table on the products page.','wc_bulk_pricing'); ?></label>
                            <textarea id="wpl-custom_footer" name="custom_footer"><?php echo stripslashes( @$wpl_item['custom_footer'] ) ?></textarea>

                            <label for="wpl-hide_discount_table" class="text_label"><?php echo __('Hide discount table','wc_bulk_pricing'); ?></label>
                            <select id="wpl-hide_discount_table" name="hide_discount_table" class=" required-entry select">
                                <option value="0" <?php if ( @$wpl_item['hide_discount_table'] != '1' ): ?>selected="selected"<?php endif; ?> >No</option>
                                <option value="1" <?php if ( @$wpl_item['hide_discount_table'] == '1' ): ?>selected="selected"<?php endif; ?> >Yes</option>
                            </select>

                        </div>
                    </div>
                        
                </div> <!-- .meta-box-sortables -->
            </div> <!-- #postbox-container-1 -->



        </div> <!-- #post-body -->
        <br class="clear">
    </div> <!-- #poststuff -->

    </form>


    <?php if ( get_option('wc_bulk_pricing_log_level') > 6 ): ?>
    <pre><?php print_r($wpl_item); ?></pre>
    <?php endif; ?>


    <script type="text/javascript">
        jQuery( document ).ready(
            function () {

            }
        );
    </script>

</div>


