<?php
/**
 * WCBP_CategoriesPage class
 * 
 */

class WCBP_CategoriesPage extends WCBP_Page {


	public function displayCategoriesPage() {

        if ( @$_POST['action']  == 'update_bulk_pricing_categories' ) $this->handleUpdateCategories();

        //Create an instance of PricingCategoriesTable
        $categoriesTable = new PricingCategoriesTable();
        $categoriesTable->prepare_items( );


        $aData = array(
            'categoriesTable'           => $categoriesTable,
            'form_action'               => 'admin.php?page=wc_bulk_pricing&tab=categories'
        );
        $this->display( 'categories_page', $aData );

	}

    public function handleUpdateCategories() {

        $post_categories = $_POST['category'];
        $categories = array();

        // save only active categories
        if ( is_array( $post_categories ) ) {        
            foreach ($post_categories as $term_id => $category) {
                if ( @$category['active'] == 'on' ) {
                    $categories[$term_id] = implode(',', $category['ruleset'] );
                }
            }
        }

        // save rulesets
        update_option('wc_bulk_pricing_categories', $categories);

    }


}
