<?php
/**
 * WCBP_Page class
 * 
 */

class WCBP_Page {

    static public $PLUGIN_PATH;
    static public $PLUGIN_DIR;
    static public $PLUGIN_URL;

    const ViewExt           = '.php';
    const ViewDir           = '../views';

    var $message;

    // init
    public function __construct() {

        self::$PLUGIN_PATH = plugin_basename( dirname(dirname(dirname(__FILE__))) );
        self::$PLUGIN_DIR = WP_PLUGIN_DIR.DS.self::$PLUGIN_PATH.DS;
        self::$PLUGIN_URL = WP_PLUGIN_URL.'/'.self::$PLUGIN_PATH.'/';

    }

    
    // generic message display
    public function showMessage($message, $errormsg = false, $echo = true) {       
        $class = ($errormsg) ? 'error' : 'updated fade';
        $this->message .= '<div id="message" class="'.$class.'"><p>'.$message.'</p></div>';
        if ($echo) echo $this->message;
    }

	// display view
	protected function display( $insView, $inaData = array(), $echo = true ) {
		// $sFile = dirname(__FILE__).DS.self::ViewDir.DS.$insView.self::ViewExt;
		$sFile = dirname(__FILE__) . '/../views/' . $insView . '.php';
		
		if ( !is_file( $sFile ) ) {
			$this->showMessage("View not found: ".$sFile,1,1);
			return false;
		}
		
		if ( count( $inaData ) > 0 ) {
			extract( $inaData, EXTR_PREFIX_ALL, 'wpl' );
		}
		
		ob_start();
			include( $sFile );
			$sContents = ob_get_contents();
		ob_end_clean();

		if ($echo) {
			echo $sContents;
			return true;
		} else {
			return $sContents;
		}
	
	}


}
