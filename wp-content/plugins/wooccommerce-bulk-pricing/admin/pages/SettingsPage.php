<?php
/**
 * WCBP_SettingsPage class
 * 
 */

class WCBP_SettingsPage extends WCBP_Page {


	public function displaySettingsPage() {

        if ( @$_POST['action'] == 'update_global_settings' ) $this->handleUpdateGlobalSettings();

        $aData = array(
            'display_price_decimals'    => get_option('wc_bulk_pricing_display_price_decimals', 2 ),
            'round_to_decimals'         => get_option('wc_bulk_pricing_round_to_decimals', 1 ),
            'show_used_ruleset_in_cart' => get_option('wc_bulk_pricing_show_used_ruleset_in_cart', 0 ),
            'default_number_of_rows'    => get_option('wc_bulk_pricing_default_number_of_rows', 5 ),
            'default_display_priority'  => get_option('wc_bulk_pricing_default_display_priority', 20 ),
            'default_display_action_hook'  => get_option('wc_bulk_pricing_default_display_action_hook', 'woocommerce_single_product_summary' ),
            'coupon_handling_mode'      => get_option('wc_bulk_pricing_coupon_handling_mode', 'default' ),
            'disable_on_sale_products'  => get_option('wc_bulk_pricing_disable_on_sale_products', 0 ),
            'display_extra_decimals'    => get_option('wc_bulk_pricing_display_extra_decimals', 1 ),
            'expert_mode_enabled'       => get_option('wc_bulk_pricing_expert_mode_enabled', 0 ),
            'cumulate_custom_rules'     => get_option('wc_bulk_pricing_cumulate_custom_rules', 0 ),
            'show_lowest_price'         => get_option('wc_bulk_pricing_show_lowest_price', 0 ),
            'variation_display_mode'    => get_option('wc_bulk_pricing_variation_display_mode', 'show'),
            'default_header'            => get_option('wc_bulk_pricing_default_header', __('Quantity discounts available', 'wc_bulk_pricing') ),
            'form_action'               => 'admin.php?page=wc_bulk_pricing&tab=settings'
        );

        $this->display( 'settings_page', $aData );

	}

    public function handleUpdateGlobalSettings() {

        update_option('wc_bulk_pricing_display_price_decimals',     $_POST['wpl_display_price_decimals'] );
        update_option('wc_bulk_pricing_display_extra_decimals',     $_POST['wpl_display_extra_decimals'] );
        update_option('wc_bulk_pricing_round_to_decimals',          $_POST['wpl_round_to_decimals'] );
        update_option('wc_bulk_pricing_default_display_action_hook',   $_POST['wpl_default_display_action_hook'] );
        update_option('wc_bulk_pricing_default_display_priority',   $_POST['wpl_default_display_priority'] );
        update_option('wc_bulk_pricing_default_number_of_rows',     $_POST['wpl_default_number_of_rows'] );
        update_option('wc_bulk_pricing_show_used_ruleset_in_cart',  $_POST['wpl_show_used_ruleset_in_cart'] );
        update_option('wc_bulk_pricing_coupon_handling_mode',       $_POST['wpl_coupon_handling_mode'] );
        update_option('wc_bulk_pricing_disable_on_sale_products',   $_POST['wpl_disable_on_sale_products'] );
        update_option('wc_bulk_pricing_show_lowest_price',          $_POST['wpl_show_lowest_price'] );
        update_option('wc_bulk_pricing_variation_display_mode',     $_POST['wpl_variation_discount_table_display'] );
        update_option('wc_bulk_pricing_default_header',             $_POST['wpl_default_header'] );
        update_option('wc_bulk_pricing_cumulate_custom_rules',      $_POST['wpl_cumulate_custom_rules'] );
        update_option('wc_bulk_pricing_expert_mode_enabled',        $_POST['wpl_expert_mode_enabled'] );

        $this->showMessage( 'Global settings were updated.');

    }


}
