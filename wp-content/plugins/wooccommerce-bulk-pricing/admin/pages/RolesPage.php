<?php
/**
 * WCBP_RolesPage class
 * 
 */

class WCBP_RolesPage extends WCBP_Page {


	public function displayRolesPage() {

        if ( @$_POST['action'] == 'update_role_settings' ) $this->handleUpdateRoleSettings();

        $aData = array(
            'role_discounts'          => get_option('wc_bulk_pricing_role_discounts', array() ),
            'exclude_roles'           => get_option('wc_bulk_pricing_exclude_roles', array() ),
            'role_discount_modes'     => get_option('wc_bulk_pricing_role_discount_modes', array() ),
            'available_roles'         => $this->getUserRoles(),
            'form_action'             => 'admin.php?page=wc_bulk_pricing&tab=roles'
        );
        $aData['available_roles']['_guest'] = 'Guest (not logged in)';
        $this->display( 'roles_page', $aData );

	}


    public function handleUpdateRoleSettings() {

        $role_discount = $_POST['role_discount'];
        $exclude_roles = $_POST['exclude_roles'];
        $role_discount_modes = $_POST['role_discount_mode'];

        $role_discount_settings = array();
        foreach ($role_discount as $role => $discount) {
            if ( floatval( $discount ) != 0 ) {
                $role_discount_settings[ $role ] = floatval( $discount );
            }
        }

        $exclude_roles_settings = array();
        foreach ($exclude_roles as $role => $exclude) {
            if ( $exclude ) {
                $exclude_roles_settings[] = $role;
            }
        }

        $role_discount_modes_settings = array();
        foreach ($role_discount_modes as $role => $mode) {
            $role_discount_modes_settings[ $role ] = $mode;
        }

        // save options
        update_option('wc_bulk_pricing_role_discounts', $role_discount_settings);
        update_option('wc_bulk_pricing_exclude_roles', $exclude_roles_settings);
        update_option('wc_bulk_pricing_role_discount_modes', $role_discount_modes_settings);
        // echo "<pre>";print_r($role_discount_settings);echo"</pre>";#die();
        // echo "<pre>";print_r($exclude_roles_settings);echo"</pre>";#die();

        $this->showMessage( 'Role settings were updated.');

    }

    public function getUserRoles() {

        if ( !isset( $wp_roles ) ) 
            $wp_roles = new WP_Roles();

        return $wp_roles->role_names;

        // $roles = array();
        // foreach ( $wp_roles->role_names as $role => $name ) {
        //     $roles[] = $role;
        // }
        // return $roles;
    }



}
