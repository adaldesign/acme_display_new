<?php
/**
 * WCBP_RulesetsPage class
 * 
 */

class WCBP_RulesetsPage extends WCBP_Page {

    public function __construct() {
        parent::__construct();

        add_action('admin_init', array(&$this, 'handleActions'));
    
    }

	public function displayRulesetsPage() {

        // handle actions
        if ( @$_GET['action']  == 'edit' )                   return $this->displayEditRulesetsPage();
        if ( @$_GET['action']  == 'duplicate' )              return $this->displayEditRulesetsPage();
        if ( @$_GET['action']  == 'delete' )                        $this->handleDeleteRuleset();
        if ( @$_POST['action'] == 'wc_bp_import_rulesets' )         $this->handleImportRulesets();
        if ( @$_POST['action'] == 'update_bulk_pricing_ruleset' )   $this->handleUpdateRuleset();


        // get pricing rules
        $rulesets = wbp_get_rules( true );

        //Create an instance of PricingRulesTable
        $rulesTable = new PricingRulesTable();
        $rulesTable->prepare_items( $rulesets );


        $aData = array(
            'rulesTable'                => $rulesTable,
            'rulesets'                  => $rulesets,
            'form_action'               => 'admin.php?page=wc_bulk_pricing'
        );
        $this->display( 'rules_page', $aData );

	}


    public function displayEditRulesetsPage() {
        
        // handle actions
        // if ( @$_POST['action'] == 'update_bulk_pricing_ruleset' ) $this->handleAddRuleset();

        // get current rule set
        $ruleset_id = @$_REQUEST['ruleset'];
        if ( $ruleset_id ) {

            $rulesets = wbp_get_rules();
            $ruleset  = $rulesets[ $ruleset_id ];
            $add_new = false;

            $duplicate = @$_GET['action'] == 'duplicate' ? true : false;
            if ( @$_GET['action'] == 'duplicate' ) {
                $ruleset['name'] .= ' (copy)';
                $ruleset['id']    = date('U');
            }
        
        } else {

            $ruleset = array(
                'id'                  => '',
                'name'                => '',
                'rules'               => array(),
                'custom_header'       => '<b>'.__('Quantity discounts available', 'wc_bulk_pricing').':</b><br>',
                'custom_footer'       => '',
                'hide_discount_table' => '0'
            );
            $add_new = true;
            $duplicate = false;

        }

        $aData = array(
            'add_new'                   => $add_new,
            'duplicate'                 => $duplicate,
            'item'                      => $ruleset,
            'active_products'           => $this->count_active_products_for_ruleset($ruleset_id),
            'active_categories'         => $this->count_active_categories_for_ruleset($ruleset_id),
            'available_roles'           => $this->getUserRoles(),
            'available_rulesets'        => wbp_get_rules( true ),
            'cumulates_with'            => isset( $ruleset['cumulates_with'] ) ? $ruleset['cumulates_with'] : array(),
            'form_action'               => 'admin.php?page=wc_bulk_pricing'
        );
        $this->display( 'rules_edit_page', $aData );

    }





    // count active products for given ruleset id
    function count_active_products_for_ruleset($ruleset_id){        
        global $wpdb;
        $active_products = 0;
        if ( ! $ruleset_id ) return 0;

        $sql = "SELECT post_id
            FROM $wpdb->postmeta
            WHERE meta_key = '_wc_bulk_pricing_ruleset' AND meta_value = '". $ruleset_id ."' 
        ";
        $wc_bulk_pricing_products = $wpdb->get_col( $sql );

        foreach ($wc_bulk_pricing_products as $post_id) {
            $active_products++;
        }

        return $active_products;
    }

    // count active categories for given ruleset id
    function count_active_categories_for_ruleset($ruleset_id){        
        global $wpdb;
        $active_categories = 0;
        if ( ! $ruleset_id ) return 0;

        $wc_bulk_pricing_categories = get_option('wc_bulk_pricing_categories', array() );
        // echo "<pre>";print_r($wc_bulk_pricing_categories);echo"</pre>";

        foreach ($wc_bulk_pricing_categories as $ruleset_ids) {

            // convert to array
            $ruleset_ids = explode(',', $ruleset_ids );

            // echo "<pre>";print_r($data);echo"</pre>";
            // if ( $id == $ruleset_id )
            if ( in_array( $ruleset_id, $ruleset_ids ) ) 
                $active_categories++;
        }

        return $active_categories;
    }

    public function getUserRoles() {

        $wp_roles = new WP_Roles();
        return $wp_roles->role_names;

    }



    public function handleUpdateRuleset() {

        $rules         = $_POST['rules'];
        $ruleset_name  = $_POST['ruleset_name'];
        $ruleset_id    = $_POST['ruleset_id'] == '' ? sanitize_key( $_POST['ruleset_name'] ) : $_POST['ruleset_id'];

        if ( $ruleset_id == '') {
            $this->showMessage( 'You did not enter a valid name.',1);
            return false;
        }
        // fetch existing rulesets
        $rulesets = wbp_get_rules();

        // filter empty rules
        foreach ($rules as $key => &$rule) {
            if ( ( trim($rule['min']) == '' ) &&  ( trim($rule['max']) == '' ) ) {
                unset( $rules[$key] );
            } elseif ( ( trim($rule['min']) == '' ) ) {
                $rule['min'] = '*';
            } elseif ( ( trim($rule['max']) == '' ) ) {
                $rule['max'] = '*';
            }
        }

        // re-index array to remove empty rows
        $rules = array_values( $rules );

        // create ruleset
        $new_ruleset = array();
        $new_ruleset['id']                  = $ruleset_id;
        $new_ruleset['name']                = $ruleset_name;
        $new_ruleset['rules']               = $rules;
        $new_ruleset['is_cumulative']       = $_POST['is_cumulative'];
        $new_ruleset['user_roles']          = $_POST['user_roles'];
        $new_ruleset['rows_limit']          = $_POST['rows_limit'];
        $new_ruleset['show_lowest_price']   = $_POST['show_lowest_price'];
        $new_ruleset['custom_header']       = $_POST['custom_header'];
        $new_ruleset['custom_footer']       = $_POST['custom_footer'];
        $new_ruleset['hide_discount_table'] = $_POST['hide_discount_table'];
        $new_ruleset['cumulates_with']      = $_POST['cumulates_with'];

        // add to rulesets
        $rulesets[ $ruleset_id ] = $new_ruleset;

        // update rulesets
        update_option('wc_bulk_pricing_rules', $rulesets);
        $this->showMessage( 'Discount profile "'. $ruleset_name . '" was updated.');

    }
    public function handleDeleteRuleset() {

        $id = $_GET['ruleset'];

        // fetch existing rulesets
        $rulesets = wbp_get_rules();

        // loop and delete
        foreach ($rulesets as $key => $rule) {
            if ( $rule['id'] == $id ) {
                $this->showMessage( 'The profile "'. $rule['name'] . '" was deleted.');
                unset( $rulesets[$key] );
            }
        }

        // save rulesets
        update_option('wc_bulk_pricing_rules', $rulesets);

    }



    // import rulesets from csv
    protected function handleImportRulesets() {
        require_once( dirname(__FILE__).'/../classes/PricingRulesImporter.php');

        $importer = new PricingRulesImporter();
        $mode     = isset( $_REQUEST['mode'] ) ? $_REQUEST['mode'] : 'csv';

        $uploaded_file = $importer->process_upload();
        if (!$uploaded_file) return;

        // handle JSON export
        if ( $mode == 'json' ) {

            $result = $importer->import_json( $uploaded_file );

            if ( $result ) {
                $this->showMessage( $result . ' discount profiles were imported.');
            } else {
                $this->showMessage( 'The uploaded file could not be imported. Please make sure you use a JSON backup file exported from this plugin.', 1 );
            }

        } else {
            $result = $importer->import_csv( $uploaded_file );

            if ( $result ) {
                $this->showMessage( $result . ' discount profiles were imported.');
            } else {
                $this->showMessage( 'The uploaded file could not be imported. Please only import CSV files which were exported from this plugin and make sure you have not added or removed any columns.', 1 );                
            }

        }


    }

    public function handleActions() {

        if ( isset($_GET['action']) && ( $_GET['action']  == 'wc_bp_export_rulesets' ) ) $this->handleExportRulesets();

    }

    // export rulesets as csv
    protected function handleExportRulesets() {
        require_once( dirname(__FILE__).'/../classes/PricingRulesImporter.php');

        $importer = new PricingRulesImporter();
        $rulesets = wbp_get_rules();
        $mode     = isset( $_REQUEST['mode'] ) ? $_REQUEST['mode'] : 'csv';

        // handle JSON export
        if ( $mode == 'json' ) {
            $importer->export_json( $rulesets );
        } else {
            $importer->export_csv( $rulesets );            
        }

    }


}
