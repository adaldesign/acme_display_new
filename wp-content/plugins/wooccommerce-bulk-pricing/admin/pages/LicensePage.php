<?php
/**
 * WCBP_LicensePage class
 * 
 */

class WCBP_LicensePage extends WCBP_Page {


    public function displayLicensePage() {

        $this->saveLicenseSettings();
        $this->handleUpdateCheck();
        $this->checkLicenseStatus();

        $aData = array(
            // 'plugin_url'                => self::$PLUGIN_URL,
            // 'message'                   => $this->message,

            'license_key'          => self::getOption( 'license_key' ),
            'license_email'        => self::getOption( 'license_email' ),
            'license_activated'    => self::getOption( 'license_activated' ),
            'update_channel'       => self::getOption( 'update_channel' ),

            'form_action'               => 'admin.php?page=wc_bulk_pricing'.'&tab=license'
        );
        $this->display( 'license_page', $aData );
    }

    protected function checkLicenseStatus() {

        // TODO: check nonce
        if ( isset( $_GET['action'] ) && ( $_GET['action'] == 'wbp_check_license_status' ) ) {

            global $wc_bulk_pricing_updater;
            $result = $wc_bulk_pricing_updater->check_license( self::getOption( 'license_key' ), self::getOption( 'license_email' ) );
            // echo "<pre>";print_r($result);echo"</pre>";die();

            if ( $result === true ) {
                $this->showMessage( __('Your license is currently active on this site.','wc_bulk_pricing') );
                self::updateOption( 'license_activated', '1' );

            } elseif ( is_object($result) && (!is_wp_error($result)) && ( $result->code == 101 ) ) {
                $this->showMessage( __('This license has not been activated on this site.','wc_bulk_pricing') . '<br><br>'
                                    . 'The update server responded:'
                                    . '<br>Error #'.$result->code.': '. $result->error, 1 );
                self::updateOption( 'license_activated', '0' );

            } elseif ( is_wp_error( $result ) ) {
                $error_string  = $result->get_error_message();
                $error_string .= $result->get_error_data();
                $this->showMessage( __('There was a problem checking your license.','wc_bulk_pricing')
                                    . ' (1)<br>' . $error_string, 1 );
            } elseif ( is_object($result) ) {
                $this->showMessage( __('There was a problem checking your license.','wc_bulk_pricing')
                                    . ' (2)<br>Error #'.$result->code.': '. $result->error, 1 );
            } else {
                $this->showMessage( __('There was a problem checking your license.','wc_bulk_pricing')
                                    . ' (3)<br>Error: '.$result, 1 );
            }                   

        }

    } // checkLicenseStatus()

    protected function handleUpdateCheck() {

        // force wp update check
        if ( @$_GET['action'] == 'force_update_check' ) {              
            global $wpdb;
            $wpdb->query("update wp_options set option_value='' where option_name='_site_transient_update_plugins'");
            $this->showMessage( 
                __('Check for updates was initiated.','wc_bulk_pricing') . ' '
                . __('You can visit your WordPress Updates now.','wc_bulk_pricing') . '<br><br>'
                . __('Since the updater runs in the background, it might take a little while before new updates appear.','wc_bulk_pricing') . '<br><br>'
                . '&raquo; <a href="update-core.php">'.__('view updates','wc_bulk_pricing') . '</a>'
            );
        }

    }

    protected function saveLicenseSettings() {

        // TODO: check nonce
        if ( isset( $_POST['wc_bulk_pricing_license_key'] ) ) {

            // new license key or email ?
            $oldLicense = self::getOption( 'license_key' );
            $oldEmail   = self::getOption( 'license_email' );
            if ( $oldLicense != $this->getValueFromPost( 'license_key' ) ) {
                self::updateOption( 'license_activated', '0' );
            }
            if ( $oldEmail != $this->getValueFromPost( 'license_email' ) ) {
                self::updateOption( 'license_activated', '0' );
            }

            // license activated ?  
            if ( self::getOption( 'license_activated' ) != '1' ) {
                global $wc_bulk_pricing_updater;
                if ( is_object( $wc_bulk_pricing_updater ) ) { // skip if no updater included
                    $result = $wc_bulk_pricing_updater->activate_license( $this->getValueFromPost( 'license_key' ), $this->getValueFromPost( 'license_email' ) );
                    if ( $result === true ) {
                        $this->showMessage( __('Your license was activated.','wc_bulk_pricing') );
                        self::updateOption( 'license_activated', '1' );
                    } elseif ( is_wp_error( $result ) ) {
                        $error_string  = $result->get_error_message();
                        $error_string .= $result->get_error_data();
                        $this->showMessage( __('There was a problem activating your license.','wc_bulk_pricing')
                                            . '<br>' . $error_string, 1 );
                    } elseif ( is_object($result) ) {
                        $this->showMessage( __('There was a problem activating your license.','wc_bulk_pricing')
                                            . '<br>Error #'.$result->code.': '. $result->error, 1 );
                    } else {
                        $this->showMessage( __('There was a problem activating your license.','wc_bulk_pricing')
                                            . '<br>Error #'.$result, 1 );
                    }                   
                }
            }

            self::updateOption( 'license_key',      $this->getValueFromPost( 'license_key' ) );
            self::updateOption( 'license_email',    $this->getValueFromPost( 'license_email' ) );
            self::updateOption( 'update_channel',   $this->getValueFromPost( 'update_channel' ) );
            // $this->showMessage( __('License settings updated.','wc_bulk_pricing') );

            if ( $this->getValueFromPost( 'deactivate_license' ) == '1') {

                global $wc_bulk_pricing_updater;
                $result = $wc_bulk_pricing_updater->deactivate_license( self::getOption( 'license_key' ), self::getOption( 'license_email' ) );
                if ( $result === true ) {
                    $this->showMessage( __('Your license was deactivated.','wc_bulk_pricing') );
                    self::updateOption( 'license_activated', '0' );
                    self::updateOption( 'license_key', '' );
                    self::updateOption( 'license_email', '' );

                } elseif ( is_wp_error( $result ) ) {
                    $error_string  = $result->get_error_message();
                    $error_string .= $result->get_error_data();
                    $this->showMessage( __('There was a problem deactivating your license.','wc_bulk_pricing')
                                        . '<br>' . $error_string, 1 );
                } elseif ( is_object($result) ) {
                    $this->showMessage( __('There was a problem deactivating your license.','wc_bulk_pricing')
                                        . '<br>Error #'.$result->code.': '. $result->error, 1 );
                } else {
                    $this->showMessage( __('There was a problem deactivating your license.','wc_bulk_pricing')
                                        . '<br>Error: '.$result, 1 );
                }                   


            }

        }
    }
    
    public function getOption( $key ) {
        return get_option('wc_bulk_pricing_'.$key);
    }
    public function updateOption( $key, $value ) {
        update_option('wc_bulk_pricing_'.$key, $value);
    }
    protected function getValueFromPost( $key ) {
        $key = 'wc_bulk_pricing_'.$key;
        return ( isset( $_POST[$key] ) ? trim( $_POST[$key] ) : false );
    }
    

}
