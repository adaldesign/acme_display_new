<?php
/**
 * WCBP_ProductsPage class
 * 
 */

class WCBP_ProductsPage extends WCBP_Page {

    public function __construct() {
        parent::__construct();

        add_action('admin_init', array(&$this, 'handleActions'));
    
    }

	public function displayProductsPage() {

        // handle actions
        if ( @$_POST['action'] == 'wc_bp_import_product_rules' )         $this->handleImportRulesets();

        //Create an instance of PricingCategoriesTable
        $productsTable = new PricingProductsTable();
        $productsTable->prepare_items( );

        $aData = array(
            'productsTable'           => $productsTable,
            'form_action'             => 'admin.php?page=wc_bulk_pricing&tab=products'
        );
        $this->display( 'products_page', $aData );

	}


    // import product level rules from csv
    protected function handleImportRulesets() {
        require_once( dirname(__FILE__).'/../classes/PricingRulesImporter.php');

        $importer = new PricingRulesImporter();
        $mode     = isset( $_REQUEST['mode'] ) ? $_REQUEST['mode'] : 'csv';

        $uploaded_file = $importer->process_upload();
        if (!$uploaded_file) return;

        // handle JSON export
        if ( $mode == 'json' ) {

            // $result = $importer->import_json( $uploaded_file );

            // if ( $result ) {
            //     $this->showMessage( $result . ' discount profiles were imported.');
            // } else {
            //     $this->showMessage( 'The uploaded file could not be imported. Please make sure you use a JSON backup file exported from this plugin.');                
            // }

        } else {
            $result = $importer->import_product_rules_from_csv( $uploaded_file );

            if ( $result ) {
                $this->showMessage( $result . ' product level discount rules were imported.');
            } else {
                $this->showMessage( 'The uploaded file could not be imported.', 1);                
            }

        }

    } // handleImportRulesets()


    public function handleActions() {

        if ( isset($_GET['action']) && ( $_GET['action']  == 'wc_bp_export_product_rules' ) ) $this->handleExportRulesets();

    }

    // export product level rules as csv
    protected function handleExportRulesets() {
        global $wpdb;
        require_once( dirname(__FILE__).'/../classes/PricingRulesImporter.php');


        // load all custom product rules
        $sql = "SELECT post_id, meta_value
            FROM $wpdb->postmeta
            WHERE meta_key = '_wc_bulk_pricing_custom_ruleset' AND NOT meta_value = '' 
        ";
        $rows = $wpdb->get_results( $sql );
        echo $wpdb->last_error;

        // handle export
        $importer = new PricingRulesImporter();
        $importer->export_product_rules_as_csv( $rows );            

    }



}
