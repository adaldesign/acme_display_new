<?php

/*************************** LOAD THE BASE CLASS *******************************
 *******************************************************************************
 * The WP_List_Table class isn't automatically available to plugins, so we need
 * to check if it's available and load it if necessary.
 */
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}




/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 * 
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 * 
 * Our theme for this list table is going to be rulesets.
 */
class PricingRulesTable extends WP_List_Table {

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We 
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'rule',     //singular name of the listed records
            'plural'    => 'rules',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }
    
    
    /** ************************************************************************
     * Recommended. This method is called when the parent class can't find a method
     * specifically build for a given column. Generally, it's recommended to include
     * one method for each column you want to render, keeping your package class
     * neat and organized. For example, if the class needs to process a column
     * named 'title', it would first see if a method named $this->column_title() 
     * exists - if it does, that method will be used. If it doesn't, this one will
     * be used. Generally, you should try to use custom column methods as much as 
     * possible. 
     * 
     * Since we have defined a column_title() method later on, this method doesn't
     * need to concern itself with any column with a name of 'title'. Instead, it
     * needs to handle everything else.
     * 
     * For more detailed insight into how columns are handled, take a look at 
     * WP_List_Table::single_row_columns()
     * 
     * @param array $item A singular item (one full row's worth of data)
     * @param array $column_name The name/slug of the column to be processed
     * @return string Text or HTML to be placed inside the column <td>
     **************************************************************************/
    function column_default($item, $column_name){
        switch($column_name){
            case 'name':
            case 'id':
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

    function column_name($item){
        
        $confirm_js = "return confirm('Are you sure you want to delete this profile?');";

        //Build row actions
        $actions = array(
            'edit'      => sprintf('<a href="?page=%s&action=%s&ruleset=%s">%s</a>',$_REQUEST['page'],'edit',$item['id'],__('Edit','wc_bulk_pricing')),
            'duplicate' => sprintf('<a href="?page=%s&action=%s&ruleset=%s">%s</a>',$_REQUEST['page'],'duplicate',$item['id'],__('Duplicate','wc_bulk_pricing')),
            'delete'    => sprintf('<a onclick="%s" href="?page=%s&action=%s&ruleset=%s">%s</a>',$confirm_js,$_REQUEST['page'],'delete',$item['id'],__('Delete','wc_bulk_pricing')),
        );
        
        $is_cumulative = isset( $item['is_cumulative'] ) ? $item['is_cumulative'] : false;
        $details = $is_cumulative ? 'cumulative' : 'exclusive';

        // user role
        if ( @$item['user_roles'] ) 
            $details .= ' - ' . ( $item['user_roles'] == '_guest' ? 'Guest' : ucfirst( $item['user_roles'] ) );

        // make title clickable
        $item_name = sprintf('<a href="?page=%s&action=%s&ruleset=%s">%s</a>',$_REQUEST['page'],'edit',$item['id'],$item['name']);

        //Return the title contents
        return sprintf('%s <br/><span style="color:silver">(ID: %s)</span><br><span style="color:silver">%s</span><br>%s',
            /*$1%s*/ $item_name,
            $item['id'],
            /*$1%s*/ $details,
            /*$3%s*/ $this->row_actions($actions)
        );
    }

    // render html pricing table
    function column_rules($item){        
        woocommerce_bulk_pricing_shortcode::showHtmlTable( $item["rules"], false, 20 );
    }

    // show selected pricing_categories
    function column_pricing_categories($item){        
        
        $active_categories = 0;
        $wc_bulk_pricing_categories = get_option('wc_bulk_pricing_categories', array() );

        foreach ($wc_bulk_pricing_categories as $term_id => $ruleset_ids) {
            #echo "rule $key - val $value <br>";

            // convert to array
            $ruleset_ids = explode(',', $ruleset_ids );

            // if ( $ruleset_id == $item['id'] ) $active_categories++;
            if ( in_array( $item['id'], $ruleset_ids ) ) $active_categories++;
        }

        if ( $active_categories > 0) {
            echo "This profile is assigned to ".$active_categories." categories.<br>";
        } else {
            echo "This profile is not assigned to any categories.<br>";
        }

        $link = sprintf('<a href="?page=%s&tab=%s&ruleset=%s">%s</a>',$_REQUEST['page'],'categories',$item['id'],__('select categories','wc_bulk_pricing'));
        echo "&raquo; $link";

    }

    // show selected products
    function column_products($item){        
        global $wpdb;

        // $wc_bulk_pricing_products = get_option('wc_bulk_pricing_products', array() );
        $active_products = $this->count_active_products_for_ruleset( $item['id'] );


        if ( $active_products > 0) {
            echo "This profile is assigned to ".$active_products." products.<br>";
            $link = sprintf('<a href="?page=%s&tab=%s&ruleset=%s">%s</a>',$_REQUEST['page'],'products',$item['id'],__('show products','wc_bulk_pricing'));
            echo "&raquo; $link";
        } else {
            echo "This profile is not assigned to any products.<br>";
        }

    }


    // count active products for given ruleset id
    function count_active_products_for_ruleset($ruleset_id){        
        global $wpdb;
        $active_products = 0;
        if ( ! $ruleset_id ) return 0;

        $sql = "SELECT post_id
            FROM $wpdb->postmeta
            WHERE meta_key = '_wc_bulk_pricing_ruleset' AND meta_value = '". $ruleset_id ."' 
        ";
        $wc_bulk_pricing_products = $wpdb->get_col( $sql );

        foreach ($wc_bulk_pricing_products as $post_id) {
            $active_products++;
        }

        return $active_products;
    }


    
    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value 
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     * 
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns(){
        $columns = array(
            'name'                  => __('Discount Profile','wc_bulk_pricing'),
            // 'id'                 => __('ID','wc_bulk_pricing'),
            'rules'                 => __('Rules','wc_bulk_pricing'),
            'pricing_categories'    => __('Categories','wc_bulk_pricing'),
            'products'              => __('Products','wc_bulk_pricing')
        );
        return $columns;
    }
    
    /** ************************************************************************
     * Optional. If you want one or more columns to be sortable (ASC/DESC toggle), 
     * you will need to register it here. This should return an array where the 
     * key is the column that needs to be sortable, and the value is db column to 
     * sort by. Often, the key and value will be the same, but this is not always
     * the case (as the value is a column name from the database, not the list table).
     * 
     * @return array An associative array containing all the columns that should be sortable: 'slugs'=>array('data_values',bool)
     **************************************************************************/
    function get_sortable_columns() {
        $sortable_columns = array(
            'name'    => array('name',true)     //true means its already sorted
        );
        return $sortable_columns;
    }
    
    /** ************************************************************************
     * Optional. If you need to include bulk actions in your list table, this is
     * the place to define them. Bulk actions are an associative array in the format
     * 'slug'=>'Visible Title'
     * 
     * If this method returns an empty value, no bulk action will be rendered. If
     * you specify any bulk actions, the bulk actions box will be rendered with
     * the table automatically on display().
     * 
     * Also note that list tables are not automatically wrapped in <form> elements,
     * so you will need to create those manually in order for bulk actions to function.
     * 
     * @return array An associative array containing all the bulk actions: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_bulk_actions() {
        $actions = array(
            // 'remove_rules'    => __('Remove rules','wc_bulk_pricing')
        );
        return $actions;
    }
    
    
    /** ************************************************************************
     * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
     * For this example package, we will handle it in the class to keep things
     * clean and organized.
     * 
     * @see $this->prepare_items()
     **************************************************************************/
    function process_bulk_action() {
        
        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            #wp_die('Items deleted (or they would be if we had items to delete)!');
        }
        
    }
    
    
    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     * 
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items( $data = false ) {                
        
        // process bulk actions
        $this->process_bulk_action();
                        
        // get pagination state
        $current_page = $this->get_pagenum();
        // $per_page = $this->get_items_per_page('rules_per_page', 100);
        // $current_page = 1;
        $per_page = 100;
        
        // define columns
        // $this->_column_headers = $this->get_column_info();
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);


        // sort data
        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'name'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');

        
        // process rules
        $total_items = count( $data );
        $data        = array_slice($data,(($current_page-1)*$per_page),$per_page);
        $this->items = $data;
        // $this->items = $this->getItems();
        // echo "<pre>";print_r($data);echo"</pre>";die();    

        // register our pagination options & calculations.
        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items/$per_page)
        ) );

    }




    
}

