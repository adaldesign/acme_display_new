<?php

/*************************** LOAD THE BASE CLASS *******************************
 *******************************************************************************
 * The WP_List_Table class isn't automatically available to plugins, so we need
 * to check if it's available and load it if necessary.
 */
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}




/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 * 
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 * 
 * Our theme for this list table is going to be products.
 */
class PricingProductsTable extends WP_List_Table {

    private $_rulesets = null;
    private $_products = null;

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We 
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'product',     //singular name of the listed records
            'plural'    => 'products',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }
    
    
    /** ************************************************************************
     * Recommended. This method is called when the parent class can't find a method
     * specifically build for a given column. Generally, it's recommended to include
     * one method for each column you want to render, keeping your package class
     * neat and organized. For example, if the class needs to process a column
     * named 'title', it would first see if a method named $this->column_title() 
     * exists - if it does, that method will be used. If it doesn't, this one will
     * be used. Generally, you should try to use custom column methods as much as 
     * possible. 
     * 
     * Since we have defined a column_title() method later on, this method doesn't
     * need to concern itself with any column with a name of 'title'. Instead, it
     * needs to handle everything else.
     * 
     * For more detailed insight into how columns are handled, take a look at 
     * WP_List_Table::single_row_columns()
     * 
     * @param array $item A singular item (one full row's worth of data)
     * @param array $column_name The name/slug of the column to be processed
     * @return string Text or HTML to be placed inside the column <td>
     **************************************************************************/
    function column_default($item, $column_name){
        switch($column_name){
            case 'product':
            case 'ruleset':
                return $item[$column_name];
            case 'post_id':
                return '&nbsp;';
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

    function column_product( $item ) {
        // return sprintf('<a href="post.php?post=%s&action=edit">%s</a>', $item['post_id'], $item['product'] );

        //Build row actions
        $actions = array(
            'edit_product' => sprintf('<a href="post.php?post=%s&action=edit">%s</a>',$item['post_id'],__('Edit Product','wc_bulk_pricing')),
            'edit_ruleset' => sprintf('<a href="admin.php?page=wc_bulk_pricing&action=edit&ruleset=%s">%s</a>',$item['ruleset_id'],__('Edit Profile','wc_bulk_pricing')),
            'delete'       => sprintf('<a href="?page=%s&tab=%s&action=%s&product=%s">%s</a>',$_REQUEST['page'],$_REQUEST['tab'],'remove_rules_from_products',$item['post_id'],__('Remove','wc_bulk_pricing')),
        );

        // make title link to edit page
        // $title = sprintf('<a href="post.php?post=%s&action=edit">%s</a>', $item['post_id'], $item['product'] );
        $title = $item['product'];

        // hide edit link for custom rulesets
        if ( $item['ruleset_id'] == '_custom' ) unset( $actions['edit_ruleset'] );
        
        //Return the title contents
        return sprintf('%1$s <br>%2$s',
            /*$1%s*/ $title,
            /*$3%s*/ $this->row_actions($actions)
        );

    }
    function column_ruleset( $item ) {
        if ( $item['ruleset'] == '_custom' ) {
    
            // get custom product rules
            $rules = maybe_unserialize( get_post_meta( $item['post_id'], '_wc_bulk_pricing_custom_ruleset', true ) );

            woocommerce_bulk_pricing_shortcode::showHtmlTable( $rules, false, 20 );
            return;

            // return '<i>Custom Product Ruleset</i>'; 
        }
        return sprintf('<a href="admin.php?page=wc_bulk_pricing&action=edit&ruleset=%s">%s</a>', $item['ruleset_id'], $item['ruleset'] );
    }

    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("profile")
            /*$2%s*/ $item['post_id']        //The value of the checkbox should be the record's id
        );
    }
    

    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value 
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     * 
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns(){
        $columns = array(
            'cb'                    => '<input type="checkbox" />', //Render a checkbox instead of text
            'product'               => __('Product','wc_bulk_pricing'),
            'ruleset'                => __('Discount Profile','wc_bulk_pricing'),
            'post_id'                => __('','wc_bulk_pricing')
        );
        return $columns;
    }
    
    
    /** ************************************************************************
     * Optional. If you need to include bulk actions in your list table, this is
     * the place to define them. Bulk actions are an associative array in the format
     * 'slug'=>'Visible Title'
     * 
     * If this method returns an empty value, no bulk action will be rendered. If
     * you specify any bulk actions, the bulk actions box will be rendered with
     * the table automatically on display().
     * 
     * Also note that list tables are not automatically wrapped in <form> elements,
     * so you will need to create those manually in order for bulk actions to function.
     * 
     * @return array An associative array containing all the bulk actions: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_bulk_actions() {
        $actions = array(
            'remove_rules_from_products'    => __('Remove rules from products','wc_bulk_pricing')
        );
        return $actions;
    }
    
    
    /** ************************************************************************
     * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
     * For this example package, we will handle it in the class to keep things
     * clean and organized.
     * 
     * @see $this->prepare_items()
     **************************************************************************/
    function process_bulk_action() {
        
        // Detect when a bulk action is being triggered...
        if( 'remove_rules_from_products' === $this->current_action() ) {
            
            $products = $_REQUEST['product'];
            if ( ! is_array( $products ) ) $products = array( $_REQUEST['product'] );

            foreach ($products as $post_id) {
                $this->removeAllRulesetsFromProduct( $post_id );
            }

        }
        
    }
    
    
    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     * 
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items( $data = false ) {                
        
        // process bulk actions
        $this->process_bulk_action();
                        
        // get pagination state
        $current_page = $this->get_pagenum();
        $per_page = $this->get_items_per_page('products_per_page', 100);
        // $current_page = 1;
        // $per_page = 100;
        
        // define columns
        // $this->_column_headers = $this->get_column_info();
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        // fetch products
        // $this->items = $data;
        $this->items = $this->loadProductRules( $current_page, $per_page );
        $total_items = $this->total_items;
        // print_r($data);

        // register our pagination options & calculations.
        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items/$per_page)
        ) );

    }



    protected function loadProductRules( $current_page, $per_page ) {
        global $wpdb;

        $active_products = array();
        $rulesets        = wbp_get_rules();
        $offset          = ( $current_page - 1 ) * $per_page;

        $sql = "SELECT post_id, meta_value
            FROM $wpdb->postmeta
            WHERE meta_key = '_wc_bulk_pricing_ruleset' AND NOT meta_value = ''
            LIMIT $offset, $per_page
        ";
        $items = $wpdb->get_results( $sql );
        // echo $wpdb->last_error;


       
        // get total items count - if needed
        if ( ( $current_page == 1 ) && ( count( $items ) < $per_page ) ) {
            $this->total_items = count( $items );
        } else {
            $this->total_items = $wpdb->get_var("
                SELECT COUNT(meta_id)
                FROM $wpdb->postmeta
                WHERE meta_key = '_wc_bulk_pricing_ruleset' AND NOT meta_value = ''
            ");         
        }


        foreach ($items as $row) {
            $product = array();
            $product['product'] = get_the_title( $row->post_id );
            $product['ruleset_id'] = $row->meta_value;
            $product['post_id'] = $row->post_id;
            $product['ruleset'] = isset( $rulesets[$row->meta_value] ) ? $rulesets[$row->meta_value]['name'] : $row->meta_value;

            $active_products[] = $product;
        }

        // echo "<pre>";print_r($active_products);echo "</pre>";

        return $active_products;
    }

    protected function removeAllRulesetsFromProduct( $post_id ) {

        delete_post_meta( $post_id, '_wc_bulk_pricing_ruleset' );
        delete_post_meta( $post_id, '_wc_bulk_pricing_custom_ruleset' );

    }

    

    
}

