<?php

if ( ! defined( 'DS' ) ) define( 'DS', DIRECTORY_SEPARATOR );

class woocommerce_bulk_pricing_admin {

    static public $PLUGIN_PATH;
    static public $PLUGIN_DIR;
    static public $PLUGIN_URL;

    const ViewExt           = '.php';
    const ViewDir           = '../views';

    private $plugin_url;
    
    var $pages = array();

    public function __construct() {

        self::$PLUGIN_PATH = plugin_basename( dirname(dirname(dirname(__FILE__))) );
        self::$PLUGIN_DIR = WP_PLUGIN_DIR.DS.self::$PLUGIN_PATH.DS;
        self::$PLUGIN_URL = WP_PLUGIN_URL.'/'.self::$PLUGIN_PATH.'/';

        add_action('admin_init', array(&$this, 'enqueue'));
        add_action('admin_head', array(&$this, 'admin_register_head'));
        // add_action('admin_init', array(&$this, 'register_settings'));
        add_action('admin_menu', array(&$this, 'on_admin_menu'), 100);

	    // Moved the Discount Profile selection to the Bulk Pricing tab
        // add_action('woocommerce_product_options_pricing', array(&$this, 'on_woocommerce_product_options_pricing'), 10);
        //add_action('woocommerce_product_options_sku', array(&$this, 'on_woocommerce_product_options_pricing'), 10);
        add_action('woocommerce_process_product_meta', array(&$this, 'process_product_meta'), 10, 2);

        // add options to variable products
        // add_action('woocommerce_variation_options', array(&$this, 'on_woocommerce_variation_options_pricing'), 10, 3);
        add_action('woocommerce_product_after_variable_attributes', array(&$this, 'on_woocommerce_variation_options_pricing'), 10, 3);
        add_action('woocommerce_process_product_meta_variable', array(&$this, 'process_product_meta_variable'), 10, 1);
        add_action('woocommerce_ajax_save_product_variations',  array( $this, 'process_product_meta_variable') ); // WC2.4

        // add custom writepanel tab
        add_action( 'woocommerce_product_write_panel_tabs', array( &$this, 'product_write_panel_tab' ) );
        add_action( 'woocommerce_product_data_panels',     array( &$this, 'product_write_panel' ) );

        $this->loadPages();

    }


    public function process_product_meta_variable( $post_id ) {
        // echo "<pre>";print_r($_POST);echo"</pre>";die();

        if (isset($_POST['variable_sku'])) {

            $variable_post_id           = $_POST['variable_post_id'];
            $variable_ruleset           = $_POST['variable_wc_bulk_pricing_ruleset'];
            $variable_rules             = isset( $_POST['var_rules'] ) ? $_POST['var_rules'] : false;

            if (isset($_POST['variable_enabled']))
                $variable_enabled           = $_POST['variable_enabled'];

            $max_loop = max( array_keys( $_POST['variable_post_id'] ) );

            for ( $i=0; $i <= $max_loop; $i++ ) {

                if ( ! isset( $variable_post_id[$i] ) ) continue;

                $variation_id = (int) $variable_post_id[$i];

                // Update post meta
                // update_post_meta( $variation_id, '_wc_bulk_pricing_ruleset', $variable_ruleset[$i] );

                if ( $variable_ruleset[$i] !== 'parent' )
                    update_post_meta( $variation_id, '_wc_bulk_pricing_ruleset', $variable_ruleset[$i] );
                else
                    delete_post_meta( $variation_id, '_wc_bulk_pricing_ruleset' );


                // update custom variations rulesets
                // echo "<pre>";print_r($variable_rules);echo"</pre>";die();
                if ( $variable_rules && isset($variable_rules[$i]) && is_array($variable_rules[$i]) ) {
                    $rules = $variable_rules[$i];

                    // filter empty rules
                    foreach ($rules as $key => &$rule) {
                        if ( ( trim($rule['min']) == '' ) &&  ( trim($rule['max']) == '' ) ) {
                            unset( $rules[$key] );
                        } elseif ( ( trim($rule['min']) == '' ) ) {
                            $rule['min'] = '*';
                        } elseif ( ( trim($rule['max']) == '' ) ) {
                            $rule['max'] = '*';
                        }
                    }
                    // re-index array to remove empty rows
                    $rules = array_values( $rules );

                    if ( sizeof( $rules ) > 1 ) {
                        update_post_meta($variation_id, '_wc_bulk_pricing_custom_ruleset', $rules );
                    } else {
                        delete_post_meta($variation_id, '_wc_bulk_pricing_custom_ruleset');
                    }
                    // echo "<pre>";print_r($rules);echo"</pre>";die();

                }


            } // each variation

        } // if product has variations

    } // process_product_meta_variable()

    function on_woocommerce_variation_options_pricing( $loop, $variation_data, $variation ) {
        // echo "<pre>";print_r($variation_data);echo"</pre>";#die();
    
        // get available pricing rules
        $rulesets = wbp_get_rules( true );

        $options = array( '' => __('Same as parent', 'wc_bulk_pricing') );
        foreach ($rulesets as $ruleset_id => $ruleset ) {
            $options[$ruleset_id] = $ruleset['name'];
        }
        $options['_custom'] = __('Custom Discounts','wc_bulk_pricing').' (beta)';


        // get variation post_id - WC2.3
        $variation_post_id = $variation ? $variation->ID : $variation_data['variation_post_id']; // $variation exists since WC2.2 (at least)

        // get current values - WC2.3
        $_wc_bulk_pricing_ruleset = get_post_meta( $variation_post_id, '_wc_bulk_pricing_ruleset', true );

        ?>

            <div>
                <p class="form-row form-row-full">
                    <label>
                        <?php _e('Discount profile', 'wc_bulk_pricing'); ?>
                        <a class="tips" data-tip="Select a discount profile for this variation. It is required that you set a default ruleset on the Bulk Pricing tab or this setting will be ignored." href="#">[?]</a>
                    </label> 
                    <select name="variable_wc_bulk_pricing_ruleset[<?php echo $loop; ?>]" class="wc_bulk_pricing_ruleset_selector">
                        <?php
                        foreach ( $options as $key => $option_name ) {
                            echo '<option value="' . $key . '" ';
                            selected( $key, $_wc_bulk_pricing_ruleset );
                            echo '>' . $option_name . '</option>';
                        }
                        ?>
                    </select>
                </p>
            </div>

        <?php


        // get custom product rules
        // $rules = maybe_unserialize( get_post_meta( $post->ID, '_wc_bulk_pricing_custom_ruleset', true ) );
        $rules = maybe_unserialize( get_post_meta( $variation_post_id, '_wc_bulk_pricing_custom_ruleset', true ) );
        // $rules = @$variation_data['_wc_bulk_pricing_custom_ruleset'];
        // echo "<pre>";print_r($variation_data);echo"</pre>";die();
        // echo "<pre>";print_r($rules);echo"</pre>";die();

        $itemdata = array(
            'rows_limit'            => get_option('wc_bulk_pricing_default_number_of_rows', 5 ),
            'rules'                 => ! empty( $rules ) ? $rules : array(),
            'id'                    => ! empty( $rules ) ? true : false,
            'loop'                  => $loop
        );
        ?>

            <tr>
                <td colspan="2">
                    <div id="" class="woocommerce_bulk_pricing_variation_rules" style="display:none">
                        <?php $this->display( 'rules_edit_table', array( 'item' => $itemdata ) ); ?>
                    </div>
                </td>
            </tr>

        <?php
    }


    function on_woocommerce_product_options_pricing() {
    
        // get available pricing rules
        $rulesets = wbp_get_rules( true );

        $options = array( '' => '-- none --' );
        foreach ($rulesets as $ruleset_id => $ruleset ) {
            $options[$ruleset_id] = $ruleset['name'];
        }
        $options['_custom'] = __('Custom Discounts','wc_bulk_pricing');

        woocommerce_wp_select( array( 
            'id' => '_wc_bulk_pricing_ruleset', 
            'label' => __('Discount profile', 'wc_bulk_pricing'), 
            'options' => $options
        ));


    }

    // adds a new tab on the edit product page
    public function product_write_panel_tab() {
        echo  '
        <style>
            #woocommerce-product-data ul.wc-tabs li.bulk_pricing_tab a::before, 
            .woocommerce ul.wc-tabs li.bulk_pricing_tab a::before {
                content: "\f480";
            }
        </style>
        ';

        echo "<li class=\"bulk_pricing_tab advanced_options x-hide_if_variable\"><a href=\"#woocommerce_bulk_pricing_rules\"><span>" . __('Bulk Pricing','wc_bulk_pricing') . "</span></a></li>";
    }


    // adds the tab panel content on the edit product page
    public function product_write_panel() {
        global $post;

        if ( version_compare( WOOCOMMERCE_VERSION, "2.1-beta-1" ) >= 0 ) {
            $style        = '';
            $active_style = '';
        } elseif ( version_compare( WOOCOMMERCE_VERSION, "2.0.0" ) >= 0 ) {
            $style = 'padding:5px 5px 5px 28px;background-repeat:no-repeat;background-position:5px 5px;';
            // $style = 'padding:5px 5px 5px  5px;background-repeat:no-repeat;background-position:5px 7px;';
            $active_style  = 'div#woocommerce_bulk_pricing_rules { padding: inherit }';
            $active_style .= 'div#woocommerce_bulk_pricing_rules table { padding-top: 10px }';
        } else {
            $style = 'padding:9px 9px 9px 34px;line-height:16px;border-bottom:1px solid #d5d5d5;text-shadow:0 1px 1px #fff;color:#555555;background-repeat:no-repeat;background-position:9px 9px;';
            // $style = 'padding:9px 9px 9px  9px;line-height:16px;border-bottom:1px solid #d5d5d5;text-shadow:0 1px 1px #fff;color:#555555;background-repeat:no-repeat;background-position:9px 9px;';
            $active_style = '#woocommerce-product-data ul.product_data_tabs li.bulk_pricing_tab.active a { border-bottom: 1px solid #F8F8F8; }';
        }
        ?>
        <style type="text/css">
            #woocommerce-product-data ul.product_data_tabs li.bulk_pricing_tab a { <?php echo $style; ?> }
            <?php echo $active_style; ?>

            /* adjust width of variation rules table */
            #variable_product_options .woocommerce_variation table td.data table.data_table td .woocommerce_bulk_pricing_variation_rules table td {
                width: auto;
            }
        </style>
        <script>
            jQuery( document ).ready( function ($) {

                $('#variable_product_options').on('change', '.wc_bulk_pricing_ruleset_selector', function() {
                    if ( $(this).val() == '_custom' ) {
                        $(this).parent().parent().parent().find('.woocommerce_bulk_pricing_variation_rules').slideDown();
                    } else {
                        $(this).parent().parent().parent().find('.woocommerce_bulk_pricing_variation_rules').slideUp();
                    }
                });
                $('#variable_product_options .wc_bulk_pricing_ruleset_selector').change();

                $("#variable_product_options").on("click", ".woocommerce_variation h3", function() {
                    var rule = $(this).parent().find('select.wc_bulk_pricing_ruleset_selector').val();

                    if ( rule == '_custom' ) {
                        $(this).parent().parent().parent().find('.woocommerce_bulk_pricing_variation_rules').slideDown();
                    } else {
                        $(this).parent().parent().parent().find('.woocommerce_bulk_pricing_variation_rules').slideUp();
                    }
                });

	            $("#_wc_bulk_pricing_ruleset").change(function() {
		            if ( $(this).val() == '_custom' ) {
			            $(this).parents('.panel').find(".rules").show();
		            } else {
			            $(this).parents('.panel').find(".rules").hide();
		            }
	            }).change();
            });
        </script>
        <?php

        // get custom product rules
        $rules = maybe_unserialize( get_post_meta( $post->ID, '_wc_bulk_pricing_custom_ruleset', true ) );

        $itemdata = array(
            'rows_limit'            => get_option('wc_bulk_pricing_default_number_of_rows', 5 ),
            'rules'                 => ! empty( $rules ) ? $rules : array(),
            'id'                    => ! empty( $rules ) ? true : false
        );
	    ?>
        <div id="woocommerce_bulk_pricing_rules" class="panel wc-metaboxes-wrapper woocommerce_options_panel">
	    <?php
	    // get available pricing rules
        $rulesets = wbp_get_rules( true );

        $options = array( '' => '-- none --' );
        foreach ($rulesets as $ruleset_id => $ruleset ) {
	        $options[$ruleset_id] = $ruleset['name'];
        }
        $options['_custom'] = __('Custom Discounts','wc_bulk_pricing');

        woocommerce_wp_select( array(
	        'id' => '_wc_bulk_pricing_ruleset',
	        'label' => __('Discount profile', 'wc_bulk_pricing'),
	        'options' => $options
        ));

        $this->display( 'rules_edit_table', array( 'item' => $itemdata ) );
        echo '<p class="rules">&nbsp;'.__('Note: These discount rules will only be active if the "Discount Profile" option is set to "Custom Discounts".','wc_bulk_pricing').'</p><br><br>';
        echo '</div>';

        // // display the custom tab panel
        // echo '<div id="woocommerce_bulk_pricing_rules" class="panel wc-metaboxes-wrapper woocommerce_options_panel">';
        // woocommerce_wp_text_input( array( 'id' => '_tab_title', 'label' => __( 'Tab Title' ), 'description' => __( 'Required for tab to be visible' ), 'value' => $tab['title'] ) );
        // echo '</div>';

    }
    
    public function process_product_meta($post_id, $post) {

        // handle discount ruleset selection
        if ( (isset($_POST['_wc_bulk_pricing_ruleset'])) && ( $_POST['_wc_bulk_pricing_ruleset'] != '' ) ) {
            update_post_meta($post_id, '_wc_bulk_pricing_ruleset', $_POST['_wc_bulk_pricing_ruleset'] );
        } else {
            delete_post_meta($post_id, '_wc_bulk_pricing_ruleset');
        }

        // handle custom product rules
        if ( (isset($_POST['rules'])) && ( $_POST['rules'] != '' ) ) {
            $rules = $_POST['rules'];

            // filter empty rules
            foreach ($rules as $key => &$rule) {
                if ( ( trim($rule['min']) == '' ) &&  ( trim($rule['max']) == '' ) ) {
                    unset( $rules[$key] );
                } elseif ( ( trim($rule['min']) == '' ) ) {
                    $rule['min'] = '*';
                } elseif ( ( trim($rule['max']) == '' ) ) {
                    $rule['max'] = '*';
                }
            }
            // re-index array to remove empty rows
            $rules = array_values( $rules );

            update_post_meta($post_id, '_wc_bulk_pricing_custom_ruleset', $rules );
            // echo "<pre>";print_r($rules);echo"</pre>";die();
        }

    }

    function admin_register_head() {
        // global $woocommerce_bulk_pricing;

        if ( ! isset($_REQUEST['page']) ) return;
        if ( $_REQUEST['page'] != 'wc_bulk_pricing' ) return;

        $url = $this->plugin_url() . '/admin/admin.css';
        echo "<link rel='stylesheet' type='text/css' href='$url' />\n";
    }

    public function enqueue() {
        // global $woocommerce_bulk_pricing;
        // wp_enqueue_script('woocommerce-pricing-admin', $woocommerce_bulk_pricing->plugin_url() . '/admin/admin.js', array('jquery'));
    }

    public function on_admin_menu() {
        $slug = add_submenu_page('woocommerce', __('Bulk Pricing','wc_bulk_pricing'), __('Bulk Pricing','wc_bulk_pricing'), 'manage_woocommerce', 'wc_bulk_pricing', array(&$this, 'onDisplayAdminPage'));
    }


    public function loadPages() {

        $this->pages['rules']      = new WCBP_RulesetsPage();
        $this->pages['categories'] = new WCBP_CategoriesPage();
        $this->pages['products']   = new WCBP_ProductsPage();
        $this->pages['license']    = new WCBP_LicensePage();
        $this->pages['roles']      = new WCBP_RolesPage();
        $this->pages['settings']   = new WCBP_SettingsPage();

    }
        

    public function onDisplayAdminPage() {
        
        $current_tab = (isset($_GET['tab'])) ? $_GET['tab'] : 'rulesets';

        if ( $current_tab  == 'categories' ) return $this->pages['categories']->displayCategoriesPage();
        if ( $current_tab  == 'products' )   return $this->pages['products']->displayProductsPage();
        if ( $current_tab  == 'license' )    return $this->pages['license']->displayLicensePage();
        if ( $current_tab  == 'roles' )      return $this->pages['roles']->displayRolesPage();
        if ( $current_tab  == 'settings' )   return $this->pages['settings']->displaySettingsPage();

        // show rules page by default
        $page = new WCBP_RulesetsPage(); 
        $page->displayRulesetsPage();

    }
    

    // display view
    protected function display( $insView, $inaData = array(), $echo = true ) {
        $sFile = dirname(__FILE__).DS.self::ViewDir.DS.$insView.self::ViewExt;
        
        if ( !is_file( $sFile ) ) {
            $this->showMessage("View not found: ".$sFile,1,1);
            return false;
        }
        
        if ( count( $inaData ) > 0 ) {
            extract( $inaData, EXTR_PREFIX_ALL, 'wpl' );
        }
        
        ob_start();
            include( $sFile );
            $sContents = ob_get_contents();
        ob_end_clean();

        if ($echo) {
            echo $sContents;
            return true;
        } else {
            return $sContents;
        }
    
    }

    /* Generic message display */
    public function showMessage($message, $errormsg = false, $echo = true) {       
        $class = ($errormsg) ? 'error' : 'updated fade';
        $this->message .= '<div id="message" class="'.$class.'"><p>'.$message.'</p></div>';
        if ($echo) echo $this->message;
    }




    /**
     * Get the plugin path
     */
    public function plugin_path() {
        if ($this->plugin_path)
            return $this->plugin_path;
        return $this->plugin_path = WP_PLUGIN_DIR . "/" . plugin_basename(dirname(dirname(dirname(__FILE__))));
    }

    public function plugin_url() {
        if ($this->plugin_url)
            return $this->plugin_url;

        if (is_ssl()) :
            return $this->plugin_url = str_replace('http://', 'https://', WP_PLUGIN_URL) . "/" . plugin_basename(dirname(dirname(dirname(__FILE__))));
        else :
            return $this->plugin_url = WP_PLUGIN_URL . "/" . plugin_basename(dirname(dirname(dirname(__FILE__))));
        endif;
    }

}
