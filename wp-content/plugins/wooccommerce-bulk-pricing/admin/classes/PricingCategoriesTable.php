<?php

/*************************** LOAD THE BASE CLASS *******************************
 *******************************************************************************
 * The WP_List_Table class isn't automatically available to plugins, so we need
 * to check if it's available and load it if necessary.
 */
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}




/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 * 
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 * 
 * Our theme for this list table is going to be categories.
 */
class PricingCategoriesTable extends WP_List_Table {

    private $_rulesets = null;
    private $_categories = null;

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We 
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'category',     //singular name of the listed records
            'plural'    => 'categories',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }
    
    
    /** ************************************************************************
     * Recommended. This method is called when the parent class can't find a method
     * specifically build for a given column. Generally, it's recommended to include
     * one method for each column you want to render, keeping your package class
     * neat and organized. For example, if the class needs to process a column
     * named 'title', it would first see if a method named $this->column_title() 
     * exists - if it does, that method will be used. If it doesn't, this one will
     * be used. Generally, you should try to use custom column methods as much as 
     * possible. 
     * 
     * Since we have defined a column_title() method later on, this method doesn't
     * need to concern itself with any column with a name of 'title'. Instead, it
     * needs to handle everything else.
     * 
     * For more detailed insight into how columns are handled, take a look at 
     * WP_List_Table::single_row_columns()
     * 
     * @param array $item A singular item (one full row's worth of data)
     * @param array $column_name The name/slug of the column to be processed
     * @return string Text or HTML to be placed inside the column <td>
     **************************************************************************/
    function column_default($item, $column_name){
        switch($column_name){
            case 'category':
            case 'ruleset':
                return $item[$column_name];
            case 'term_id':
                return '&nbsp;';
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

    function column_category( $item ) {
        if ( ! $this->_rulesets ) $this->_rulesets = wbp_get_rules();
        if ( ! $this->_categories ) $this->_categories = get_option('wc_bulk_pricing_categories', array() );
        $isChecked = ( '' != @$this->_categories[ $item['term_id'] ]) ? 'checked' : '';

        $checkbox  = sprintf('<input id="category_%s" name="category[%s][active]" type="checkbox" class="ipstyle" %s/>', $item['term_id'], $item['term_id'], $isChecked );
        $checkbox .= sprintf('<label for="category_%s" class="ipstyle">%s</label>', $item['term_id'], $item['category'] );
        return $checkbox;
    }

    function column_ruleset( $item ) {

        if ( ! $this->_rulesets ) $this->_rulesets = wbp_get_rules();
        if ( ! $this->_categories ) $this->_categories = get_option('wc_bulk_pricing_categories', array() );
        $categoryRulesets = isset( $this->_categories[ $item['term_id'] ] ) ? explode(',', $this->_categories[ $item['term_id'] ] ) : false;

        // open
        $html = sprintf('<div id="select_category_%s" class="select_ruleset_wrapper">', $item['term_id'] );

        // add / remove buttons
        $btn_add = '<a href="#" class="btn_add_ruleset button-secondary">+</a>';
        $btn_del = '<a href="#" class="btn_del_ruleset button-secondary">&ndash;</a>';
        $btn_del_hidden = '<a href="#" class="btn_del_ruleset button-secondary" style="display:none">&ndash;</a>';

        // select options
        $select = '';
        if ( is_array( $categoryRulesets ) ) {
            $count = 0;
            foreach ($categoryRulesets as $ruleset_id ) {                
                $select .= $this->getRulesetSelector( $item, $ruleset_id );
                if ( $count == 0 ) {
                    $select .= $btn_del_hidden;
                    $select .= $btn_add;
                } else {
                    $select .= $btn_del;                    
                }
                $count++;
            }        
        } else {
            $select = $this->getRulesetSelector( $item );
            $select .= $btn_del_hidden;
            $select .= $btn_add;
        }


        // close
        $html   .= $select;
        // $html   .= $btn_add;# . $btn_del;
        $html   .= '</div>';

        return $html;
    }

    private function getRulesetSelector( $item, $ruleset_id = false ) {
        $categoryRulesets = isset( $this->_categories[ $item['term_id'] ] ) ? $this->_categories[ $item['term_id'] ] : false;

        $select = sprintf('<select name="category[%s][ruleset][]" >', $item['term_id'], $item['term_id'] );
        foreach ($this->_rulesets as $ruleset) {
            $isSelected = ( $ruleset['id'] == $ruleset_id ) ? 'selected' : '';
            $ruleset_title = $ruleset['user_roles'] ? $ruleset['name'] .' ('. ucfirst( $ruleset['user_roles'] ) .')' : $ruleset['name'];
            $ruleset_title = str_replace('_guest', 'Guest', $ruleset_title);
            $select .= sprintf('<option value="%s" %s>%s</option>', $ruleset['id'], $isSelected, $ruleset_title );
        }
        $select .= "</select>";

        return $select;
    }
    
    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value 
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     * 
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns(){
        $columns = array(
            'category'               => __('Category','wc_bulk_pricing'),
            'ruleset'                => __('Discount Profile','wc_bulk_pricing'),
            'term_id'                => __('','wc_bulk_pricing')
        );
        return $columns;
    }
    
    
    /** ************************************************************************
     * Optional. If you need to include bulk actions in your list table, this is
     * the place to define them. Bulk actions are an associative array in the format
     * 'slug'=>'Visible Title'
     * 
     * If this method returns an empty value, no bulk action will be rendered. If
     * you specify any bulk actions, the bulk actions box will be rendered with
     * the table automatically on display().
     * 
     * Also note that list tables are not automatically wrapped in <form> elements,
     * so you will need to create those manually in order for bulk actions to function.
     * 
     * @return array An associative array containing all the bulk actions: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_bulk_actions() {
        $actions = array(
            // 'remove_categories'    => __('Remove categories','wc_bulk_pricing')
        );
        return $actions;
    }
    
    
    /** ************************************************************************
     * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
     * For this example package, we will handle it in the class to keep things
     * clean and organized.
     * 
     * @see $this->prepare_items()
     **************************************************************************/
    function process_bulk_action() {
        
        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            #wp_die('Items deleted (or they would be if we had items to delete)!');
        }
        
    }
    
    
    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     * 
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items( $data = false ) {                
        
        // process bulk actions
        $this->process_bulk_action();
                        
        // get pagination state
        // $current_page = $this->get_pagenum();
        // $per_page = $this->get_items_per_page('categories_per_page', 100);
        $current_page = 1;
        $per_page = 1000;
        
        // define columns
        // $this->_column_headers = $this->get_column_info();
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        // fetch categories
        // $this->items = $data;
        $this->items = $this->loadProductCategories();
        $total_items = count( $this->items );
        // print_r($data);

        // register our pagination options & calculations.
        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items/$per_page)
        ) );

    }



    protected function loadProductCategories() {
    global $wpdb;

        $tree = get_terms( 'product_cat', 'orderby=count&hide_empty=0' );

        $result = $this->parseTree( $tree );
        $flatlist = $this->printTree( $result );
        // echo "<pre>";print_r($flatlist);echo "</pre>";

        return $flatlist;
    }

    // parses wp terms array into a hierarchical tree structure
    function parseTree( $tree, $root = 0 ) {
        $return = array();

        // Traverse the tree and search for direct children of the root
        foreach ( $tree as $key => $item ) {

            // A direct child item is found
            if ( $item->parent == $root ) {

                // Remove item from tree (we don't need to traverse this again)
                unset( $tree[ $key ] );
                
                // Append the item into result array and parse its children
                $item->children = $this->parseTree( $tree, $item->term_id );
                $return[] = $item;

            }
        }
        return empty( $return ) ? null : $return;
    }

    function printTree( $tree, $depth = 0, $result = array() ) {
        // $categories_map_ebay  = self::getOption( 'categories_map_ebay'  );
        // $categories_map_store = self::getOption( 'categories_map_store' );
        if( ($tree != 0) && (count($tree) > 0) ) {
            foreach($tree as $node) {
                // indent category name accourding to depth
                $node->name = str_repeat('&ndash; ' , $depth) . $node->name;
                // echo $node->name;
                
                // get ebay category and (full) name
                // $ebay_category_id  = @$categories_map_ebay[$node->term_id];
                // $store_category_id = @$categories_map_store[$node->term_id];

                // add item to results - excluding children
                $tmpnode = array(
                    'term_id'             => $node->term_id,
                    'parent'              => $node->parent,
                    'category'            => $node->name,
                    // 'ebay_category_id'    => $ebay_category_id,
                    // 'ebay_category_name'  => EbayCategoriesModel::getFullEbayCategoryName( $ebay_category_id ),
                    // 'store_category_id'   => $store_category_id,
                    // 'store_category_name' => EbayCategoriesModel::getFullStoreCategoryName( $store_category_id ),
                    'description'         => $node->description
                );

                $result[] = $tmpnode;
                $result = $this->printTree( $node->children, $depth+1, $result );
            }
        }
        return $result;
    }



    
}

