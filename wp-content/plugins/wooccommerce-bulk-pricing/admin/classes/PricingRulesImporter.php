<?php

class PricingRulesImporter  {

    private $target_path;

    public function __construct() {

        $this->target_path = WP_CONTENT_DIR.'/uploads/discount-profiles.csv';

    }



    /**
     * Import rulesets from a csv file
     */
    public static function import_csv( $filename, $delimiter = ',' ) {

        include( plugin_dir_path( __FILE__ ) . 'class-readcsv.php' );

        // Loop through the file lines
        $file_handle = fopen( $filename, 'r' );
        $csv_reader = new ReadCSV( $file_handle, $delimiter, "\xEF\xBB\xBF" ); // Skip any UTF-8 byte order mark.

        $rulesets = array();
        $first = true;
        $rkey = 0;
        while ( ( $line = $csv_reader->get_row() ) !== NULL ) {

            // echo "<pre>";print_r($line);echo"</pre>";#die();

            // If the first line is empty, abort
            // If another line is empty, just skip it
            if ( empty( $line ) ) {
                if ( $first )
                    break;
                else
                    continue;
            }

            // If we are on the first line, the columns are the headers
            if ( $first ) {
                $headers = $line;
                $first = false;
                continue;
            }

            // check delimiter - abort if tab character found
            if ( strpos($line[0], "\t") ) {
                self::showMessage('Invalid file format - CSV files need to use <i>comma</i> as separator, not <i>tab</i>. Please adjust your CSV export settings in Excel.',1);
                return false;                
            }

            // User data fields list used to differentiate with user meta
            $itemdata_fields = array(
                'ID'           => 'id',
                'SKU'          => 'sku',
                'sku'          => 'sku',
                'Ruleset Name' => 'name',
                'Mode'         => 'is_cumulative',
                'Show Lowest'  => 'show_lowest_price',
                'User Role'    => 'user_roles',
                'Qty 1'        => 'qty_1',
                'Price 1'      => 'price_1',
                'Qty 2'        => 'qty_2',
                'Price 2'      => 'price_2',
                'Qty 3'        => 'qty_3',
                'Price 3'      => 'price_3',
                'Qty 4'        => 'qty_4',
                'Price 4'      => 'price_4',
                'Qty 5'        => 'qty_5',
                'Price 5'      => 'price_5',
                'Qty 6'        => 'qty_6',
                'Price 6'      => 'price_6',
                'Qty 7'        => 'qty_7',
                'Price 7'      => 'price_7',
                'Qty 8'        => 'qty_8',
                'Price 8'      => 'price_8',
                'Qty 9'        => 'qty_9',
                'Price 9'      => 'price_9',
                'Qty 10'       => 'qty_10',
                'Price 10'     => 'price_10',
                'Qty 11'       => 'qty_11',
                'Price 11'     => 'price_11',
                'Qty 12'       => 'qty_12',
                'Price 12'     => 'price_12',
                'Qty 13'       => 'qty_13',
                'Price 13'     => 'price_13',
                'Qty 14'       => 'qty_14',
                'Price 14'     => 'price_14',
                'Qty 15'       => 'qty_15',
                'Price 15'     => 'price_15',
                'Qty 16'       => 'qty_16',
                'Price 16'     => 'price_16',
                'Qty 17'       => 'qty_17',
                'Price 17'     => 'price_17',
                'Qty 18'       => 'qty_18',
                'Price 18'     => 'price_18',
                'Qty 19'       => 'qty_19',
                'Price 19'     => 'price_19',
                'Qty 20'       => 'qty_20',
                'Price 20'     => 'price_20',
                'Cumulates With 1' => 'cumulates_with_0',
                'Cumulates With 2' => 'cumulates_with_1',
                'Cumulates With 3' => 'cumulates_with_2',
                'Cumulates With 4' => 'cumulates_with_3',
                'Cumulates With 5' => 'cumulates_with_4',
                'Cumulates With 6' => 'cumulates_with_5',
                'Cumulates With 7' => 'cumulates_with_6',
                'Cumulates With 8' => 'cumulates_with_7',
                'Cumulates With 9' => 'cumulates_with_8',
                'Cumulates With 10' => 'cumulates_with_9',
                'Reserved'     => 'reserved',
                'reserved'     => 'reserved'
            );

            // auto generate the rest
            for ($i=1; $i <= 500; $i++) { 
                $itemdata_fields[ 'Qty '.$i ]   = 'qty_'.$i;
                $itemdata_fields[ 'Price '.$i ] = 'price_'.$i;
            }

            // Separate user data from meta
            $itemdata = $itemmeta = array();
            foreach ( $line as $ckey => $column ) {
                $column_name = $headers[$ckey];
                $column = trim( $column );
                // if ( empty( $column ) ) continue;

                if ( array_key_exists( $column_name, $itemdata_fields ) ) {
                    // $itemdata[$column_name] = $column;
                    $column_slug = $itemdata_fields[ $column_name ];
                    $itemdata[$column_slug] = $column;
                } else {
                    // $itemmeta[$column_name] = $column;
                    // echo "<pre>unknown key '$column';";print_r($itemmeta);echo"</pre>";#die();
                    // echo "<pre>unknown column '$column_name' ('$column')</pre>";#die();
                    // echo "<pre>Import process was aborted.</pre>";#die();
                    self::showMessage('Invalid file format - found unknown column "'.$column_name.'"',1);
                    return false;
                }
            }

            // If no data, bailout!
            if ( empty( $itemdata ) )
                continue;

            // echo "<pre>";print_r($itemdata);echo"</pre>";die();

            // adapt to number of rules / columns
            $max_rules_count = 5;
            if ( isset( $itemdata['qty_6']  ) )  $max_rules_count = 10;
            if ( isset( $itemdata['qty_11'] ) )  $max_rules_count = 20;
            if ( isset( $itemdata['qty_21'] ) )  $max_rules_count = 50;
            if ( isset( $itemdata['qty_51'] ) )  $max_rules_count = 100;
            if ( isset( $itemdata['qty_101'] ) ) $max_rules_count = 200;
            if ( isset( $itemdata['qty_201'] ) ) $max_rules_count = 500;

            // parse rules
            $rules = array();
            for ($i=1; $i <= $max_rules_count; $i++) { 
                
                $qty      = @$itemdata['qty_'.$i];
                $price    = @$itemdata['price_'.$i];
                $next_qty = @$itemdata['qty_'.($i+1)];
                
                if ($qty) {

                    $rule        = array();
                    $rule['min'] = $qty;
                    $rule['max'] = $next_qty ? $next_qty - 1 : '*';
                    $rule['val'] = $price;
                    $rules[]     = $rule;

                }

                unset( $itemdata['qty_'.$i] );
                unset( $itemdata['price_'.$i] );
            }
            $itemdata['rules'] = $rules;

            // parse cumulative scope
            for ( $i = 0; $i < 10; $i++ ) {
                $itemdata['cumulates_with'][] = isset( $itemdata['cumulates_with_'. $i ] ) ? $itemdata['cumulates_with_'. $i] : '';
                unset( $itemdata['cumulates_with_'. $i] );
            }


            // set some additional values
            $ruleset_id = $itemdata['id'];
            $itemdata['is_cumulative']       = @$itemdata['is_cumulative'] == 'cm' ? true : false;
            $itemdata['show_lowest_price']   = @$itemdata['show_lowest_price'] == 'Y' ? true : false;
            $itemdata['hide_discount_table'] = false;
            $itemdata['rows_limit']          = $max_rules_count;
            $itemdata['custom_header']       = '';
            $itemdata['custom_footer']       = '';
            //$itemdata['cumulates_with']      = array();
            if ( ! isset( $itemdata['user_roles'] ) ) $itemdata['user_roles'] = '';
            unset( $itemdata['reserved'] );

            // skip rules without name
            if ( ! $itemdata['name'] ) {
                echo "<pre>skipped ruleset without name - ";print_r($ruleset_id);echo"</pre>";
                continue;                
            }

            // add ruleset            
            $rulesets[ $ruleset_id ] = $itemdata;
            $rkey++;

            // check for product with matching SKU
            $SKUs = isset( $itemdata['sku'] ) ? $itemdata['sku'] : $ruleset_id;
            $SKUs = explode('|', $SKUs );
            foreach ($SKUs as $sku) {
                if ( $product_ids = self::getProductsBySKU( $sku ) ) {
                    foreach ($product_ids as $post_id) {
                        
                        // update ruleset with matching SKU
                        update_post_meta( $post_id, '_wc_bulk_pricing_ruleset', $ruleset_id );

                        // update regular price if first column is not a percentage
                        $regular_price = $itemdata['rules'][0]['val'];
                        if ( ( $regular_price > 0 ) && ( strpos( $regular_price, '%' ) < 1 ) ) {
                            
                            update_post_meta( $post_id, '_regular_price', $regular_price );
                            
                            // only update _price if _sale_price is not set
                            if ( ! get_post_meta( $post_id, '_sale_price', true ) > 0 ) {
                                update_post_meta( $post_id, '_price', $regular_price );
                            }

                        }

                    } // foreach product
                } // if products found
            } // foreach sku

        }
        fclose( $file_handle );

        // echo "<hr><pre>";print_r($rulesets);echo"</pre>";#die();

        if ( is_array($rulesets) && ( sizeof($rulesets) > 0 ) ) {

            // to set the max_allowed_packet to 16MB
            global $wpdb;
            $wpdb->query( 'SET GLOBAL max_allowed_packet=16777216' );
            // echo $wpdb->last_error;
            if ( $wpdb->last_error ) echo 'The database error you see above can be ignored in most cases. If the import failed, please contact WP Lab support.';

            update_option('wc_bulk_pricing_rules', $rulesets);
            return sizeof($rulesets);
        } 
        return false;

    } // import_csv()


    /**
     * find product ID by SKU
     **/
    public function getProductsBySKU( $sku ) {
        global $wpdb;
        if ( empty($sku) ) return false;

        $post_ids = $wpdb->get_col("
            SELECT post_id FROM {$wpdb->prefix}postmeta 
            WHERE meta_key   = '_sku'
              AND meta_value = '{$sku}' 
        ");
        return $post_ids;
    }

    /**
     * find all product SKUs by ruleset ID
     **/
    public function getSKUsForRulesetID( $ruleset_id ) {
        global $wpdb;
        if ( empty($ruleset_id) ) return false;

        $post_ids = $wpdb->get_col("
            SELECT post_id FROM {$wpdb->prefix}postmeta 
            WHERE meta_key   = '_wc_bulk_pricing_ruleset'
              AND meta_value = '{$ruleset_id}' 
        ");

        $sku_array = array();
        foreach ( $post_ids as $post_id ) {
            $sku = get_post_meta( $post_id, '_sku', true );
            if ( ! empty( $sku ) ) $sku_array[] = $sku;
        }

        return join( '|', $sku_array );
    }


    /**
     * Process content of JSON file
     **/
    public function import_json( $uploaded_file ) {

        $json = file_get_contents($uploaded_file);
        $rulesets = json_decode($json, true);
        // echo "<pre>";print_r($rulesets);echo"</pre>";#die();

        if ( is_array($rulesets) && ( sizeof($rulesets) > 0 ) ) {
            update_option('wc_bulk_pricing_rules', $rulesets);
            return sizeof($rulesets);
        } 
        return false;

    }

    /**
     * process file upload
     **/
    public function process_upload() {

        if(isset($_FILES['wpl_file_upload'])) {

            $target_path = $this->target_path;
            //echo "<br />Target Path: ".$target_path;

            // delete last import
            if ( file_exists($target_path) ) unlink($target_path);

            // echo '<div id="message" class="X-updated X-fade"><p>';
            if(move_uploaded_file($_FILES['wpl_file_upload']['tmp_name'], $target_path))
            {
                // echo "The file ".  basename( $_FILES['wpl_file_upload']['name'])." has been uploaded";               
                // $file_name = WP_CSV_TO_DB_URL.'/uploads/'.basename( $_FILES['wpl_file_upload']['name']);
                // update_option('wp_csvtodb_input_file_url', $file_name);
                // return true;
                return $target_path;
            } 
            else
            {
                echo "There was an error uploading the file, please try again!";
            }
            // echo '</p></div>';
            return false;
        }
        echo "no file_upload set";
        return false;
    }




    /**
     * Export data as JSON
     **/
    public function export_json( $data ) {

        header("Content-Disposition: attachment; filename=discount-profiles.json");
        echo json_encode( $data );
        exit;

    }

    /**
     * Export data as CSV
     **/
    public function export_csv( $rulesets ) {

        // get max number of rules
        $max_rules_count = $this->get_max_rules_count( $rulesets );

        // create CSV header
        // $csv = 'ID,Ruleset Name,Qty 1,Price 1,Qty 2,Price 2,Qty 3,Price 3,Qty 4,Price 4,Qty 5,Price 5,Mode,Show Lowest,User Role,reserved'."\n";
        $csv = 'ID,Ruleset Name,';
        for ($i=1; $i <= $max_rules_count; $i++) { 
            $csv .= "Qty $i,Price $i,";
        }
        $csv .= 'Mode,Show Lowest,User Role,SKU,';
        for ( $i = 1; $i <= 10; $i++ ) {
            $csv .= "Cumulates With {$i},";
        }
        $csv .= 'reserved'."\n";

        // CSV rows
        foreach ($rulesets as $id => $ruleset) {

            // find products using this discount profile
            $sku_list = $this->getSKUsForRulesetID( $ruleset['id'] );

            $csv .= $ruleset['id'].',';
            $csv .= '"'.$ruleset['name'].'",';
            for ($i=0; $i < $max_rules_count; $i++) { 
                $csv .= @$ruleset['rules'][$i]['min'].',';
                $csv .= @$ruleset['rules'][$i]['val'].',';
            }
            $csv .= ( $ruleset['is_cumulative'] ? 'cm' : 'ex' ) .',';
            $csv .= ( $ruleset['show_lowest_price'] ? 'Y' : 'N' ) .',';
            $csv .= $ruleset['user_roles'] .',';
            $csv .= $sku_list .',';
            for ($i = 0; $i < 10; $i++) {
                $csv .= @$ruleset['cumulates_with'][$i] .',';
            }
            $csv .= '[reserved]'."\n";
        }

        // echo "<pre>";print_r($csv);echo"</pre>";die();

        header('Content-Description: File Transfer');
        header("Content-Type: application/csv") ;
        header("Content-Disposition: attachment; filename=discount-profiles.csv");
        echo $csv;
        exit;

    }

    public function get_max_rules_count( $rulesets ) {
        $max_rules_count = 5; // default

        // loop rulesets
        foreach ($rulesets as $id => $ruleset) {
            if ( $ruleset['rows_limit'] > $max_rules_count ) $max_rules_count = $ruleset['rows_limit'];
        }

        return $max_rules_count;
    }













    /**
     * Import rulesets from a csv file
     */
    public static function import_product_rules_from_csv( $filename, $delimiter = ',' ) {

        include( plugin_dir_path( __FILE__ ) . 'class-readcsv.php' );

        // Loop through the file lines
        $file_handle = fopen( $filename, 'r' );
        $csv_reader = new ReadCSV( $file_handle, $delimiter, "\xEF\xBB\xBF" ); // Skip any UTF-8 byte order mark.

        $headers          = false;
        $row_index        = 0;
        $products_updated = 0;

        while ( ( $line = $csv_reader->get_row() ) !== NULL ) {

            $row_index++;
            // echo "<pre>";print_r($line);echo"</pre>";#die();

            // If the first line is empty, abort
            // If another line is empty, just skip it
            if ( empty( $line ) ) {
                if ( $first )
                    break;
                else
                    continue;
            }

            // If we are on the first line, the columns are the headers
            if ( ! $headers ) {
                $headers = $line;
                continue;
            }

            // check delimiter - abort if tab character found
            if ( strpos($line[0], "\t") ) {
                self::showMessage('Invalid file format - CSV files need to use <i>comma</i> as separator, not <i>tab</i>. Please adjust your CSV export settings in Excel.',1);
                return false;                
            }

            // User data fields list used to differentiate with user meta
            $itemdata_fields = array(
                'ID'           => 'post_id',
                'SKU'          => 'sku',
                'Ruleset ID'   => 'ruleset_id',
                'Qty 1'        => 'qty_1',
                'Price 1'      => 'price_1',
                'Qty 2'        => 'qty_2',
                'Price 2'      => 'price_2',
                'Qty 3'        => 'qty_3',
                'Price 3'      => 'price_3',
                'Qty 4'        => 'qty_4',
                'Price 4'      => 'price_4',
                'Qty 5'        => 'qty_5',
                'Price 5'      => 'price_5',
                'Qty 6'        => 'qty_6',
                'Price 6'      => 'price_6',
                'Qty 7'        => 'qty_7',
                'Price 7'      => 'price_7',
                'Qty 8'        => 'qty_8',
                'Price 8'      => 'price_8',
                'Qty 9'        => 'qty_9',
                'Price 9'      => 'price_9',
                'Reserved'     => 'reserved',
                'reserved'     => 'reserved'
            );

            // auto generate the rest
            for ($i=1; $i <= 20; $i++) { 
                $itemdata_fields[ 'Qty '.$i ]   = 'qty_'.$i;
                $itemdata_fields[ 'Price '.$i ] = 'price_'.$i;
            }

            // Separate user data from meta
            $itemdata = $itemmeta = array();
            foreach ( $line as $ckey => $column ) {
                $column_name = $headers[$ckey];
                $column = trim( $column );
                // if ( empty( $column ) ) continue;

                if ( array_key_exists( $column_name, $itemdata_fields ) ) {
                    // $itemdata[$column_name] = $column;
                    $column_slug = $itemdata_fields[ $column_name ];
                    $itemdata[$column_slug] = $column;
                } else {
                    // $itemmeta[$column_name] = $column;
                    // echo "<pre>unknown key '$column';";print_r($itemmeta);echo"</pre>";#die();
                    // echo "<pre>unknown column '$column_name' ('$column')</pre>";#die();
                    // echo "<pre>Import process was aborted.</pre>";#die();
                    self::showMessage('Invalid file format - found unknown column "'.$column_name.'"',1);
                    return false;
                }
            }

            // skip rules without required fields
            if ( ! @$itemdata['post_id'] && ! @$itemdata['sku'] ) {
                echo "<pre>skipped rules with neither ID nor SKU</pre>";
                continue;                
            }
            if ( ! $itemdata['ruleset_id'] && ( ! $itemdata['qty_1'] || ! $itemdata['price_1'] ) ) {
                echo "<pre>skipped invalid rule in row $i</pre>";
                continue;                
            }

            // If no data, bailout!
            if ( empty( $itemdata ) )
                continue;

            // echo "<pre>";print_r($itemdata);echo"</pre>";die();

            // skip rules without name
            // if ( ! $itemdata['name'] ) {
            //     echo "<pre>skipped ruleset without name - ";print_r($ruleset_id);echo"</pre>";
            //     continue;                
            // }


            // adapt to number of rules / columns
            $max_rules_count = 5;
            if ( isset( $itemdata['qty_6']  ) )  $max_rules_count = 10;
            if ( isset( $itemdata['qty_11'] ) )  $max_rules_count = 20;
            if ( isset( $itemdata['qty_21'] ) )  $max_rules_count = 50;
            if ( isset( $itemdata['qty_51'] ) )  $max_rules_count = 100;
            if ( isset( $itemdata['qty_101'] ) ) $max_rules_count = 200;
            if ( isset( $itemdata['qty_201'] ) ) $max_rules_count = 500;

            // set some additional values
            $post_id = @$itemdata['post_id'];
            $sku     = @$itemdata['sku'];
            unset( $itemdata['reserved'] );


            // parse rules
            $rules = array();
            for ($i=1; $i <= $max_rules_count; $i++) { 
                
                $qty      = @$itemdata['qty_'.$i];
                $price    = @$itemdata['price_'.$i];
                $next_qty = @$itemdata['qty_'.($i+1)];
                $price    = str_replace( array('$','€','£'), '', $price );

                if ($qty) {

                    $rule        = array();
                    $rule['min'] = $qty;
                    $rule['max'] = $next_qty ? $next_qty - 1 : '*';
                    $rule['val'] = $price;
                    $rules[]     = $rule;

                }

                unset( $itemdata['qty_'.$i] );
                unset( $itemdata['price_'.$i] );
            }
            $itemdata['rules'] = $rules;




            // check for product with matching ID
            if ( $post_id ) {

                // check if product exists!
                $post = get_post( $post_id );
                $product = wc_get_product( $post_id );

                if ( $product->exists() ) {
                    if ( $itemdata['ruleset_id'] ) {
                        update_post_meta( $post_id, '_wc_bulk_pricing_ruleset', $itemdata['ruleset_id'] );
                    } else {
                        update_post_meta( $post_id, '_wc_bulk_pricing_ruleset', '_custom' );
                        update_post_meta( $post_id, '_wc_bulk_pricing_custom_ruleset', $rules );
                    }

                    // echo "<pre>assigned rules to product $post_id by ID</pre>";#die();
                    $products_updated++;
                } else {
                    echo "<pre>could not find product ID $post_id (row $row_index)</pre>";#die();
                }


            // check for product with matching SKU
            } else if ( $product_ids = self::getProductsBySKU( $sku ) ) {
                foreach ($product_ids as $post_id) {
                    
                    // update ruleset with matching SKU
                    if ( $itemdata['ruleset_id'] ) {
                        update_post_meta( $post_id, '_wc_bulk_pricing_ruleset', $itemdata['ruleset_id'] );
                    } else {
                        // update_post_meta( $post_id, '_wc_bulk_pricing_ruleset', $ruleset_id );
                        update_post_meta( $post_id, '_wc_bulk_pricing_ruleset', '_custom' );
                        update_post_meta( $post_id, '_wc_bulk_pricing_custom_ruleset', $rules );
                    }

                    // echo "<pre>assigned rules to product $post_id by SKU $sku</pre>";#die();
                    $products_updated++;

                    // update regular price if first column is not a percentage
                    // $regular_price = $itemdata['rules'][0]['val'];
                    // if ( ( $regular_price > 0 ) && ( strpos( $regular_price, '%' ) < 1 ) ) {
                        
                    //     update_post_meta( $post_id, '_regular_price', $regular_price );
                        
                    //     // only update _price if _sale_price is not set
                    //     if ( ! get_post_meta( $post_id, '_sale_price', true ) > 0 ) {
                    //         update_post_meta( $post_id, '_price', $regular_price );
                    //     }

                    // }

                }
            } else {
                echo "<pre>could not find product SKU $sku (row $row_index)</pre>";#die();                    
            }

        }
        fclose( $file_handle );

        return $products_updated;

    } // import_product_rules_from_csv()



    /**
     * Export custom product rules as CSV
     **/
    public function export_product_rules_as_csv( $rows ) {

        // get max number of rules
        // $max_rules_count = $this->get_max_rules_count( $rulesets );
        $max_rules_count = 10;

        // create CSV header
        // $csv = 'ID,SKU,Ruleset ID,Qty 1,Price 1,Qty 2,Price 2,Qty 3,Price 3,Qty 4,Price 4,Qty 5,Price 5,reserved'."\n";
        $csv = 'ID,SKU,Ruleset ID,';
        for ($i=1; $i <= $max_rules_count; $i++) { 
            $csv .= "Qty $i,Price $i,";
        }
        $csv .= 'reserved'."\n";

        // wp_postmeta rows
        foreach ($rows as $row) {

            $post_id    = $row->post_id;
            $sku        = get_post_meta( $post_id, '_sku', true );
            $ruleset_id = get_post_meta( $post_id, '_wc_bulk_pricing_ruleset', true );
            $rules      = maybe_unserialize( $row->meta_value );
            // echo "<pre>";print_r($rules);echo"</pre>";#die();

            // skip products with just one rule - which is the default rule 1-*:100%
            if ( ! $ruleset_id && ( sizeof($rules) == 1 ) ) 
                continue;

            if ( $ruleset_id == '_custom' ) $ruleset_id = '';

            $csv .= $post_id.',';       // Product ID
            $csv .= $sku . ',';                // SKU
            $csv .= $ruleset_id.',';    // Ruleset ID
            for ($i=0; $i < $max_rules_count; $i++) { 
                $csv .= @$rules[$i]['min'].',';
                $csv .= @$rules[$i]['val'].',';
            }
            $csv .= '[reserved]'."\n";
        }

        // echo "<pre>";print_r($csv);echo"</pre>";die();

        header('Content-Description: File Transfer');
        header("Content-Type: application/csv") ;
        header("Content-Disposition: attachment; filename=product-discounts.csv");
        echo $csv;
        exit;

    }


    /* Generic message display */
    public function showMessage($message, $errormsg = false, $echo = true) {       
        $class = ($errormsg) ? 'error' : 'updated fade';
        $message = '<div id="message" class="'.$class.'"><p>'.$message.'</p></div>';
        if ($echo) echo $message;
    }

    
} // class PricingRulesImporter
