<?php

class woocommerce_bulk_pricing_shortcode {

    static function showHtmlTable( $rules, $post_id = false, $max_columns = false ) {
        global $woocommerce;
        global $woocommerce_bulk_pricing;

        if ( ! is_array($rules) ) return;

        // show the price with or without taxes
        $tax_display_mode = get_option( 'woocommerce_tax_display_shop' );

        $original_price = false;
        $table = '';

        if ( $post_id ) {
            $_product           = self::wc2_get_product( $post_id );
            $original_price     = $_product->get_price();
            $combine_wholesale  = false;

            if ( function_exists( 'wc_get_price_including_tax' ) ) {
                $original_price   = $tax_display_mode == 'incl' ? wc_get_price_including_tax( $_product, array( 'price' => $original_price ) ) : wc_get_price_excluding_tax( $_product, array( 'price' => $original_price ) );
            } else {
                $original_price   = $tax_display_mode == 'incl' ? $_product->get_price_including_tax( 1, $original_price ) : $_product->get_price_excluding_tax( 1, $original_price );
            }

            // if this is a variable product and we didn't get a price, try to get the price for the default variation
            if ( $_product && wbp_get_product_meta( $_product, 'product_type' ) == 'variable' && $original_price == 0 ) {
                if ( $default_variation_id = self::get_default_variation_id( $_product ) ) {
                    $_def_variation = self::wc2_get_product( $default_variation_id );

                    if ( function_exists( 'wc_get_price_including_tax' ) ) {
                        $original_price = $tax_display_mode == 'incl' ? wc_get_price_including_tax( $_def_variation, array( 'price' => $original_price ) ) : wc_get_price_excluding_tax( $_def_variation, array( 'price' => $original_price ) );
                    } else {
                        $original_price = $tax_display_mode == 'incl' ? $_def_variation->get_price_including_tax( 1, $original_price ) : $_def_variation->get_price_excluding_tax( 1, $original_price );
                    }
                }
            }

            // wholesale mod
            if ( $global_percent = $woocommerce_bulk_pricing->getUserDiscount() ) {

                if ( $woocommerce_bulk_pricing->userDiscountIsCombinedWithBulkPricing() ) {
                    
                    // apply global percentage discount to original_price
                    $original_price = $original_price - ( $original_price * $global_percent / 100 );
                    $combine_wholesale = true;

                } else {
                    // apply global percentage discount only
                    // $price_adjusted = $original_price - ( $original_price * $global_percent / 100 );
                }
            }

        }
        $table = '<table>';

        // table header
        $table .= '<tr class="wc_quantity">';
        $col_index = 0;
        foreach ( $rules as $rule ) {
            $title = $rule['min'] . '-' . $rule['max'];
            if ( $rule['max'] == '*' ) $title = $rule['min'] . '+';
            if ( $rule['min'] == $rule['max'] ) $title = $rule['min'];
            $title = apply_filters( 'woocommerce_bulk_pricing_table_title', $title, $original_price, $rule, $rules, $post_id, $col_index );
            $table .= '<th>'.$title.'</th>';
            $col_index++;

            if ( $max_columns && ( $col_index > $max_columns ) ) break;
        }
        $table .= '</tr>';

        // table body
        $table .= '<tr class="wc_price">';
        $col_index = 0;
        foreach ( $rules as $rule ) {
            $value = $rule['val'];

            // calc prices if product id provided
            if ( $post_id ) {
                if ( $value == '100%' ) {
                    // 100% equals original price
                    $formatted_value = $woocommerce_bulk_pricing->format_price( $original_price );
                } elseif ( strpos( $value, '%' ) > 0 ) {
                    // no currency for percentages
                    $value = $value;

                    // calculate relative prices as well
                    $percentage = floatval( str_replace( '%', '', $value ) / 100 );
                    $formatted_value = $woocommerce_bulk_pricing->format_price( $original_price * $percentage );

                } elseif ( '+' == substr( $value, 0, 1 ) ) {
                    // increase price by adding value
                    $value = $original_price + $value;
                    $formatted_value = $woocommerce_bulk_pricing->format_price( $value );
                } elseif ( '-' == substr( $value, 0, 1 ) ) {
                    // reduce price by adding negative value
                    $value = $original_price + $value;
                    $formatted_value = $woocommerce_bulk_pricing->format_price( $value );
                } else {
                    // default - add currency symbol
                    if ( $combine_wholesale ) $value = $value - ( $value * $global_percent / 100 );
                    $product = wc_get_product( $post_id );

                    if ( function_exists( 'wc_get_price_including_tax' ) ) {
                        $value = $tax_display_mode == 'incl' ? wc_get_price_including_tax( $product, array( 'price' => $value ) ) : wc_get_price_excluding_tax( $product, array( 'price' => $value ) );
                    } else {
                        $value = $tax_display_mode == 'incl' ? $product->get_price_including_tax( 1, $value ) : $product->get_price_excluding_tax( 1, $value );
                    }

                    $formatted_value = $woocommerce_bulk_pricing->format_price( $value );
                }
            }

            $value = apply_filters( 'woocommerce_bulk_pricing_table_value', $formatted_value, $original_price, $rule, $rules, $post_id, $value, $col_index );
            if ( method_exists( $_product, 'get_price_suffix' ) ) {
                $value .= method_exists( $_product, 'get_price_suffix' ) ? $_product->get_price_suffix( $value ) : '';
            }
            $table .= '<td align="center">'.$value.'</td>';
            $col_index++;

            if ( $max_columns && ( $col_index > $max_columns ) ) break;
        }
        $table .= '</tr>';

        $table .= '</table>';
        do_action( 'woocommerce_bulk_pricing_table_footer', $rule, $rules, $post_id );

        echo apply_filters( 'woocommerce_bulk_pricing_table_html', $table, $rules, $post_id );
    }

    static function showInlineJS( $ruleset_id, $post_id ) {

        ?>
            <script type="text/javascript">

                jQuery(document).ready(function() {

                    jQuery('form.variations_form').on( 'wc_variation_form', function( event, variation ) {

                        wcbp_update_variable_pricing_table();

                        jQuery('form.variations_form').on( 'found_variation', function( event, variation ) {

                            wcbp_update_variable_pricing_table();

                        });

                    });

                    // non-ajax variations
                    jQuery(document).on( 'change', '.variations select', function() {

                        wcbp_update_variable_pricing_table();

                    });

                });

                function wcbp_update_variable_pricing_table() {

                    variation_form = jQuery('form.variations_form');
                    var variation_id = variation_form.find('input[name=variation_id]').val();

                    if ( ! ajaxurl ) var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";

                    if ( variation_id > 0 ) {
                        // console.log('loading... ',variation_id);

                        jQuery('#wpl_bp_wrap').load( ajaxurl, { 
                            'action': 'wpl_bp_load_discount_table', 
                            'post_id': "<?php echo $post_id ?>",
                            'ruleset_id': "<?php echo $ruleset_id ?>",
                            'variation_id': variation_id 
                        }, function() {
                            // only show the table if there are discount rules for the selected variation
                            jQuery(".bulk_pricing_discounts_wrapper").hide();
                            if ( jQuery("#wpl_bp_wrap table tr").length > 1 ) {
                                jQuery(".bulk_pricing_discounts_wrapper").show();
                            }
                        });

                        jQuery('#wpl_bp_wrap span.amount').css('color','transparent');
                        jQuery('#wpl_bp_wrap span.amount').css('color','rgba(0,0,0,.3)');
                        // jQuery('#wpl_bp_wrap span.amount').animate({'color':'#ffffff'},200);

                    } else {
                        if ( 'hide' == wbp_variation_display_mode ) {
                            // hide the table if no variation is selected
                            //jQuery("#wpl_bp_wrap").hide();
                            jQuery('.bulk_pricing_discounts_wrapper').hide();
                        }
                    }

                }

            </script>
        <?php

                // jQuery(document).bind('woocommerce_update_variation_values', function() {
                //     jQuery('.variations select option').each( function(index, el) {
                //     });
                // });
    }


    /**
     * Gets the identified product. Compatible with WC 2.0 and backwards
     * compatible with previous versions
     *
     * @param int     $product_id the product identifier
     * @param array   $args       optional array of arguments
     *
     * @return WC_Product the product
     */
    static function wc2_get_product( $product_id, $args = array() ) {
        $product = null;

        if ( version_compare( WOOCOMMERCE_VERSION, "2.0.0" ) >= 0 ) {
            // WC 2.0
            $product = wc_get_product( $product_id, $args );
        } else {

            // old style, get the product or product variation object
            if ( isset( $args['parent_id'] ) && $args['parent_id'] ) {
                $product = new WC_Product_Variation( $product_id, $args['parent_id'] );
            } else {
                // get the regular product, but if it has a parent, return the product variation object
                $product = new WC_Product( $product_id );
                if ( $product->get_parent() ) {
                    $product = new WC_Product_Variation( $product->id, $product->get_parent() );
                }
            }
        }

        return $product;
    }

    static function get_default_variation_id( $product ) {
        $variation_id = 0;

        foreach ( $product->get_available_variations() as $pav ){
            $is_def = true;
            foreach ( $product->get_variation_default_attributes() as $defkey => $defval ) {
                if ( $pav['attributes']['attribute_'.$defkey] != $defval ) {
                    $is_def = false;
                }
            }
            if ( $is_def ) {
                $variation_id = $pav['variation_id'];
            }
        }

        return $variation_id;
    }

} // class woocommerce_bulk_pricing_shortcode


// 
// template tag
// wc_bulk_pricing_discount_table()
// 
function wc_bulk_pricing_discount_table() {
    global $woocommerce_bulk_pricing;
    $woocommerce_bulk_pricing->display_discount_info();
}
