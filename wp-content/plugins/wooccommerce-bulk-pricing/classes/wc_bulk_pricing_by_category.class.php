<?php

// class woocommerce_bulk_pricing_by_category extends woocommerce_bulk_pricing_base {
class woocommerce_bulk_pricing_by_category  {

    private $applied_rules = false;
    private $active_categories = false;
    public $object_terms_cache = array();

    protected $discounter = 'advanced_category';
    protected $discount_data = array();

    public function __construct($priority) {
        // add_action('init', array(&$this, 'on_init'));
        // add_action('woocommerce_after_cart_item_quantity_update', array(&$this, 'on_update_cart_item_quantity'), $priority, 2);
        add_action('woocommerce_before_calculate_totals', array(&$this, 'on_calculate_totals'), $priority);
        add_action('woocommerce_before_mini_cart', array(&$this, 'woocommerce_before_mini_cart'), $priority);

        add_filter('woocommerce_get_cart_item_from_session', array(&$this, 'on_get_cart_item_from_session'), 1, 3);

        // fix issue in WooCommerce caused by rounding percentage discounts per product to 2 decimals
        add_filter('woocommerce_coupon_get_discount_amount', array(&$this, 'on_coupon_get_discount_amount'), 10, 5);
    }

    public function on_init() {
    }
    public function on_update_cart_item_quantity($cart_item, $quantity) {        
        // echo "*** on_update_cart_item_quantity( $cart_item )";
    }

    public function woocommerce_before_mini_cart() {        
        global $woocommerce;
        // echo "*** woocommerce_before_mini_cart()";

        // $cart_contents = WC()->cart->get_cart(); // WC2.1+
        $cart_contents = $woocommerce->cart->get_cart();

        $this->on_calculate_totals( $cart_contents );
    }

    public function loadRules() {        
        if ( ! $this->applied_rules ) {
            $this->applied_rules = wbp_get_rules();           // fetch defined rule sets
            $this->active_categories = get_option('wc_bulk_pricing_categories', array() );  // fetch list of categories and rule sets
        }
    }

    public function is_applied_to($_product, $cat_id = false) {

        if (is_admin()) {
            // echo "*** isadmin()";
            // return false;
        }
        
        // echo "*** is_applied_to() $cat_id ".print_r($_product,1);
        // echo "*** is_applied_to() $cat_id - ".$_product->id;
        $this->loadRules();

        if (isset($this->applied_rules) && count($this->applied_rules) > 0) {
            if ($cat_id) {
                // return is_object_in_term($_product->id, 'product_cat', $cat_id);
                return $this->productInCategories( wbp_get_product_meta( $_product, 'id' ), $cat_id );
            } else {
                // return is_object_in_term($_product->id, 'product_cat', array_keys($this->applied_rules));
                return $this->productInCategories( wbp_get_product_meta( $_product, 'id' ), array_keys($this->applied_rules) );
            }
        }
    }

    function productInCategories( $post_id, $term_ids ) {
        // replace is_object_in_term() which is too slow
        // if ( is_object_in_term( $post_id, 'product_cat', array( $term_id_1, $term_id_2 ) ) ) {}
        if ( ! is_array( $term_ids ) ) $term_ids = array( $term_ids );

        // get object terms from cache
        if ( isset( $this->object_terms_cache[ $post_id ] ) ) {
            $objterms = $this->object_terms_cache[ $post_id ];
        } else {
            $objterms = wp_get_object_terms( $post_id, 'product_cat' );
            $this->object_terms_cache[ $post_id ] = $objterms;
        }

        // print_r($objterms);
        foreach ($objterms as $term) {
            // if ( $term->term_id == $term_id ) return true;
            if ( in_array( $term->term_id, $term_ids ) ) return true;
        }

        return false;
    } // productInCategory()

    public function on_get_cart_item_from_session($cart_item, $values, $cart_item_key) {

        // trigger on_calculate_totals       
        global $woocommerce;
        if ( isset( $woocommerce->session->subtotal ) ) unset( $woocommerce->session->subtotal );
        if ( isset( $_SESSION ) ) unset( $_SESSION['subtotal'] );

        // this won't be neccessary
        // $this->adjust_cart_item($cart_item_key, $values);

        return $cart_item;
    }


    public function on_calculate_totals($_cart) {
        global $woocommerce;
        // $_cart->_test = 'Hello';

        // echo "*** on_calculate_totals() <br>";
        $this->loadRules();
        // print_r($this->active_categories);

        // coupon handling
        if ( get_option('wc_bulk_pricing_coupon_handling_mode', 'default' ) == 'coupon' ) {

            // check if coupon discounts are applied to cart
            $applied_coupons = $woocommerce->cart->get_applied_coupons();

            // don't adjust cart items if coupons are applied
            if ( ( sizeof( $applied_coupons ) > 0 ) || ( isset ( $_REQUEST['coupon_code'] ) ) ) {
                return;
            }

        }

        // adjust cart items
        if ( is_object( $_cart ) )
        if ( sizeof( $_cart->cart_contents ) > 0) {
            foreach ( $_cart->cart_contents as $cart_item_key => &$values ) {
                $this->adjust_cart_item( $cart_item_key, $values );
            }
        }

        // if array, then $_cart is actually $_cart->cart_contents
        if ( is_array( $_cart ) )
        if ( sizeof( $_cart ) > 0) {
            foreach ( $_cart as $cart_item_key => &$values ) {
                $this->adjust_cart_item( $cart_item_key, $values );
            }
        }

        return;
    }

    private function adjust_cart_item($cart_item_key, &$cart_item) {
        global $woocommerce;
        global $woocommerce_bulk_pricing;

        // echo "adjust_cart_item($cart_item_key)<br>";
        // echo "<pre>";print_r($cart_item);echo"</pre>";#die();
        // echo "is_cumulative: ".$this->is_cumulative($cart_item)."<br>";

        // Support for WooCommerce Product Bundles plugin (Thanks Ewout)
        // prevent overriding the bundle price - if there bundle price is used (has no effect if per item pricing is used for bundle)
        if ( isset($cart_item['bundled_by']) && isset($cart_item['data']->bundled_value) && $cart_item['data']->bundled_value == 0 ) {
            return false;
        }

        // prevent fatal error if no cart_item provided
        if ( empty($cart_item) ) {
            return false;
        }
        
        $this->discount_data = array();
        $execute_rules = false;
        $collector = false;

        if ( get_option( 'wc_bulk_pricing_disable_on_sale_products', 0 ) == 1 && $cart_item['data']->is_on_sale() ) {
            // Do not apply any discounts to products on sale
            return $cart_item;
        }

        if (!$this->is_cumulative($cart_item)) {
            if ($this->is_item_discounted($cart_item)) {
                // echo "is_item_discounted($cart_item_key) = true <br>";                
                // return false; // disabled to fix discounts not showing up in cart on WC2.3
            }
            $this->reset_cart_item_price($cart_item);
        }

        $_product = $cart_item['data'];

        // Get the correct ID. We need the parent ID for product variations.
        if ( version_compare( WC_VERSION, '3.0', '>=' ) ) {
            if ( in_array( $_product->get_type(), array( 'variation', 'subscription_variation' ) ) ) {
                $_product_id = $_product->get_parent_id();
            } else {
                $_product_id = $_product->get_id();
            }
        } else {
            $_product_id = $_product->id;
        }

        // echo "<pre>";print_r($cart_item);echo"</pre>";#die();

        $original_price = $this->get_price_to_discount($cart_item);
        $original_price_ex_tax = $this->get_price_excluding_tax_to_discount($cart_item);


        // check if a category rule applies to $_product
        $category_rule_sets = get_option('wc_bulk_pricing_categories', array());
        if (is_array($category_rule_sets) && sizeof($category_rule_sets) > 0) {
            // foreach ($category_rule_sets as $pricing_rule_set) {
            foreach ($category_rule_sets as $term_id => $ruleset_ids) {
                $execute_rules = false;

                // choose one ruleset from multiple ones
                $ruleset_id = $this->chooseCategoryRuleset( $ruleset_ids );

                $collector = $this->get_collector($ruleset_id);

                // check if $_product is in category $term_id
                if ( $this->is_applied_to($_product, $term_id) ) {
                    // echo "*** rule applies to term $term_id !<br>";
                    if ( ! isset( $this->applied_rules[ $ruleset_id ] )) continue;

                    $execute_rules = true;
                    $ruleset = $this->applied_rules[ $ruleset_id ];
                }

            } // foreach rule set
        } // if ($category_rule_sets)

        // check if $_product has a rule set assigned
        $product_ruleset = $woocommerce_bulk_pricing->get_ruleset_for_product($_product_id);
        if ( $product_ruleset ) {
            // echo "*** rule applies to product $_product->id !<br>";
            // echo "<pre>";print_r($product_ruleset);echo"</pre>";#die();
            $ruleset = $product_ruleset;
            $collector = $ruleset['id'];
            $execute_rules = true;
        }

        // check if $_product is variable and the selected variation has a rule set assigned
        $product_type = wbp_get_product_meta( $_product, 'product_type' );
        if ( in_array( $product_type, array( 'variable', 'variation', 'subscription_variation' ) ) ) {
            // echo "<pre>";print_r($_product);echo"</pre>";
            // echo "adjust_cart_item() - ".$_product->product_type." product found!";
            $variation_ruleset    = false;
            $variation_ruleset_id = get_post_meta( $cart_item['variation_id'], '_wc_bulk_pricing_ruleset', true );

            // check parent if ruleset_id is _custom
            if ( $variation_ruleset_id == '_custom' ) {
                $variation_id = wbp_get_product_meta( $_product, 'variation_id' );

                // load custom ruleset from variation
                // $_product->id is already the parent_id - var id is stored in $_product->variation_id
                $variation_ruleset = $woocommerce_bulk_pricing->get_custom_product_ruleset( $variation_id );

                // or load custom ruleset from parent 
                if ( ! $variation_ruleset ) {
                    $variation_ruleset = $woocommerce_bulk_pricing->get_custom_product_ruleset( $_product_id );
                }

            // otherwise get load ruleset by id
            } elseif ( $variation_ruleset_id ) {
                $variation_ruleset = $woocommerce_bulk_pricing->getRulesetByID( $variation_ruleset_id );
            }

            if ( $variation_ruleset ) {
                $ruleset = $variation_ruleset;
                $collector = $ruleset['id'];
                $execute_rules = true;                  
            }
            // echo "*** rule applies to product $_product->id - variation {$cart_item['variation_id']} !<br>";
            // echo "<pre>";print_r($variation_ruleset);echo"</pre>";die();
        }


        // wholesale mod
        $global_percent = $woocommerce_bulk_pricing->getUserDiscount();

        // do we have a ruleset to apply?
        if ($execute_rules || $global_percent )  {
            // $pricing_rules = $pricing_rule_set['rules'];

            // wholesale mod
            if ( $global_percent  ) {

                if ( $woocommerce_bulk_pricing->userDiscountIsCombinedWithBulkPricing() ) {
                    
                    // apply global percentage discount first
                    // $wholesale_price = $original_price - ( $original_price * $global_percent / 100 );
                    $wholesale_price = $original_price; // (logic has been moved to get_adjusted_price() )

                    // apply bulk pricing rule
                    $price_adjusted = $this->get_adjusted_price($ruleset, $wholesale_price, $collector, $cart_item, $global_percent );
                    // echo "***1 price_adjusted: $price_adjusted<br>";

                } else {
                    // apply global percentage discount only
                    $price_adjusted = $original_price - ( $original_price * $global_percent / 100 );
                    // echo "***2 price_adjusted: $price_adjusted<br>";
                }


            } else {

                // apply bulk pricing rule
                $price_adjusted = $this->get_adjusted_price($ruleset, $original_price, $collector, $cart_item);

            }
            // echo "*** price_adjusted $price_adjusted<br>";

            if ($price_adjusted !== false && floatval($original_price) != floatval($price_adjusted)) {
                // echo "*** price_adjusted $price_adjusted<br>";
                // echo "<pre>";print_r($cart_item);echo"</pre>";die();
                $this->add_adjustment($cart_item, $price_adjusted);
                $this->add_discount_info($cart_item, $original_price, $original_price_ex_tax, $price_adjusted);
                $this->track_cart_item($cart_item_key, $cart_item);
                // break;
            } else {
                //Reset discount data
                $this->remove_discount_info($cart_item);
                //Should we be tracking the variation?  
                $tracking_variation = isset($collector['type']) && $collector['type'] == 'variation' && isset($cart_item['variation_id']);
                //Remove the tracked item
                $woocommerce_bulk_pricing->remove_discounted_cart_item($cart_item_key, $cart_item, $tracking_variation);
            }
        } // if ($execute_rules)

        return false;
    }

    function chooseCategoryRuleset( $category_ruleset_ids ) {
        global $woocommerce_bulk_pricing;
        // echo "<pre>";print_r($category_ruleset_ids);echo"</pre>";#die();

        // convert to array
        if ( !is_array($category_ruleset_ids) ) 
            $category_ruleset_ids = explode(',', $category_ruleset_ids );

        $cat_specific_ruleset_id = false; 
        $user_specific_ruleset_id = false; 

        // loop rulesets
        foreach ($category_ruleset_ids as $ruleset_id) {

            // load ruleset
            $ruleset = $woocommerce_bulk_pricing->getRulesetByID( $ruleset_id );
            // echo "<pre>role: ";print_r($ruleset['user_roles']);echo"</pre>";#die();

            // check if ruleset is limited to specific user roles
            if ( $ruleset['user_roles'] ) {
                
                $capability = $ruleset['user_roles'];
                // echo "<pre>role: ";print_r($ruleset['user_roles']);echo"</pre>";#die();

                // handle wp user roles
                if ( current_user_can( $capability ) ) {
                    // echo "<pre>found user specific rule: ";print_r($ruleset_id);echo"</pre>";#die();
                    $user_specific_ruleset_id = $ruleset_id;                    
                }

                // handle _guest role
                if ( ( '_guest' == $capability ) && ( ! is_user_logged_in() ) ) {
                    // echo "<pre>found _guest rule: ";print_r($ruleset_id);echo"</pre>";#die();
                    $user_specific_ruleset_id = $ruleset_id;                    
                }

                // ruleset doesn't apply to user
                // $ruleset_id = null;
                // continue;

            } else {
                $cat_specific_ruleset_id = $ruleset_id; 
                // echo "<pre>found cat_specific_ruleset_id rule: ";print_r($ruleset_id);echo"</pre>";#die();
            }

        } // foreach ruleset

        if ( $user_specific_ruleset_id )
            return $user_specific_ruleset_id;

        // echo "<pre>final: ";print_r($cat_specific_ruleset_id);echo"</pre>";#die();
        return $cat_specific_ruleset_id;
    }

    private function get_adjusted_price($ruleset, $price, $collector, $cart_item, $global_percent = false ) {
        $result = false;
        // echo "adjusting price according to ruleset ".$ruleset['name']."<br>";
        // echo "cumulative ".$ruleset['is_cumulative']."<br>";
        // echo "collector ".$collector."<br>";

        // handle case where there is no ruleset but only a global discount active
        if ( empty($ruleset) && $global_percent ) return $price - ( $price * $global_percent / 100 );

        $pricing_rules = $ruleset['rules'];
        $is_cumulative = isset( $ruleset['is_cumulative'] ) && ($ruleset['is_cumulative'] == '1') ? true : false;

        // handle cumulate_custom_rules option
        if ( ( $collector == '_custom' ) && ( get_option('wc_bulk_pricing_cumulate_custom_rules') == 1 ) ) {
            $is_cumulative = true;
        }

        if (is_array($pricing_rules) && sizeof($pricing_rules) > 0) {
            foreach ($pricing_rules as $rule) {

                $q = $this->get_quantity_to_compare($cart_item, $collector, $is_cumulative);

                if ($rule['min'] == '*') {
                    $rule['min'] = 0;
                }

                if ($rule['max'] == '*') {
                    $rule['max'] = $q;
                }

                if ($q >= $rule['min'] && $q <= $rule['max']) {
                    $this->discount_data['rule'] = $rule;
                    $this->discount_data['ruleset_id'] = $ruleset['id'];
                    $this->discount_data['ruleset_name'] = $ruleset['name'];

                    $result = $this->applyPriceModifier( $price, $rule['val'] );
                    if ( $global_percent ) $result = $result - ( $result * $global_percent / 100 );
                    
                    // round single item price
                    if ( get_option('wc_bulk_pricing_round_to_decimals', 1 ) ) {
                        $result = round( $result, get_option('wc_bulk_pricing_display_price_decimals', 2 ) );
                    }

                    // switch ($rule['type']) {
                    //     case 'price_discount':
                    //         $adjusted = floatval($price) - floatval($rule['amount']);
                    //         $result = $adjusted >= 0 ? $adjusted : 0;
                    //         break;
                    //     case 'percentage_discount':

                    //         if ($rule['amount'] > 1) {
                    //             $rule['amount'] = $rule['amount'] / 100;
                    //         }

                    //         $result = round(floatval($price) - ( floatval($rule['amount']) * $price), 2);
                    //         break;
                    //     case 'fixed_price':
                    //         $result = round($rule['amount'], 2);
                    //         break;
                    //     default:
                    //         $result = false;
                    //         break;
                    // }

                    break; //break out here only the first matched pricing rule will be evaluated.
                }
            }
        }

        return $result;
    }

    private function get_collector($pricing_rule_set) {
        // $this->discount_data['collector'] = $pricing_rule_set['collector'];
        $this->discount_data['collector'] = $pricing_rule_set;
        return $pricing_rule_set;
    }

    // this is called only for cumulative rulesets
    private function count_all_cart_items_with_collector($collector) {
        global $woocommerce_bulk_pricing;
        global $woocommerce;

        // check if we need to check other rulesets / collector as well
        if ( $collector != '_custom' ) {
            $this_ruleset   = $this->applied_rules[ $collector ];
            $other_rulesets = isset( $this_ruleset['cumulates_with'] ) ? $this_ruleset['cumulates_with'] : array();
        } else {
            $other_rulesets = array();
        }

        // check all cart items
        $total_quantity = 0;
        foreach ($woocommerce->cart->cart_contents as $cart_item) {
            // echo "<pre>";print_r($cart_item);echo "</pre>";

            $post_id           = $cart_item['product_id'];
            $quantity          = $cart_item['quantity'];

            // consider variations when loading the appropriate ruleset for the cart item #15670
            if ( $cart_item['variation_id'] ) {
                $cart_item_ruleset = $woocommerce_bulk_pricing->get_ruleset_for_product( $cart_item['variation_id'] );

                if ( !$cart_item_ruleset )
                    $cart_item_ruleset = $woocommerce_bulk_pricing->get_ruleset_for_product( $post_id );
            } else {
                $cart_item_ruleset = $woocommerce_bulk_pricing->get_ruleset_for_product( $post_id );
            }

            // classic cumulative mode
            if ( $cart_item_ruleset['id'] == $collector ) {
                // echo " matching rule $collector for product $post_id with quantity $quantity <br>";
                $total_quantity += $quantity;
            } elseif ( in_array( $cart_item_ruleset['id'], $other_rulesets ) ) {

                // skip if empty
                if ( $cart_item_ruleset['id'] == '' ) continue;

                // extended cumulative mode
                $total_quantity += $quantity;

                // echo " matching rule $collector for product $post_id with quantity $quantity <br>";
                // echo " matching rule <b>$collector</b> for product <b>".get_the_title($post_id)."</b> with quantity $quantity <br>";
                // echo " cart_item_ruleset['id']: ".$cart_item_ruleset['id']." <br>";
                // echo "<pre>";print_r($other_rulesets);echo"</pre>";
                // echo " product: ".get_the_title($post_id)." <br>";
            }

            // if (isset($cart_item['discounts'])) {
            //     $cart_item['data']->price = $cart_item['discounts']['price'];
            //     unset($cart_item['discounts']);
            // }
        }

        return $total_quantity;
    }

    private function get_quantity_to_compare($cart_item, $collector, $count_all_in_cart = false ) {
        global $woocommerce_bulk_pricing;
        $quantity = 0;

        // count other cart items to which this rule is applied too
        if ( $count_all_in_cart ) {
            $total_quantity = $this->count_all_cart_items_with_collector( $collector );
            return $total_quantity;
        }

        return $cart_item['quantity'];

        switch ($collector['type']) {
            case 'cat_product':
                if (isset($collector['args']) && isset($collector['args']['cats']) && is_array($collector['args']['cats'])) {
                    // if (is_object_in_term($cart_item['product_id'], 'product_cat', $collector['args']['cats'])) {
                    if ( $this->productInCategories( $cart_item['product_id'], $collector['args']['cats'] ) ) {
                        $quantity = $cart_item['quantity'];
                    }
                }
                break;
            case 'cat' :
                if (isset($collector['args']) && isset($collector['args']['cats']) && is_array($collector['args']['cats'])) {
                    // if (is_object_in_term($cart_item['product_id'], 'product_cat', $collector['args']['cats'])) {
                    if ( $this->productInCategories( $cart_item['product_id'], $collector['args']['cats'] ) ) {
                        foreach ($collector['args']['cats'] as $cat) {
                            if (isset($woocommerce_bulk_pricing->category_counts[$cat])) {
                                $quantity += $woocommerce_bulk_pricing->category_counts[$cat];
                            }
                        }
                    }
                }
                break;
        }

        $this->discount_data['collected_quantity'] = $quantity;

        return $quantity;
    }


    function applyPriceModifier( $product_price, $price_mod ) {
    
        // $this->logger->debug('applyProfilePrice(): '.$product_price.' - '.$price_mod );

        // remove all spaces from profile setting
        $price_mod = str_replace( ' ','', trim($price_mod) );
        
        // return product price if profile is empty
        if ( $price_mod == '' ) return $product_price;
    
        // handle percent
        if ( preg_match('/\%/',$price_mod) ) {
            // $this->logger->debug('percent mode');
        
            // parse percent syntax
            if ( preg_match('/([\+\-]?)([0-9\.]+)(\%)/',$price_mod, $matches) ) {
                // $this->logger->debug('matches:' . print_r($matches,1) );

                $modifier = $matches[1];
                $value = $matches[2];
                
                if ($modifier == '+') {
                    return $product_price + ( $product_price * $value/100 );                            
                } elseif ($modifier == '-') {
                    return $product_price - ( $product_price * $value/100 );                
                } else {
                    return ( $product_price * $value/100 );
                }
            
            } else {
                // no valid syntax
                return $product_price;      
            }
                        
        } else {

            // $this->logger->debug('value mode');
        
            // parse value syntax
            if ( preg_match('/([\+\-]?)([0-9\.]+)/',$price_mod, $matches) ) {
                // $this->logger->debug('matches:' . print_r($matches,1) );

                $modifier = $matches[1];
                $value = $matches[2];
                
                if ($modifier == '+') {
                    return $product_price + $value;             
                } elseif ($modifier == '-') {
                    return $product_price - $value;             
                } else {
                    return $value;
                }
            
            } else {
                // no valid syntax
                return $product_price;      
            }
        
        }

    }

    // fix issue caused by rounding single percentage discounts per product to 2 decimals
    // example: price after discount rules is 1.375, a 10% coupon should be 0.1375 discount per product and not 0.14
    // problem: class-wc-coupon.php -> get_discount_amount()
    function on_coupon_get_discount_amount( $discount, $discounting_amount, $cart_item, $single, $coupon ) {
        $coupon_type    = is_callable( array( $coupon, 'get_type' ) ) ? $coupon->get_type() : $coupon->type;
        $coupon_amount  = is_callable( array( $coupon, 'get_amount' ) ) ? $coupon->get_amount() : $coupon->amount;
        $limit_usage    = is_callable( array( $coupon, 'get_limit_usage_to_x_items' ) ) ? $coupon->get_limit_usage_to_x_items() : $coupon->limit_usage_to_x_items;

        // only fix percentage discounts
        if ( $coupon_type == 'percent_product' || $coupon_type == 'percent' ) {

            // $discount = round( ( $discounting_amount / 100 ) * $coupon->amount, WC()->cart->dp );
            $discount = round( ( $discounting_amount / 100 ) * $coupon_amount, 4 );

            // since we recalculated the $discount, we have to apply the following code from get_discount_amount() again
            // Handle the limit_usage_to_x_items option
            if ( in_array( $coupon_type, array( 'percent_product', 'fixed_product' ) ) && ! is_null( $cart_item ) ) {
                $qty = empty( $limit_usage ) ? $cart_item['quantity'] : min( $limit_usage, $cart_item['quantity'] );

                if ( $single ) {
                    $discount = ( $discount * $qty ) / $cart_item['quantity'];
                } else {
                    $discount = ( $discount / $cart_item['quantity'] ) * $qty;
                }
            }

        }

        return $discount;
    }

    /* base methods */

    public function add_adjustment(&$cart_item, $price_adjusted, $applied_rule = false) {
        if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
            $cart_item['data']->price = $price_adjusted;
        } else {
            $cart_item['data']->set_price( $price_adjusted ); // this is how to adjust prices in WC 3.0
        }
    }

    protected function add_discount_info(&$cart_item, $original_price, $original_price_ex_tax, $adjusted_price) {
        $discounters = isset($cart_item['discounts']['discounters']) ? $cart_item['discounts']['discounters'] : array();
        $discounters[$this->discounter] = array('price' => $original_price, 'price_excluding_tax' => $original_price_ex_tax, 'discounted' => $adjusted_price, 'by' => $this->discounter, 'data' => $this->discount_data);
        if (isset($cart_item['discounts'])) {
            $cart_item['discounts'] = array_merge($cart_item['discounts'], array('discounted' => $adjusted_price, 'by' => $this->discounter, 'data' => $this->discount_data, 'discounters' => $discounters));
        } else {
            $cart_item['discounts'] = array('price' => $original_price, 'price_excluding_tax' => $original_price_ex_tax, 'discounted' => $adjusted_price, 'by' => $this->discounter, 'data' => $this->discount_data, 'discounters' => $discounters);
        }
    }

    protected function remove_discount_info(&$cart_item) {
        if (isset($cart_item['discounts']) && isset($cart_item['discounts']['by']) && $cart_item['discounts']['by'] == $this->discounter) {
            unset($cart_item['discounts']);
        }
    }

    protected function reset_cart_item_price(&$cart_item) {
        if (isset($cart_item['discounts']) && isset($cart_item['discounts']['by']) && $cart_item['discounts']['by'] == $this->discounter) {
            if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
                $cart_item['data']->price = $cart_item['discounts']['price'];
            } else {
                $cart_item['data']->set_price( $cart_item['discounts']['price'] );
            }

        }
    }

    protected function track_cart_item(&$cart_item_key, $cart_item) {
        global $woocommerce_bulk_pricing;

        $tracking_variation = isset($cart_item['variation_id']);
        $woocommerce_bulk_pricing->add_discounted_cart_item($cart_item_key, $cart_item, $tracking_variation);
    }

    protected function is_item_discounted($cart_item) {
        return isset($cart_item['discounts']);
    }

    protected function is_cumulative($cart_item, $default = false) {
        return apply_filters('woocommerce_dynamic_pricing_is_cumulative', $default, $this->discounter, $cart_item);
    }

    protected function get_price_to_discount($cart_item) {
        return apply_filters('woocommerce_dyanmic_pricing_working_price', $cart_item['data']->get_price(), $this->discounter, $cart_item);
    }

    protected function get_price_excluding_tax_to_discount($cart_item) {
        if ( function_exists( 'wc_get_price_excluding_tax' ) ) {
            return apply_filters('woocommerce_dynamic_pricing_working_price_excluding_tax', wc_get_price_excluding_tax( $cart_item['data'] ), $this->discounter, $cart_item);
        }

        return apply_filters('woocommerce_dynamic_pricing_working_price_excluding_tax', $cart_item['data']->get_price_excluding_tax(), $this->discounter, $cart_item);
    }
    

}
