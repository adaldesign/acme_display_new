<?php

class woocommerce_bulk_pricing {

    private $discounted_products = array();
    public $object_terms_cache = array();
    public static $init_count = 0;
    public $variation_counts = array();
    public $product_counts = array();
    public $category_counts = array();
    public $_discounted;
    public $_discounted_cart;
    public $pricing_by_category;
    public $pricing_by_store_category;
    public $pricing_by_product;
    public $pricing_by_membership;
    public $pricing_by_totals;
    private $plugin_url;
    private $plugin_path;

    public function __construct() {
        self::$init_count++;

        $this->_discounted = array();
        $this->_discounted_cart = array();

        // $this->pricing_by_product = new woocommerce_pricing_by_product(1);
        // $this->pricing_by_category = new woocommerce_pricing_by_category(2);
        $this->pricing_by_store_category = new woocommerce_bulk_pricing_by_category(3);
        // $this->pricing_by_membership = new woocommerce_pricing_by_membership(4);
        // $this->pricing_by_totals = new woocommerce_pricing_by_totals(5);

        // add_filter('woocommerce_get_cart_item_from_session', array(&$this, 'update_counts'), 1, 2);
        // add_action('woocommerce_after_cart_item_quantity_update', array(&$this, 'on_update_cart_item_quantity'), 1, 2);

        add_filter('woocommerce_cart_item_price_html', array(&$this, 'on_display_cart_item_price_html'), 10, 3);
        add_filter('woocommerce_cart_item_price', array(&$this, 'on_display_cart_item_price_html'), 10, 3);
        // add_filter('woocommerce_widget_cart_item_quantity', array(&$this, 'on_display_widget_cart_item_quantity'), 10, 3);

        add_filter('woocommerce_grouped_price_html', array(&$this, 'on_price_html'), 10, 2);
        add_filter('woocommerce_variable_price_html', array(&$this, 'on_price_html'), 10, 2);
        add_filter('woocommerce_sale_price_html', array(&$this, 'on_price_html'), 10, 2);
        add_filter('woocommerce_price_html', array(&$this, 'on_price_html'), 10, 2);
        add_filter('woocommerce_variation_price_html', array(&$this, 'on_price_html'), 10, 2);
        add_filter('woocommerce_variation_sale_price_html', array(&$this, 'on_price_html'), 10, 2);
        add_filter('woocommerce_empty_price_html', array(&$this, 'on_price_html'), 10, 2);
        
        add_filter('woocommerce_get_price_html', array(&$this, 'on_woocommerce_get_price_html'), 10, 2);

        /* add bulk discount info on products page */
        // add_action('woocommerce_after_single_product', array(&$this, 'display_discount_info'));
        // add_action( 'woocommerce_after_single_product_summary', array(&$this, 'display_discount_info') );

        add_action('wp_ajax_wpl_bp_load_discount_table', array(&$this, 'ajax_load_discount_table' ) );
        add_action('wp_ajax_nopriv_wpl_bp_load_discount_table', array(&$this, 'ajax_load_discount_table' ) );

        // add_action('woocommerce_before_mini_cart', array(&$this, 'woocommerce_before_mini_cart' ) );
        // add_action('woocommerce_cart_loaded_from_session', array(&$this, 'woocommerce_cart_loaded_from_session' ), 10, 2 );

        // init plugin after theme is loaded    
        add_action('init', array(&$this, 'init' ) );

        // Start a PHP session
        // if ( ! session_id() ) session_start();
        // Only call this in the frontend. $_SESSION is not needed in wp-admin and it only breaks WP's Loopback functionality #21595
        if ( !is_admin() ) {
            add_action('init', array(&$this, 'register_session' ) );
        }

        // add shortcode
        add_shortcode( 'wcbp_discount_table', array(&$this, 'shortcode_wcbp_discount_table' ) );

        // increase theshold for ajax variations
        add_filter( 'woocommerce_ajax_variation_threshold', array(&$this, 'custom_wc_ajax_variation_threshold' ), 10, 2 );
        
    }

    // increase theshold for ajax variations (for better performance on updating pricing table)
    function custom_wc_ajax_variation_threshold( $qty, $product ) {
        return 100;
    }

    function register_session() {
        if ( ! session_id() ) @session_start();
    }

    // init plugin after theme is loaded    
    public function init() {

        // make priority (position) configurable by filter in functions.php
        // usage:
        // add_filter( 'wc_bulk_pricing_discount_info_priority', 'custom_discount_info_priority' );
        // function custom_discount_info_priority( $priority ) { return 5; }

        $priority = get_option('wc_bulk_pricing_default_display_priority', 20 ); // default priority
        $priority = apply_filters( 'wc_bulk_pricing_discount_info_priority', $priority );

        $action   = get_option('wc_bulk_pricing_default_display_action_hook', 'woocommerce_single_product_summary' ); // default action
        $action   = apply_filters( 'wc_bulk_pricing_discount_info_action_hook', $action );

        // add_action( 'woocommerce_single_product_summary', array(&$this, 'display_discount_info'), $priority );
        add_action( $action, array(&$this, 'display_discount_info'), $priority );

        // if 'before add to cart' is selected as the discount info position, ensure that the
        // discount table also renders for variable and subscription products
        if ( $action == 'woocommerce_template_single_add_to_cart' ) {
            add_action( 'woocommerce_simple_add_to_cart', array(&$this, 'display_discount_info'), $priority );
            add_action( 'woocommerce_variable_add_to_cart', array(&$this, 'display_discount_info'), $priority );
            add_action( 'woocommerce_subscription_add_to_cart', array(&$this, 'display_discount_info'), $priority );
        }

        // custom template tag: do_action('wcbp_display_discount_info')
        add_action( 'wcbp_display_discount_info', array(&$this, 'display_discount_info'), $priority );

        // add_action( 'woocommerce_before_single_product_summary', array(&$this, 'display_discount_info'), $priority );
        // add_action( 'woocommerce_after_single_product_summary', array(&$this, 'display_discount_info'), $priority );
        // add_action( 'woocommerce_template_single_excerpt', array(&$this, 'display_discount_info'), $priority );

        // support for WooCommerce Composite Products extension - untested!
        // add_action( 'woocommerce_composite_product_add_to_cart', array(&$this, 'display_discount_info'), 10 );

    }
    
    // public function update_counts($cart_item, $values) {
    //     global $woocommerce;
    //     $_product = $cart_item['data'];

    //     echo "*** update_counts <br>";
    //     return $cart_item;
    // }

    // public function woocommerce_cart_loaded_from_session($_cart) {
    //     echo "<pre>woocommerce_cart_loaded_from_session(): ";print_r($_cart);echo"</pre>";die();
    // }
    // public function woocommerce_before_mini_cart() {
    //     global $woocommerce_cart;
    //     echo "<pre>";print_r($woocommerce_cart);echo"</pre>";die();
    // }

    public function on_display_cart_item_price_html($html, $cart_item, $cart_item_key) {
        // echo "*** on_display_cart_item_price_html <br>";

        if ($this->is_cart_item_discounted($cart_item_key)) {
            // echo "cart_item_key $cart_item_key is discounted <br>";
            // echo "<pre>";print_r($cart_item);echo"</pre>";

            if (get_option('woocommerce_display_cart_prices_excluding_tax') == 'yes') :
                $price = $this->_discounted_cart[$cart_item_key]['discounts']['price_excluding_tax'];
                if ( function_exists( 'wc_get_price_excluding_tax' ) ) {
                    $discounted_price = wc_get_price_excluding_tax( $cart_item['data'] );
                } else {
                    $discounted_price = $cart_item['data']->get_price_excluding_tax();
                }
            else :
                $price = $this->_discounted_cart[$cart_item_key]['discounts']['price'];
                $discounted_price = $cart_item['data']->get_price();
            endif;

            // $html = '<del>' . woocommerce_price($price) . '</del><ins> ' . woocommerce_price($discounted_price) . '</ins>';
            $html = '<del>' . wc_price($price) . '</del><ins> ' . $this->format_price($discounted_price) . '</ins>';

            // if (defined('WP_DEBUG') && WP_DEBUG) :
            if ( get_option('wc_bulk_pricing_show_used_ruleset_in_cart', 0 ) == 1 ) :
                if ( isset( $this->_discounted_cart[$cart_item_key]['discounts']['data']['ruleset_name'] ) )
                    $html .= '&nbsp;<br><small>(' . $this->_discounted_cart[$cart_item_key]['discounts']['data']['ruleset_name'] . ')</small>';

                // $html .= '<br /><strong>Debug Info - Discounted By: ' . $this->_discounted_cart[$cart_item_key]['discounts']['by'] . '</strong>';
                // $html .= '<br /><pre>' . print_r($this->_discounted_cart[$cart_item_key]['discounts']['data'], true) . '</pre>';
                // $html .= '<br /><pre>' . print_r($this->_discounted_cart[$cart_item_key]['discounts'], true) . '</pre>';
                // $html .= '<br />Discount: ' . $this->_discounted_cart[$cart_item_key]['discounts']['data']['collector']. '';
            endif;
        } else {
            // echo "cart_item_key $cart_item_key is NOT discounted <br>";            
        }

        return $html;
    }

    public function on_price_html($html, $_product) {
        // echo "<pre>*** on_price_html()";print_r($_product->id);echo"</pre>";#die();
        // $from = strstr($html, 'From') !== false ? ' From ' : ' ';
        $from = strstr($html, __('From', 'wc_bulk_pricing')) !== false ? ' ' . __('From', 'wc_bulk_pricing') . ' ' : ' ';

        /*
         * This section displays prices as they adjust based on the advanced rules. UX review indicated this is too confusing. 
         * Leaving block in here for now until I receive more feedback.  
          if (array_key_exists($_product->id, $this->_discounted)) {
          $prices = array();
          foreach ($this->_discounted_cart as $cart_item_key => $cart_item) {
          if ($cart_item['product_id'] == $_product->id) {
          $prices[] = floatval($cart_item['discounts']['discounted']);
          }
          }

          rsort($prices);
          if (floatval($_product->get_price())) {
          $html = '<del>' . woocommerce_price($_product->get_price()) . '</del><ins>' . (count($prices) > 1 ? ' From ' : ' ') . woocommerce_price(array_pop($prices)) . '</ins>';
          }
          return $html;
          }
         */

        $discount_price = false;
        $id = wbp_get_product_meta( $_product, 'id' );

        if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
            $variable_id = wbp_get_product_meta( $_product, 'variable_id' );
            if ( $variable_id ) {
                $id = $variable_id;
            }
        }


        $working_price = isset($this->discounted_products[$id]) ? $this->discounted_products[$id] : $_product->get_price();

        $base_price = $_product->get_price();

        // wholesale mod
        if ( $percent = $this->getUserDiscount() ) {
            // echo "*** user discount: $percent <br>";
            $base_price = $base_price - ( $base_price * $percent / 100 );
            $html = $from . wc_price( $base_price );
        }

        // probably obsolete
        if ($this->pricing_by_store_category->is_applied_to($_product)) {
            if (floatval($working_price)) {
                if ( isset( $this->pricing_by_store_category->get_price ) ) // why does the next line throw a fatal error otherwise?
                $discount_price = $this->pricing_by_store_category->get_price($_product, $working_price);
                if ($discount_price && $discount_price != $base_price) {
                    $html = '<del>' . wc_price($base_price) . '</del><ins>' . $from . wc_price($discount_price) . '</ins>';
                }
            }
        }

        $this->discounted_products[$id] = $discount_price ? $discount_price : $base_price;

        return $html;
    }

    /* Helper getter methods */

    // public function get_product_ids($variations = false) {
    //     if ($variations) {
    //         return array_merge(array_keys($this->product_counts), array_keys($this->variation_counts));
    //     } else {
    //         return array_keys($this->product_counts);
    //     }
    // }

    // public function reset_counts() {
    //     global $woocommerce_bulk_pricing;

    //     $this->variation_counts = array();
    //     $this->product_counts = array();
    //     $this->category_counts = array();
    // }

    // public function reset_products() {
    //     global $woocommerce;

    //     foreach ($woocommerce->cart->cart_contents as $cart_item) {
    //         if (isset($cart_item['discounts'])) {
    //             $cart_item['data']->price = $cart_item['discounts']['price'];

    //             unset($cart_item['discounts']);
    //         }
    //     }
    // }

    // public function update_counts($cart_item, $values) {
    //     global $woocommerce;
    //     $_product = $cart_item['data'];

    //     //Gather product id counts
    //     $this->product_counts[$_product->id] = isset($this->product_counts[$_product->id]) ? $this->product_counts[$_product->id] + $cart_item['quantity'] : $cart_item['quantity'];

    //     //Gather product variation id counts
    //     if (isset($cart_item['variation_id']) && !empty($cart_item['variation_id'])) {
    //         $this->variation_counts[$cart_item['variation_id']] = isset($this->variation_counts[$cart_item['variation_id']]) ? $this->variation_counts[$cart_item['variation_id']] + $cart_item['quantity'] : $cart_item['quantity'];
    //     }

    //     //Gather product category counts
    //     $product_categories = wp_get_post_terms($_product->id, 'product_cat');
    //     foreach ($product_categories as $category) {
    //         $this->category_counts[$category->term_id] = isset($this->category_counts[$category->term_id]) ? $this->category_counts[$category->term_id] + $cart_item['quantity'] : $cart_item['quantity'];
    //     }

    //     return $cart_item;
    // }

    // public function on_update_cart_item_quantity($cart_item, $quantity) {
    //     global $woocommerce;

    //     $this->reset_counts();
    //     $this->reset_products();
    //     if (sizeof($woocommerce->cart->get_cart()) > 0) {
    //         foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {
    //             $this->update_counts($values, null);
    //         }
    //     }
    // }

    public function plugin_url() {
        if ($this->plugin_url)
            return $this->plugin_url;

        if (is_ssl()) :
            return $this->plugin_url = str_replace('http://', 'https://', WP_PLUGIN_URL) . "/" . plugin_basename(dirname(dirname(__FILE__)));
        else :
            return $this->plugin_url = WP_PLUGIN_URL . "/" . plugin_basename(dirname(dirname(__FILE__)));
        endif;
    }

    /**
     * Get the plugin path
     */
    public function plugin_path() {
        if ($this->plugin_path)
            return $this->plugin_path;
        return $this->plugin_path = WP_PLUGIN_DIR . "/" . plugin_basename(dirname(dirname(__FILE__)));
    }

    public function reset_totals() {
        $this->_discounted = array();
        $this->_discounted_cart = array();
    }

    public function is_cart_item_discounted($cart_item_key) {
        return isset($this->_discounted_cart[$cart_item_key]) && isset($this->_discounted_cart[$cart_item_key]['discounts']);
    }

    public function add_discounted_cart_item(&$cart_item_key, $cart_item, $track_variation = false) {
        $this->_discounted[$cart_item['product_id']] = $cart_item;
        if ($track_variation) {
            $this->_discounted[$cart_item['variation_id']] = $cart_item;
        }

        $this->_discounted_cart[$cart_item_key] = $cart_item;

        // echo "add_discounted_cart_item($cart_item_key)";
        // echo "<pre>";print_r($this->_discounted_cart);echo"</pre>";die();
    }

    public function remove_discounted_cart_item($cart_item_key, $cart_item, $track_variation = false) {

        //unset($this->_discounted[$cart_item['product_id']]);
        if ($track_variation) {
            //unset($this->_discounted[$cart_item['variation_id']]);
        }

        //unset($this->_discounted_cart[$cart_item_key]);
    }



    // create custom product ruleset
    function get_custom_product_ruleset( $post_id = null ) {

        // get custom product rules
        $rules = maybe_unserialize( get_post_meta( $post_id, '_wc_bulk_pricing_custom_ruleset', true ) );
        // $rules = is_array( $rules ) ? $rules : array();
        if ( ! is_array( $rules ) ) return false;

        // create custom ruleset
        $ruleset = array(
            'id'                  => '_custom',
            'name'                => __('Custom Product Ruleset', 'wc_bulk_pricing'),
            'rules'               => $rules,
            'is_cumulative'       => 0,
            'rows_limit'          => 5,
            'show_lowest_price'   => 0,
            'hide_discount_table' => 0,
            'custom_header'       => '',
            'custom_header'       => '',
            'user_roles'          => null,
            'cumulates_with'      => array()
        );

        return $ruleset;
    }



    // find matching rule set for (current) product
    function get_ruleset_for_product( $post_id = null ) {
        global $woocommerce_bulk_pricing;
        global $post;

        /*
        $_product = $this->wc2_get_product( $post_id );
        if ( ( 'variable' == $_product->product_type ) || ( 'variation' == $_product->product_type ) ) {
            // echo "<pre>";print_r($_product);echo"</pre>";
            // echo "get_ruleset_for_product() - variable product found!";
        }
        $variation_ruleset_id = get_post_meta( $post_id, '_wc_bulk_pricing_ruleset', true );
        // echo "***<pre>";print_r($post_id);echo"</pre>";

        // handle ruleset on variation level
        // if ( $variation_ruleset_id = get_post_meta( $variation_id, '_wc_bulk_pricing_ruleset', true ) ) {
        //     $ruleset_id = $variation_ruleset_id;
        // }
        */

        // use current post per default
        if ( ! $post_id ) $post_id = $post->ID;
        $ruleset_id = false;

        // check if user is allowed to see bulk pricing
        if ( ! $this->userCanSeeBulkPricing() ) return false;
        
        // check for product ruleset first
        $product_ruleset_id = get_post_meta($post_id, '_wc_bulk_pricing_ruleset', true);

        if ( $product_ruleset_id == '_custom') {

            $ruleset = $this->get_custom_product_ruleset( $post_id );
            return $ruleset;

        } elseif ( $product_ruleset_id != '') {

            // return $this->getRulesetByID( $product_ruleset_id );
            $ruleset_id = $product_ruleset_id;

        } else {

            // check for any category match
            $pricing_categories = get_option('wc_bulk_pricing_categories', array());
            if (is_array($pricing_categories) && sizeof($pricing_categories) > 0) {
                foreach ($pricing_categories as $term_id => $category_ruleset_ids) {

                    // choose one ruleset from multiple ones
                    $category_ruleset_id = $this->pricing_by_store_category->chooseCategoryRuleset( $category_ruleset_ids );

                    // $discounted_categories = array_keys( $pricing_categories );
                    // if ( has_term( $term_id, 'product_cat', $post_id ) ) {
                    if ( $this->productInCategory( $term_id, $post_id ) ) {
                        // return $this->getRulesetByID( $category_ruleset_id );
                        $ruleset_id = $category_ruleset_id;
                    }

                }
            }
        }

        // fetch ruleset
        $ruleset = $this->getRulesetByID( $ruleset_id );

        // handle user role restrictions
        if ( isset( $ruleset['user_roles'] ) and ! empty( $ruleset['user_roles'] ) ) {
            $capability = $ruleset['user_roles'];

            // check uf guest rule applies
            if ( ( '_guest' == $capability ) && ( ! is_user_logged_in() ) ) return $ruleset;

            if ( ! current_user_can( $capability ) ) {
                return false;
            }

        }

        return $ruleset;
    } // get_ruleset_for_product()


    function productInCategory( $term_id, $post_id ) {
        // replace has_term() which is too slow
        // if ( has_term( $term_id, 'product_cat', $post_id ) ) {}

        // If $post_id is a variation, test the parent ID instead #17938 #17897
        if ( wbp_get_product_meta( $post_id, 'product_type' ) == 'variation' ) {
            $product = wc_get_product( $post_id );
            if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
                $post_id = $product->id;
            } else {
                // set the $product_id to the parent's ID
                $post_id = $product->get_parent_id();
            }
        }

        // get object terms from cache
        if ( isset( $this->object_terms_cache[ $post_id ] ) ) {
            $objterms = $this->object_terms_cache[ $post_id ];
        } else {
            $objterms = wp_get_object_terms( $post_id, 'product_cat' );
            $this->object_terms_cache[ $post_id ] = $objterms;
        }

        // print_r($objterms);
        foreach ($objterms as $term) {
            if ( $term->term_id == $term_id ) return true;
        }
        return false;
    } // productInCategory()



    function getRulesetByID( $ruleset_id ) {
        $rulesets = wbp_get_rules();
        if ( isset( $rulesets[ $ruleset_id ] )) {
            return $rulesets[ $ruleset_id ];
        }
        return false;
    }


    // implement shortcode wcbp_discount_table
    // usage: [wcbp_discount_table id="123"]
    function shortcode_wcbp_discount_table( $atts ) {
        global $post;
        $post_id = $post ? $post->ID : false;

        // extract attributes
        extract( shortcode_atts(
            array(
                'id' => $post_id,
            ), $atts )
        );
        if ( ! $id ) return;

        // find matching rule set for current product
        $active_ruleset = $this->get_ruleset_for_product( $id );
        if ( ! $active_ruleset ) return;

        return $this->generate_discount_table( $id, $active_ruleset );
    }



    function display_discount_info() {
        // global $woocommerce_bulk_pricing;
        global $post;

        // if item is on sale and ignore_sale_items is enabled, don't show the discount table #15240
        $type = wbp_get_product_meta( $post->ID, 'product_type' );

        // find matching rule set for current product
        $active_ruleset = $this->get_ruleset_for_product();

        // echo "<pre>";print_r($active_ruleset);echo"</pre>";die();

        $show_discount_table = apply_filters( 'wc_bulk_pricing_discount_info_enabled', true );
        if ( ( $type != 'variable' && ! $active_ruleset ) || ! $show_discount_table ) return;

        echo $this->generate_discount_table( $post->ID, $active_ruleset );

    } // display_discount_info()

    function generate_discount_table( $post_id, $active_ruleset = null ) {
        $product = wc_get_product( $post_id );

        // Skip generating the table if disable_on_sale_products is enabled
        if ( $product->is_on_sale() && get_option( 'wc_bulk_pricing_disable_on_sale_products', 0 ) == 1 ) {
            return;
        }

        $html = '<div class="bulk_pricing_discounts_wrapper">';

        // header
        if ( isset( $active_ruleset['custom_header'] ) && $active_ruleset['custom_header'] != '' ) {
            $custom_header = stripslashes( $active_ruleset['custom_header'] );
        } else {
            $custom_header = get_option( 'wc_bulk_pricing_default_header', __('Quantity discounts available', 'wc_bulk_pricing') );
            $custom_header = $custom_header ? '<b>'.$custom_header.'</b><br>' : '';
        }
        // apply filter wc_bulk_pricing_custom_header
        $custom_header = apply_filters( 'wc_bulk_pricing_custom_header', $custom_header, $active_ruleset, $post_id );
        if ( $custom_header ) $html .= $custom_header;

        if ( ! isset( $active_ruleset['hide_discount_table'] ) || $active_ruleset['hide_discount_table'] == '0' ) {
            ob_start();
            $wbp_variation_display_mode = get_option( 'wc_bulk_pricing_variation_display_mode', 'show');
            echo "<script type='text/javascript'>var wbp_variation_display_mode = '{$wbp_variation_display_mode}';</script>";

            echo '<div class="bulk_pricing_discounts" id="wpl_bp_wrap">';
            woocommerce_bulk_pricing_shortcode::showHtmlTable( $active_ruleset['rules'], $post_id );
            echo '</div>';
            woocommerce_bulk_pricing_shortcode::showInlineJS( $active_ruleset['id'], $post_id );
            $html .= ob_get_clean();
        }

        // footer
        $custom_footer = '';
        if ( isset( $active_ruleset['custom_footer'] ) && $active_ruleset['custom_footer'] != '' ) {
            $custom_footer = stripslashes( $active_ruleset['custom_footer'] );
        }
        // apply filter wc_bulk_pricing_custom_header
        $custom_footer = apply_filters( 'wc_bulk_pricing_custom_footer', $custom_footer, $active_ruleset, $post_id );
        if ( $custom_footer ) $html .= $custom_footer;

        $html .= '</div>';

        $html .= '<style>
            .bulk_pricing_discounts table {
                width: 100%;
                border: 1px solid #ddd;
            }
            .bulk_pricing_discounts table th,
            .bulk_pricing_discounts table td {
                text-align:center;
                border: 1px solid #eee;
            }
            .bulk_pricing_discounts_wrapper {
                clear: both;
            }
        </style>';

        return $html;
    } // echo_discount_table()


    function ajax_load_discount_table() {

        $post_id      = $_REQUEST['post_id'];
        $ruleset_id   = $_REQUEST['ruleset_id'];
        $variation_id = $_REQUEST['variation_id'];

        // handle ruleset on variation level
        if ( $variation_ruleset_id = get_post_meta( $variation_id, '_wc_bulk_pricing_ruleset', true ) ) {
            $ruleset_id = $variation_ruleset_id;
        }

        // echo "<pre>";print_r($ruleset_id);echo"</pre>";#die();
        if ( $ruleset_id == '_custom') {

            // check for custom variation rules
            if ( get_post_meta( $variation_id, '_wc_bulk_pricing_custom_ruleset', true ) ) {

                // get custom ruleset for parent product
                $active_ruleset = $this->get_custom_product_ruleset( $variation_id );

            } else {

                // get custom ruleset for parent product
                $active_ruleset = $this->get_custom_product_ruleset( $post_id );

            }

        } else {

            // get global rule set
            $active_ruleset = $this->getRulesetByID( $ruleset_id );

        }

        // return table for variation
        woocommerce_bulk_pricing_shortcode::showHtmlTable( $active_ruleset['rules'], $variation_id );
        exit;
        
    } // ajax_load_discount_table()


    function on_woocommerce_get_price_html( $price, $_product) {
        // echo "<pre>*** on_woocommerce_get_price_html() ";print_r($price);echo"</pre>";#die();

        // find matching rule set for current product
        $active_ruleset = $this->get_ruleset_for_product( wbp_get_product_meta( $_product, 'id' ) );
        $original_price = $_product->get_price();

        $from = strstr($price, __('From', 'wc_bulk_pricing')) !== false ? ' ' . __('From', 'wc_bulk_pricing') . ' ' : ' ';

        // echo "<pre>***";print_r($_product);echo"</pre>";die();
        // print_r($_product);
        // print_r($active_ruleset);

        $show_lowest_price = false;
        if ( ( $active_ruleset ) && ( isset($active_ruleset['show_lowest_price']) ) && ( $active_ruleset['show_lowest_price'] == '1') ) 
            $show_lowest_price = true;
        if ( get_option('wc_bulk_pricing_show_lowest_price', 0 ) == 1 )
            $show_lowest_price = true;

        if ( $active_ruleset && $show_lowest_price ) {
            $lowest_price = $this->getLowestPrice( $active_ruleset, $_product->get_price() );

            if ( function_exists( 'wc_get_price_html_from_text' ) ) {
                $price = wc_get_price_html_from_text() . wc_price( $lowest_price ) . $_product->get_price_suffix();
            } else {
                $price = $_product->get_price_html_from_text() . wc_price( $lowest_price ) . $_product->get_price_suffix();
            }
            // $price = $_product->get_price_html_from_text() . $price;
        }

        // wholesale mod
        if ( $percent = $this->getUserDiscount() ) {
            // echo "*** user discount: $percent <br>";

            if ( isset($lowest_price) && $this->userDiscountIsCombinedWithBulkPricing() ) {
                $price = $lowest_price - ( $lowest_price * $percent / 100 );
                $price = $_product->get_price_html_from_text() . wc_price( $price );
            } else {
                $price = $original_price - ( $original_price * $percent / 100 );
                $price = $from . wc_price( $price );
            }
        }

        return $price;

    } // on_woocommerce_get_price_html()


    function getUserDiscount() {
        global $current_user;

        // get user role
        $user_roles = $current_user->roles;
        $user_role = array_shift($user_roles);
        // echo "*** user role: $user_role <br>";

        $role_discounts = get_option('wc_bulk_pricing_role_discounts', array() );

        if ( isset( $role_discounts[$user_role] ) and ! empty( $role_discounts[$user_role] ) ) {
            return $role_discounts[$user_role];
        }
        return false;

    } // getUserDiscount()

    function userDiscountIsCombinedWithBulkPricing() {
        global $current_user;

        // get user role
        $user_roles = $current_user->roles;
        $user_role = array_shift($user_roles);
        // echo "*** user role: $user_role <br>";

        $role_discount_modes = get_option('wc_bulk_pricing_role_discount_modes', array() );
        // echo "<pre>";print_r($user_role);echo"</pre>";die();
        // echo "<pre>";print_r($role_discount_modes);echo"</pre>";die();

        if ( isset( $role_discount_modes[$user_role] ) and ! empty( $role_discount_modes[$user_role] ) ) {
            // echo "** match:".$role_discount_modes[$user_role]."<br>";
            return $role_discount_modes[$user_role];
        }
        return false;

    } // userCanSeeBulkPricing()

    function userCanSeeBulkPricing() {
        global $current_user;

        // get user role
        $user_roles = $current_user->roles;
        $user_role = array_shift($user_roles);
        // echo "*** user role: $user_role <br>";

        $excluded_rules = get_option('wc_bulk_pricing_exclude_roles', array() );

        // if user role is in list of excluded roles, return false
        if ( in_array( $user_role, $excluded_rules ) ) {
            return false;
        }

        // if '_guest' is in list of excluded roles and user is not logged in, return false
        if ( in_array( '_guest', $excluded_rules ) && ! is_user_logged_in() ) {
            return false;
        }

        return true;

    } // userCanSeeBulkPricing()


    function getLowestPrice( $ruleset, $original_price ) {
        // echo "<pre>";print_r($ruleset);echo"</pre>";die();
        
        $lowest_price = false;

        foreach ($ruleset['rules'] as $rule) {
            
            // calc discounted price
            $rule_price = $this->pricing_by_store_category->applyPriceModifier( $original_price, $rule['val'] );

            if ( ( $rule_price < $lowest_price ) || ( ! $lowest_price ) ) {
                $lowest_price = $rule_price;                
            }

        }

        return $lowest_price;

    } // getLowestPrice()





    function format_price( $price ) {
        global $woocommerce;
        
        $return          = '';
        // $num_decimals    = (int) get_option( 'woocommerce_price_num_decimals' );
        $num_decimals    = (int) get_option('wc_bulk_pricing_display_price_decimals', 2 );
        $currency_pos    = get_option( 'woocommerce_currency_pos' );
        $currency_symbol = get_woocommerce_currency_symbol();
        $decimal_sep     = wp_specialchars_decode( stripslashes( get_option( 'woocommerce_price_decimal_sep' ) ), ENT_QUOTES );
        $thousands_sep   = wp_specialchars_decode( stripslashes( get_option( 'woocommerce_price_thousand_sep' ) ), ENT_QUOTES );
        
        $price           = apply_filters( 'raw_woocommerce_price', (double) $price );
        $price           = number_format( $price, $num_decimals, $decimal_sep, $thousands_sep );
        
        if ( $num_decimals > 2 ) {
            $num_extra_decimals = $num_decimals - 2;
            $extra_decimals = substr( $price, 0 - $num_extra_decimals );
            $extra_decimals = rtrim( $extra_decimals, '0' );
            $price = substr( $price, 0, 0 - $num_extra_decimals );

            if ( 1 == get_option('wc_bulk_pricing_display_extra_decimals', 1 ) ) {
                $price .= '<sup class="extra_decimals">'.$extra_decimals.'</sup>';
            } elseif ( 2 == get_option('wc_bulk_pricing_display_extra_decimals', 1 ) ) {
                $price .= '<span class="extra_decimals">'.$extra_decimals.'</span>';
            } else {
                $price .= $extra_decimals;                
            }
        } elseif ( get_option( 'woocommerce_price_trim_zeros' ) == 'yes' && $num_decimals > 0 ) {
            $price = woocommerce_trim_zeros( $price );
        }
        
        $return = '<span class="amount">' . sprintf( self::get_woocommerce_price_format(), $currency_symbol, $price ) . '</span>';
        
        // if ( $ex_tax_label && get_option( 'woocommerce_calc_taxes' ) == 'yes' )
        //   $return .= ' <small>' . $woocommerce->countries->ex_tax_or_vat() . '</small>';
        
        return $return;
    }

    function get_woocommerce_price_format() {
        $currency_pos = get_option( 'woocommerce_currency_pos' );
        
        switch ( $currency_pos ) {
         case 'left' :
             $format = '%1$s%2$s';
         break;
         case 'right' :
             $format = '%2$s%1$s';
         break;
         case 'left_space' :
             $format = '%1$s&nbsp;%2$s';
         break;
         case 'right_space' :
             $format = '%2$s&nbsp;%1$s';
         break;
        }
        
        return apply_filters( 'woocommerce_price_format', $format, $currency_pos );
    }
        

    /**
     * Gets the identified product. Compatible with WC 2.0 and backwards
     * compatible with previous versions
     *
     * @param int     $product_id the product identifier
     * @param array   $args       optional array of arguments
     *
     * @return WC_Product the product
     */
    function wc2_get_product( $product_id, $args = array() ) {
        $product = null;

        if ( version_compare( WOOCOMMERCE_VERSION, "2.0.0" ) >= 0 ) {
            // WC 2.0
            $product = get_product( $product_id, $args );
        } else {

            // old style, get the product or product variation object
            if ( isset( $args['parent_id'] ) && $args['parent_id'] ) {
                $product = new WC_Product_Variation( $product_id, $args['parent_id'] );
            } else {
                // get the regular product, but if it has a parent, return the product variation object
                $product = new WC_Product( $product_id );
                if ( $product->get_parent() ) {
                    $product = new WC_Product_Variation( $product->id, $product->get_parent() );
                }
            }
        }

        return $product;
    }

} // class woocommerce_bulk_pricing

