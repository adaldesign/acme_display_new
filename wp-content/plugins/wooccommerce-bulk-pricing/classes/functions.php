<?php

// Shorthand way to access a product's property
function wbp_get_product_meta( $product, $key ) {

    if ( ! is_object( $product ) ) {
        if ( function_exists( 'wc_get_product' ) ) {
            $product = wc_get_product( $product );
        } else {
            $product = new WC_Product( $product );
        }
    }

    $product_id = is_callable( array( $product, 'get_id' ) ) ? $product->get_id() : $product->id;

    if ( $key == 'product_type' && is_callable( array( $product, 'get_type' ) ) ) {
        return call_user_func( array( $product, 'get_type' ) );
    }

    // custom WPLE postmeta
    if ( substr( $key, 0, 5 ) == 'ebay_' ) {
        return get_post_meta( $product_id, '_'. $key, true );
    }

    if ( is_callable( array( $product, 'get_'. $key ) ) ) {
        return call_user_func( array( $product, 'get_'. $key ) );
    } else {
        return $product->$key;
    }
}

function wbp_get_rules( $alphabetical = false ) {
    // get available pricing rules
    $rulesets = get_option('wc_bulk_pricing_rules', array() );
    $names    = array();

    if ( $alphabetical ) {
        // Obtain a list of columns
        foreach ($rulesets as $key => $row) {
            $names[$key] = $row['name'];
        }

        // Add $data as the last parameter, to sort by the common key
        array_multisort($names, SORT_ASC, $rulesets );
    }

    return $rulesets;
}