<?php
/*
Plugin Name: WooCommerce Bulk Pricing
Plugin URI: https://www.wplab.com/plugins/bulk-pricing-for-woocommerce/
Description: Offer quantity discounts on your products the easy way
Version: 1.9.4
Author: WP Lab
Author URI: https://www.wplab.com/
WC requires at least: 3.0.0
WC tested up to: 3.3.3
Text Domain: wc_bulk_pricing
Domain Path: /languages/
License: GPLv3
*/

define('WC_BULK_PRICING_VERSION', '1.9.4');

// check if woocommerce is active - in this site or network wide
$wpl_woocommerce_activated = false;
if ( in_array('woocommerce/woocommerce.php', get_option( 'active_plugins', array() ) ) ) $wpl_woocommerce_activated = true;
if ( array_key_exists('woocommerce/woocommerce.php', get_site_option( 'active_sitewide_plugins', array() ) ) ) $wpl_woocommerce_activated = true;

if ( function_exists( 'WC' ) ) $wpl_woocommerce_activated = true;

if ( $wpl_woocommerce_activated )  {    

    require 'classes/functions.php';
    require 'classes/wc_bulk_pricing.class.php';
    require 'classes/wc_bulk_pricing_by_category.class.php';
    require 'classes/wc_bulk_pricing_shortcode.class.php';

    // load language
    load_plugin_textdomain( 'wc_bulk_pricing', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );  
   
    if (is_admin()) {

        // require 'admin/admin-init.php';
        require 'admin/classes/PricingRulesTable.php';
        require 'admin/classes/PricingCategoriesTable.php';
        require 'admin/classes/PricingProductsTable.php';
        require 'admin/classes/bulk_pricing_admin.class.php';
        require 'classes/wc_bulk_pricing_updater.php';

        require 'admin/pages/WCBP_Page.php';
        require 'admin/pages/RulesetsPage.php';
        require 'admin/pages/ProductsPage.php';
        require 'admin/pages/CategoriesPage.php';
        require 'admin/pages/RolesPage.php';
        require 'admin/pages/SettingsPage.php';
        require 'admin/pages/LicensePage.php';

        $woocommerce_bulk_pricing_admin = new woocommerce_bulk_pricing_admin();
    }

    global $woocommerce_bulk_pricing;
    $woocommerce_bulk_pricing = new woocommerce_bulk_pricing();
}

