<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
defined( 'ABSPATH' ) || exit;
get_header( 'shop' );
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

/*
 * Get Category Banner Image (fallback on parent cat banner, if fails, fallback on default)
 */
global $wp, $wp_query;
$cat = $wp_query->get_queried_object();
$image = get_field('banner_image', $cat);
if ( $image ) { $image = $image['sizes']['large'] ? $image['sizes']['large'] : $image['url'] ; }
else {
    $image = get_field('banner_image', 'term_' . $cat->parent);
	if ( $image ) { $image = $image['sizes']['large'] ? $image['sizes']['large'] : $image['url'] ; }
}
$imageBGStyle = $image ? 'style="background-image: url(' . $image . ');"' : 'style="background-image: url(' . get_home_url() . '/wp-content/uploads/2018/06/home-slide-1-1600x1013.jpg);"';

?>
    <header class="woocommerce-products-header elementor-section elementor-section-stretched hasImgBG" <?php echo $imageBGStyle; ?>>
		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
            <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
		<?php endif; ?>

		<?php
		/**
		 * Hook: woocommerce_archive_description.
		 *
		 * @hooked woocommerce_taxonomy_archive_description - 10
		 * @hooked woocommerce_product_archive_description - 10
		 */
		do_action( 'woocommerce_archive_description' );
		?>
    </header>
<?php
if ( woocommerce_product_loop() ) {
	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked wc_print_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	// Place featured products carousel ( defined in functions.php )
	featured_products_in_category($cat->slug);

    // The loop!
    ?>
    <div class="row"><div class="col-lg-8">
    <?php
    woocommerce_product_loop_start();
    if ( wc_get_loop_prop( 'total' ) ) {
        while ( have_posts() ) {
            the_post();
            /**
             * Hook: woocommerce_shop_loop.
             * @hooked WC_Structured_Data::generate_product_data() - 10
             */

            do_action( 'woocommerce_shop_loop' );
            wc_get_template_part( 'content', 'product' );
        }
    }
    woocommerce_product_loop_end();


	/**
	 * Hook: woocommerce_after_shop_loop.
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}
?>
    </div>
    <div class="col-lg-4">
<?php
/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */

// do_action( 'woocommerce_sidebar' );
// Was removed to create custom sidebar


// First display Sub-Categories!   ( defined in functions.php )
product_cat_children_list($cat, home_url( $wp->request ) );


// Then Product Stars
starred_products_in_category($cat->slug);


// Display static ACME add -- this is AD-SPACE
echo '<img src="', get_stylesheet_directory_uri(), '/img/ACME-sidebar-ad.jpg" />';

// close columns and row ?>
        </div></div>
<?php

// Display category long description for SEO
echo '<div id="long-desc">', get_field('long_description', $cat), '</div>';



/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );


get_footer( 'shop' );