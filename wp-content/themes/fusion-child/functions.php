<?php

/**
 * Enqueue child theme styles
 */

function fusion_child_enqueue_styles() {
    wp_enqueue_style('fusion-demo',
        get_stylesheet_directory_uri() . '/style.css',
	    ['fusion-main', 'elementor-frontend', 'elementor-pro']
    );
}
add_action('wp_enqueue_scripts', 'fusion_child_enqueue_styles');


/*
 *  Register additional menu location
 */

add_action( 'after_setup_theme', 'register_my_menu' );
function register_my_menu() {
	register_nav_menu( 'top-bar', __( 'Top Bar Nav', 'fusion-child' ) );
}


/*
 *  Create Product Categories Super Menu
 */

function prod_cat_menu() {


	$cat_args = array(
		'orderby'    => 'name',
		'order'      => 'asc',
		'hide_empty' => true,
		'taxonomy'   => 'product_cat',
	);
	wp_list_categories( $cat_args );

}



/*
 *   Add Footer Scripts
 */
function add_this_script_footer(){
?>
<script type="text/javascript">
jQuery( document ).ready(function($) {

    var servicesBoxes = $('#hanger-printing, #bag-printing, #rentals, #repair-steamers');

    servicesBoxes.hide();

    $('#services').on('click', '.elementor-flip-box__back', function() {

        var targetTxt = $(this).attr('href');
        servicesBoxes.hide();
        $(targetTxt).slideDown();
    });
});
</script>
<?php
}

add_action('wp_footer', 'add_this_script_footer');



/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );




/**
 * Move breadcrumb on Prod Cat page
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
add_action( 'woocommerce_before_shop_loop', 'woocommerce_breadcrumb', 20, 0 );

/**
 * Remove Product Short Description from Single Product page
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_single_excerpt', 20, 0 );



/*
 * Display Freight-Only image and text automatically
 */

function woo_freight_only_msg() {

	global $post, $wpdb;
	$custom = get_post_custom($post->ID);
	if ( $custom['freight_only'][0] ) {
		echo '<div id="freight-only">',
            wp_get_attachment_image(19951),
            '<h3>This item ships by freight only.</h3></div>';
	}
}

add_action( 'woocommerce_after_single_product_summary', 'woo_freight_only_msg', 12 );



/*
 * Add message about downloading Manuals in email confirmation
 */

function add_to_confirmation_email() {
    echo '<h3>For the product assembly manual, please visit the product page.</h3>';
}

add_action( 'woocommerce_email_order_meta', 'add_to_confirmation_email', 15 );








/*
 *  Reusable function to display a Product Category's Sub-Categories in a list
*/
function product_cat_children_list($cat, $url) {

	$children = get_terms( $cat->taxonomy, array(
		'parent'    => $cat->term_id,
		'hide_empty' => true
	) );
	if ($children) { ?>
        <ul id="subCat-sidebar">
			<?php foreach ($children as $subCat) { ?>
                <li class="<?php echo $subCat->taxonomy, '-', $subCat->term_id; ?>">
                    <a href="<?php echo $url, '/', $subCat->slug; ?>" title="<?php echo $subCat->name; ?>">
						<?php echo $subCat->name; ?>
                    </a>
                </li>
			<?php } // end foreach ?>
        </ul>
	<?php } // end IF $children
}




/*
 *  Wig Selection code for Gravity Forms Product Add On
 */

add_filter( 'gform_pre_render_1', 'populate_posts' );
add_filter( 'gform_pre_validation_1', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_1', 'populate_posts' );
add_filter( 'gform_admin_pre_render_1', 'populate_posts' );
function populate_posts( $form ) {

	foreach ( $form['fields'] as &$field ) {

		if ( $field->type != 'radio' || strpos( $field->cssClass, 'populate-posts' ) === false ) {
			continue;
		}

		// The Query
		$posts = new WP_Query( array(
			'post_type' => 'product',
			'tax_query' => array(
				'relation' => 'OR',
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'slug',
					'terms'    => 'wigs-body-displays',
				),
				array(
					'taxonomy' => 'product_tag',
					'field'    => 'slug',
					'terms'    => 'wigs',
				),
			)
		) );

		$choices = array();

		foreach ( $posts->posts as $post ) {

		    $product = wc_get_product( $post->ID );
			$SKU = $product->get_sku();

			$label = '<img src="'. get_the_post_thumbnail_url( $post->ID, 'thumbnail' ) .'" />';
			$label .= '<br /><span>'. $post->post_title .'</span>';
			$label .= '<br /><a href="'. get_the_permalink( $post->ID ) .'" target="_blank" title="'. $post->post_title .'">View Details</a>';

		    $choices[] = array( 'text' => $label, 'value' => $SKU );
		}

		// update 'Select a Post' to whatever you'd like the instructive option to be
		$field->placeholder = 'Select a Complimentary Wig';
		$field->choices = $choices;

	}

	wp_reset_postdata();

	return $form;
}




/*
 *  Reusable function to display carousel of products with "featured" tag within a given category
*/

function featured_products_in_category($cat_slug, $tag_slug='Featured') {

    // The Query
    $featQuery = new WP_Query( array(
        'post_type' => 'product',
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'slug',
                'terms'    => $cat_slug,
            ),
            array(
	            'taxonomy' => 'product_tag',
	            'field'    => 'slug',
	            'terms'    => $tag_slug,
            ),
        )
    ) );

    if ( $featQuery->have_posts() ) {
        // Wrapper ?>
<h2 class="featured-title">FEATURED PRODUCTS</h2>
<div class="elementor-element elementor-widget elementor-widget-post-carousel" data-element_type="post-carousel.default">
    <div class="elementor-widget-container">
        <div class="carousel-wrapper elementor-slick-slider " dir="ltr" data-carousel-settings='{&quot;slidesToShow&quot;:5,&quot;autoplaySpeed&quot;:5000,&quot;autoplay&quot;:false,&quot;infinite&quot;:true,&quot;pauseOnHover&quot;:true,&quot;centerMode&quot;:false,&quot;asNavFor&quot;:&quot;&quot;,&quot;speed&quot;:500,&quot;arrows&quot;:true,&quot;dots&quot;:false,&quot;rtl&quot;:false,&quot;slidesToScroll&quot;:4}'>
            <div class="carousel  slick-arrows-outside slick-slider">
	<?php // The Loop

	while ( $featQuery->have_posts() ) {
		$featQuery->the_post();
		$prodID = get_the_ID();
		?>
                <div class="slick-slide">
                    <div class="slick-slide-inner">
                        <div class="grid-item">
                            <article class="post-<?php echo $prodID; ?> product type-product status-publish <?php echo ( has_post_thumbnail() ? 'has-post-thumbnail' : '' ); ?> product_cat-<?php echo $cat_slug; ?> product_tag-featured instock taxable shipping-taxable purchasable product-type-simple<?php // echo get_product_type($prodID); ?>">
                                <?php // WooCommerce hooks make it easy ;)
                                do_action( 'woocommerce_before_shop_loop_item' );
	                            do_action( 'woocommerce_before_shop_loop_item_title' );
	                            do_action( 'woocommerce_shop_loop_item_title' );
	                            do_action( 'woocommerce_after_shop_loop_item_title' );
	                            do_action( 'woocommerce_after_shop_loop_item' );
	                            ?>
                            </article>
                        </div>
                    </div>
                </div>
	<?php }
	// end loop
    ?>
            </div>
        </div>
    </div>
</div>
<hr />
       <?php wp_reset_postdata();
    }
}





/*
 *  Reusable function to display carousel of products with "star" tag within a given category
*/

function starred_products_in_category($cat_slug, $tag_slug='star') {

	// The Query
	$featQuery = new WP_Query( array(
		'post_type' => 'product',
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'product_cat',
				'field'    => 'slug',
				'terms'    => $cat_slug,
			),
			array(
				'taxonomy' => 'product_tag',
				'field'    => 'slug',
				'terms'    => $tag_slug,
			),
		)
	) );

	if ( $featQuery->have_posts() ) {
		while ( $featQuery->have_posts() ) {
            $featQuery->the_post();
            $prodID = get_the_ID();
            ?>

<article class="product-star">
    <?php do_action( 'woocommerce_before_shop_loop_item' ); // Thumbnail with link
    do_action( 'woocommerce_before_shop_loop_item_title' );
    echo '<h3>PRODUCT STAR! &#9734;</h3>';
    do_action( 'woocommerce_shop_loop_item_title' );
    the_excerpt();
    echo '<button>VIEW</button></a>'
    ?>
</article>

        <?php }
        wp_reset_postdata();
	}
}















/*
 *   Call Elementor customizations
 */

//require_once get_stylesheet_directory() . '/elementor.php';

