<?php

/*
 *   Add "Products & Product Categories" custom Elementor Carousel Widget
 */

namespace Elementor;

class Widget_Product_Carousel extends Widget_Carousel_Base {

	public function get_name() {
		return 'product-carousel';
	}

	public function get_title() {
		return esc_html__( 'Product Carousel', 'fusion' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_carousel',
			[
				'label' => esc_html__( 'Carousel', 'fusion' ),
			]
		);

		$post_styles = apply_filters( 'luxe_post_styles', array('default' => 'Default'));
		$this->add_control(
			'portfolio_style',
			[
				'label' => esc_html__( 'Post Style', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => $post_styles,
			]
		);

		$this->carousel_controls();

	}

	protected function render() {
		$settings = $this->get_settings();
		$GLOBALS['post_grid_style'] = $settings['post_style'];

		$this->render_carousel(null, $settings, 'product');
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Product_Carousel() );