<?php
/**
 * Cart item data (when outputting non-flat)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-item-data.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version 	2.4.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<dl class="variation">
	<?php foreach ( $item_data as $data ) :

        // get the simple wig title inside the <span>
		$dom = new domDocument('1.0', 'utf-8');
		// load the html into the object
		$internalErrors = libxml_use_internal_errors(true);
        // load HTML
		$dom->loadHTML($data['display']);
        // Restore error level
		libxml_use_internal_errors($internalErrors);
		//discard white space
		$dom->preserveWhiteSpace = false;
		$shortTitle= $dom->getElementsByTagName('span'); // here u use your desired tag
		$shortTitle = $shortTitle->item(0)->nodeValue;
        ?>
        <dt class="<?php echo sanitize_html_class( 'variation-' . $data['key'] ); ?>"><?php echo $shortTitle; ?>:</dt>
        <dd class="<?php echo sanitize_html_class( 'variation-' . $data['key'] ); ?>"><?php echo 'SKU: ', $data['value']; ?></dd>
	<?php endforeach; ?>
</dl>
