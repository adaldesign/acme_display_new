<?php
/**
 * GeneratePress child theme functions and definitions.
 *
 * Add your custom PHP in this file. 
 * Only edit this file if you have direct access to it on your server (to fix errors if they happen).
 */

function generatepress_child_enqueue_scripts() {
	if ( is_rtl() ) {
		wp_enqueue_style( 'generatepress-rtl', trailingslashit( get_template_directory_uri() ) . 'rtl.css' );
	}
	wp_enqueue_style( 'navx', trailingslashit( get_stylesheet_directory_uri() ) . 'navx/css/navigation.css' );
	wp_enqueue_style( 'navx-colored', trailingslashit( get_stylesheet_directory_uri() ) . 'navx/css/skins/navigation-skin-colored.css', 'navx' );
//	wp_enqueue_style( 'navx-boxed', trailingslashit( get_stylesheet_directory_uri() ) . 'navx/css/skins/navigation-skin-boxed.css', 'navx' );

	//navigation-skin-boxed.css
	wp_enqueue_script( 'navx', trailingslashit( get_stylesheet_directory_uri() ) . 'navx/js/navigation.js' );

}
add_action( 'wp_enqueue_scripts', 'generatepress_child_enqueue_scripts', 100 );


/*
 *  Add meta tags
 */

function add_meta_tags() {
	echo '<meta name="p:domain_verify" content="865b8d430e83a2a0fd867d7c78962154"/>';
}
add_action('wp_head', 'add_meta_tags');


/*
 *  Register additional menu location
 */

add_action( 'after_setup_theme', 'register_my_menu' );
function register_my_menu() {
	register_nav_menu( 'top-bar', __( 'Top Bar Nav', 'generatepress-child' ) );
}


/*
 *  Add Author support to Woo Order post types
 */

function add_author_to_products() {
    if ( post_type_exists( 'shop_order' ) ) {
        add_post_type_support( 'shop_order', 'author' );
    }
}
add_action( 'init', 'add_author_to_products' );



/*
 *  Create Product Categories Super Menu
 */


function prod_cat_menu() {

	function navx_product_cats() {

	    // Start with the basics
	    echo '<nav id="navigation" class="navigation">
                <div class="navigation-header">
                    <div class="navigation-brand-text">
                        <a href="#" class="hide-on-landscape">All Categories</a>
                    </div>
                    <div class="navigation-button-toggler">
                        <i class="hamburger-icon"></i>
                    </div>
                </div>
                <div class="navigation-body">
                    <div class="navigation-body-header">
                        <div class="navigation-brand-text">
                            <a href="#" class="hide-on-landscape">&nbsp;</a>
                        </div>
                        <span class="navigation-body-close-button">&#10005;</span>
                    </div>
                    <ul class="navigation-menu">
                        <li class="navigation-item" id="topCat">
                            <a class="navigation-link" href="/shop">CATEGORIES</a>
                            <ul class="navigation-dropdown" id="subCats">';


	    // Dynamically fetch all top level cats
		$topCatArgs = [
			'taxonomy'   => 'product_cat',
			'parent'     => 0,
			'hide_empty' => true,
			'orderby'    => 'name',
			'order'      => 'asc'
		];
		$topCats = get_terms( $topCatArgs );

		foreach ($topCats as $topCat) {

		    echo '<li class="navigation-dropdown-item">';
			echo '<a class="navigation-dropdown-link" href="', esc_url( get_term_link( $topCat ) ), '">', $topCat->name, '</a>';

			subcats($topCat->term_id);

            echo '</li>';
        }

        echo '</li>
            </ul>
        </div>
    </nav>';
    }

    function subcats($parentID) {

	    $subCatArgs = [
	        'taxonomy'      => 'product_cat',
            'parent'     => $parentID,
			'hide_empty'    => true
        ];
	    $subCats = get_terms( $subCatArgs );
	    if ($subCats) {

		    echo '<ul class="navigation-dropdown">';

		    foreach ($subCats as $subCat) {

			    echo '<li class="navigation-dropdown-item">';
			    $term = get_term_by( 'id', $subCat->term_id, 'product_cat' );
			    echo '<a class="navigation-dropdown-link" href="', esc_url( get_term_link( $term ) ),'">', $term->name, '</a>';

			    subcats($term->term_id);

			    echo '</li>';
		    }

		    echo '</ul>';
	    }
    }

	echo '<div id="prod_cat-nav">';

	    woocommerce_breadcrumb();

	    navx_product_cats();
        echo '<div class="clear"></div>';
	echo '</div>';
}

add_action( 'generate_after_header', 'prod_cat_menu', 5 );


/*
 *   Add Footer Scripts
 */
function add_this_script_footer(){
	?>
	<script type="text/javascript">

        // var navigation = new Navigation(document.getElementById("navigation"));

        var navigation = new Navigation(document.getElementById("navigation"),{
            breakpoint: 992,
            scrollMomentum: false,
            submenuTrigger: 'click',
            onShowOffCanvas: function(){
                document.getElementById("topCat").classList.add("is-active");
                document.getElementById("subCats").classList.add("is-visible");
            }
        });

        jQuery( document ).ready(function($) {

            var servicesBoxes = $('#hanger-printing, #bag-printing, #rentals, #repair-steamers');

            servicesBoxes.hide();

            $('#services').on('click', '.elementor-flip-box__back', function() {

                var targetTxt = $(this).attr('href');
                servicesBoxes.hide();
                $(targetTxt).slideDown();
            });


	        <?php if( is_front_page() ) {  ?>

            // handle button to show more posts
            $('.news-resources').not(':first').hide();

            $('#show-news').on( 'click', 'a', function (e) {

                e.preventDefault();
                $('.news-resources').not(':first').slideToggle();
                $(this).toggleClass('less');
            });

	        <?php } // end front page only

            elseif ( is_product() ) { // Single product page only  ?>

            // handle button to show more posts
            $('#input_1_1 li:nth-child(n+5)').hide();

            $('.gform_variation_wrapper .gform_footer').prepend('<button id="show-more">Show </button>');

            $('#show-more').on( 'click', function (e) {

                e.preventDefault();
                $('#input_1_1 li:nth-child(n+5)').slideToggle();
                $(this).toggleClass('less');
            });

            <?php } // end product page only ?>

        });

	</script>
	<?php
}

add_action('wp_footer', 'add_this_script_footer');



/*
 *  Remove Generate Press from Footer Copywrite
 */
function custom_generate_add_footer_info() {

	$copyright = '<span class="copyright">&copy;</span> ';
	$copyright .= date( 'Y' );
	$copyright .= ' - ';
	$copyright .= '<a href="' . get_bloginfo('url') . '">' . get_bloginfo( 'name' ) . '</a>';
	$copyright .= ' - ';
	$copyright .= get_bloginfo('description');
	$copyright .= ' - ';
	$copyright .= '<a href="http://adaldesign.com">Website by Adal Design</a>';

	return $copyright;
}
add_filter( 'generate_copyright', 'custom_generate_add_footer_info' );


/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


/*
 *  Add ACF options page
 */

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();
}


/**
 *  ---------------------------------
 *  ---------------------------------
 *  Change order default status to On Hold
 *  ---------------------------------
 *  ---------------------------------
 */

add_filter( 'woocommerce_payment_complete_order_status', 'prefix_filter_wc_complete_order_status', 10, 3 );
/**
 * @param string   $status
 * @param int      $order_id
 * @param WC_Order $order
 *
 * @return string
 */
function prefix_filter_wc_complete_order_status( $status, $order_id, $order ) {
	return 'on-hold';
}


/*
 *  Paytrace $1 authorization
 */

function acme_paytrace_onedollar( $request ) {
	$request["AMOUNT"] = '1';
    return $request;
}
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'woocommerce-gateway-paytrace/woocommerce-gateway-paytrace.php' ) ) {
	add_filter( 'wc_paytrace_transaction_request', 'acme_paytrace_onedollar');
}

/*
 *  Add Category Hero image to Product Archives page
 */


add_action( 'generate_after_header', 'acme_prod_cat_header', 20, 0 );

function acme_prod_cat_header() {

	if( is_product_category() || is_shop()) {
		global $wp, $wp_query;
		$cat   = $wp_query->get_queried_object();
		$image = get_field( 'banner_image', $cat );
		if ( $image ) {
			$image = $image['sizes']['large'] ? $image['sizes']['large'] : $image['url'];
		} elseif( isset($cat->parent) ) {
			$image = get_field( 'banner_image', 'term_' . $cat->parent );
			if ( $image ) {
				$image = $image['sizes']['large'] ? $image['sizes']['large'] : $image['url'];
			}
		}
		$backupImg = get_field('default_category_image', 'option');
		$backupImg = $backupImg ? $backupImg['url'] : get_home_url() . '/wp-content/uploads/2018/06/home-slide-1-1600x1013.jpg);"';
		$imageBGStyle = $image
            ? 'style="background-image: url(' . $image      . ');"'
            : 'style="background-image: url(' . $backupImg  . ');"';
		?>

        <header class="woocommerce-products-header elementor-section elementor-section-stretched hasImgBG" <?php echo $imageBGStyle; ?>>
            <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
			<?php // do_action( 'woocommerce_archive_description' ); ?>
        </header>

		<?php
	}
}

/*
 *  Remove shipping from cart page
 */
function disable_shipping_calc_on_cart( $show_shipping ) {
	if( is_cart() ) {
		return false;
	}
	return $show_shipping;
}
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99 );



/**
 * ReMove breadcrumb on Prod Cat page (placed elsewhere)
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
//add_action( 'woocommerce_before_shop_loop', 'woocommerce_breadcrumb', 20, 0 );

/*
 *  Remove Category page title on Prod Cat page (placed elsewhere)
 */

add_filter( 'woocommerce_show_page_title' , 'woo_hide_page_title' );

function woo_hide_page_title() {
	return false;
}


/**
 * Remove Product Short Description from Single Product page and place it on Category page instead
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
//add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_single_excerpt', 20, 0 );


function woocommerce_after_shop_loop_item_title_short_description() {
	global $product;
	if ( ! $product->get_short_description() ) return;
	?>
    <div itemprop="description">
		<?php echo apply_filters( 'woocommerce_short_description', $product->get_short_description() ) ?>
    </div>
	<?php
}
add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_after_shop_loop_item_title_short_description', 5);


/*
 *  Add Product SKU to Cat page
 */
add_action( 'woocommerce_shop_loop_item_title', 'custom_before_title', 10 );
function custom_before_title() {

	global $product;
	if ( $product->get_sku() ) {
		echo '<p class="sku">SKU: ', $product->get_sku(), '</p>';
	}
}


/*
 *  Remove Add to Cart button from Category pages
 */

add_action( 'woocommerce_after_shop_loop_item', 'remove_add_to_cart_buttons', 1 );

function remove_add_to_cart_buttons() {
	if( is_product_category() || is_shop() || is_product_tag() ) {
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
	}
}


/*
 *  Add "Sold by Case" custom field message near top of Product page template
 */

add_action( 'woocommerce_single_product_summary', 'sold_by_message', 5 );
function sold_by_message() {

	global $product;
	$soldBy = get_field('sold_by', $product->get_id() );
	if( $soldBy ) echo '<h3 id="sold-by">', $soldBy, '</h3>';
}


/*
 *  Add "Quantity Discounts Available" message when bulk pricing rules present
 */

add_action( 'woocommerce_after_shop_loop_item_title', 'pricing_comment', 5 );
function pricing_comment() {

	global $product;
	$discountsAvailable = get_field('discounts_available', $product->get_id() );
	if( $discountsAvailable ) echo '<p class="bulk">Quantity Discounts Available</p>';
}


/*
 *  Change display of "with wigs" product item meta
 */

// $html = apply_filters( 'woocommerce_display_item_meta', $html, $item, $args );
function custom_with_wig_meta( $html, $item, $args ) {

    if ( strpos( $html, 'wig') ) {

	    $meta_data = $item->get_meta_data();

	    $mydata = $meta_data[0];
	    $newvar = $mydata->get_data();

	    $sku = $newvar['value']['_gravity_form_lead'][1];

	    return '<strong>Wig: </strong>' . $sku;
    }
    else {
        return $html;
    }

}
add_filter( 'woocommerce_display_item_meta', 'custom_with_wig_meta', 10, 3 );


/*
 * Display Freight-Only image and text automatically
 */

function woo_freight_only_msg() {

	global $post, $wpdb;
	$freightOnly = get_field('freight_only', $post->ID);
	if ( $freightOnly ) {
		echo '<div id="freight-only">',
		wp_get_attachment_image(32604),
		'<h3>This item ships by freight only.</h3></div>';
	}
}

add_action( 'woocommerce_after_single_product_summary', 'woo_freight_only_msg', 12 );



/*
 * Add message about downloading Manuals in email confirmation
 */

function add_to_confirmation_email() {
	echo '<h3>For the product assembly manual, please visit the product page.</h3>';
}

add_action( 'woocommerce_email_order_meta', 'add_to_confirmation_email', 15 );





/*
 *  Custom Widget to display things in sidebar
 */

// Register and load the widget
function acme_load_widget() {
	register_widget( 'acme_widget' );
}
add_action( 'widgets_init', 'acme_load_widget' );

// Creating the widget
class acme_widget extends WP_Widget {

	function __construct() {
		parent::__construct(

// Base ID of your widget
			'acme_widget',

// Widget name will appear in UI
			__('ACME Widget', 'acme_widget_domain'),

// Widget description
			array( 'description' => __( 'Display Custom Sidebar Content (made by Adal)', 'acme_widget_domain' ), )
		);
	}

// Creating widget front-end

	public function widget( $args, $instance ) {

	    $title = $function = null;
	    if(!empty($instance['title']))
	        $title = apply_filters( 'widget_title', $instance['title'] );
		if(!empty($instance['function']))
		    $function = apply_filters( 'widget_text', $instance['function'] );

// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
		if ( ! empty( $function ) )
            $function();

		echo $args['after_widget'];
	}

// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( '', 'acme_widget_domain' );
		}

		if ( isset( $instance[ 'function' ] ) ) {
			$function = $instance[ 'function' ];
		}
		else {
			$function = '';
		}

// Widget admin form
		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'function' ); ?>"><?php _e( 'Function:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'function' ); ?>" name="<?php echo $this->get_field_name( 'function' ); ?>" type="text" value="<?php echo esc_attr( $function ); ?>" />
        </p>

        <blockquote>Functions to use:
            <br /> product_cat_children_list
            <br /> starred_products_in_category
        </blockquote>
		<?php
	}

// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['function'] = ( ! empty( $new_instance['function'] ) ) ? strip_tags( $new_instance['function'] ) : '';
		return $instance;
	}
} // Class acme_widget ends here



/*
 *  Reusable function to display products with "star" tag within a given category
*/

function starred_products_in_category($tag_slug='star') {

	if( is_product_category() ) {

		global $wp_query;
		$cat      = $wp_query->get_queried_object();
		$cat_children = get_term_children($cat->term_id, 'product_cat');


		// The Query
		$featQuery = new WP_Query( array(
			'post_type' => 'product',
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'term_id',
					'terms'    => $cat->term_id,
					'include_children' => false,
				),
				array(
					'taxonomy' => 'product_tag',
					'field'    => 'slug',
					'terms'    => $tag_slug,
				),
			)
		) );

		if ( $featQuery->have_posts() ) {
			while ( $featQuery->have_posts() ) {
				$featQuery->the_post();
				$prodID = get_the_ID();
				?>

                <article class="product-star">
					<?php do_action( 'woocommerce_before_shop_loop_item' ); // Thumbnail with link
					do_action( 'woocommerce_before_shop_loop_item_title' );
					echo '<h3>PRODUCT STAR! &#9734;</h3>';
					do_action( 'woocommerce_shop_loop_item_title' );
					echo '<button>VIEW</button></a>'
					?>
                </article>

			<?php }
			wp_reset_postdata();
		}

	} // end IF is_product_category
}


/*
 *  Reusable function to display a Product Category's Sub-Categories in a list
*/
function product_cat_children_list() {

    if( is_product_category() ) {

        global $wp, $wp_query;
        $cat = $wp_query->get_queried_object();
        $url = home_url( $wp->request );

        $children = get_terms( $cat->taxonomy, array(
            'parent'    => $cat->term_id,
            'hide_empty' => true
        ) );

        if ($children) { ?>
            <ul id="subCat-sidebar">
                <?php foreach ($children as $subCat) { ?>
                    <li class="<?php echo $subCat->taxonomy, '-', $subCat->term_id; ?>">
                        <a href="<?php echo $url, '/', $subCat->slug; ?>" title="<?php echo $subCat->name; ?>">
                            <?php echo $subCat->name; ?>
                        </a>
                    </li>
                <?php } // end foreach ?>
            </ul>
        <?php } // end IF $children

        elseif( isset($cat->parent) ) {

	        $siblings = get_terms( $cat->taxonomy, array(
		        'parent'    => $cat->parent,
		        'hide_empty' => true
	        ) );
	        if ($siblings) { ?>
                <ul id="subCat-sidebar">
			        <?php foreach ($siblings as $subCat) { ?>
                        <li class="<?php echo $subCat->taxonomy, '-', $subCat->term_id; ?>">
                            <a href="<?php echo $url, '/', $subCat->slug; ?>" title="<?php echo $subCat->name; ?>">
						        <?php echo $subCat->name; ?>
                            </a>
                        </li>
			        <?php } // end foreach ?>
                </ul>
	        <?php } // end IF $siblings
        }

    } // end IF is_product_category
}




/*
 *  Wig Selection code for Gravity Forms Product Add On
 */

add_filter( 'gform_pre_render_1', 'populate_posts' );
add_filter( 'gform_pre_validation_1', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_1', 'populate_posts' );
add_filter( 'gform_admin_pre_render_1', 'populate_posts' );
function populate_posts( $form ) {

	foreach ( $form['fields'] as &$field ) {

		if ( $field->type != 'radio' || strpos( $field->cssClass, 'populate-posts' ) === false ) {
			continue;
		}

		// The Query
		$posts = new WP_Query( array(
			'post_type' => 'product',
			'tax_query' => array(
				'relation' => 'OR',
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'slug',
					'terms'    => 'wigs-body-displays',
				),
				array(
					'taxonomy' => 'product_tag',
					'field'    => 'slug',
					'terms'    => 'wigs',
				),
			)
		) );

		$choices = array();

		foreach ( $posts->posts as $post ) {

			$product = wc_get_product( $post->ID );
			$SKU = $product->get_sku();

			$label = '<img src="'. get_the_post_thumbnail_url( $post->ID, 'thumbnail' ) .'" />';
			$label .= '<br /><span>'. $post->post_title .'</span>';
			$label .= '<br /><a href="'. get_the_permalink( $post->ID ) .'" target="_blank" title="'. $post->post_title .'">View Details</a>';

			$choices[] = array( 'text' => $label, 'value' => $SKU );
		}

		// update 'Select a Post' to whatever you'd like the instructive option to be
		$field->placeholder = 'Select a Complimentary Wig';
		$field->choices = $choices;

	}

	wp_reset_postdata();

	return $form;
}




/*
 *  Reusable function to display carousel of products with "featured" tag within a given category
*/

function featured_products_in_category($cat_slug, $tag_slug='Featured') {

	// The Query
	$featQuery = new WP_Query( array(
		'post_type' => 'product',
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'product_cat',
				'field'    => 'slug',
				'terms'    => $cat_slug,
			),
			array(
				'taxonomy' => 'product_tag',
				'field'    => 'slug',
				'terms'    => $tag_slug,
			),
		)
	) );

	if ( $featQuery->have_posts() ) {
		// Wrapper ?>
		<h2 class="featured-title">FEATURED PRODUCTS</h2>
		<div class="elementor-element elementor-widget elementor-widget-post-carousel" data-element_type="post-carousel.default">
			<div class="elementor-widget-container">
				<div class="carousel-wrapper elementor-slick-slider " dir="ltr" data-carousel-settings='{&quot;slidesToShow&quot;:5,&quot;autoplaySpeed&quot;:5000,&quot;autoplay&quot;:false,&quot;infinite&quot;:true,&quot;pauseOnHover&quot;:true,&quot;centerMode&quot;:false,&quot;asNavFor&quot;:&quot;&quot;,&quot;speed&quot;:500,&quot;arrows&quot;:true,&quot;dots&quot;:false,&quot;rtl&quot;:false,&quot;slidesToScroll&quot;:4}'>
					<div class="carousel  slick-arrows-outside slick-slider">
						<?php // The Loop

						while ( $featQuery->have_posts() ) {
							$featQuery->the_post();
							$prodID = get_the_ID();
							?>
							<div class="slick-slide">
								<div class="slick-slide-inner">
									<div class="grid-item">
										<article class="post-<?php echo $prodID; ?> product type-product status-publish <?php echo ( has_post_thumbnail() ? 'has-post-thumbnail' : '' ); ?> product_cat-<?php echo $cat_slug; ?> product_tag-featured instock taxable shipping-taxable purchasable product-type-simple<?php // echo get_product_type($prodID); ?>">
											<?php // WooCommerce hooks make it easy ;)
											do_action( 'woocommerce_before_shop_loop_item' );
											do_action( 'woocommerce_before_shop_loop_item_title' );
											do_action( 'woocommerce_shop_loop_item_title' );
											do_action( 'woocommerce_after_shop_loop_item_title' );
											do_action( 'woocommerce_after_shop_loop_item' );
											?>
										</article>
									</div>
								</div>
							</div>
						<?php }
						// end loop
						?>
					</div>
				</div>
			</div>
		</div>
		<hr />
		<?php wp_reset_postdata();
	}
}




// Quick fix from Relevanssi support to solve SKU searches special characters   -

add_filter('relevanssi_remove_punctuation', 'remove_chars', 9);
function remove_chars($a) {
	$a = str_replace(array('-','*','/'), '', $a);
	return $a;
}


// Another fix for Relevanssi to include Product Variation SKUs - https://www.relevanssi.com/knowledge-base/indexing-product-variation-skus-main-product

add_filter('relevanssi_content_to_index', 'rlv_index_variation_skus', 10, 2);
function rlv_index_variation_skus($content, $post) {
	if ($post->post_type == "product") {
		$args = array('post_parent' => $post->ID, 'post_type' => 'product_variation', 'posts_per_page' => -1);
		$variations = get_posts($args);
		if (!empty($variations)) {
			foreach ($variations as $variation) {
				$sku = get_post_meta($variation->ID, '_sku', true);
				$content .= " $sku";
			}
		}
	}

	return $content;
}





